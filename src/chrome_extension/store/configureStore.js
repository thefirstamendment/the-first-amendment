import { createStore, applyMiddleware } from 'redux';
import rootReducer from '../../app/reducers/rootReducer';
import reduxImmutableStateInvariant from 'redux-immutable-state-invariant';
import thunk from 'redux-thunk';
import {alias } from 'react-chrome-redux';
import aliases from '../aliases';

export default function configureStore(initialState) {
  return createStore(
    rootReducer,
    initialState,
    applyMiddleware(alias(aliases), thunk, reduxImmutableStateInvariant())
  );
}
