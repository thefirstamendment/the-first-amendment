import { expect } from 'chai';
import Utils from './Utils';
import { JSDOM } from 'jsdom';
import Persona from './Persona';

describe('test Utils', () => {

  before(() => {
    global.chrome = {
      extension: {
        getURL: function() {
          return "defaultAvatarImg";
        }
      }
    };

    const jsdom = new JSDOM('<!doctype html><html><body></body></html>', {
      referrer: 'https://example.com/baz',
      url: 'https://example.com/foo#anchor'
    });
    const { window } = jsdom;
    global.window = window;
    global.document = window.document;
  });

  it('is empty object - should allow null, undefined or empty objects', () => {
    expect(Utils.isEmptyObject(null)).to.equal(true);
    expect(Utils.isEmptyObject(undefined)).to.equal(true);
    expect(Utils.isEmptyObject({})).to.equal(true);
    expect(Utils.isEmptyObject()).to.equal(true);
  });

  it('is empty object - should not allo wnon empty objects', () => {
    expect(Utils.isEmptyObject({abc: "abc"})).to.equal(false);
    expect(Utils.isEmptyObject(Utils)).to.equal(false);
    expect(Utils.isEmptyObject(22)).to.equal(false);
    expect(Utils.isEmptyObject(0)).to.equal(false);
    expect(Utils.isEmptyObject("")).to.equal(false);
    expect(Utils.isEmptyObject("asdasdf")).to.equal(false);
    expect(Utils.isEmptyObject(true)).to.equal(false);
    expect(Utils.isEmptyObject(false)).to.equal(false);
    expect(Utils.isEmptyObject([])).to.equal(false);
  });

  it('get default avatar img', () => {
    expect(Utils.getDefaultAvatarImg()).to.equal("defaultAvatarImg");
  });

  it('get avatar img - empty persona and ampty avatar', () => {
    const persona = new Persona("account", "name", "", 0);
    expect(Utils.getAvatarImg(null)).to.equal("defaultAvatarImg");
    expect(Utils.getAvatarImg(undefined)).to.equal("defaultAvatarImg");
    expect(Utils.getAvatarImg(persona)).to.equal("defaultAvatarImg");
  });

  it('get avatar img - with persona', () => {
    const persona = new Persona("account", "name", "avatar image", 0);
    expect(Utils.getAvatarImg(persona)).to.equal("avatar image");
  });

  it('get current url ', () => {
    expect(Utils.getCurrentUrl()).to.equal("https://example.com/foo");
  });

  it('get persona name - empty persona', () => {
    expect(Utils.getPersonaName(null)).to.equal("Anonymous");
    expect(Utils.getPersonaName(undefined)).to.equal("Anonymous");
    expect(Utils.getPersonaName({})).to.equal("Anonymous");
  });

  it('get persona name - empty persona', () => {
    expect(Utils.getPersonaName(new Persona("a", "nam"))).to.equal("nam");
    expect(Utils.getPersonaName(new Persona("a", "naddsafdsaf"))).to.equal("naddsafdsaf");
  });
});
