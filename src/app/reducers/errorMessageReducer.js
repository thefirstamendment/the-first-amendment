import * as types from '../actions/actionTypes';
import initialState from './initialState';

export default function errorMessageReducer(state = initialState.errorMessage, action) {
  switch (action.type) {
    case types.SHOW_ERROR:
      return action.errorMessage;

    case types.HIDE_ERROR:
      return "";

    default:
      return state;
  }
}
