/*jshint esversion: 6 */
import persistenceApi from '../api/persistenceApi';
import 'babel-polyfill';
import configureStore from './store/configureStore';
import { wrapStore } from 'react-chrome-redux';
import * as reduxPortName from './reduxPortName';
import { setAccounts } from '../app/actions/accountsActions';
import { setPersonas } from '../app/actions/personaActions';
import { setCurrentPersona } from '../app/actions/currentPersonaActions';
import { showError } from '../app/actions/errorActions';

/****************** Init ******************/

const ICON_PATH_ON = "icons/tfa-on.png";
const ICON_PATH_OFF = "icons/tfa-off.png";
let displaySidebar = false;

const store = configureStore();
wrapStore(store, { portName: reduxPortName.THE_FIRST_AMENDMENT_PORT });

persistenceApi.startIpfs().then(() => {
  persistenceApi.setup(window);
  init();
}).catch(err => {
  // TODO: 05 handle if ipfs can't start
  console.error(err); //eslint-disable-line no-console
  store.dispatch(showError(err));
});

/**
 * initializes everything
 */
function init() {
  const accounts = persistenceApi.getAccounts();
  store.dispatch(setAccounts(accounts));
  persistenceApi.getPersonas(accounts).then((personas) => {
    if (personas.length !== 0) {
      store.dispatch(setPersonas(personas));
      store.dispatch(setCurrentPersona(personas[0]));
    }
  }).catch(error => {
    store.dispatch(showError(error));
  });

  //TODO: check what this is for
  chrome.tabs.getSelected(null, (tab) => { //eslint-disable-line no-undef
    chrome.pageAction.show(tab.id); //eslint-disable-line no-undef
  });

  // Send request to current tab when page action is clicked
  chrome.pageAction.onClicked.addListener((tab) => { //eslint-disable-line no-undef
    displaySidebar = !displaySidebar;
    toggleSidebar(tab.id);
  });

  // toggle sidebar on tab activated
  chrome.tabs.onActivated.addListener((activeInfo) => { //eslint-disable-line no-undef
    toggleSidebar(activeInfo.tabId);
  });

  chrome.tabs.onUpdated.addListener((tabId, changeInfo) => { //eslint-disable-line no-undef
    chrome.pageAction.show(tabId); //eslint-disable-line no-undef
    if (changeInfo.url) {
      toggleSidebar(tabId);
    }
  });

  // toggle sidebar on page loaded
  chrome.webNavigation.onCompleted.addListener(details => { //eslint-disable-line no-undef
    toggleSidebar(details.tabId);
  });
}

function toggleSidebar(tabId) {
  let iconPath = displaySidebar ? ICON_PATH_ON : ICON_PATH_OFF;
  chrome.pageAction.setIcon({ //eslint-disable-line no-undef
    path: iconPath,
    tabId: tabId
  });
  chrome.tabs.query({ //eslint-disable-line no-undef
    active: true,
    currentWindow: true
  }, (tabs) => {
    chrome.tabs.sendMessage(tabs[0].id, { //eslint-disable-line no-undef
      callFunction: "toggleSidebar",
      messageParams: { displaySidebar: displaySidebar }
    });
  });
}
