import * as types from '../actions/actionTypes';
import initialState from './initialState';

export default function commentsReducer(state = initialState.comments, action) {
  switch (action.type) {
    case types.GET_COMMENTS_SUCCESS:
      return action.comments;

    case types.ADD_COMMENT_SUCCESS:
      if (state.find(comment => comment.txHash == action.comment.txHash)) {
        return [...state];
      } else {
        return [ // creating a deep copy of an array
          ...state,
          Object.assign({}, action.comment)
        ];
      }

    case types.UPDATE_COMMENT_SUCCESS:
      return [...state.filter(comment => comment.txHash != action.txHash), Object.assign({}, action.newComment)];

    case types.UPDATE_PERSONA_SUCCESS: {
        const updatedComments = state.filter(comment => comment.persona.account == action.persona.account);
        if (updatedComments) {
          // make an array of deep copies of all comments with updated persona
          const updatedCommentsCopy = [];
          updatedComments.forEach(updatedComment => {
            let updatedCommentCopy = Object.assign({}, updatedComment);
            updatedCommentCopy.persona = Object.assign({}, action.persona);
            updatedCommentsCopy.push(updatedCommentCopy);
          });
          return [...state.filter(comment => comment.persona.account != action.persona.account), ...updatedCommentsCopy];
        }

        return state;
      }
    default:
      return state;
  }
}
