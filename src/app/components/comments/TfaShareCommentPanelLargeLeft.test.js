import { expect } from 'chai';
import React from 'react';
import TfaShareCommentPanelLargeLeft from './TfaShareCommentPanelLargeLeft';
import chrome from 'sinon-chrome/extensions';
import { createMount } from '@material-ui/core/test-utils';
import { JSDOM } from 'jsdom';
import Enzyme from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

Enzyme.configure({ adapter: new Adapter() });

describe("<TfaShareCommentPanelLargeLeft /> tests", () => {
  let mount;

  function setup(personaImageAvatar) {
    const props = {
      personaImageAvatar: personaImageAvatar
    };
    return mount(<TfaShareCommentPanelLargeLeft {...props} />);
  }


  before(() => {
    global.chrome = chrome;
    const jsdom = new JSDOM('<!doctype html><html><body></body></html>');
    const { window } = jsdom;
    global.window = window;
    global.document = window.document;
    mount = createMount();
  });

  after(() => {
    mount.cleanUp();
  });

  it("renders share comment panel large left", () => {
    const personaImageAvatar = "data:image/png;base64,p1";
    const wrapper = setup(personaImageAvatar);
    expect(wrapper.props().personaImageAvatar).to.equal(personaImageAvatar);
    expect(wrapper.find("Avatar").length).to.equal(1);
    expect(wrapper.find("Avatar").props().src).to.equal(personaImageAvatar);
  });
});
