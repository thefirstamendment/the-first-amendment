import { expect } from 'chai';
import React from 'react';
import TfaComment from './TfaComment';
import sinon from 'sinon';
import chrome from 'sinon-chrome/extensions';
import Utils from '../../../common/Utils';
import { createMount } from '@material-ui/core//test-utils';
import personasTestJson from '../../../../test/integration/test_personas.json';
import Persona from '../../../common/Persona';
import Comment from '../../../common/Comment';
import { JSDOM } from 'jsdom';
import Enzyme from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

Enzyme.configure({ adapter: new Adapter() });

function generateComment(isEmptyAvatar, isEnabled) {
  const personaAvatar = isEmptyAvatar ? "" : personasTestJson.persona1.avatar;
  const tokenPersona = new Persona("account", personasTestJson.persona1.name, personaAvatar, 0);
  return new Comment("txHash", "content", "url", tokenPersona, 0, 0, 0, isEnabled);
}

describe("<TfaComment /> tests", () => {
  let mount;
  let getDefaultAvatarImgStub;

  function setup(comment) {
    const props = {
      upvoteComment: () => { },
      downvoteComment: () => { },
      comment: comment
    };
    return mount(<TfaComment {...props} />);
  }

  before(() => {
    global.chrome = chrome;
    const jsdom = new JSDOM('<!doctype html><html><body></body></html>');
    const { window } = jsdom;
    global.window = window;
    global.document = window.document;
    getDefaultAvatarImgStub = sinon.stub(Utils, "getDefaultAvatarImg");
    getDefaultAvatarImgStub.returns("defaultAvatarImg");
    mount = createMount();
  });

  after(() => {
    mount.cleanUp();
    getDefaultAvatarImgStub.restore();
  });

  it("renders confirmed comment with empty avatar", () => {
    const comment = generateComment(true, true);
    const wrapper = setup(comment);
    const expectedAvatarImg = Utils.getAvatarImg({});
    expect(wrapper.props().comment).to.equal(comment);
    expect(wrapper.props().upvoteComment).to.exist;
    expect(wrapper.props().downvoteComment).to.exist;
    expect(wrapper.find("ListItem").length).to.equal(1);
    expect(wrapper.find("Avatar").length).to.equal(1);
    expect(wrapper.find("Avatar").props().src).to.not.be.undefined;
    expect(wrapper.find("Avatar").props().src).to.not.equal("");
    expect(wrapper.find("Avatar").props().src).to.equal(expectedAvatarImg);
    expect(wrapper.find("strong").length).to.equal(1);
    expect(wrapper.find("strong").text().trim()).to.equal(personasTestJson.persona1.name);
    expect(wrapper.find("TimeAgo").length).to.equal(1);
    expect(wrapper.find("p").length).to.equal(1);
    expect(wrapper.find("p").text()).to.equal("content");
    expect(wrapper.find("a").length).to.equal(2);
    expect(wrapper.find("a").at(0).props().role).to.equal("button");
    expect(wrapper.find("a").at(1).props().role).to.equal("button");
    expect(wrapper.find("span").length).to.equal(2);
    expect(wrapper.find("span").at(0).text()).to.equal("0");
    expect(wrapper.find("span").at(1).text()).to.equal("0");
    expect(wrapper.find("Tooltip").length).to.equal(0);
  });

  it("renders confirmed comment with existing avatar", () => {
    const comment = generateComment(false, true);
    const wrapper = setup(comment);
    expect(wrapper.props().comment).to.equal(comment);
    expect(wrapper.props().upvoteComment).to.exist;
    expect(wrapper.props().downvoteComment).to.exist;
    expect(wrapper.find("ListItem").length).to.equal(1);
    expect(wrapper.find("Avatar").length).to.equal(1);
    expect(wrapper.find("Avatar").props().src).to.equal(personasTestJson.persona1.avatar);
    expect(wrapper.find("strong").length).to.equal(1);
    expect(wrapper.find("strong").text().trim()).to.equal(personasTestJson.persona1.name);
    expect(wrapper.find("TimeAgo").length).to.equal(1);
    expect(wrapper.find("p").length).to.equal(1);
    expect(wrapper.find("p").text()).to.equal("content");
    expect(wrapper.find("a").length).to.equal(2);
    expect(wrapper.find("a").at(0).props().role).to.equal("button");
    expect(wrapper.find("a").at(1).props().role).to.equal("button");
    expect(wrapper.find("span").length).to.equal(2);
    expect(wrapper.find("span").at(0).text()).to.equal("0");
    expect(wrapper.find("span").at(1).text()).to.equal("0");
    expect(wrapper.find("Tooltip").length).to.equal(0);
  });

  it("renders unconfirmed comment with empty avatar", () => {
    const comment = generateComment(true, false);
    const wrapper = setup(comment);
    const expectedAvatarImg = Utils.getAvatarImg({});
    expect(wrapper.props().comment).to.equal(comment);
    expect(wrapper.props().upvoteComment).to.exist;
    expect(wrapper.props().downvoteComment).to.exist;
    expect(wrapper.find("ListItem").length).to.equal(1);
    expect(wrapper.find("Avatar").length).to.equal(1);
    expect(wrapper.find("Avatar").props().src).to.not.be.undefined;
    expect(wrapper.find("Avatar").props().src).to.not.equal("");
    expect(wrapper.find("Avatar").props().src).to.equal(expectedAvatarImg);
    expect(wrapper.find("strong").length).to.equal(1);
    expect(wrapper.find("strong").text().trim()).to.equal(personasTestJson.persona1.name);
    expect(wrapper.find("TimeAgo").length).to.equal(0);
    expect(wrapper.find("p").length).to.equal(1);
    expect(wrapper.find("p").text()).to.equal("content");
    expect(wrapper.find("a").length).to.equal(2);
    expect(wrapper.find("a").at(0).props().role).to.equal("button");
    expect(wrapper.find("a").at(1).props().role).to.equal("button");
    expect(wrapper.find("span").length).to.equal(3);
    expect(wrapper.find("span").at(0).text()).to.equal("Pending...");
    expect(wrapper.find("span").at(1).text()).to.equal("0");
    expect(wrapper.find("span").at(2).text()).to.equal("0");
    expect(wrapper.find("Tooltip").length).to.equal(1);
    expect(wrapper.find("Tooltip").props().enterDelay).to.equal(100);
    expect(wrapper.find("Tooltip").props().leaveDelay).to.equal(100);
    expect(wrapper.find("Tooltip").props().placement).to.equal("bottom");
    expect(wrapper.find("Tooltip").props().title).to.equal("This option is disabled until comment is mined and stored on blockchain");
  });

  it("renders unconfirmed comment with existing avatar", () => {
    const comment = generateComment(false, false);
    const wrapper = setup(comment);
    expect(wrapper.props().comment).to.equal(comment);
    expect(wrapper.props().upvoteComment).to.exist;
    expect(wrapper.props().downvoteComment).to.exist;
    expect(wrapper.find("ListItem").length).to.equal(1);
    expect(wrapper.find("Avatar").length).to.equal(1);
    expect(wrapper.find("Avatar").props().src).to.equal(personasTestJson.persona1.avatar);
    expect(wrapper.find("strong").length).to.equal(1);
    expect(wrapper.find("strong").text().trim()).to.equal(personasTestJson.persona1.name);
    expect(wrapper.find("TimeAgo").length).to.equal(0);
    expect(wrapper.find("p").length).to.equal(1);
    expect(wrapper.find("p").text()).to.equal("content");
    expect(wrapper.find("a").length).to.equal(2);
    expect(wrapper.find("a").at(0).props().role).to.equal("button");
    expect(wrapper.find("a").at(1).props().role).to.equal("button");
    expect(wrapper.find("span").length).to.equal(3);
    expect(wrapper.find("span").at(0).text()).to.equal("Pending...");
    expect(wrapper.find("span").at(1).text()).to.equal("0");
    expect(wrapper.find("span").at(2).text()).to.equal("0");
    expect(wrapper.find("Tooltip").length).to.equal(1);
    expect(wrapper.find("Tooltip").props().enterDelay).to.equal(100);
    expect(wrapper.find("Tooltip").props().leaveDelay).to.equal(100);
    expect(wrapper.find("Tooltip").props().placement).to.equal("bottom");
    expect(wrapper.find("Tooltip").props().title).to.equal("This option is disabled until comment is mined and stored on blockchain");
  });

  it("simulates upvote/downvote click on confirmed comment", () => {
    const upvoteComment = sinon.spy();
    const downvoteComment = sinon.spy();
    const comment = generateComment(false, true);
    const isConfirmed = true;
    const wrapper = mount(
      <TfaComment
        comment={comment}
        upvoteComment={upvoteComment}
        downvoteComment={downvoteComment}
      />
    );
    wrapper.find("a").at(0).simulate("click");
    expect(upvoteComment.calledWith("txHash"));
    wrapper.find("a").at(1).simulate("click");
    expect(downvoteComment.calledWith("txHash"));
  });

  it("simulates upvote/downvote click on unconfirmed comment", () => {
    const upvoteComment = sinon.spy();
    const downvoteComment = sinon.spy();
    const comment = generateComment(false, false);
    const isConfirmed = false;
    const wrapper = mount(
      <TfaComment
        comment={comment}
        upvoteComment={upvoteComment}
        downvoteComment={downvoteComment}
      />
    );
    wrapper.find("a").at(0).simulate("click");
    expect(upvoteComment.calledOnce).to.be.false;
    wrapper.find("a").at(1).simulate("click");
    expect(downvoteComment.calledOnce).to.be.false;
  });
});
