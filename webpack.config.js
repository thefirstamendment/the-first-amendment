import path from 'path';
import webpack from 'webpack';
import UglifyJsPlugin from 'uglifyjs-webpack-plugin';
import HtmlWebpackPlugin from 'html-webpack-plugin';
import CopyWebpackPlugin from 'copy-webpack-plugin';
const ExtractTextPlugin = require('extract-text-webpack-plugin');

export default {
  mode: 'production',
  entry: {
    background: path.resolve(__dirname, 'src/chrome_extension/background'),
    content: path.resolve(__dirname, 'src/chrome_extension/content')
  },
  output: {
    path: path.resolve(__dirname, 'dist/chrome'),
    filename: '[name].bundle.js'
  },
  plugins: [
    new webpack.optimize.OccurrenceOrderPlugin(),
    new webpack.IgnorePlugin(/[^/]+\/[\S]+.dev$/),
    new webpack.LoaderOptionsPlugin({
      minimize: true,
      debug: false
    }),
    new UglifyJsPlugin({
      sourceMap: true
    }),
    new webpack.DefinePlugin({
      'process.env': {
        NODE_ENV: JSON.stringify('production')
      }
    }),
    new HtmlWebpackPlugin({
      template: path.resolve(__dirname, 'src/chrome_extension/background.html'),
      filename: "background.html"
    }),
    new CopyWebpackPlugin([
      { from: 'src/chrome_extension/icons', to: path.resolve(__dirname, 'dist/chrome/icons') },
      'src/chrome_extension/manifest.json'
    ], { debug: 'info' }),
    new ExtractTextPlugin({ filename: 'css/styles.css', allChunks: true }),
    new webpack.ProvidePlugin({
      $: "jquery",
      jQuery: "jquery"
    })
  ],
  resolve: {
    extensions: ['*', '.js', ".css"]
  },
  module: {
    rules: [{
      test: /\.js$/,
      loader: 'babel-loader',
      exclude: /node_modules/,
      query: {
        presets: ['react-optimize']
      }
    }, {
      test: /\.css$/,
      loader: ExtractTextPlugin.extract({
        fallback: 'style-loader',
        use: [{
          loader: 'css-loader',
          query: {
            modules: true,
            sourceMap: false,
            minimize: true,
            importLoaders: 2,
            localIdentName: '[name]__[local]__[hash:base64:5]'
          }
        }]
      }),
      exclude: /node_modules/
    }, {
      test: /\.(png|jpg|eot|ttf|eot|svg|woff|woff2)$/,
      loader: "url-loader?limit=10000",
      exclude: /node_modules/
    }, {
      test: /\.scss$/,
      loader: ExtractTextPlugin.extract({
        fallback: 'style-loader',
        use: [{
          loader: 'css-loader',
          options: {
            modules: true,
            sourceMap: false,
            minimize: true,
            importLoaders: 2,
            localIdentName: '[name]__[local]__[hash:base64:5]'
          }
        },
          'sass-loader'
        ]
      }),
      exclude: /node_modules/
    }]
  }
};
