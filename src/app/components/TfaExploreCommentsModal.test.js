import { expect } from 'chai';
import React from 'react';
import TfaExploreCommentsModal from './TfaExploreCommentsModal';
import sinon from 'sinon';
import chrome from 'sinon-chrome/extensions';
import { createMount } from '@material-ui/core/test-utils';
import { JSDOM } from 'jsdom';
import Enzyme from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import Comment from '../../common/Comment';
import Persona from '../../common/Persona';

Enzyme.configure({ adapter: new Adapter() });

function generateComments(commentsCount) {
  const tokenPersona = new Persona("account", "name", "", 0);
  const comments = [];
  for (let i = 0; i < commentsCount; i++) {
    comments.push(new Comment("txHash" + i, "content" + i, "url" + i, tokenPersona, 0, 0, 0, true));
  }
  return comments;
}

describe("<TfaExploreCommentsModal /> tests", () => {
  let mount;

  function setup(
    exploreCommentsModalIsOpen,
    allComments,
    allCommentsLoading,
    closeExploreCommentsModal  = () => { },
    upvoteComment = () => { },
    downvoteComment = () => { }
    ) {
    const props = {
      exploreCommentsModalIsOpen: exploreCommentsModalIsOpen,
      closeExploreCommentsModal: closeExploreCommentsModal,
      upvoteComment: upvoteComment,
      downvoteComment: downvoteComment,
      allComments: allComments,
      allCommentsLoading: allCommentsLoading,
    };
    return mount(<TfaExploreCommentsModal {...props} />);
  }

  before(() => {
    global.chrome = chrome;
    const jsdom = new JSDOM('<!doctype html><html><body></body></html>');
    const { window } = jsdom;
    global.window = window;
    global.document = window.document;
    mount = createMount();
  });

  after(() => {
    mount.cleanUp();
  });

  it("renders closed explore comments modal with no comments", () => {
    const wrapper = setup(false, [], false);
    expect(wrapper.props().exploreCommentsModalIsOpen).to.be.false;
    expect(wrapper.props().allComments).to.be.empty;
    expect(wrapper.props().allCommentsLoading).to.be.false;
    expect(wrapper.props().closeExploreCommentsModal).to.exist;
    expect(wrapper.props().upvoteComment).to.exist;
    expect(wrapper.props().downvoteComment).to.exist;
    expect(wrapper.find("Dialog").length).to.equal(1);
    expect(wrapper.find("Dialog").props().open).to.be.false;
    expect(wrapper.find("Dialog").props().onClose).to.exist;
  });

  it("renders open explore comments modal with no comments", () => {
    const wrapper = setup(true, [], false);
    expect(wrapper.props().exploreCommentsModalIsOpen).to.be.true;
    expect(wrapper.props().allComments).to.be.empty;
    expect(wrapper.props().allCommentsLoading).to.be.false;
    expect(wrapper.props().closeExploreCommentsModal).to.exist;
    expect(wrapper.props().upvoteComment).to.exist;
    expect(wrapper.props().downvoteComment).to.exist;
    expect(wrapper.find("Dialog").length).to.equal(1);
    expect(wrapper.find("Dialog").props().open).to.be.true;
    expect(wrapper.find("Dialog").props().onClose).to.exist;
    expect(wrapper.find("DialogTitle").length).to.equal(1);
    expect(wrapper.find("DialogTitle").text()).to.equal("Explore Comments");
    expect(wrapper.find("DialogContent").length).to.equal(1);
    expect(wrapper.find("TfaDisplayMessagePanel").length).to.equal(1);
    expect(wrapper.find("TfaDisplayMessagePanel").props().message).to.equal("No comments on the network...");
    expect(wrapper.find("DialogActions").length).to.equal(1);
    expect(wrapper.find("Button").length).to.equal(1);
    expect(wrapper.find("Button").at(0).props().disabled).to.be.false;
    expect(wrapper.find("Button").at(0).props().onClick).to.exist;
  });

  it("renders open explore comments modal when loading comments", () => {
    const wrapper = setup(true, [], true);
    expect(wrapper.props().exploreCommentsModalIsOpen).to.be.true;
    expect(wrapper.props().allComments).to.be.empty;
    expect(wrapper.props().allCommentsLoading).to.be.true;
    expect(wrapper.props().closeExploreCommentsModal).to.exist;
    expect(wrapper.props().upvoteComment).to.exist;
    expect(wrapper.props().downvoteComment).to.exist;
    expect(wrapper.find("Dialog").length).to.equal(1);
    expect(wrapper.find("Dialog").props().open).to.be.true;
    expect(wrapper.find("Dialog").props().onClose).to.exist;
    expect(wrapper.find("DialogTitle").length).to.equal(1);
    expect(wrapper.find("DialogTitle").text()).to.equal("Explore Comments");
    expect(wrapper.find("DialogContent").length).to.equal(1);
    expect(wrapper.find("TfaLoadingPanel").length).to.equal(1);
    expect(wrapper.find("TfaDisplayMessagePanel").length).to.equal(0);
    expect(wrapper.find("TfaCommentList").length).to.equal(0);
    expect(wrapper.find("DialogActions").length).to.equal(1);
    expect(wrapper.find("Button").length).to.equal(1);
    expect(wrapper.find("Button").at(0).props().disabled).to.be.false;
    expect(wrapper.find("Button").at(0).props().onClick).to.exist;
  });

  it("renders open explore comments modal with comments", () => {
    const comments = generateComments(2);
    const wrapper = setup(true, comments, false);
    expect(wrapper.props().exploreCommentsModalIsOpen).to.be.true;
    expect(wrapper.props().allComments.length).to.equal(2);
    expect(wrapper.props().allCommentsLoading).to.be.false;
    expect(wrapper.props().closeExploreCommentsModal).to.exist;
    expect(wrapper.props().upvoteComment).to.exist;
    expect(wrapper.props().downvoteComment).to.exist;
    expect(wrapper.find("Dialog").length).to.equal(1);
    expect(wrapper.find("Dialog").props().open).to.be.true;
    expect(wrapper.find("Dialog").props().onClose).to.exist;
    expect(wrapper.find("DialogTitle").length).to.equal(1);
    expect(wrapper.find("DialogTitle").text()).to.equal("Explore Comments");
    expect(wrapper.find("DialogContent").length).to.equal(1);
    expect(wrapper.find("TfaLoadingPanel").length).to.equal(0);
    expect(wrapper.find("TfaDisplayMessagePanel").length).to.equal(0);
    expect(wrapper.find("TfaCommentList").length).to.equal(1);
    expect(wrapper.find("DialogActions").length).to.equal(1);
    expect(wrapper.find("Button").length).to.equal(1);
    expect(wrapper.find("Button").at(0).props().disabled).to.be.false;
    expect(wrapper.find("Button").at(0).props().onClick).to.exist;

    expect(wrapper.find("List").length).to.equal(1);
    expect(wrapper.find("List").props().disablePadding).to.exist;
    expect(wrapper.find("TfaComment").length).to.equal(2);
    for (let i = 0; i < wrapper.find("TfaComment").length; i++) {
      expect(wrapper.find("TfaComment").at(i).key()).to.equal(comments[i].txHash);
      expect(wrapper.find("TfaComment").at(i).props().comment).to.equal(comments[i]);
      expect(wrapper.find("TfaComment").at(i).props().upvoteComment).to.exist;
      expect(wrapper.find("TfaComment").at(i).props().downvoteComment).to.exist;
    }
  });

  it("simulates upvote/downvote comment disabled", () => {
    const comments = generateComments(2);
    comments.forEach(comment => {
      comment.isEnabled = false;
    });
    const closeModal = sinon.spy();
    const upvoteComment = sinon.spy();
    const downvoteComment = sinon.spy();
    // upvoting/downvoting is disabled because comments are not confirmed
    const wrapper = setup(true, comments, false, closeModal, upvoteComment, downvoteComment);
    wrapper.find("a").at(0).simulate("click");
    expect(upvoteComment.called).to.be.false;
    wrapper.find("a").at(1).simulate("click");
    expect(downvoteComment.called).to.be.false;
  });

  it("simulates upvote/downvote comment enabled", () => {
    const comments = generateComments(2);
    const closeModal = sinon.spy();
    const upvoteComment = sinon.spy();
    const downvoteComment = sinon.spy();
    // upvoting/downvoting is enabled because comments are confirmed
    const wrapper = setup(true, comments, false, closeModal, upvoteComment, downvoteComment);
    wrapper.find("a").at(0).simulate("click");
    expect(upvoteComment.calledOnce).to.be.true;
    wrapper.find("a").at(1).simulate("click");
    expect(downvoteComment.calledOnce).to.be.true;
  });


  it("simulates click on close button", () => {
    const comments = generateComments(2);
    const closeModal = sinon.spy();
    const wrapper = setup(true, comments, false, closeModal);
    wrapper.find("Button").at(0).simulate("click");
    expect(closeModal.calledOnce).to.be.true;
  });
});
