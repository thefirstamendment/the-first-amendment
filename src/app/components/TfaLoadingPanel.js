import React from 'react';
import { CircularProgress } from '@material-ui/core/';
import style from './css/TfaLoadingPanel.scss';

const TfaLoadingPanel = () => {
  return (
    <div className={style.tfa_progress}>
      <CircularProgress size={50} />
    </div>
  );
};

export default TfaLoadingPanel;
