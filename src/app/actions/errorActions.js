import * as types from './actionTypes';

export function showError(error) {
  return { type: types.SHOW_ERROR, errorMessage: error.message };
}

export function hideError() {
  return { type: types.HIDE_ERROR };
}
