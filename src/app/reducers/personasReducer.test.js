import {expect} from 'chai';
import personasReducer from './personasReducer';
import * as actions from '../actions/personaActions';
import Persona from '../../common/Persona';
import personasTestJson from '../../../test/integration/test_personas.json';


function generatePersonas(personasCount) {
  const personas = [];
  for (let i = 0; i < personasCount; i++) {
    personas.push(new Persona("account" + i, "name" + i, "", 0));
  }
  return personas;
}

describe("personasReducer test", () => {
  it("should set personas when passed SET_PERSONAS", () => {
    const personas = generatePersonas(3);
    const action = actions.setPersonas(personas);
    const newState = personasReducer([], action);

    expect(newState.length).to.equal(3);
    for (let i = 0; i < 3; i++) {
      expect(newState[i]).to.equal(personas[i]);
    }
  });

  it("should add new persona when passed ADD_PERSONA_SUCCESS", () => {
    const persona = new Persona("account", personasTestJson.persona1.name, personasTestJson.persona1.avatar, 0);
    const action = actions.addPersonaSuccess(persona);
    const newState = personasReducer([], action);

    expect(newState.length).to.equal(1);
    expect(newState[0].account).to.equal(persona.account);
    expect(newState[0].name).to.equal(persona.name);
    expect(newState[0].avatar).to.equal(persona.avatar);
    expect(newState[0].balance).to.equal(persona.balance);
  });

  it("should not add new persona when passed ADD_PERSONA_SUCCESS and persona with same account exists", () => {
    const initPersona = new Persona("account", personasTestJson.persona1.name, personasTestJson.persona1.avatar, 0);
    const initState = [initPersona];
    const newPersona = new Persona("account", personasTestJson.persona1.name, personasTestJson.persona1.avatar, 1);
    const action = actions.addPersonaSuccess(newPersona);
    const newState = personasReducer(initState, action);

    expect(newState).to.not.equal(initState);
    expect(newState.length).to.equal(1);
    expect(newState[0].account).to.equal(initPersona.account);
    expect(newState[0].name).to.equal(initPersona.name);
    expect(newState[0].avatar).to.equal(initPersona.avatar);
    expect(newState[0].balance).to.equal(initPersona.balance);
  });

  it("should update persona when passed UPDATE_PERSONA_SUCCESS", () => {
    const initPersonas = generatePersonas(3);
    let newPersona = Object.assign({}, initPersonas[2]);
    newPersona.name = "newName";
    const action = actions.updatePersonaSuccess(newPersona);
    const newState = personasReducer(initPersonas, action);

    expect(newState.length).to.equal(3);
    for (let i = 0; i < 2; i++) {
      expect(newState[i]).to.equal(initPersonas[i]);
    }
    expect(newState[2].account).to.equal(newPersona.account);
    expect(newState[2].name).to.equal(newPersona.name);
    expect(newState[2].avatar).to.equal(newPersona.avatar);
    expect(newState[2].balance).to.equal(newPersona.balance);
  });
});
