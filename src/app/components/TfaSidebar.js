import React from 'react';
import PropTypes from 'prop-types';
import { Drawer } from '@material-ui/core/';
import style from './css/TfaSidebar.scss';
import TfaHeaderPanelContainer from './header/TfaHeaderPanelContainer'; //eslint-disable-line import/no-named-as-default
import TfaPersonaManagementPanelContainer from './personas/TfaPersonaManagementPanelContainer'; //eslint-disable-line import/no-named-as-default
import TfaCommentsManagementPanelContainer from './comments/TfaCommentsManagementPanelContainer'; //eslint-disable-line import/no-named-as-default
import TfaExploreCommentsModalContainer from './TfaExploreCommentsModalContainer'; //eslint-disable-line import/no-named-as-default
import TfaErrNotification from './TfaErrNotification';
import TfaNotification from './TfaNotification';

const TfaSidebar = (props) => {

  const bodyPanel = props.showPersonaManagementPanel ? (<TfaPersonaManagementPanelContainer />) : (<TfaCommentsManagementPanelContainer />);
  return (
    <Drawer
      className={style.tfa_side_nav}
      variant="persistent"
      anchor="right"
      open={props.displaySidebar}
      classes={{
        paper: style.tfa_drawer_paper,
      }}
    >
      <TfaHeaderPanelContainer
        openExploreCommentsModal={props.openExploreCommentsModal}
      />
      {bodyPanel}
      <TfaNotification
        isOpen={props.notificationMessage && props.notificationMessage != ""}
        notificationMessage={props.notificationMessage}
        hideNotification={props.handleNotificationClose}
      />
      <TfaErrNotification
        isOpen={props.errorMessage && props.errorMessage != ""}
        errorMessage={props.errorMessage}
        hideError={props.handleErrorNotificationClose}
      />
      <TfaExploreCommentsModalContainer
        exploreCommentsModalIsOpen={props.isExploreCommentsModalOpen}
        closeExploreCommentsModal={props.closeExploreCommentsModal}
      />
    </Drawer>
  );
};


TfaSidebar.propTypes = {
  showPersonaManagementPanel: PropTypes.bool.isRequired,
  displaySidebar: PropTypes.bool.isRequired,
  errorMessage: PropTypes.string.isRequired,
  notificationMessage: PropTypes.string.isRequired,
  handleErrorNotificationClose: PropTypes.func.isRequired,
  handleNotificationClose: PropTypes.func.isRequired,
  isExploreCommentsModalOpen: PropTypes.bool.isRequired,
  openExploreCommentsModal: PropTypes.func.isRequired,
  closeExploreCommentsModal: PropTypes.func.isRequired
};

export default TfaSidebar;
