import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import InputValidationError from '../../common/errors/InputValidationError';
import * as commentsActions from '../actions/commentsActions';
import * as errorActions from '../actions/errorActions';
import TfaExploreCommentsModal from './TfaExploreCommentsModal';

export class TfaExploreCommentsModalContainer extends React.Component {

  constructor(props) {
    super(props);
    this.upvoteComment = this.upvoteComment.bind(this);
    this.downvoteComment = this.downvoteComment.bind(this);
  }

  // TODO: upvoteComment, downvoteComment functions are copy/pasted
  // from CommentsManagementPanelContainer, refactor later
  upvoteComment(txHash) {
    const upvotedComment = this.props.allComments.find(comment => comment.txHash == txHash);
    if(upvotedComment) {
      this.props.commentsActions.upvoteComment(upvotedComment, this.props.currentPersona.account);
    } else {
      this.props.errorActions.showError(new InputValidationError("No comment with matching txHash found."));
    }
  }

  downvoteComment(txHash) {
    const downvotedComment = this.props.allComments.find(comment => comment.txHash == txHash);
    if (downvotedComment) {
      this.props.commentsActions.downvoteComment(downvotedComment, this.props.currentPersona.account);
    } else {
      this.props.errorActions.showError(new InputValidationError("No comment with matching txHash found."));
    }
  }

  render() {
    return (
      <TfaExploreCommentsModal
        exploreCommentsModalIsOpen={this.props.exploreCommentsModalIsOpen}
        closeExploreCommentsModal={this.props.closeExploreCommentsModal}
        upvoteComment={this.upvoteComment}
        downvoteComment={this.downvoteComment}
        allComments={this.props.allComments}
        allCommentsLoading={this.props.allCommentsLoading}
      />
    );
  }
}

TfaExploreCommentsModalContainer.propTypes = {
  exploreCommentsModalIsOpen: PropTypes.bool.isRequired,
  closeExploreCommentsModal: PropTypes.func.isRequired,
  commentsActions: PropTypes.object.isRequired,
  errorActions: PropTypes.object.isRequired,
  allComments: PropTypes.array.isRequired,
  allCommentsLoading: PropTypes.bool.isRequired,
  currentPersona: PropTypes.object.isRequired,
};

function mapStateToProps(state, ownProps) {
  return {
    allComments: state.allComments,
    allCommentsLoading: state.allCommentsLoading,
    currentPersona: state.currentPersona,
    exploreCommentsModalIsOpen: ownProps.exploreCommentsModalIsOpen,
    closeExploreCommentsModal: ownProps.closeExploreCommentsModal
  };
}

function mapDispatchToProps(dispatch) {
  return {
    commentsActions: bindActionCreators(commentsActions, dispatch),
    errorActions: bindActionCreators(errorActions, dispatch)
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(TfaExploreCommentsModalContainer);
