import React from 'react';
import PropTypes from 'prop-types';
import { Snackbar } from '@material-ui/core/';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faCheck, faTimes } from '@fortawesome/free-solid-svg-icons';
import style from './css/TfaNotification.scss';

const TfaNotification = (props) => {
  const errMessage = (
    <div className={style.tfa_notification_message}>
      <div className={style.tfa_notification_header}>
        <div className={style.tfa_fa_notification_icon}>
          <FontAwesomeIcon icon={faCheck} size="lg" />
        </div>
        <div>
          <h3>Notification</h3>
        </div>
      </div>
      <div className={style.tfa_notification_content}>
        <span>{props.notificationMessage}</span>
      </div>
      <div className={style.tfa_notification_action}>
        <a href="#" rel="noopener" role="button" onClick={() => props.hideNotification()}>
          <FontAwesomeIcon icon={faTimes} />
        </a>&nbsp;
      </div>
    </div>
  );

  return (
    <Snackbar
      classes={{
        root: style.tfa_notification
      }}
      anchorOrigin={{
        vertical: 'top',
        horizontal: 'right',
      }}
      open={props.isOpen}
      onClose={() => props.hideNotification()}
      autoHideDuration={2500}
      SnackbarContentProps={{
        'aria-describedby': 'message-id',
      }}
      message={errMessage}
    />
  );
};

TfaNotification.propTypes = {
  isOpen: PropTypes.bool.isRequired,
  hideNotification: PropTypes.func.isRequired,
  notificationMessage: PropTypes.string.isRequired
};

export default TfaNotification;

