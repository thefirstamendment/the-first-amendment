import { expect } from 'chai';
import React from 'react';
import { TfaPersonaManagementPanelContainer } from './TfaPersonaManagementPanelContainer';
import chrome from 'sinon-chrome/extensions';
import sinon from 'sinon';
import Utils from '../../../common/Utils';
import { createMount } from '@material-ui/core/test-utils';
import { JSDOM } from 'jsdom';
import Enzyme from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import Persona from '../../../common/Persona';

Enzyme.configure({ adapter: new Adapter() });

describe("<TfaPersonaManagementPanelContainer /> tests", () => {
  let mount;
  let getDefaultAvatarImgStub;

  function setup(
    accounts,
    currentPersona,
    personas,
    personaManagementPanelActions = {},
    errorActions = {},
    personaActions = {},
    currentPersonaActions = {},
    togglePersonaManagementPanel = () => {},
    addPersona = () => {},
    updatePersona = () => {},
    selectPersona = () => {}
  ) {
    const props = {
      accounts: accounts,
      currentPersona: currentPersona,
      personas: personas,
      personaManagementPanelActions: personaManagementPanelActions,
      errorActions: errorActions,
      personaActions: personaActions,
      currentPersonaActions: currentPersonaActions,
      togglePersonaManagementPanel: togglePersonaManagementPanel,
      addPersona: addPersona,
      updatePersona: updatePersona,
      selectPersona: selectPersona
    };
    return mount(<TfaPersonaManagementPanelContainer {...props} />);
  }

  before(() => {
    global.chrome = chrome;
    const jsdom = new JSDOM('<!doctype html><html><body></body></html>');
    const { window } = jsdom;
    global.window = window;
    global.document = window.document;
    getDefaultAvatarImgStub = sinon.stub(Utils, "getDefaultAvatarImg");
    getDefaultAvatarImgStub.returns("defaultAvatarImg");
    mount = createMount();
  });

  after(() => {
    mount.cleanUp();
    getDefaultAvatarImgStub.restore();
  });

  it("renders persona management container without accounts, current persona and personas", () => {
    const wrapper = setup([], {}, []);
    expect(wrapper.props().accounts).to.be.empty;
    expect(wrapper.props().currentPersona).to.be.empty;
    expect(wrapper.props().personas).to.be.empty;
    expect(wrapper.props().personaManagementPanelActions).to.exist;
    expect(wrapper.props().errorActions).to.exist;
    expect(wrapper.props().personaActions).to.exist;
    expect(wrapper.props().currentPersonaActions).to.exist;
    expect(wrapper.props().addPersona).to.exist;
    expect(wrapper.props().updatePersona).to.exist;
    expect(wrapper.props().selectPersona).to.exist;
    expect(wrapper.props().togglePersonaManagementPanel).to.exist;
    expect(wrapper.find("TfaPersonaManagementPanel").length).to.equal(1);
  });

  it("renders persona management container without accounts, current persona and personas", () => {
    const account = "account";
    const persona = new Persona(account, "name", "avatar", 0, true);
    const wrapper = setup([account], persona, [persona]);
    expect(wrapper.props().accounts[0]).to.equal(account);
    expect(wrapper.props().currentPersona).to.equal(persona);
    expect(wrapper.props().personas[0]).to.equal(persona);
    expect(wrapper.props().personaManagementPanelActions).to.exist;
    expect(wrapper.props().errorActions).to.exist;
    expect(wrapper.props().personaActions).to.exist;
    expect(wrapper.props().currentPersonaActions).to.exist;
    expect(wrapper.props().addPersona).to.exist;
    expect(wrapper.props().updatePersona).to.exist;
    expect(wrapper.props().selectPersona).to.exist;
    expect(wrapper.props().togglePersonaManagementPanel).to.exist;
    expect(wrapper.find("TfaPersonaManagementPanel").length).to.equal(1);
  });
});
