import { expect } from 'chai';
import React from 'react';
import TfaHeaderPanel from './TfaHeaderPanel';
import sinon from 'sinon';
import chrome from 'sinon-chrome/extensions';
import Utils from '../../../common/Utils';
import { createMount } from '@material-ui/core/test-utils';
import personasTestJson from '../../../../test/integration/test_personas.json';
import Persona from '../../../common/Persona';
import { JSDOM } from 'jsdom';
import Enzyme from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

Enzyme.configure({ adapter: new Adapter() });

describe("<TfaHeaderPanel /> tests", () => {
  let mount;
  let getDefaultAvatarImgStub;

  function setup(showPersonaManagementPanel, currentPersona) {
    const props = {
      togglePersonaManagementPanel: () => { },
      showPersonaManagementPanel: showPersonaManagementPanel,
      currentPersona: currentPersona
    };
    return mount(<TfaHeaderPanel {...props} />);
  }

  before(() => {
    global.chrome = chrome;
    const jsdom = new JSDOM('<!doctype html><html><body></body></html>');
    const { window } = jsdom;
    global.window = window;
    global.document = window.document;
    getDefaultAvatarImgStub = sinon.stub(Utils, "getDefaultAvatarImg");
    getDefaultAvatarImgStub.returns("defaultAvatarImg");
    mount = createMount();
  });

  after(() => {
    mount.cleanUp();
    getDefaultAvatarImgStub.restore();
  });

  it("renders persona management panel and empty persona", () => {
    const currentPersona = {};
    const showPersonaManagementPanel = true;
    const expectedAvatarImg = Utils.getAvatarImg(currentPersona);
    const wrapper = setup(showPersonaManagementPanel, currentPersona);
    expect(wrapper.props().showPersonaManagementPanel).to.be.true;
    expect(wrapper.props().currentPersona).to.be.empty;
    expect(wrapper.find("p").length).to.equal(1);
    expect(wrapper.find("p").text()).to.equal("Hello, Anonymous");
    expect(wrapper.find("Avatar").length).to.equal(1);
    expect(wrapper.find("Avatar").props().src).to.not.be.undefined;
    expect(wrapper.find("Avatar").props().src).to.not.equal("");
    expect(wrapper.find("Avatar").props().src).to.equal(expectedAvatarImg);
    expect(wrapper.find("Tooltip").length).to.equal(2);
    expect(wrapper.find("Tooltip").at(1).props().title).to.equal("Return to comments management panel");
  });

  it("renders comments management panel and empty persona", () => {
    const currentPersona = {};
    const showPersonaManagementPanel = false;
    const expectedAvatarImg = Utils.getAvatarImg(currentPersona);
    const wrapper = setup(showPersonaManagementPanel, currentPersona);
    expect(wrapper.props().showPersonaManagementPanel).to.be.false;
    expect(wrapper.props().currentPersona).to.be.empty;
    expect(wrapper.find("p").length).to.equal(1);
    expect(wrapper.find("p").text()).to.equal("Hello, Anonymous");
    expect(wrapper.find('Avatar').length).to.equal(1);
    expect(wrapper.find("Avatar").props().src).to.not.be.undefined;
    expect(wrapper.find("Avatar").props().src).to.not.equal("");
    expect(wrapper.find('Avatar').props().src).to.equal(expectedAvatarImg);
    expect(wrapper.find("Tooltip").length).to.equal(2);
    expect(wrapper.find("Tooltip").at(1).props().title).to.equal("Persona profile and settings");
  });

  it("renders persona management panel and existing persona", () => {
    const currentPersona = new Persona("account", personasTestJson.persona1.name, personasTestJson.persona1.avatar, 0);
    const showPersonaManagementPanel = true;
    const wrapper = setup(showPersonaManagementPanel, currentPersona);
    expect(wrapper.props().showPersonaManagementPanel).to.be.true;
    expect(wrapper.props().currentPersona).to.equal(currentPersona);
    expect(wrapper.find("p").length).to.equal(1);
    expect(wrapper.find("p").text()).to.equal("Hello, " + currentPersona.name);
    expect(wrapper.find("Avatar").length).to.equal(1);
    expect(wrapper.find("Avatar").props().src).to.equal(currentPersona.avatar);
    expect(wrapper.find("Tooltip").length).to.equal(2);
    expect(wrapper.find("Tooltip").at(1).props().title).to.equal("Return to comments management panel");
  });

  it("renders comments management panel and existing persona", () => {
    const currentPersona = new Persona("account", personasTestJson.persona1.name, personasTestJson.persona1.avatar, 0);
    const showPersonaManagementPanel = false;
    const wrapper = setup(showPersonaManagementPanel, currentPersona);
    expect(wrapper.props().showPersonaManagementPanel).to.be.false;
    expect(wrapper.props().currentPersona).to.equal(currentPersona);
    expect(wrapper.find("p").length).to.equal(1);
    expect(wrapper.find("p").text()).to.equal("Hello, " + currentPersona.name);
    expect(wrapper.find("Avatar").length).to.equal(1);
    expect(wrapper.find("Avatar").props().src).to.equal(currentPersona.avatar);
    expect(wrapper.find("Tooltip").length).to.equal(2);
    expect(wrapper.find("Tooltip").at(1).props().title).to.equal("Persona profile and settings");
  });

  it("simulates click on avatar button", () => {
    const togglePersonaManagementPanel = sinon.spy();
    const currentPersona = {};
    const showPersonaManagementPanel = true;
    const wrapper = mount(
      <TfaHeaderPanel
        togglePersonaManagementPanel={togglePersonaManagementPanel}
        showPersonaManagementPanel={showPersonaManagementPanel}
        currentPersona={currentPersona}
      />
    );
    wrapper.find("a").simulate("click");
    expect(togglePersonaManagementPanel.calledOnce).to.be.true;
  });
});
