import React from 'react';
import PropTypes from 'prop-types';
import {
  Dialog,
  Button,
  DialogTitle,
  DialogContent,
  DialogActions
} from '@material-ui/core/';
import style from './css/TfaExploreCommentsModal.scss';
import TfaCommentList from './comments/TfaCommentList';
import TfaLoadingPanel from './TfaLoadingPanel';
import TfaDisplayMessagePanel from './TfaDisplayMessagePanel';

const TfaExploreCommentsModal = (props) => {

  // TODO: consider sorting comments outside of render (hodl sorted comments
  // in state when props are received), could be expensive operation
  let sortedComments = [...props.allComments];
  if (props.allComments.length > 0) {
    sortedComments.sort((a, b) => {
      return (b.timestamp - a.timestamp);
    });
  }
  let commentsContent;
  if (props.allCommentsLoading) {
    commentsContent = (<TfaLoadingPanel />);
  } else if (props.allComments.length > 0) {
    commentsContent = (
      <TfaCommentList
        comments={sortedComments}
        upvoteComment={props.upvoteComment}
        downvoteComment={props.downvoteComment}
        showUrl
      />
    );
  } else {
    commentsContent = (
      <TfaDisplayMessagePanel message="No comments on the network..." />
    );
  }
  return (
    <Dialog
      disableRestoreFocus
      open={props.exploreCommentsModalIsOpen}
      onClose={() => props.closeExploreCommentsModal()}
      aria-labelledby="form-dialog-title"
      className={style.tfa_dialog}
    >
      <DialogTitle className={style.tfa_dialog_title}>Explore Comments</DialogTitle>
      <DialogContent className={style.tfa_dialog_content}>
        {commentsContent}
      </DialogContent>
      <DialogActions>
        <Button className={style.close_btn} variant="raised" color="default" onClick={() => props.closeExploreCommentsModal()}>Close</Button>
      </DialogActions>
    </Dialog>
  );
};

TfaExploreCommentsModal.propTypes = {
  exploreCommentsModalIsOpen: PropTypes.bool.isRequired,
  closeExploreCommentsModal: PropTypes.func.isRequired,
  upvoteComment: PropTypes.func.isRequired,
  downvoteComment: PropTypes.func.isRequired,
  allComments: PropTypes.array.isRequired,
  allCommentsLoading: PropTypes.bool.isRequired
};

export default TfaExploreCommentsModal;
