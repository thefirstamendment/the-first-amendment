import React from 'react';
import PropTypes from 'prop-types';
import { List } from '@material-ui/core/';
import TfaComment from './TfaComment';
import style from '../css/TfaCommentList.scss';

const TfaCommentList = ({ comments, upvoteComment, downvoteComment, showUrl }) => {
  let commentList = comments.map((comment) => (
    <TfaComment
      key={comment.txHash}
      comment={comment}
      upvoteComment={upvoteComment}
      downvoteComment={downvoteComment}
      showUrl={showUrl}
    />
  ));

  return (
    <List component="ol" className={style.tfa_comment_list} disablePadding>
      {commentList}
    </List>
  );
};

TfaCommentList.propTypes = {
  comments: PropTypes.array.isRequired,
  upvoteComment: PropTypes.func.isRequired,
  downvoteComment: PropTypes.func.isRequired,
  showUrl: PropTypes.bool.isRequired,
};

export default TfaCommentList;
