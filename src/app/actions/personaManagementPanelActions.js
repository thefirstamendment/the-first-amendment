import * as types from './actionTypes';

export function togglePersonaManagementPanel() {
  return { type: types.TOGGLE_PERSONA_MANAGEMENT_PANEL };
}
