import * as types from './actionTypes';

export function displaySidebar(isSidebarVisible) {
  return { type: types.DISPLAY_SIDEBAR, isSidebarVisible: isSidebarVisible };
}
