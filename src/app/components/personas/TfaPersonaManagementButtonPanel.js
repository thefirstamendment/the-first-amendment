import React from 'react';
import PropTypes from 'prop-types';
import { Button } from '@material-ui/core/';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faUserPlus } from '@fortawesome/free-solid-svg-icons';
import style from '../css/TfaPersonaManagementButtonPanel.scss';

const TfaPersonaManagementButtonPanel = (props) => {

  return (
    <div className={style.tfa_persona_management_button_panel}>
      <Button className={style.tfa_btn_add_persona} variant="raised" color="primary" onClick={() => props.openAddPersonaModal()}>
        Add Persona&nbsp;&nbsp;
        <FontAwesomeIcon icon={faUserPlus} />
      </Button>
      <Button className={style.tfa_btn_close} variant="raised" color="default" onClick={() => props.togglePersonaManagementPanel()}>
        Close
      </Button>
    </div>
  );
};

TfaPersonaManagementButtonPanel.propTypes = {
  togglePersonaManagementPanel: PropTypes.func.isRequired,
  openAddPersonaModal: PropTypes.func.isRequired
};

export default TfaPersonaManagementButtonPanel;

