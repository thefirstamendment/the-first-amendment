import * as types from './actionTypes';

export function setCommentsLoading(commentsLoading) {
  return { type: types.SET_COMMENTS_LOADING, commentsLoading: commentsLoading };
}
