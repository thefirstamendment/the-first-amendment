import * as types from '../actions/actionTypes';
import initialState from './initialState';

export default function showPersonaManagementPanelReducer(state = initialState.personaManagementPanelVisible, action) {
  switch (action.type) {
    case types.TOGGLE_PERSONA_MANAGEMENT_PANEL:
      return !state;

    default:
      return state;
  }
}
