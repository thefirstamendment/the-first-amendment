/*jshint esversion: 6 */
import { expect } from 'chai';
import IpfsError from './IpfsError';
import Web3Error from './Web3Error';
import InputValidationError from './InputValidationError';

describe('test IpfsError', () => {
  it('IpfsError should show message', () => {
    let expectedMessage = "My ipfs descriptive message";
    let ipfsError = new IpfsError(expectedMessage);
    expect(ipfsError.message).to.equal(expectedMessage);
  });
});

describe('test Web3Error', () => {
  it('Web3Error should show message', () => {
    let expectedMessage = "My web3 descriptive message";
    let web3Error = new Web3Error(expectedMessage);
    expect(web3Error.message).to.equal(expectedMessage);
  });
});

describe('test InputValidationError', () => {
  it('InputValidationError should show message', () => {
    let expectedMessage = "My InputValidationError descriptive message";
    let inputValidationError = new InputValidationError(expectedMessage);
    expect(inputValidationError.message).to.equal(expectedMessage);
  });
});
