import {expect} from 'chai';
import commentsReducer from './commentsReducer';
import * as commentsActions from '../actions/commentsActions';
import * as personaActions from '../actions/personaActions';
import Comment from '../../common/Comment';
import Persona from '../../common/Persona';
import personasTestJson from '../../../test/integration/test_personas.json';


function generateComments(commentsCount) {
  const tokenPersona = new Persona("account", personasTestJson.persona1.name, personasTestJson.persona1.avatar, 0);
  const comments = [];
  for (let i = 0; i < commentsCount; i++) {
    comments.push(new Comment("txHash" + i, "content" + i, "url" + i, tokenPersona, 0, 0, 0, true));
  }
  return comments;
}

describe("commentsReducer tests", () => {
  it("should set comments when passed GET_COMMENTS_SUCCESS", () => {
    const comments = generateComments(3);
    const action = commentsActions.getCommentsSuccess(comments);
    const newState = commentsReducer([], action);

    expect(newState.length).to.equal(3);
    for (let i = 0; i < 3; i++) {
      expect(newState[i]).to.equal(comments[i]);
    }
  });

  it("should add new comment when passed ADD_COMMENT_SUCCESS", () => {
    const tokenPersona = new Persona("account", personasTestJson.persona1.name, personasTestJson.persona1.avatar, 0);
    const comment = new Comment("txHash", "content", "url", tokenPersona, 0, 0, 0, true);
    const action = commentsActions.addCommentSuccess(comment);
    const newState = commentsReducer([], action);

    expect(newState.length).to.equal(1);
    expect(newState[0].txHash).to.equal(comment.txHash);
    expect(newState[0].content).to.equal(comment.content);
    expect(newState[0].url).to.equal(comment.url);
    expect(newState[0].persona).to.equal(comment.persona);
    expect(newState[0].upvoteCount).to.equal(comment.upvoteCount);
    expect(newState[0].downvoteCount).to.equal(comment.downvoteCount);
    expect(newState[0].timestamp).to.equal(comment.timestamp);
  });

  it("should not add new comment when passed ADD_COMMENT_SUCCESS and comment with same txHash exists", () => {
    const tokenPersona = new Persona("account", personasTestJson.persona1.name, personasTestJson.persona1.avatar, 0);
    const initComment = new Comment("txHash", "content", "url", tokenPersona, 0, 0, 0, true);
    const initState = [initComment];
    const newComment = new Comment("txHash", "content1", "url1", tokenPersona, 0, 0, 0, true);
    const action = commentsActions.addCommentSuccess(newComment);
    const newState = commentsReducer(initState, action);

    expect(newState).to.not.equal(initState);
    expect(newState.length).to.equal(1);
    expect(newState[0].txHash).to.equal(initComment.txHash);
    expect(newState[0].content).to.equal(initComment.content);
    expect(newState[0].url).to.equal(initComment.url);
    expect(newState[0].persona).to.equal(initComment.persona);
    expect(newState[0].upvoteCount).to.equal(initComment.upvoteCount);
    expect(newState[0].downvoteCount).to.equal(initComment.downvoteCount);
    expect(newState[0].timestamp).to.equal(initComment.timestamp);
  });

  it("should update comment when passed UPDATE_COMMENT_SUCCESS", () => {
    const tokenPersona = new Persona("account", personasTestJson.persona1.name, personasTestJson.persona1.avatar, 0);
    const initComment = new Comment("txHash", "content", "url", tokenPersona, 0, 0, 0, true);
    const initState = [initComment];
    const newComment = new Comment("txHash1", "content1", "url1", tokenPersona, 0, 0, 0, false);
    const action = commentsActions.updateCommentSuccess(initComment.txHash, newComment);
    const newState = commentsReducer(initState, action);

    expect(newState).to.not.equal(initState);
    expect(newState.length).to.equal(1);
    expect(newState[0].txHash).to.equal(newComment.txHash);
    expect(newState[0].content).to.equal(newComment.content);
    expect(newState[0].url).to.equal(newComment.url);
    expect(newState[0].persona).to.equal(newComment.persona);
    expect(newState[0].upvoteCount).to.equal(newComment.upvoteCount);
    expect(newState[0].downvoteCount).to.equal(newComment.downvoteCount);
    expect(newState[0].timestamp).to.equal(newComment.timestamp);
  });

  it("should update persona in all comments when passed UPDATE_PERSONA_SUCCESS", () => {
    const comments = generateComments(3);
    const newPersona = new Persona("account", "new name", "new avatar", 0);
    const action = personaActions.updatePersonaSuccess(newPersona);
    const newState = commentsReducer(comments, action);

    expect(newState.length).to.equal(3);
    for (let i = 0; i < 3; i++) {
      expect(newState[i].persona.account).to.equal(newPersona.account);
      expect(newState[i].persona.name).to.equal(newPersona.name);
      expect(newState[i].persona.avatar).to.equal(newPersona.avatar);
      expect(newState[i].persona.balance).to.equal(newPersona.balance);
      expect(newState[i].persona.isEnabled).to.equal(newPersona.isEnabled);
    }
  });
});
