import React from 'react';
import PropTypes from 'prop-types';
import TfaCommentsManagementPanel from './TfaCommentsManagementPanel';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as commentsActions from '../../actions/commentsActions';
import * as errorActions from '../../actions/errorActions';
import * as shareCommentsPanelActions from '../../actions/shareCommentsPanelActions';
import Comment from '../../../common/Comment';
import Utils from '../../../common/Utils';
import InputValidationError from '../../../common/errors/InputValidationError';

export class TfaCommentsManagementPanelContainer extends React.Component {

  constructor(props) {
    super(props);
    this.addComment = this.addComment.bind(this);
    this.upvoteComment = this.upvoteComment.bind(this);
    this.downvoteComment = this.downvoteComment.bind(this);
    this.selectCommentOrdering = this.selectCommentOrdering.bind(this);
    this.sortComments = this.sortComments.bind(this);
    this.handleSmallSharePanelVisible = this.handleSmallSharePanelVisible.bind(this);
    this.state = {
      commentOrdering: 0 // 0: NEW, 1: HOT
    };
  }

  addComment(commentContent) {
    // this.props.unconfirmedCommentsCount is set as a temporary comment identifier
    // in order to be able to manipulate comments in the reducer before it is added to blockchain and receives
    // txHash as its permanent identifier
    const comment = new Comment(this.props.unconfirmedCommentsCount, commentContent, Utils.getCurrentUrl(), this.props.currentPersona, 0, 0, Date.now());
    this.props.commentsActions.addComment(comment);
  }

  upvoteComment(txHash) {
    const upvotedComment = this.props.comments.find(comment => comment.txHash == txHash);
    if(upvotedComment) {
      this.props.commentsActions.upvoteComment(upvotedComment, this.props.currentPersona.account);
    } else {
      this.props.errorActions.showError(new InputValidationError("No comment with matching txHash found."));
    }
  }

  downvoteComment(txHash) {
    const downvotedComment = this.props.comments.find(comment => comment.txHash == txHash);
    if (downvotedComment) {
      this.props.commentsActions.downvoteComment(downvotedComment, this.props.currentPersona.account);
    } else {
      this.props.errorActions.showError(new InputValidationError("No comment with matching txHash found."));
    }
  }

  selectCommentOrdering(ordering) {
    this.setState({ commentOrdering: ordering });
  }

  handleSmallSharePanelVisible(isSmallPanelVisible) {
    this.props.shareCommentsPanelActions.showSmallShareCommentPanel(isSmallPanelVisible);
  }

  sortComments(comments) {
    let sortedComments = [...comments];
    if (this.state.commentOrdering) { // sort by HOT
      // NOTE: since comments are added on top, we sort in reverse order
      sortedComments.sort((a, b) => {
        return (b.upvoteCount - a.upvoteCount);
      });
    } else { // sort by NEW
      // NOTE: since comments are added on top, we sort in reverse order
      sortedComments.sort((a, b) => {
        return (b.timestamp - a.timestamp);
      });
    }
    return sortedComments;
  }

  render() {
    let sortedComments = [];
    if (this.props.comments.length > 0) { // sort comments based on selected ordering (HOT or NEW)
      sortedComments = this.sortComments(this.props.comments);
    }
    return (
      <TfaCommentsManagementPanel
        comments={sortedComments}
        commentOrdering={this.state.commentOrdering}
        selectCommentOrdering={this.selectCommentOrdering}
        currentPersona={this.props.currentPersona}
        commentsLoading={this.props.commentsLoading}
        showSmallShareCommentPanel={this.props.showSmallShareCommentPanel}
        addComment={this.addComment}
        upvoteComment={this.upvoteComment}
        downvoteComment={this.downvoteComment}
        handleSmallSharePanelVisible={this.handleSmallSharePanelVisible}
      />
    );
  }
}

TfaCommentsManagementPanelContainer.propTypes = {
  commentsActions: PropTypes.object.isRequired,
  errorActions: PropTypes.object.isRequired,
  shareCommentsPanelActions: PropTypes.object.isRequired,
  comments: PropTypes.array.isRequired,
  unconfirmedCommentsCount: PropTypes.number.isRequired,
  currentPersona: PropTypes.object.isRequired,
  commentsLoading: PropTypes.bool.isRequired,
  showSmallShareCommentPanel: PropTypes.bool.isRequired,
};

function mapStateToProps(state, ownProps) {
  return {
    currentPersona: state.currentPersona,
    comments: state.comments,
    unconfirmedCommentsCount: state.unconfirmedCommentsCount,
    commentsLoading: state.commentsLoading,
    showSmallShareCommentPanel: state.showSmallShareCommentPanel,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    commentsActions: bindActionCreators(commentsActions, dispatch),
    errorActions: bindActionCreators(errorActions, dispatch),
    shareCommentsPanelActions: bindActionCreators(shareCommentsPanelActions, dispatch)
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(TfaCommentsManagementPanelContainer);
