import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import { Avatar } from '@material-ui/core/';
import style from '../css/TfaShareCommentPanelLargeLeft.scss';

const TfaShareCommentPanelLargeLeft = ({ personaImageAvatar }) => {
  return (
    <div className={style.tfa_share_comment_panel_large_left}>
      <Avatar
        src={personaImageAvatar}
        className={style.tfa_avatar_img_size48}
      />
    </div>
  );
};

TfaShareCommentPanelLargeLeft.propTypes = {
  personaImageAvatar: PropTypes.string.isRequired
};

export default withStyles(style)(TfaShareCommentPanelLargeLeft);
