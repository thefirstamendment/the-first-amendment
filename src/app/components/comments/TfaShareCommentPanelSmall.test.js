import { expect } from 'chai';
import React from 'react';
import sinon from 'sinon';
import TfaShareCommentPanelSmall from './TfaShareCommentPanelSmall';
import chrome from 'sinon-chrome/extensions';
import { createMount } from '@material-ui/core/test-utils';
import { JSDOM } from 'jsdom';
import personasTestJson from '../../../../test/integration/test_personas.json';
import Persona from '../../../common/Persona';
import Enzyme from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

Enzyme.configure({ adapter: new Adapter() });

describe("<TfaShareCommentPanelSmall /> tests", () => {
  let mount;

  function setup(
    currentPersona,
    handleSmallSharePanelVisible = () => { }
  ) {
    const props = {
      currentPersona: currentPersona,
      handleSmallSharePanelVisible: handleSmallSharePanelVisible
    };
    return mount(<TfaShareCommentPanelSmall {...props} />);
  }


  before(() => {
    global.chrome = chrome;
    const jsdom = new JSDOM('<!doctype html><html><body></body></html>');
    const { window } = jsdom;
    global.window = window;
    global.document = window.document;
    mount = createMount();
  });

  after(() => {
    mount.cleanUp();
  });

  it("renders share comment panel small no current persona, no unconfirmed personas", () => {
    const currentPersona = {};
    const wrapper = setup(currentPersona);
    expect(wrapper.props().currentPersona).to.be.empty;
    expect(wrapper.props().handleSmallSharePanelVisible).to.exist;
    expect(wrapper.find("FormGroup").length).to.equal(1);
    expect(wrapper.find("Tooltip").length).to.equal(1);
    expect(wrapper.find("Tooltip").props().enterDelay).to.equal(100);
    expect(wrapper.find("Tooltip").props().leaveDelay).to.equal(100);
    expect(wrapper.find("Tooltip").props().placement).to.equal("bottom");
    expect(wrapper.find("Tooltip").props().title).to.equal("Please register at least one persona to be able to leave comments");
    expect(wrapper.find("TextField").length).to.equal(1);
    expect(wrapper.find("TextField").props().placeholder).to.equal(" Please register persona!");
    expect(wrapper.find("TextField").props().disabled).to.be.true;
  });

  it("renders share comment panel small with current persona undefined", () => {
    const currentPersona = undefined;
    const wrapper = setup(currentPersona);
    expect(wrapper.props().currentPersona).to.be.undefined;
    expect(wrapper.props().handleSmallSharePanelVisible).to.exist;
    expect(wrapper.find("FormGroup").length).to.equal(1);
    expect(wrapper.find("Tooltip").length).to.equal(1);
    expect(wrapper.find("Tooltip").props().enterDelay).to.equal(100);
    expect(wrapper.find("Tooltip").props().leaveDelay).to.equal(100);
    expect(wrapper.find("Tooltip").props().placement).to.equal("bottom");
    expect(wrapper.find("Tooltip").props().title).to.equal("Please register at least one persona to be able to leave comments");
    expect(wrapper.find("TextField").length).to.equal(1);
    expect(wrapper.find("TextField").props().placeholder).to.equal(" Please register persona!");
    expect(wrapper.find("TextField").props().disabled).to.be.true;
  });

  it("renders share comment panel small with enabled current persona", () => {
    const currentPersona = new Persona("account", personasTestJson.persona1.name, personasTestJson.persona1.avatar, 0, true);
    const wrapper = setup(currentPersona);
    expect(wrapper.props().currentPersona).to.equal(currentPersona);
    expect(wrapper.props().handleSmallSharePanelVisible).to.exist;
    expect(wrapper.find("FormGroup").length).to.equal(1);
    expect(wrapper.find("Tooltip").length).to.equal(0);
    expect(wrapper.find("TextField").length).to.equal(1);
    expect(wrapper.find("TextField").props().placeholder).to.equal(" Speak up!");
    expect(wrapper.find("TextField").props().disabled).to.not.exist;
  });

  it("renders share comment panel small with disabled current persona", () => {
    const currentPersona = new Persona("account", personasTestJson.persona1.name, personasTestJson.persona1.avatar, 0);
    const wrapper = setup(currentPersona);
    expect(wrapper.props().currentPersona).to.equal(currentPersona);
    expect(wrapper.props().handleSmallSharePanelVisible).to.exist;
    expect(wrapper.find("FormGroup").length).to.equal(1);
    expect(wrapper.find("Tooltip").length).to.equal(1);
    expect(wrapper.find("Tooltip").props().enterDelay).to.equal(100);
    expect(wrapper.find("Tooltip").props().leaveDelay).to.equal(100);
    expect(wrapper.find("Tooltip").props().placement).to.equal("bottom");
    expect(wrapper.find("Tooltip").props().title).to.equal("This option is disabled until selected persona is mined and stored on blockchain");
    expect(wrapper.find("TextField").length).to.equal(1);
    expect(wrapper.find("TextField").props().placeholder).to.equal(" Please select confirmed persona!");
    expect(wrapper.find("TextField").props().disabled).to.be.true;
  });

  it("triggers handleSmallSharePanelFocus on focus", () => {
    const currentPersona = new Persona("account", personasTestJson.persona1.name, personasTestJson.persona1.avatar, 0, true);
    const handleSmallSharePanelVisible = sinon.spy();
    const wrapper = setup(currentPersona, handleSmallSharePanelVisible);
    wrapper.find("TextField").props().onFocus();
    expect(handleSmallSharePanelVisible.calledOnce).to.be.true;
  });
});
