import * as types from './actionTypes';

export function setAllCommentsLoading(allCommentsLoading) {
  return { type: types.SET_ALL_COMMENTS_LOADING, allCommentsLoading: allCommentsLoading };
}
