import React from 'react';
import PropTypes from 'prop-types';
import { Button, FormGroup, FormControl, TextField } from '@material-ui/core/';
import style from '../css/TfaShareCommentPanelLargeRight.scss';
import commentSchema from '../../../common/schema/CommentSchema.json';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faShare } from '@fortawesome/free-solid-svg-icons';
import InputValidator from '../../../common/InputValidator';

export default class TfaShareCommentPanelLargeRight extends React.Component {

  constructor(props) {
    super(props);
    this.addComment = this.addComment.bind(this);
    this.handleCommentChange = this.handleCommentChange.bind(this);
    this.inputValidator = new InputValidator(window);
    this.escFunction = this.escFunction.bind(this);
    this.state = {
      comment: '',
      isCommentValid: false
    };
  }

  componentDidMount() {
    document.addEventListener("keydown", this.escFunction, false);
  }

  componentWillUnmount() {
    const input = document.activeElement;
    if (input) {
      input.blur();
    }
    document.removeEventListener("keydown", this.escFunction, false);
  }

  escFunction(e) {
    if (e.keyCode === 27) {
      this.setState({
        comment: '',
        isCommentValid: false
      });
      this.props.handleSmallSharePanelVisible(true);
    }
  }

  handleCommentChange(e) {
    // disable hijacking of text input field from native page by restoring focus on blur event
    const textField = document.activeElement;
    document.activeElement.onblur = (e) => {
      e.preventDefault();
      textField.focus();
    };
    const sanitizedContent = this.inputValidator.sanitizeComment(e.target.value);
    this.setState({
      comment: e.target.value,
      isCommentValid: this.inputValidator.isCommentContentValid(sanitizedContent)
    });
  }

  addComment() {
    const sanitizedContent = this.inputValidator.sanitizeComment(this.state.comment);
    this.props.addComment(sanitizedContent);
    this.setState({
      comment: '',
      isCommentValid: false
    });
  }

  render() {
    // if in testing environment, don't set autoFocus prop because it's causing
    // Error: Uncaught [TypeError: activeElement.detachEvent is not a function]
    const notTestingEnv = process.env.NODE_ENV != JSON.stringify('test');
    const textField = notTestingEnv ?
      (
        <TextField
          className={style.tfa_share_comment_large_input}
          id="tfaShareCommentPanelInputLarge"
          autoFocus
          multiline
          fullWidth
          placeholder="Speak up!"
          rows="1"
          rowsMax="4"
          margin="none"
          onChange={this.handleCommentChange}
          inputProps={{ maxLength: commentSchema.properties.content.maxLength }}
          InputProps={{
            classes: {
              input: style.tfa_share_comment_large_input,
            }
          }}
        />
      ) : (
        <TextField
          className={style.tfa_share_comment_large_input}
          id="tfaShareCommentPanelInputLarge"
          multiline
          fullWidth
          placeholder="Speak up!"
          rows="1"
          rowsMax="4"
          margin="none"
          onChange={this.handleCommentChange}
          inputProps={{ maxLength: commentSchema.properties.content.maxLength }}
          InputProps={{
            classes: {
              input: style.tfa_share_comment_large_input,
            }
          }}
        />
      );
    const shareButton = this.state.isCommentValid ?
      (
        <Button className={style.share_button} variant="raised" color="primary" onClick={() => this.addComment()}>
          Share&nbsp;&nbsp;
        <FontAwesomeIcon icon={faShare} />
        </Button>
      ) :
      (
        <Button className={style.share_button_disabled} variant="raised" color="primary" disabled>
          Share&nbsp;&nbsp;
        <FontAwesomeIcon icon={faShare} />
        </Button>
      );

    return (
      <div className={style.tfa_share_comment_panel_large_right}>
        <FormGroup>
          <FormControl
            className={style.form_control_tfa_large_input}
          >
            {textField}
          </FormControl>
        </FormGroup>
        <div className={style.tfa_share_comment_panel_large_right_lower}>
          {shareButton}
        </div>
      </div>
    );
  }
}

TfaShareCommentPanelLargeRight.propTypes = {
  addComment: PropTypes.func.isRequired,
  handleSmallSharePanelVisible: PropTypes.func.isRequired
};

