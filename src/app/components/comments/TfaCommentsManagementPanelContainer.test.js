import { expect } from 'chai';
import React from 'react';
import { TfaCommentsManagementPanelContainer } from './TfaCommentsManagementPanelContainer';
import chrome from 'sinon-chrome/extensions';
import { createMount } from '@material-ui/core/test-utils';
import { JSDOM } from 'jsdom';
import Enzyme from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import Comment from '../../../common/Comment';
import Persona from '../../../common/Persona';

Enzyme.configure({ adapter: new Adapter() });

describe("<TfaCommentsManagementPanelContainer /> tests", () => {
  let mount;

  function setup(
    comments,
    unconfirmedCommentsCount,
    currentPersona,
    commentsLoading,
    showSmallShareCommentPanel,
    commentsActions = {},
    errorActions = {},
    shareCommentsPanelActions = {}
  ) {
    const props = {
      comments: comments,
      unconfirmedCommentsCount: unconfirmedCommentsCount,
      currentPersona: currentPersona,
      commentsLoading: commentsLoading,
      showSmallShareCommentPanel: showSmallShareCommentPanel,
      commentsActions: commentsActions,
      errorActions: errorActions,
      shareCommentsPanelActions: shareCommentsPanelActions
    };
    return mount(<TfaCommentsManagementPanelContainer {...props} />);
  }

  before(() => {
    global.chrome = chrome;
    const jsdom = new JSDOM('<!doctype html><html><body></body></html>');
    const { window } = jsdom;
    global.window = window;
    global.document = window.document;
    mount = createMount();
  });

  after(() => {
    mount.cleanUp();
  });

  it("renders comments management container without comments and current persona", () => {
    const wrapper = setup([], 0, {}, false, true);
    expect(wrapper.props().comments).to.be.empty;
    expect(wrapper.props().unconfirmedCommentsCount).to.equal(0);
    expect(wrapper.props().currentPersona).to.be.empty;
    expect(wrapper.props().commentsLoading).to.be.false;
    expect(wrapper.props().showSmallShareCommentPanel).to.be.true;
    expect(wrapper.props().commentsActions).to.exist;
    expect(wrapper.props().errorActions).to.exist;
    expect(wrapper.props().shareCommentsPanelActions).to.exist;
    expect(wrapper.find("TfaCommentsManagementPanel").length).to.equal(1);
  });

  it("renders comments management container comments and current persona", () => {
    const persona = new Persona("account", "name", "avatar", 0);
    const comment = new Comment("txHash", "content", "url", persona, 0, 0, 0);
    const wrapper = setup([comment], 0, persona, true, false);
    expect(wrapper.props().comments[0]).to.equal(comment);
    expect(wrapper.props().unconfirmedCommentsCount).to.equal(0);
    expect(wrapper.props().currentPersona).to.equal(persona);
    expect(wrapper.props().commentsLoading).to.be.true;
    expect(wrapper.props().showSmallShareCommentPanel).to.be.false;
    expect(wrapper.props().commentsActions).to.exist;
    expect(wrapper.props().errorActions).to.exist;
    expect(wrapper.props().shareCommentsPanelActions).to.exist;
    expect(wrapper.find("TfaCommentsManagementPanel").length).to.equal(1);
  });
});
