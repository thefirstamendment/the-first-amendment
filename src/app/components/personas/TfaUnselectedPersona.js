import React from 'react';
import PropTypes from 'prop-types';
import { Card, CardContent, CardActions, Avatar, Tooltip, ListItem } from '@material-ui/core/';
import style from '../css/TfaUnselectedPersona.scss';
import Utils from '../../../common/Utils';

const TfaUnselectedPersona = (props) => {

  const personaAvatarImg = Utils.getAvatarImg(props.persona);
  let unselectedPersonaPanelStyle;
  let unselectedPersonaTooltipTitle = "";
  if (props.persona.isEnabled) {
    unselectedPersonaPanelStyle = style.unselected_persona_panel;
    unselectedPersonaTooltipTitle = "Select as active persona";
  } else {
    unselectedPersonaPanelStyle = style.unselected_persona_panel_disabled;
    unselectedPersonaTooltipTitle = "This persona is disabled until it is mined and stored on blockchain";
  }
  return (
    <Tooltip
      enterDelay={100}
      id="tfa_avatar_drag_and_drop_tooltip"
      leaveDelay={100}
      placement="bottom"
      title={unselectedPersonaTooltipTitle}
      classes={{
        popper: style.tfa_tooltip_popover,
        tooltip: style.tfa_tooltip
      }}
    >
      <ListItem className={style.tfa_unselected_persona_li} divider>
        <Card className={unselectedPersonaPanelStyle} onClick={() => props.selectPersona(props.persona.account)}>
          <CardActions className={style.unselected_persona_panel_card_action} disableActionSpacing>
            <CardContent className={style.unselected_persona_panel_content}>
              <div className={style.unselected_persona_panel_left}>
                <Avatar
                  src={personaAvatarImg}
                  className={style.tfa_avatar_img_size48}
                />
              </div>
              <div className={style.unselected_persona_panel_right}>
                <p>
                  <strong>{props.persona.name}</strong>
                </p>
                <p>
                  {props.persona.account}
                </p>
              </div>
            </CardContent>
          </CardActions>
        </Card>
      </ListItem>
    </Tooltip>
  );
};

TfaUnselectedPersona.propTypes = {
  persona: PropTypes.object.isRequired,
  selectPersona: PropTypes.func.isRequired
};

export default TfaUnselectedPersona;
