export default class Utils {

  static isEmptyObject(obj) {
    return (obj == null ||
      (Object.keys(obj).length === 0 && obj.constructor === Object));
  }

  static getDefaultAvatarImg() {
    return chrome.extension.getURL("icons/anon.png");  //eslint-disable-line no-undef
  }

  static getAvatarImg(persona) {
    return (this.isEmptyObject(persona) || (persona.avatar.length == 0)) ? this.getDefaultAvatarImg() : persona.avatar;
  }

  static getCurrentUrl() {
    return window.location.toString().split("#")[0]; // remove anchor from url
  }

  static getPersonaName(persona) {
    return this.isEmptyObject(persona) ? "Anonymous" : persona.name;
  }
}
