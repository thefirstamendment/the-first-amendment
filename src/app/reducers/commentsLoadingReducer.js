import * as types from '../actions/actionTypes';
import initialState from './initialState';

export default function commentsLoadingReducer(state = initialState.commentsLoading, action) {
  switch (action.type) {
    case types.SET_COMMENTS_LOADING:
      return action.commentsLoading;

    case types.GET_COMMENTS_SUCCESS:
      return false;

    default:
      return state;
  }
}
