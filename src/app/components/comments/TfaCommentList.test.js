import { expect } from 'chai';
import React from 'react';
import TfaCommentList from './TfaCommentList';
import chrome from 'sinon-chrome/extensions';
import { createMount } from '@material-ui/core/test-utils';
import Comment from '../../../common/Comment';
import Persona from '../../../common/Persona';
import { JSDOM } from 'jsdom';
import Enzyme from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import raf from 'raf';

Enzyme.configure({ adapter: new Adapter() });

function generateComments(commentsCount) {
  const tokenPersona = new Persona("account", "name", "", 0);
  const comments = [];
  for (let i = 0; i < commentsCount; i++) {
    comments.push(new Comment("txHash" + i, "content" + i, "url" + i, tokenPersona, 0, 0, 0, true));
  }
  return comments;
}

describe("<TfaCommentList /> tests", () => {
  let mount;

  function setup(comments) {
    const props = {
      upvoteComment: () => { },
      downvoteComment: () => { },
      comments: comments
    };
    return mount(<TfaCommentList {...props} />);
  }


  before(() => {
    global.chrome = chrome;
    const jsdom = new JSDOM('<!doctype html><html><body></body></html>');
    const { window } = jsdom;
    global.window = window;
    global.document = window.document;
    // components that use maps need to have requestAnimationFrame polyfill
    raf.polyfill(global);
    mount = createMount();
  });

  after(() => {
    mount.cleanUp();
  });

  it("renders empty comments list", () => {
    const wrapper = setup([]);
    expect(wrapper.props().upvoteComment).to.exist;
    expect(wrapper.props().downvoteComment).to.exist;
    expect(wrapper.props().comments).to.be.empty;
    expect(wrapper.find("List").length).to.equal(1);
    expect(wrapper.find("List").props().disablePadding).to.exist;
    expect(wrapper.find("TfaComment").length).to.equal(0);
  });

  it("renders comment list without unconfirmed comments", () => {
    const comments = generateComments(2);
    const wrapper = setup(comments);
    expect(wrapper.props().comments.length).to.equal(2);
    expect(wrapper.props().upvoteComment).to.exist;
    expect(wrapper.props().downvoteComment).to.exist;
    expect(wrapper.find("List").length).to.equal(1);
    expect(wrapper.find("List").props().disablePadding).to.exist;
    expect(wrapper.find("TfaComment").length).to.equal(2);
    for (let i = 0; i < wrapper.find("TfaComment").length; i++) {
      expect(wrapper.find("TfaComment").at(i).key()).to.equal(comments[i].txHash);
      expect(wrapper.find("TfaComment").at(i).props().comment).to.equal(comments[i]);
      expect(wrapper.find("TfaComment").at(i).props().upvoteComment).to.exist;
      expect(wrapper.find("TfaComment").at(i).props().downvoteComment).to.exist;
    }
  });

  it("renders comment list with all unconfirmed comments", () => {
    const comments = generateComments(2);
    comments.forEach(comment => {
      comment.isEnabled = false;
    });
    const wrapper = setup(comments);
    expect(wrapper.props().comments.length).to.equal(2);
    expect(wrapper.props().upvoteComment).to.exist;
    expect(wrapper.props().downvoteComment).to.exist;
    expect(wrapper.find("List").length).to.equal(1);
    expect(wrapper.find("List").props().disablePadding).to.exist;
    expect(wrapper.find("TfaComment").length).to.equal(2);
    for (let i = 0; i < wrapper.find("TfaComment").length; i++) {
      expect(wrapper.find("TfaComment").at(i).key()).to.equal(comments[i].txHash);
      expect(wrapper.find("TfaComment").at(i).props().comment).to.equal(comments[i]);
      expect(wrapper.find("TfaComment").at(i).props().upvoteComment).to.exist;
      expect(wrapper.find("TfaComment").at(i).props().downvoteComment).to.exist;
    }
  });

  it("renders comment list with one unconfirmed comment", () => {
    const comments = generateComments(2);
    comments[1].isEnabled = false;
    const wrapper = setup(comments);
    expect(wrapper.props().comments.length).to.equal(2);
    expect(wrapper.props().upvoteComment).to.exist;
    expect(wrapper.props().downvoteComment).to.exist;
    expect(wrapper.find("List").length).to.equal(1);
    expect(wrapper.find("List").props().disablePadding).to.exist;
    expect(wrapper.find("TfaComment").length).to.equal(2);
    expect(wrapper.find("TfaComment").at(0).key()).to.equal(comments[0].txHash);
    expect(wrapper.find("TfaComment").at(0).props().comment).to.equal(comments[0]);
    expect(wrapper.find("TfaComment").at(0).props().upvoteComment).to.exist;
    expect(wrapper.find("TfaComment").at(0).props().downvoteComment).to.exist;
    expect(wrapper.find("TfaComment").at(1).key()).to.equal(comments[1].txHash);
    expect(wrapper.find("TfaComment").at(1).props().comment).to.equal(comments[1]);
    expect(wrapper.find("TfaComment").at(1).props().upvoteComment).to.exist;
    expect(wrapper.find("TfaComment").at(1).props().downvoteComment).to.exist;
  });
});
