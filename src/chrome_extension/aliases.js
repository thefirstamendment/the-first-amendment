import {
  ADD_PERSONA,
  UPDATE_PERSONA,
  GET_COMMENTS,
  ADD_COMMENT,
  UPVOTE_COMMENT,
  DOWNVOTE_COMMENT
} from '../app/actions/actionTypes';
import persistenceApi from '../api/persistenceApi';
import * as personaActions from '../app/actions/personaActions';
import * as commentsActions from '../app/actions/commentsActions';
import * as allCommentsActions from '../app/actions/allCommentsActions';
import * as commentsLoadingActions from '../app/actions/commentsLoadingActions';
import * as allCommentsLoadingActions from '../app/actions/allCommentsLoadingActions';
import * as shareCommentsPanelActions from '../app/actions/shareCommentsPanelActions';
import { showError } from '../app/actions/errorActions';
import { showNotification } from '../app/actions/notificationActions';

const addPersonaAlias = (originalAction) => {
  return (dispatch) => {
    dispatch(personaActions.addPersonaSuccess(originalAction.persona));
    return persistenceApi.addPersona(originalAction.persona, { from: originalAction.persona.account }).then(tx => {
      let newPersona = Object.assign({}, originalAction.persona);
      newPersona.isEnabled = true;
      dispatch(personaActions.updatePersonaSuccess(newPersona));
      dispatch(showNotification("Persona [ " + originalAction.persona.name + " ] is now ready for use"));
    }).catch(error => {
      dispatch(showError(error));
    });
  };
};

const updatePersonaAlias = (originalAction) => {
  return (dispatch) => {
    originalAction.persona.isEnabled = false;
    dispatch(personaActions.updatePersonaSuccess(originalAction.persona));
    return persistenceApi.updatePersona(originalAction.persona, { from: originalAction.persona.account }).then(tx => {
      let newPersona = Object.assign({}, originalAction.persona);
      newPersona.isEnabled = true;
      dispatch(personaActions.updatePersonaSuccess(newPersona));
    }).catch(error => {
      dispatch(showError(error));
    });
  };
};

const getCommentsAlias = (originalAction) => {
  return (dispatch) => {
    dispatch(commentsLoadingActions.setCommentsLoading(true));
    return persistenceApi.getComments(originalAction.url).then(comments => {
      dispatch(commentsActions.getCommentsSuccess(comments));
    }).catch(error => {
      dispatch(commentsLoadingActions.setCommentsLoading(false));
      dispatch(showError(error));
    });
  };
};

const getAllCommentsAlias = (originalAction) => {
  return (dispatch) => {
    dispatch(allCommentsLoadingActions.setAllCommentsLoading(true));
    return persistenceApi.getComments().then(allComments => {
      dispatch(allCommentsActions.getAllCommentsSuccess(allComments));
    }).catch(error => {
      dispatch(allCommentsLoadingActions.setAllCommentsLoading(false));
      dispatch(showError(error));
    });
  };
};

const addCommentAlias = (originalAction) => {
  return (dispatch) => {
    dispatch(shareCommentsPanelActions.showSmallShareCommentPanel(true));
    dispatch(commentsActions.addCommentSuccess(originalAction.comment));
    return persistenceApi.addComment(originalAction.comment, { from: originalAction.comment.persona.account }).then(tx => {
      let txHash = originalAction.comment.txHash;
      let newComment = Object.assign({}, originalAction.comment);
      newComment.txHash = tx;
      newComment.timestamp = persistenceApi.getCommentTimestamp(tx);
      newComment.isEnabled = true;
      dispatch(commentsActions.updateCommentSuccess(txHash, newComment));
    }).catch(error => {
      dispatch(showError(error));
    });
  };
};

const upvoteCommentAlias = (originalAction) => {
  return (dispatch) => {
    originalAction.comment.upvoteCount = originalAction.comment.upvoteCount + 1;
    originalAction.comment.isEnabled = false;
    dispatch(commentsActions.updateCommentSuccess(originalAction.comment.txHash, originalAction.comment));
    return persistenceApi.upvoteComment(originalAction.comment.txHash, { from: originalAction.address }).then(upvoteCount => {
      let newComment = Object.assign({}, originalAction.comment);
      newComment.upvoteCount = upvoteCount;
      newComment.isEnabled = true;
      dispatch(commentsActions.updateCommentSuccess(originalAction.comment.txHash, newComment));
    }).catch(error => {
      dispatch(showError(error));
    });
  };
};

const downvoteCommentAlias = (originalAction) => {
  return (dispatch) => {
    originalAction.comment.downvoteCount = originalAction.comment.downvoteCount + 1;
    originalAction.comment.isEnabled = false;
    dispatch(commentsActions.updateCommentSuccess(originalAction.comment.txHash, originalAction.comment));
    return persistenceApi.downvoteComment(originalAction.comment.txHash, { from: originalAction.address }).then(downvoteCount => {
      let newComment = Object.assign({}, originalAction.comment);
      newComment.downvoteCount = downvoteCount;
      newComment.isEnabled = true;
      dispatch(commentsActions.updateCommentSuccess(originalAction.comment.txHash, newComment));
    }).catch(error => {
      dispatch(showError(error));
    });
  };
};

export default {
  ADD_PERSONA: addPersonaAlias,
  UPDATE_PERSONA: updatePersonaAlias,
  ADD_COMMENT: addCommentAlias,
  GET_COMMENTS: getCommentsAlias,
  GET_ALL_COMMENTS: getAllCommentsAlias,
  UPVOTE_COMMENT: upvoteCommentAlias,
  DOWNVOTE_COMMENT: downvoteCommentAlias
};
