import { expect } from 'chai';
import React from 'react';
import TfaCurrentPersona from './TfaCurrentPersona';
import sinon from 'sinon';
import chrome from 'sinon-chrome/extensions';
import Utils from '../../../common/Utils';
import { createMount } from '@material-ui/core/test-utils';
import personasTestJson from '../../../../test/integration/test_personas.json';
import Persona from '../../../common/Persona';
import { JSDOM } from 'jsdom';
import Enzyme from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

Enzyme.configure({ adapter: new Adapter() });

describe("<TfaCurrentPersona /> tests", () => {
  let mount;
  let getDefaultAvatarImgStub;

  function setup(currentPersona) {
    const props = {
      openEditPersonaModal: () => { },
      currentPersona: currentPersona
    };
    return mount(<TfaCurrentPersona {...props} />);
  }

  before(() => {
    global.chrome = chrome;
    const jsdom = new JSDOM('<!doctype html><html><body></body></html>');
    const { window } = jsdom;
    global.window = window;
    global.document = window.document;
    getDefaultAvatarImgStub = sinon.stub(Utils, "getDefaultAvatarImg");
    getDefaultAvatarImgStub.returns("defaultAvatarImg");
    mount = createMount();
  });

  after(() => {
    mount.cleanUp();
    getDefaultAvatarImgStub.restore();
  });

  it("renders confirmed persona with empty avatar", () => {
    const currentPersona = new Persona("account", personasTestJson.persona1.name, "", 0, true);
    const expectedAvatarImg = Utils.getAvatarImg(currentPersona);
    const wrapper = setup(currentPersona);
    expect(wrapper.props().currentPersona).to.equal(currentPersona);
    expect(wrapper.props().openEditPersonaModal).to.exist;
    expect(wrapper.find("Card").length).to.equal(1);
    expect(wrapper.find("Avatar").length).to.equal(1);
    expect(wrapper.find("Avatar").props().src).to.not.be.undefined;
    expect(wrapper.find("Avatar").props().src).to.not.equal("");
    expect(wrapper.find("Avatar").props().src).to.equal(expectedAvatarImg);
    expect(wrapper.find("h4").length).to.equal(1);
    expect(wrapper.find("h4").text()).to.equal(currentPersona.name);
    expect(wrapper.find("p").length).to.equal(2);
    expect(wrapper.find("p").at(0).text()).to.equal(currentPersona.account);
    expect(wrapper.find("p").at(1).text()).to.equal("Balance: 0 ETH");
    expect(wrapper.find("span").length).to.equal(0);
    expect(wrapper.find("Tooltip").length).to.equal(1);
    expect(wrapper.find("Tooltip").props().enterDelay).to.equal(100);
    expect(wrapper.find("Tooltip").props().leaveDelay).to.equal(100);
    expect(wrapper.find("Tooltip").props().placement).to.equal("bottom");
    expect(wrapper.find("Tooltip").props().title).to.equal("Edit persona");
    expect(wrapper.find("a").length).to.equal(1);
  });

  it("renders confirmed persona with avatar", () => {
    const currentPersona = new Persona("account", personasTestJson.persona1.name, personasTestJson.persona1.avatar, 0, true);
    const wrapper = setup(currentPersona);
    expect(wrapper.props().currentPersona).to.equal(currentPersona);
    expect(wrapper.props().openEditPersonaModal).to.exist;
    expect(wrapper.find("Card").length).to.equal(1);
    expect(wrapper.find("Avatar").length).to.equal(1);
    expect(wrapper.find("Avatar").props().src).to.equal(currentPersona.avatar);
    expect(wrapper.find("h4").length).to.equal(1);
    expect(wrapper.find("h4").text()).to.equal(currentPersona.name);
    expect(wrapper.find("p").length).to.equal(2);
    expect(wrapper.find("p").at(0).text()).to.equal(currentPersona.account);
    expect(wrapper.find("p").at(1).text()).to.equal("Balance: 0 ETH");
    expect(wrapper.find("span").length).to.equal(0);
    expect(wrapper.find("Tooltip").length).to.equal(1);
    expect(wrapper.find("Tooltip").props().enterDelay).to.equal(100);
    expect(wrapper.find("Tooltip").props().leaveDelay).to.equal(100);
    expect(wrapper.find("Tooltip").props().placement).to.equal("bottom");
    expect(wrapper.find("Tooltip").props().title).to.equal("Edit persona");
    expect(wrapper.find("a").length).to.equal(1);
  });

  it("renders unconfirmed persona with empty avatar", () => {
    const currentPersona = new Persona("account", personasTestJson.persona1.name, "", 0);
    const expectedAvatarImg = Utils.getAvatarImg(currentPersona);
    const wrapper = setup(currentPersona);
    expect(wrapper.props().currentPersona).to.equal(currentPersona);
    expect(wrapper.props().openEditPersonaModal).to.exist;
    expect(wrapper.find("Card").length).to.equal(1);
    expect(wrapper.find("Avatar").length).to.equal(1);
    expect(wrapper.find("Avatar").props().src).to.not.be.undefined;
    expect(wrapper.find("Avatar").props().src).to.not.equal("");
    expect(wrapper.find("Avatar").props().src).to.equal(expectedAvatarImg);
    expect(wrapper.find("h4").length).to.equal(1);
    expect(wrapper.find("h4").text()).to.equal(currentPersona.name + "Pending...");
    expect(wrapper.find("p").length).to.equal(2);
    expect(wrapper.find("p").at(0).text()).to.equal(currentPersona.account);
    expect(wrapper.find("p").at(1).text()).to.equal("Balance: 0 ETH");
    expect(wrapper.find("span").length).to.equal(1);
    expect(wrapper.find("span").text()).to.equal("Pending...");
    expect(wrapper.find("Tooltip").length).to.equal(0);
    expect(wrapper.find("a").length).to.equal(0);
  });

  it("renders unconfirmed persona with avatar", () => {
    const currentPersona = new Persona("account", personasTestJson.persona1.name, personasTestJson.persona1.avatar, 0);
    const wrapper = setup(currentPersona);
    expect(wrapper.props().currentPersona).to.equal(currentPersona);
    expect(wrapper.props().openEditPersonaModal).to.exist;
    expect(wrapper.find("Card").length).to.equal(1);
    expect(wrapper.find("Avatar").length).to.equal(1);
    expect(wrapper.find("Avatar").props().src).to.equal(currentPersona.avatar);
    expect(wrapper.find("h4").length).to.equal(1);
    expect(wrapper.find("h4").text()).to.equal(currentPersona.name + "Pending...");
    expect(wrapper.find("p").length).to.equal(2);
    expect(wrapper.find("p").at(0).text()).to.equal(currentPersona.account);
    expect(wrapper.find("p").at(1).text()).to.equal("Balance: 0 ETH");
    expect(wrapper.find("span").length).to.equal(1);
    expect(wrapper.find("span").text()).to.equal("Pending...");
    expect(wrapper.find("Tooltip").length).to.equal(0);
    expect(wrapper.find("a").length).to.equal(0);
  });

  it("simulates click on edit current persona button", () => {
    const openEditPersonaModal = sinon.spy();
    const currentPersona = new Persona("account", personasTestJson.persona1.name, personasTestJson.persona1.avatar, 0, true);
    const wrapper = mount(
      <TfaCurrentPersona
        currentPersona={currentPersona}
        openEditPersonaModal={openEditPersonaModal}
      />
    );
    wrapper.find("a").simulate("click");
    expect(openEditPersonaModal.calledOnce).to.be.true;
  });
});
