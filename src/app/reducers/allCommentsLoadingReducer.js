import * as types from '../actions/actionTypes';
import initialState from './initialState';

export default function allCommentsLoadingReducer(state = initialState.allCommentsLoading, action) {
  switch (action.type) {
    case types.SET_ALL_COMMENTS_LOADING:
      return action.allCommentsLoading;

    case types.GET_ALL_COMMENTS_SUCCESS:
      return false;

    default:
      return state;
  }
}
