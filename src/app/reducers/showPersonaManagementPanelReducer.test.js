import {expect} from 'chai';
import showPersonaManagementPanelReducer from './showPersonaManagementPanelReducer';
import * as actions from '../actions/personaManagementPanelActions';

describe("showPersonaManagementPanelReducer test", () => {
  it("should toggle show persona management panel when passed TOGGLE_PERSONA_MANAGEMENT_PANEL", () => {
    const initState = false;
    const action = actions.togglePersonaManagementPanel();
    let newState = showPersonaManagementPanelReducer(initState, action);

    expect(newState).to.be.true;

    newState = showPersonaManagementPanelReducer(newState, action);

    expect(newState).to.be.false;
  });
});
