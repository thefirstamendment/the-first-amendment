import { expect } from 'chai';
import React from 'react';
import TfaAddPersonaModal from './TfaAddPersonaModal';
import sinon from 'sinon';
import chrome from 'sinon-chrome/extensions';
import Utils from '../../../common/Utils';
import { createMount } from '@material-ui/core/test-utils';
import { JSDOM } from 'jsdom';
import Enzyme from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import personaSchema from '../../../common/schema/PersonaSchema.json';

Enzyme.configure({ adapter: new Adapter() });

function generateAccounts(accountCount) {
  const accounts = [];
  for (let i = 0; i < accountCount; i++) {
    accounts.push("account" + i);
  }
  return accounts;
}

describe("<TfaAddPersonaModal /> tests", () => {
  let mount;
  let getDefaultAvatarImgStub;

  function setup(
    addPersonaModalIsOpen,
    accounts,
    closeAddPersonaModal = () => { },
    addPersona = () => { },
    showError = () => { }
  ) {
    const props = {
      addPersonaModalIsOpen: addPersonaModalIsOpen,
      accounts: accounts,
      closeAddPersonaModal: closeAddPersonaModal,
      addPersona: addPersona,
      showError: showError
    };
    return mount(<TfaAddPersonaModal {...props} />);
  }

  before(() => {
    global.chrome = chrome;
    const jsdom = new JSDOM('<!doctype html><html><body></body></html>');
    const { window } = jsdom;
    global.window = window;
    global.document = window.document;
    getDefaultAvatarImgStub = sinon.stub(Utils, "getDefaultAvatarImg");
    getDefaultAvatarImgStub.returns("defaultAvatarImg");
    mount = createMount();
  });

  after(() => {
    mount.cleanUp();
    getDefaultAvatarImgStub.restore();
  });

  it("renders closed add persona modal with no accounts", () => {
    const wrapper = setup(false, []);
    expect(wrapper.props().addPersonaModalIsOpen).to.be.false;
    expect(wrapper.props().accounts).to.be.empty;
    expect(wrapper.props().closeAddPersonaModal).to.exist;
    expect(wrapper.props().addPersona).to.exist;
    expect(wrapper.find("Dialog").length).to.equal(1);
    expect(wrapper.find("Dialog").props().open).to.be.false;
    expect(wrapper.find("Dialog").props().onClose).to.exist;
  });

  it("renders open add persona modal with no accounts", () => {
    const wrapper = setup(true, []);
    expect(wrapper.props().addPersonaModalIsOpen).to.be.true;
    expect(wrapper.props().accounts).to.be.empty;
    expect(wrapper.props().closeAddPersonaModal).to.exist;
    expect(wrapper.props().addPersona).to.exist;
    expect(wrapper.find("Dialog").length).to.equal(1);
    expect(wrapper.find("Dialog").props().open).to.be.true;
    expect(wrapper.find("Dialog").props().onClose).to.exist;
    expect(wrapper.find("DialogTitle").length).to.equal(1);
    expect(wrapper.find("DialogTitle").text()).to.equal("Add New Persona");
    expect(wrapper.find("DialogContent").length).to.equal(1);
    expect(wrapper.find("TfaDisplayMessagePanel").length).to.equal(1);
    expect(wrapper.find("TfaDisplayMessagePanel").props().message).to.equal("No available accounts for persona creation...");
    expect(wrapper.find("DialogActions").length).to.equal(1);
    expect(wrapper.find("Button").length).to.equal(2);
    expect(wrapper.find("Button").at(0).props().disabled).to.exist;
    expect(wrapper.find("Button").at(1).props().color).to.equal("default");
    expect(wrapper.find("Button").at(1).props().onClick).to.exist;
  });

  it("renders open add persona modal with 3 accounts", () => {
    const accounts = generateAccounts(3);
    const expectedAvatarImg = Utils.getAvatarImg({});
    const wrapper = setup(true, accounts);
    expect(wrapper.props().addPersonaModalIsOpen).to.be.true;
    expect(wrapper.props().accounts.length).to.equal(3);
    expect(wrapper.props().accounts).to.equal(accounts);
    expect(wrapper.props().closeAddPersonaModal).to.exist;
    expect(wrapper.props().addPersona).to.exist;
    expect(wrapper.find("Dialog").length).to.equal(1);
    expect(wrapper.find("Dialog").props().disableRestoreFocus).to.exist;
    expect(wrapper.find("Dialog").props().open).to.be.true;
    expect(wrapper.find("Dialog").props().onClose).to.exist;
    expect(wrapper.find("DialogTitle").length).to.equal(1);
    expect(wrapper.find("DialogTitle").text()).to.equal("Add New Persona");
    expect(wrapper.find("TfaDisplayMessagePanel").length).to.equal(0);
    expect(wrapper.find("DialogContent").length).to.equal(1);
    expect(wrapper.find("DialogActions").length).to.equal(1);
    expect(wrapper.find("Button").length).to.equal(2);
    expect(wrapper.find("Button").at(0).props().disabled).to.exist;
    expect(wrapper.find("Button").at(1).props().color).to.equal("default");
    expect(wrapper.find("Button").at(1).props().onClick).to.exist;
    expect(wrapper.find("FormControl").length).to.equal(4);
    expect(wrapper.find("TfaAvatarDragAndDrop").length).to.equal(1);
    expect(wrapper.find("TfaAvatarDragAndDrop").props().avatar).to.equal(expectedAvatarImg);
    expect(wrapper.find("TfaAvatarDragAndDrop").props().setAvatar).to.exist;
    expect(wrapper.find("TfaAvatarDragAndDrop").props().showError).to.exist;
    expect(wrapper.find("TextField").length).to.equal(1);
    expect(wrapper.find("TextField").props().fullWidth).to.exist;
    expect(wrapper.find("TextField").props().label).to.equal("Username");
    expect(wrapper.find("TextField").props().margin).to.equal("normal");
    expect(wrapper.find("TextField").props().value).to.equal("");
    expect(wrapper.find("TextField").props().onChange).to.exist;
    expect(wrapper.find("TextField").props().inputProps.maxLength).to.equal(personaSchema.properties.name.maxLength);
    expect(wrapper.find("InputLabel").length).to.equal(2);
    expect(wrapper.find("InputLabel").at(1).text()).to.equal("Account");
    expect(wrapper.find("Select").length).to.equal(1);
    expect(wrapper.find("Select").props().native).to.exist;
    expect(wrapper.find("Select").props().value).to.equal("");
    expect(wrapper.find("Select").props().onChange).to.exist;
    expect(wrapper.find("option").length).to.equal(4);
    expect(wrapper.find("option").at(0).props().value).to.equal("");
    expect(wrapper.find("InputAdornment").length).to.equal(1);
    expect(wrapper.find("InputAdornment").props().position).to.equal("end");
    expect(wrapper.find("Tooltip").length).to.equal(2);
    for(let i = 1; i < 4; i++) {
      expect(wrapper.find("option").at(i).props().value).to.equal(accounts[i-1]);
    }
  });

  it("simulates click on close button", () => {
    const accounts = generateAccounts(3);
    const closeAddPersonaModal = sinon.spy();
    const wrapper = setup(true, accounts, closeAddPersonaModal);
    wrapper.find("Button").at(1).simulate("click");
    expect(closeAddPersonaModal.calledOnce).to.be.true;
  });

  it("simulates click on add persona button disabled", () => {
    const accounts = generateAccounts(3);
    const closeAddPersonaModal = sinon.spy();
    const addPersona = sinon.spy();
    const wrapper = setup(true, accounts, closeAddPersonaModal, addPersona);
    wrapper.find("Button").at(0).simulate("click");
    expect(addPersona.calledOnce).to.be.false;
  });

  it("simulates click on add persona button enabled", () => {
    const accounts = generateAccounts(3);
    const closeAddPersonaModal = sinon.spy();
    const addPersona = sinon.spy();
    const wrapper = setup(true, accounts, closeAddPersonaModal, addPersona);
    wrapper.find("TextField").props().onChange({target: {value: "username"}});
    wrapper.find("Select").props().onChange({target: {value: accounts[0]}});
    wrapper.find("Button").at(0).simulate("click");
    expect(addPersona.calledOnce).to.be.true;
  });
});
