import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import TfaHeaderPanel from './TfaHeaderPanel';
import * as personaManagementPanelActions from '../../actions/personaManagementPanelActions';

// For container classes we're using both default export of connected component for production (last line)
// and named export of disconnected component for unit tests (class declaration)
export class TfaHeaderPanelContainer extends React.Component {

  constructor(props) {
    super(props);
    this.togglePersonaManagementPanel = this.togglePersonaManagementPanel.bind(this);
  }

  togglePersonaManagementPanel() {
    this.props.actions.togglePersonaManagementPanel();
  }

  render() {
    return (
      <TfaHeaderPanel
        togglePersonaManagementPanel={this.togglePersonaManagementPanel}
        showPersonaManagementPanel={this.props.showPersonaManagementPanel}
        currentPersona={this.props.currentPersona}
        openExploreCommentsModal={this.props.openExploreCommentsModal}
      />
    );
  }
}

TfaHeaderPanelContainer.propTypes = {
  actions: PropTypes.object.isRequired,
  showPersonaManagementPanel: PropTypes.bool.isRequired,
  currentPersona: PropTypes.object.isRequired,
  togglePersonaManagementPanel: PropTypes.func.isRequired,
  openExploreCommentsModal: PropTypes.func.isRequired
};

function mapStateToProps(state, ownProps) {
  return {
    showPersonaManagementPanel: state.showPersonaManagementPanel,
    currentPersona: state.currentPersona,
    openExploreCommentsModal: ownProps.openExploreCommentsModal
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(personaManagementPanelActions, dispatch)
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(TfaHeaderPanelContainer);
