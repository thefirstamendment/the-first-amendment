# The First Amendment

## Prerequisites

### Python

Python version 2.7 is needed to be installed first.

### IPFS

Ipfs needs to be installed, follow the instructions here: 
```
https://ipfs.io/docs/install/
```
Start Ipfs daemon instance:
```
ipfs daemon
```
and then in another terminal configure Ipfs with the following:
```
ipfs config --json API.HTTPHeaders.Access-Control-Allow-Origin "[\"*\"]"
ipfs config --json API.HTTPHeaders.Access-Control-Allow-Credentials "[\"true\"]"
ipfs config --json API.HTTPHeaders.Access-Control-Allow-Methods "[\"PUT\", \"POST\", \"GET\"]"
```

### Ethereum

There are three options to run ethereum software - run Geth test/main network, Geth private network or TestRPC/Ganache.

#### Running TestRPC

Before building and deploying Solidity contract, testrpc needs to be running:
```
npm install
npm run testrpc
```
Now we are ready to build and deploy contract.

#### Running Geth test/main net

Install Ethereum Wallet and run it. Geth should run in a background. Select test or main net from menu. If running testnet, RPC must be enabled. 
To enable RPC, navigate to Geth folder and from terminal type:
```
geth attach
admin.startRPC("127.0.0.1", 8545, "*", "web3,db,net,eth");
```
Also, make sure to unlock account:
```
personal.unlockAccount("0x6034d31ab80f7d55163a6e708142b0c8a8acf22f", "password", 30000);
```

#### Running Geth private network

Install Geth from command line:
```
sudo apt-get install software-properties-common
sudo add-apt-repository -y ppa:ethereum/ethereum
sudo apt-get update
sudo apt-get install ethereum
```
To run a private network genesis file is needed for Geth initialization. Start a terminal window and navigate to project's folder and then type:
```
geth init genesis.json
```
to create a Dag object that is needed. After it finishes, in the same window type:
```
geth console
personal.newAccount();
```
This will create a new account that will be used to send transactions to contract. Now you got some address like this: 0x6034d31ab80f7d55163a6e708142b0c8a8acf22f.
All set, next we start geth with rpc server listening on http://localhost:8545 and start mining:
```
geth --rpc --rpccorsdomain="*" --mine --minerthreads=1 --etherbase=0x0000000000000000000000000000000000000000
```
In another terminal window type:
```
geth attach
personal.unlockAccount("0x6034d31ab80f7d55163a6e708142b0c8a8acf22f", "", 30000);
```
to unlock account for use (make sure to replace example address with the address Geth displayed after calling personal.newAccount()).
Just to make sure everything works, you can check your balance by typing:
```
eth.getBalance("0x6034d31ab80f7d55163a6e708142b0c8a8acf22f");
```
in the same window. Note that balance should increase every few seconds since mining is on. Now we are ready to build and deploy contract.

## The First Amendment project

Download The FirstAmendment project from repository and navigate to project's folder. Then run:
```
npm install
```
to install all dependencies.

### Build and deploy contracts

Now we are ready to build and deploy Solidity contract. Make sure Geth or TestRPC is running and type:
```
npm run build-contracts
npm run deploy-contracts
```
If all ok, you can run tests.

### Running tests

Before running a test, make sure there are IPFS and Geth or TestRPC running. This can be achieved by calling:

```
npm run ipfs
```
and
```
npm run testrpc
```
or 
```
geth --rpc --rpccorsdomain="*" --mine --minerthreads=1 --etherbase=0x0000000000000000000000000000000000000000
```
Don't forget to unlock account if using Geth.
After that, run tests using: 
```
npm test
```
If all ok, you can try using it in browser.

### Run browser add-on

If you are running for the first time, install TamperMonkey browser add-on (for Chrome or Firefox), and create new user script and copy/paste content from tfa.user.js in new script.
This will enable you to use TFA comments in browser on every page. Before browsing the web, run the following: 
```
npm start
```

