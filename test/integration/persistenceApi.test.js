/*jshint esversion: 6 */
import { assert } from 'chai';
import Promise from 'bluebird';
import { JSDOM } from 'jsdom';
import Web3 from 'web3';
import fs from 'fs';
import config from '../../config.json';
import persistenceApi from '../../src/api/persistenceApi';
import startProviders from './providerUtil';
import commentsTestJson from './test_comments.json';
import personasTestJson from './test_personas.json';
import childProcess from 'child_process';
import Comment from '../../src/common/Comment';
import Persona from '../../src/common/Persona';
import IpfsError from '../../src/common/errors/IpfsError.js';
import Web3Error from '../../src/common/errors/Web3Error.js';
import InputValidationError from '../../src/common/errors/InputValidationError';
import personaSchema from '../../src/common/schema/PersonaSchema.json';

const web3 = new Web3();
let accounts;

function setConfiguration(selection) {
  return new Promise((accept, reject) => {
    // set selection
    config['selection'] = selection;

    // copy contract addresses from local to test_no_ipfs and test_no_web3 selection
    config["test_no_ipfs"].commentContractAddress = config["local"].commentContractAddress;
    config["test_no_ipfs"].uportRegistryContractAddress = config["local"].uportRegistryContractAddress;
    config["test_no_web3"].commentContractAddress = config["local"].commentContractAddress;
    config["test_no_web3"].uportRegistryContractAddress = config["local"].uportRegistryContractAddress;

    // now update config file
    let output2 = JSON.stringify(config, null, 2) + '\n';
    fs.writeFile('../../config.json', output2, (err) => {
      if (err) {
        reject(err);
        return;
      }
      accept();
    });
  });
}

function initConfigAndProviders() {
  return new Promise((accept, reject) => {
    setConfiguration("local").then(() => {
      startProviders((err, provs) => {
        if (err) {
          reject(err);
        }
        // get providers from providerUtil
        let web3Prov = provs.web3Provider;
        // get accounts, and setup new contracts
        web3.setProvider(web3Prov);
        web3.eth.getAccounts((err, accs) => {
          if (err) {
            reject(err);
          }
          accounts = accs;
          // run clean migrate of all contracts
          childProcess.exec("npm run deploy-contracts-local", (err, stdout, stderr) => {
            if (err) {
              console.log(stderr); //eslint-disable-line no-console
              reject(err);
            } else {
              // init persistence api
              persistenceApi.setup((new JSDOM('')).window);
              accept();
            }
          });
        });
      });
    });
  });
}

describe('Tfa API integration tests', function () {
  this.timeout(120000);
  before((beforeDone) => {
    persistenceApi.startIpfs().then(() => {
      beforeDone();
    });
  });

  beforeEach((done) => {
    initConfigAndProviders().then(() => {
      done();
    }).catch((err) => {
      throw err;
    });
  });

  it('Get first account', () => {
    assert.deepEqual(accounts[0], persistenceApi.getFirstAccount(), "First account does not match.");
  });

  it('Get first account when web3 is down', (getFirstAccountWoWeb3Done) => {
    setConfiguration("test_no_web3").then(() => {
      persistenceApi.setup((new JSDOM('')).window);
      try {
        persistenceApi.getFirstAccount();
        assert.isTrue(false, "should throw exception");
      } catch (err) {
        assert.isTrue(err instanceof Web3Error, "should get instance of Web3Error");
        getFirstAccountWoWeb3Done();
      }
    });
  });

  it('Get accounts', () => {
    assert.deepEqual(accounts, persistenceApi.getAccounts(), "Retrieved accounts do not match.");
  });

  it('Get accounts when web3 is down', (getAccountsNoWeb3Done) => {
    setConfiguration("test_no_web3").then(() => {
      persistenceApi.setup((new JSDOM('')).window);
      try {
        persistenceApi.getAccounts();
        assert.isTrue(false, "should throw exception");
      } catch (err) {
        assert.isTrue(err instanceof Web3Error, "should get instance of Web3Error");
        getAccountsNoWeb3Done();
      }
    });
  });

  it('Get account balance', () => {
    let expectedBalance = web3.fromWei(web3.eth.getBalance(accounts[0]), 'ether');
    assert.deepEqual(persistenceApi.getAccountBalance(accounts[0]), expectedBalance, "Account balances do not match.");
  });

  it('Get account balance when web3 is down', (getAccountBalanceNoWeb3Done) => {
    setConfiguration("test_no_web3").then(() => {
      persistenceApi.setup((new JSDOM('')).window);
      try {
        persistenceApi.getAccountBalance(accounts[0]);
        assert.isTrue(false, "should throw exception");
      } catch (err) {
        assert.isTrue(err instanceof Web3Error, "should get instance of Web3Error");
        getAccountBalanceNoWeb3Done();
      }
    });
  });

  it('Add and fetch comments - url with one comment', (addAndFetchComments1Done) => {
    let commentJson1 = commentsTestJson.comment1;
    let expectedComment = new Comment(null, commentJson1.content, commentJson1.url, null, 0, 0, 0, true);
    return persistenceApi.addComment(expectedComment, {
      from: accounts[0]
    }).then((txHash) => {
      assert.match(txHash, /0[xX][0-9a-fA-F]+/, "txHash should be a hexadecimal value.");
      expectedComment.txHash = txHash;
      return persistenceApi.getComments(commentJson1.url).then((actualComments) => {
        expectedComment.timestamp = actualComments[0].timestamp;
        assert.deepEqual(actualComments[0], expectedComment, "retrieved comments do not match added comments.");
        addAndFetchComments1Done();
      });
    });
  });

  it('Add and fetch comments - url with two comments', (addAndFetchComments2Done) => {
    let commentJson1 = commentsTestJson.comment2;
    let commentJson2 = commentsTestJson.comment3;
    let expectedComment1 = new Comment(null, commentJson1.content, commentJson1.url, null, 0, 0, 0, true);
    let expectedComment2 = new Comment(null, commentJson2.content, commentJson2.url, null, 0, 0, 0, true);
    let expectedComments = [expectedComment1, expectedComment2];

    let promises = [
      persistenceApi.addComment(expectedComment1, {
        from: accounts[0]
      }).then((txHash) => {
        assert.match(txHash, /0[xX][0-9a-fA-F]+/, "txHash should be a hexadecimal value.");
        expectedComment1.txHash = txHash;
      }),
      persistenceApi.addComment(expectedComment2, {
        from: accounts[0]
      }).then((txHash) => {
        assert.match(txHash, /0[xX][0-9a-fA-F]+/, "txHash should be a hexadecimal value.");
        expectedComment2.txHash = txHash;
      })
    ];
    return Promise.all(promises).then(() => {
      return persistenceApi.getComments(commentJson1.url).then((actualComments) => {
        // check if all of the added comments are included in retrieved comments
        for (let i = 0; i < expectedComments.length; i++) {
          let actualComment = actualComments.find(x => x.txHash === expectedComments[i].txHash);
          expectedComments[i].timestamp = actualComment.timestamp;
          assert.deepEqual(actualComment, expectedComments[i], "retrieved comments do not match added comments.");
        }
        addAndFetchComments2Done();
      });
    });
  });

  it('Add and fetch all comments - no url', (addAndFetchAllCommentsDone) => {
    let commentJson1 = commentsTestJson.comment1;
    let commentJson2 = commentsTestJson.comment2;
    let expectedComment1 = new Comment(null, commentJson1.content, commentJson1.url, null, 0, 0, 0, true);
    let expectedComment2 = new Comment(null, commentJson2.content, commentJson2.url, null, 0, 0, 0, true);
    let expectedComments = [expectedComment1, expectedComment2];

    let promises = [
      persistenceApi.addComment(expectedComment1, {
        from: accounts[0]
      }).then((txHash) => {
        assert.match(txHash, /0[xX][0-9a-fA-F]+/, "txHash should be a hexadecimal value.");
        expectedComment1.txHash = txHash;
      }),
      persistenceApi.addComment(expectedComment2, {
        from: accounts[0]
      }).then((txHash) => {
        assert.match(txHash, /0[xX][0-9a-fA-F]+/, "txHash should be a hexadecimal value.");
        expectedComment2.txHash = txHash;
      })
    ];
    return Promise.all(promises).then(() => {
      return persistenceApi.getComments().then((actualComments) => {
        // check if all of the added comments are included in retrieved comments
        for (let i = 0; i < expectedComments.length; i++) {
          let actualComment = actualComments.find(x => x.txHash === expectedComments[i].txHash);
          expectedComments[i].timestamp = actualComment.timestamp;
          assert.deepEqual(actualComment, expectedComments[i], "retrieved comments do not match added comments.");
        }
        addAndFetchAllCommentsDone();
      });
    });
  });

  it('Fetch comments - non-existent url', (fetchCommentsNoUrlDone) => {
    let expectedComments = [];
    return persistenceApi.getComments("http://www.some-other-url.com").then((actualComments) => {
      assert.deepEqual(actualComments, expectedComments, "retrieved comments do not match");
      fetchCommentsNoUrlDone();
    });
  });

  it('Add and fetch comments with persona - url with one comment', (addAndFetchCommentsWithPersonaDone) => {
    let commentJson1 = commentsTestJson.comment1;
    let personaJson1 = personasTestJson.persona1;
    let expectedPersona = new Persona(accounts[0], personaJson1.name, personaJson1.avatar, 100, true);
    let expectedComment = new Comment(null, commentJson1.content, commentJson1.url, expectedPersona, 0, 0, 0, true);

    return persistenceApi.addPersona(expectedPersona, {
      from: accounts[0]
    }).then(() => {
      return persistenceApi.addComment(expectedComment, {
        from: accounts[0]
      }).then((txHash2) => {
        assert.match(txHash2, /0[xX][0-9a-fA-F]+/, "txHash should be a hexadecimal value.");
        expectedComment.txHash = txHash2;
        return persistenceApi.getComments(commentJson1.url).then((actualComments) => {
          expectedComment.timestamp = actualComments[0].timestamp;
          expectedComment.persona.balance = actualComments[0].persona.balance;
          assert.deepEqual(actualComments[0], expectedComment, "retrieved comments do not match added comments.");
          addAndFetchCommentsWithPersonaDone();
        });
      });
    });
  });

  it('Add comment with web3 down', (addCommentWeb3DownDone) => {
    let commentJson1 = commentsTestJson.comment1;
    setConfiguration("test_no_web3").then(() => {
      persistenceApi.setup((new JSDOM('')).window);
      return persistenceApi.addComment(commentJson1, {
        from: accounts[0]
      }).then(() => {
        assert.isTrue(false, "should not return anything");
      }).catch((err) => {
        assert.isTrue(err instanceof Web3Error, "should get instance of Web3Error");
        addCommentWeb3DownDone();
      });
    });
  });

  it('Fetch comments with web3 down', (fetchCommentsWeb3DownDone) => {
    let commentJson1 = commentsTestJson.comment1;
    let expectedComment = new Comment(null, commentJson1.content, commentJson1.url, null, 0, 0, 0, true);
    return persistenceApi.addComment(commentJson1, {
      from: accounts[0]
    }).then((txHash) => {
      assert.match(txHash, /0[xX][0-9a-fA-F]+/, "txHash should be a hexadecimal value.");
      expectedComment.txHash = txHash;
      setConfiguration("test_no_web3").then(() => {
        persistenceApi.setup((new JSDOM('')).window);
        return persistenceApi.getComments(commentJson1.url).then(() => {
          assert.isTrue(false, "should not return anything");
        }).catch((err) => {
          assert.isTrue(err instanceof Web3Error, "should get instance of Web3Error");
          fetchCommentsWeb3DownDone();
        });
      });
    });
  });

  it('Fetch comments with invalid content length - over maximum size', (fetchCommentsMaxSizeDone) => {
    let commentJson = commentsTestJson.commentWithLongContent;
    let expectedComment = new Comment(null, commentJson.content, commentJson.url, null, 0, 0, 0, true);
    return persistenceApi.addComment(commentJson, {
      from: accounts[0]
    }).then((txHash) => {
      assert.match(txHash, /0[xX][0-9a-fA-F]+/, "txHash should be a hexadecimal value.");
      return persistenceApi.getComments(commentJson.url).then((comments) => {
        assert.isEmpty(comments, "getComments returned comments, should return empty array");
        fetchCommentsMaxSizeDone();
      });
    });
  });

  it('Fetch comments with invalid content length - under minimum size', (fetchCommentsContentMinSizeDone) => {
    let commentJson = commentsTestJson.comment1;
    let expectedComment = new Comment(null, "", commentJson.url, null, 0, 0, 0, true);
    return persistenceApi.addComment(expectedComment, {
      from: accounts[0]
    }).then((txHash) => {
      assert.match(txHash, /0[xX][0-9a-fA-F]+/, "txHash should be a hexadecimal value.");
      return persistenceApi.getComments(commentJson.url).then((comments) => {
        assert.isEmpty(comments, "getComments returned comments, should return empty array");
        fetchCommentsContentMinSizeDone();
      });
    });
  });

  it('Fetch comments with comment with malicious content', (fetchCommentsMaliciousContentDone) => {
    let commentJson = commentsTestJson.commentWithMaliciousCommentContent;
    return persistenceApi.addComment(commentJson, {
      from: accounts[0]
    }).then((txHash) => {
      assert.match(txHash, /0[xX][0-9a-fA-F]+/, "txHash should be a hexadecimal value.");
      return persistenceApi.getComments(commentJson.url).then((comments) => {
        assert.isEmpty(comments, "getComments returned comments, should return empty array");
        fetchCommentsMaliciousContentDone();
      });
    });
  });

  it('Fetch comments with invalid url length - over maximum size', (fetchCommentsUrlMaxSizeDone) => {
    let commentJson = commentsTestJson.commentWithLongUrl;
    return persistenceApi.addComment(commentJson, {
      from: accounts[0]
    }).then((txHash) => {
      assert.match(txHash, /0[xX][0-9a-fA-F]+/, "txHash should be a hexadecimal value.");
      return persistenceApi.getComments(commentJson.url).then((comments) => {
        assert.isEmpty(comments, "getComments returned comments, should return empty array");
        fetchCommentsUrlMaxSizeDone();
      });
    });
  });

  it('Fetch comments with invalid url length - under minimum size', (fetchCommentsUrlMinSizeDone) => {
    let commentJson = commentsTestJson.commentWithLongUrl;
    let expectedComment = new Comment(null, commentJson.content, "http:/", null, 0, 0, 0, true);
    return persistenceApi.addComment(expectedComment, {
      from: accounts[0]
    }).then((txHash) => {
      assert.match(txHash, /0[xX][0-9a-fA-F]+/, "txHash should be a hexadecimal value.");
      return persistenceApi.getComments(commentJson.url).then((comments) => {
        assert.isEmpty(comments, "getComments returned comments, should return empty array");
        fetchCommentsUrlMinSizeDone();
      });
    });
  });

  it('Upvote comment - fetch comments with persona and upvote - url with one comment', (upvoteCommentDone) => {
    let commentJson1 = commentsTestJson.comment1;
    let personaJson1 = personasTestJson.persona1;
    let expectedPersona = new Persona(accounts[0], personaJson1.name, personaJson1.avatar, 100, true);
    // creation of comment with one upvote
    let expectedComment = new Comment(null, commentJson1.content, commentJson1.url, expectedPersona, 1, 0, 0, true);

    return persistenceApi.addPersona(expectedPersona, {
      from: accounts[0]
    }).then(() => {
      return persistenceApi.addComment(expectedComment, {
        from: accounts[0]
      }).then((txHash2) => {
        assert.match(txHash2, /0[xX][0-9a-fA-F]+/, "txHash should be a hexadecimal value.");
        expectedComment.txHash = txHash2;
        return persistenceApi.upvoteComment(expectedComment.txHash, {
          from: accounts[0]
        }).then(() => {
          return persistenceApi.getComments(commentJson1.url).then((actualComments) => {
            expectedComment.timestamp = actualComments[0].timestamp;
            expectedComment.persona.balance = actualComments[0].persona.balance;
            assert.deepEqual(actualComments[0], expectedComment, "retrieved comments do not match added comments.");
            upvoteCommentDone();
          });
        });
      });
    });
  });

  it('Upvote comment when web3 is down', (upvoteCommentWeb3Down) => {
    let commentJson1 = commentsTestJson.comment1;
    let personaJson1 = personasTestJson.persona1;
    let expectedPersona = new Persona(accounts[0], personaJson1.name, personaJson1.avatar, 100, true);
    let expectedComment = new Comment(null, commentJson1.content, commentJson1.url, expectedPersona, 1, 0, 0, true);

    return persistenceApi.addPersona(expectedPersona, {
      from: accounts[0]
    }).then(() => {
      return persistenceApi.addComment(expectedComment, {
        from: accounts[0]
      }).then((txHash2) => {
        assert.match(txHash2, /0[xX][0-9a-fA-F]+/, "txHash should be a hexadecimal value.");
        expectedComment.txHash = txHash2;
        setConfiguration("test_no_web3").then(() => {
          persistenceApi.setup((new JSDOM('')).window);
          return persistenceApi.upvoteComment(expectedComment.txHash, {
            from: accounts[0]
          }).then(() => {
            assert.isTrue(false, "should not return anything");
          }).catch((err) => {
            assert.isTrue(err instanceof Web3Error, "should get instance of Web3Error");
            upvoteCommentWeb3Down();
          });
        });
      });
    });
  });

  it('Downvote comment - fetch comments with persona and downvote - url with one comment', (downvoteCommentDone) => {
    let commentJson1 = commentsTestJson.comment1;
    let personaJson1 = personasTestJson.persona1;
    let expectedPersona = new Persona(accounts[0], personaJson1.name, personaJson1.avatar, 100, true);
    // creation of comment with one downvote
    let expectedComment = new Comment(null, commentJson1.content, commentJson1.url, expectedPersona, 0, 1, 0, true);

    return persistenceApi.addPersona(expectedPersona, {
      from: accounts[0]
    }).then(() => {
      return persistenceApi.addComment(expectedComment, {
        from: accounts[0]
      }).then((txHash2) => {
        assert.match(txHash2, /0[xX][0-9a-fA-F]+/, "txHash should be a hexadecimal value.");
        expectedComment.txHash = txHash2;
        return persistenceApi.downvoteComment(expectedComment.txHash, {
          from: accounts[0]
        }).then(() => {
          return persistenceApi.getComments(commentJson1.url).then((actualComments) => {
            expectedComment.timestamp = actualComments[0].timestamp;
            expectedComment.persona.balance = actualComments[0].persona.balance;
            assert.deepEqual(actualComments[0], expectedComment, "retrieved comments do not match added comments.");
            downvoteCommentDone();
          });
        });
      });
    });
  });

  it('Downvote comment when web3 is down', (downvoteCommentWeb3Down) => {
    let commentJson1 = commentsTestJson.comment1;
    let personaJson1 = personasTestJson.persona1;
    let expectedPersona = new Persona(accounts[0], personaJson1.name, personaJson1.avatar, 100, true);
    let expectedComment = new Comment(null, commentJson1.content, commentJson1.url, expectedPersona, 1, 0, 0, true);
    return persistenceApi.addPersona(expectedPersona, {
      from: accounts[0]
    }).then(() => {
      return persistenceApi.addComment(expectedComment, {
        from: accounts[0]
      }).then((txHash2) => {
        assert.match(txHash2, /0[xX][0-9a-fA-F]+/, "txHash should be a hexadecimal value.");
        expectedComment.txHash = txHash2;
        setConfiguration("test_no_web3").then(() => {
          persistenceApi.setup((new JSDOM('')).window);
          return persistenceApi.downvoteComment(expectedComment.txHash, {
            from: accounts[0]
          }).then(() => {
            assert.isTrue(false, "should not return anything");
          }).catch((err) => {
            assert.isTrue(err instanceof Web3Error, "should get instance of Web3Error");
            downvoteCommentWeb3Down();
          });
        });
      });
    });
  });

  it('Add and get personas - two personas', (addAndFetchPersona2Done) => {
    let personaJson1 = personasTestJson.persona2;
    let personaJson2 = personasTestJson.persona3;
    let expectedPersona1 = new Persona(accounts[0], personaJson1.name, personaJson1.avatar, 100, true);
    let expectedPersona2 = new Persona(accounts[1], personaJson2.name, personaJson1.avatar, 100, true);
    let expectedPersonas = [expectedPersona1, expectedPersona2];

    let promises = [
      persistenceApi.addPersona(expectedPersona1, {
        from: accounts[0]
      }),
      persistenceApi.addPersona(expectedPersona2, {
        from: accounts[1]
      })
    ];
    return Promise.all(promises).then(() => {
      return persistenceApi.getPersonas(accounts).then((actualPersonas) => {
        // check if all of the added personas are included in retrieved comments
        for (let i = 0; i < expectedPersonas.length; i++) {
          let actualPersona = actualPersonas.find(x => x.account === expectedPersonas[i].account);
          expectedPersonas[i].balance = actualPersona.balance;
          assert.deepEqual(actualPersona, expectedPersonas[i], "retrieved personas do not match added personas.");
        }
        addAndFetchPersona2Done();
      });
    });
  });

  it('Add persona with same account twice', (addSamePersonaTwiceDone) => {
    let persona1Json = personasTestJson.persona1;
    let persona2Json = personasTestJson.persona2;
    let originalPersona = new Persona(accounts[0], persona1Json.name, persona1Json.avatar, 100, 100, true);
    let newPersona = new Persona(accounts[0], persona2Json.name, persona2Json.avatar, 100, 100, true);

    return persistenceApi.addPersona(originalPersona, {
      from: accounts[0]
    }).then((txHash1) => {
      assert.match(txHash1, /0[xX][0-9a-fA-F]+/, "txHash1 should be a hexadecimal value.");
      return persistenceApi.addPersona(newPersona, {
        from: accounts[0]
      }).then((txHash2) => {
        assert.isTrue(false, "should not return anything");
      }).catch((err) => {
        assert.isTrue(err instanceof InputValidationError, "should get instance of InputValidationError");
        addSamePersonaTwiceDone();
      });
    });
  });

  it('Update persona', (updatePersonaDone) => {
    let accountsSubset = [accounts[0]];
    let persona1Json = personasTestJson.persona1;
    let persona2Json = personasTestJson.persona2;
    let expectedPersona = new Persona(accounts[0], persona2Json.name, persona2Json.avatar, 100, true);

    return persistenceApi.addPersona(expectedPersona, {
      from: accounts[0]
    }).then((txHash1) => {
      expectedPersona.name = persona2Json.name;
      expectedPersona.avatar = persona2Json.avatar;
      assert.match(txHash1, /0[xX][0-9a-fA-F]+/, "txHash1 should be a hexadecimal value.");
      return persistenceApi.updatePersona(expectedPersona, {
        from: accounts[0]
      }).then((txHash2) => {
        assert.match(txHash2, /0[xX][0-9a-fA-F]+/, "txHash2 should be a hexadecimal value.");
        return persistenceApi.getPersonas(accountsSubset).then((actualPersonas) => {
          expectedPersona.balance = actualPersonas[0].balance;
          assert.equal(actualPersonas.length, 1, "retrieved personas length is not 1.");
          assert.deepEqual(actualPersonas[0], expectedPersona, "retrieved personas do not match added personas.");
          updatePersonaDone();
        });
      });
    });
  });

  it('Update persona with non-matching accounts', (updatePersonaNonMatchingAccountsDone) => {
    let persona1Json = personasTestJson.persona1;
    let persona2Json = personasTestJson.persona2;
    let originalPersona = new Persona(accounts[0], persona1Json.name, persona1Json.avatar, 100, true);
    let expectedPersona = new Persona(accounts[1], persona2Json.name, persona2Json.avatar, 100, true);

    return persistenceApi.addPersona(originalPersona, {
      from: accounts[0]
    }).then((txHash1) => {
      assert.match(txHash1, /0[xX][0-9a-fA-F]+/, "txHash1 should be a hexadecimal value.");
      return persistenceApi.updatePersona(expectedPersona, {
        from: accounts[0]
      }).then((txHash2) => {
        assert.isTrue(false, "should not return anything");
      }).catch((err) => {
        assert.isTrue(err instanceof InputValidationError, "should get instance of InputValidationError");
        updatePersonaNonMatchingAccountsDone();
      });
    });
  });

  it('Get personas - non-existent persona', (fetchNonExistentPersona2Done) => {
    let accountsSubset = [accounts[0]];
    return persistenceApi.getPersonas(accountsSubset).then((actualPersonas) => {
      assert.isTrue(actualPersonas.length === 0, "retrieved personas should be an empty array.");
      fetchNonExistentPersona2Done();
    });
  });

  it('Get personas with invalid name length - over maximum size', (fetchPersonasMaxSizeDone) => {
    let accountsSubset = [accounts[0]];
    let personaJson = personasTestJson.personaWithLongName;
    let expectedPersona = new Persona(accounts[0], personaJson.name, personaJson.avatar, 100, true);

    return persistenceApi.addPersona(expectedPersona, {
      from: accounts[0]
    }).then((txHash) => {
      assert.match(txHash, /0[xX][0-9a-fA-F]+/, "txHash should be a hexadecimal value.");
      return persistenceApi.getPersonas(accountsSubset).then((personas) => {
        assert.isEmpty(personas, "getPersonas returned an array that is not empty");
        fetchPersonasMaxSizeDone();
      });
    });
  });

  it('Get personas with invalid name length - under minimum size', (fetchPersonasMinSizeDone) => {
    let accountsSubset = [accounts[0]];
    let personaJson = personasTestJson.persona1;
    let expectedPersona = new Persona(accounts[0], "ab", personaJson.avatar, 100, true);

    return persistenceApi.addPersona(expectedPersona, {
      from: accounts[0]
    }).then((txHash) => {
      assert.match(txHash, /0[xX][0-9a-fA-F]+/, "txHash should be a hexadecimal value.");
      return persistenceApi.getPersonas(accountsSubset).then((personas) => {
        assert.isEmpty(personas, "getPersonas returned an array that is not empty");
        fetchPersonasMinSizeDone();
      });
    });
  });

  it('Get personas with invalid avatar length - over maximum size', (fetchPersonasAvatarInvalidSizeDone) => {
    let accountsSubset = [accounts[0]];
    let personaJson = personasTestJson.persona1;
    // create avatar image larger than limit by one char
    let largeAvatar = '1';
    for (let i = 0; i < personaSchema.properties.avatar.maxLength; i++) {
      largeAvatar += 'a';
    }
    let expectedPersona = new Persona(accounts[0], personaJson.name, largeAvatar, 100, true);

    return persistenceApi.addPersona(expectedPersona, {
      from: accounts[0]
    }).then((txHash) => {
      assert.match(txHash, /0[xX][0-9a-fA-F]+/, "txHash should be a hexadecimal value.");
      return persistenceApi.getPersonas(accountsSubset).then((personas) => {
        assert.isEmpty(personas, "getPersonas returned an array that is not empty");
        fetchPersonasAvatarInvalidSizeDone();
      });
    });
  });

  it('Get personas with name with malicious content', (fetchPersonasMaliciousContentDone) => {
    let accountsSubset = [accounts[0]];
    let personaJson = personasTestJson.personaWithMaliciosContent;
    let expectedPersona = new Persona(accounts[0], personaJson.name, personaJson.avatar, 100, true);

    return persistenceApi.addPersona(expectedPersona, {
      from: accounts[0]
    }).then((txHash) => {
      assert.match(txHash, /0[xX][0-9a-fA-F]+/, "txHash should be a hexadecimal value.");
      return persistenceApi.getPersonas(accountsSubset).then((personas) => {
        assert.isEmpty(personas, "getPersonas returned an array that is not empty");
        fetchPersonasMaliciousContentDone();
      });
    });
  });

  it('Get personas with invalid avatar content - not base64 image', (fetchPersonasInvalidAvatarContentDone) => {
    let accountsSubset = [accounts[0]];
    let personaJson = personasTestJson.personaWithInvalidAvatarContent;
    let expectedPersona = new Persona(accounts[0], personaJson.name, personaJson.avatar, 100, true);

    return persistenceApi.addPersona(expectedPersona, {
      from: accounts[0]
    }).then((txHash) => {
      assert.match(txHash, /0[xX][0-9a-fA-F]+/, "txHash should be a hexadecimal value.");
      return persistenceApi.getPersonas(accountsSubset).then((personas) => {
        assert.isEmpty(personas, "getPersonas returned an array that is not empty");
        fetchPersonasInvalidAvatarContentDone();
      });
    });
  });

  it('Add persona when web3 is down', (addPersonaWeb3DownDone) => {
    setConfiguration("test_no_web3").then(() => {
      persistenceApi.setup((new JSDOM('')).window);
      let personaJson1 = personasTestJson.persona1;
      let expectedPersona = new Persona(accounts[0], personaJson1.name, personaJson1.avatar, 100, true);

      return persistenceApi.addPersona(expectedPersona, {
        from: accounts[0]
      }).then(() => {
        assert.isTrue(false, "should not return anything");
      }).catch((err) => {
        assert.isTrue(err instanceof Web3Error, "should get instance of Web3Error");
        addPersonaWeb3DownDone();
      });
    });
  });

  it('Get personas when web3 is down', (fetchPersona2Web3DownDone) => {
    let personaJson1 = personasTestJson.persona1;
    let expectedPersona = new Persona(accounts[0], personaJson1.name, personaJson1.avatar, 100, true);

    return persistenceApi.addPersona(expectedPersona, {
      from: accounts[0]
    }).then((txHash) => {
      assert.match(txHash, /0[xX][0-9a-fA-F]+/, "txHash should be a hexadecimal value.");
      setConfiguration("test_no_web3").then(() => {
        persistenceApi.setup((new JSDOM('')).window);
        let accountsSubset = [accounts[0]];
        return persistenceApi.getPersonas(accountsSubset).then(() => {
          assert.isTrue(false, "should not return anything");
        }).catch((err) => {
          assert.isTrue(err instanceof Web3Error, "should get instance of Web3Error"); fetchPersona2Web3DownDone();
        });
      });
    });
  });

  describe('Tfa API integration tests with ipfs down', function () {
    this.timeout(120000);
    before((noIpfsDone) => {
      persistenceApi.stopIpfs().then(() => {
        noIpfsDone();
      });
    });

    beforeEach((done) => {
      initConfigAndProviders().then(() => {
        done();
      }).catch((err) => {
        throw err;
      });
    });

    it('Add comment with ipfs down', (addCommentIpfsDownDone) => {
      let commentJson1 = commentsTestJson.comment1;
      setConfiguration("test_no_ipfs").then(() => {
        persistenceApi.setup((new JSDOM('')).window);
        return persistenceApi.addComment(commentJson1, {
          from: accounts[0]
        }).then(() => {
          assert.isTrue(false, "should not return anything");
        }).catch((err) => {
          assert.isTrue(err instanceof IpfsError, "should get instance of IpfsError");
          addCommentIpfsDownDone();
        });
      });
    });

    it('Add persona when ipfs is down', (addPersonaIpfsDownDone) => {
      setConfiguration("test_no_ipfs").then(() => {
        persistenceApi.setup((new JSDOM('')).window);
        let personaJson1 = personasTestJson.persona1;
        let expectedPersona = new Persona(accounts[0], personaJson1.name, personaJson1.avatar, 100, true);
        return persistenceApi.addPersona(expectedPersona, {
          from: accounts[0]
        }).then(() => {
          assert.isTrue(false, "should not return anything");
        }).catch((err) => {
          assert.isTrue(err instanceof IpfsError, "should get instance of IpfsError");
          addPersonaIpfsDownDone();
        });
      });
    });

    it('Fetch personas when ipfs is down', (fetchPersona2IpfsDownDone) => {
      let personaJson1 = personasTestJson.persona1;
      let expectedPersona = new Persona(accounts[0], personaJson1.name, personaJson1.avatar, 100, true);

      persistenceApi.startIpfs().then(() => {
        return persistenceApi.addPersona(expectedPersona, {
          from: accounts[0]
        }).then((txHash) => {
          assert.match(txHash, /0[xX][0-9a-fA-F]+/, "txHash should be a hexadecimal value.");
          setConfiguration("test_no_ipfs").then(() => {
            persistenceApi.setup((new JSDOM('')).window);
            let accountsSubset = [accounts[0]];
            persistenceApi.stopIpfs().then(() => {
              return persistenceApi.getPersonas(accountsSubset).then(() => {
                assert.isTrue(false, "should not return anything");
              }).catch((err) => {
                assert.isTrue(err instanceof IpfsError, "should get instance of IpfsError");
                fetchPersona2IpfsDownDone();
              });
            });
          });
        });
      });
    });
  });
});
