import { expect } from 'chai';
import React from 'react';
import { TfaHeaderPanelContainer } from './TfaHeaderPanelContainer';
import sinon from 'sinon';
import chrome from 'sinon-chrome/extensions';
import { createMount } from '@material-ui/core/test-utils';
import personasTestJson from '../../../../test/integration/test_personas.json';
import Persona from '../../../common/Persona';
import { JSDOM } from 'jsdom';
import Enzyme from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

Enzyme.configure({ adapter: new Adapter() });

describe("<TfaHeaderPanelContainer /> tests", () => {
  let mount;

  function setup(showPersonaManagementPanel, currentPersona) {
    const props = {
      togglePersonaManagementPanel: () => { },
      showPersonaManagementPanel: showPersonaManagementPanel,
      currentPersona: currentPersona
    };
    return mount(<TfaHeaderPanelContainer {...props} />);
  }

  before(() => {
    global.chrome = chrome;
    const jsdom = new JSDOM('<!doctype html><html><body></body></html>');
    const { window } = jsdom;
    global.window = window;
    global.document = window.document;
    mount = createMount();
  });

  after(() => {
    mount.cleanUp();
  });

  it("renders header panel container with existing persona and show persona management panel", () => {
    const currentPersona = new Persona("account", personasTestJson.persona1.name, personasTestJson.persona1.avatar, 0);
    const wrapper = setup(true, currentPersona);
    expect(wrapper.props().showPersonaManagementPanel).to.be.true;
    expect(wrapper.props().currentPersona).to.equal(currentPersona);
    expect(wrapper.props().togglePersonaManagementPanel).to.exist;
    expect(wrapper.find("TfaHeaderPanel").length).to.equal(1);
  });

  it("renders header panel container with empty persona and show persona management panel", () => {
    const wrapper = setup(true, {});
    expect(wrapper.props().showPersonaManagementPanel).to.be.true;
    expect(wrapper.props().currentPersona).to.be.empty;
    expect(wrapper.props().togglePersonaManagementPanel).to.exist;
    expect(wrapper.find("TfaHeaderPanel").length).to.equal(1);
  });

  it("renders header panel container with undefined persona and show persona management panel", () => {
    const wrapper = setup(true, undefined);
    expect(wrapper.props().showPersonaManagementPanel).to.be.true;
    expect(wrapper.props().currentPersona).to.be.undefined;
    expect(wrapper.props().togglePersonaManagementPanel).to.exist;
    expect(wrapper.find("TfaHeaderPanel").length).to.equal(1);
  });

  it("renders header panel container with existing persona and show comments management panel", () => {
    const currentPersona = new Persona("account", personasTestJson.persona1.name, personasTestJson.persona1.avatar, 0);
    const wrapper = setup(false, currentPersona);
    expect(wrapper.props().showPersonaManagementPanel).to.be.false;
    expect(wrapper.props().currentPersona).to.equal(currentPersona);
    expect(wrapper.props().togglePersonaManagementPanel).to.exist;
    expect(wrapper.find("TfaHeaderPanel").length).to.equal(1);
  });
});
