import {expect} from 'chai';
import displaySidebarReducer from './displaySidebarReducer';
import * as actions from '../actions/displaySidebarActions';

describe("displaySidebarReducer test", () => {
  it("should set display sidebar when passed DISPLAY_SIDEBAR", () => {
    const initState = false;
    const action1 = actions.displaySidebar(true);
    let newState = displaySidebarReducer(initState, action1);

    expect(newState).to.be.true;

    const action2 = actions.displaySidebar(false);
    newState = displaySidebarReducer(initState, action2);

    expect(newState).to.be.false;
  });
});
