import { expect } from 'chai';
import React from 'react';
import TfaUnselectedPersona from './TfaUnselectedPersona';
import sinon from 'sinon';
import chrome from 'sinon-chrome/extensions';
import Utils from '../../../common/Utils';
import { createMount } from '@material-ui/core/test-utils';
import personasTestJson from '../../../../test/integration/test_personas.json';
import Persona from '../../../common/Persona';
import { JSDOM } from 'jsdom';
import Enzyme from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

Enzyme.configure({ adapter: new Adapter() });

describe("<TfaUnselectedPersona /> tests", () => {
  let mount;
  let getDefaultAvatarImgStub;

  function setup(persona) {
    const props = {
      selectPersona: () => { },
      persona: persona
    };
    return mount(<TfaUnselectedPersona {...props} />);
  }

  before(() => {
    global.chrome = chrome;
    const jsdom = new JSDOM('<!doctype html><html><body></body></html>');
    const { window } = jsdom;
    global.window = window;
    global.document = window.document;
    getDefaultAvatarImgStub = sinon.stub(Utils, "getDefaultAvatarImg");
    getDefaultAvatarImgStub.returns("defaultAvatarImg");
    mount = createMount();
  });

  after(() => {
    mount.cleanUp();
    getDefaultAvatarImgStub.restore();
  });

  it("renders unconfirmed persona", () => {
    const persona = new Persona("account", personasTestJson.persona1.name, personasTestJson.persona1.avatar, 0);
    const wrapper = setup(persona);
    expect(wrapper.props().persona).to.equal(persona);
    expect(wrapper.props().selectPersona).to.exist;
    expect(wrapper.find("Tooltip").length).to.equal(1);
    expect(wrapper.find("Tooltip").props().title).to.equal("This persona is disabled until it is mined and stored on blockchain");
    expect(wrapper.find("ListItem").length).to.equal(1);
    expect(wrapper.find("ListItem").props().divider).to.exist;
    expect(wrapper.find("Card").length).to.equal(1);
    expect(wrapper.find("Avatar").length).to.equal(1);
    expect(wrapper.find("Avatar").props().src).to.equal(persona.avatar);
    expect(wrapper.find("p").length).to.equal(2);
    expect(wrapper.find("p").at(0).text()).to.equal(persona.name);
    expect(wrapper.find("p").at(1).text()).to.equal(persona.account);
  });

  it("renders confirmed persona", () => {
    const persona = new Persona("account", personasTestJson.persona1.name, personasTestJson.persona1.avatar, 0, true);
    const wrapper = setup(persona);
    expect(wrapper.props().persona).to.equal(persona);
    expect(wrapper.props().selectPersona).to.exist;
    expect(wrapper.find("Tooltip").length).to.equal(1);
    expect(wrapper.find("Tooltip").props().title).to.equal("Select as active persona");
    expect(wrapper.find("ListItem").length).to.equal(1);
    expect(wrapper.find("ListItem").props().divider).to.exist;
    expect(wrapper.find("Card").length).to.equal(1);
    expect(wrapper.find("Avatar").length).to.equal(1);
    expect(wrapper.find("Avatar").props().src).to.equal(persona.avatar);
    expect(wrapper.find("p").length).to.equal(2);
    expect(wrapper.find("p").at(0).text()).to.equal(persona.name);
    expect(wrapper.find("p").at(1).text()).to.equal(persona.account);
  });

  it("renders confirmed persona with empty avatar", () => {
    const persona = new Persona("account", personasTestJson.persona1.name, "", 0, true);
    const expectedAvatarImg = Utils.getAvatarImg(persona);
    const wrapper = setup(persona);
    expect(wrapper.props().persona).to.equal(persona);
    expect(wrapper.props().selectPersona).to.exist;
    expect(wrapper.find("Tooltip").length).to.equal(1);
    expect(wrapper.find("Tooltip").props().title).to.equal("Select as active persona");
    expect(wrapper.find("ListItem").length).to.equal(1);
    expect(wrapper.find("ListItem").props().divider).to.exist;
    expect(wrapper.find("Card").length).to.equal(1);
    expect(wrapper.find("Avatar").length).to.equal(1);
    expect(wrapper.find("Avatar").props().src).to.not.equal("");
    expect(wrapper.find("Avatar").props().src).to.not.be.undefined;
    expect(wrapper.find("Avatar").props().src).to.equal(expectedAvatarImg);
    expect(wrapper.find("p").length).to.equal(2);
    expect(wrapper.find("p").at(0).text()).to.equal(persona.name);
    expect(wrapper.find("p").at(1).text()).to.equal(persona.account);
  });

  it("simulates confirmed persona selection", () => {
    const selectPersona = sinon.spy();
    const persona = new Persona("account", personasTestJson.persona1.name, personasTestJson.persona1.avatar, 0, true);
    const wrapper = mount(
      <TfaUnselectedPersona
        persona={persona}
        selectPersona={selectPersona}
      />
    );
    wrapper.find("Card").simulate("click");
    expect(selectPersona.calledOnce).to.be.true;
  });
});
