/*jshint esversion: 6 */
import { assert } from 'chai';
import Promise from 'bluebird';
import { JSDOM } from 'jsdom';
import rewire from 'rewire';
import commentsTestJson from '../../test/integration/test_comments.json';
import personasTestJson from '../../test/integration/test_personas.json';
import Comment from '../common/Comment';
import Persona from '../common/Persona';
import IpfsError from '../common/errors/IpfsError';
import Web3Error from '../common/errors/Web3Error';
import InputValidationError from '../common/errors/InputValidationError';
import personaSchema from '../common/schema/PersonaSchema.json';
import config from '../../config.json';
const persistenceApi = rewire("./persistenceApi");
let web3Mock, accounts;

class EventMock {
  constructor() {
    this._events = [];
  }

  add(logItem) {
    this._events.push(logItem);
  }

  get(callback) {
    return new Promise((accept) => {
      accept(callback(null, this._events));
    });
  }

  getLast() {
    if (this._events.length > 0) {
      return this._events[this._events.length - 1];
    }
    return null;
  }

  stopWatching() { }
}

class CommentInstanceMock {
  constructor() {
    this._commentAddedEvent = new EventMock();
    this._commentUpvotedEvent = new EventMock();
    this._commentDownvotedEvent = new EventMock();
  }

  // adds comments with tx hash 0xaa.. and length of log
  addComment(urlHash, commentHash, txData) {
    return new Promise((accept) => {
      // create hash of tx
      let size = this._commentAddedEvent._events.length;
      let txHash = "0xaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa" + size;

      // create log item
      let logItem = {
        args: {
          commentHashRef: commentHash,
          urlHashRef: urlHash
        },
        transactionHash: txHash,
        txData: txData
      };
      this._commentAddedEvent.add(logItem);

      // return tx object
      let tx = {
        tx: txHash
      };
      accept(tx);
    });
  }

  upvoteComment(txHash) {
    return new Promise((accept) => {
      // create log item
      let logItem = {
        txHash: txHash
      };
      this._commentUpvotedEvent.add(logItem);
      accept();
    });
  }

  downvoteComment(txHash) {
    return new Promise((accept) => {
      // create log item
      let logItem = {
        txHash: txHash
      };
      this._commentDownvotedEvent.add(logItem);

      accept();
    });
  }

  CommentAdded() {
    return this._commentAddedEvent;
  }

  CommentUpvoted() {
    return this._commentUpvotedEvent;
  }

  CommentDownvoted() {
    return this._commentDownvotedEvent;
  }
}

class CommentMock {
  constructor(isUp) {
    this._instance = new CommentInstanceMock();
    this._isUp = isUp;
  }

  deployed() {
    return new Promise((accept, reject) => {
      if (this._isUp) {
        accept(this._instance);
      } else {
        reject(new Web3Error("Web3 is down"));
      }
    });
  }
}

class UportRegistryInstanceMock {
  constructor() {
    this._attributesSet = new EventMock();
  }

  getAttributes(account) {
    return new Promise((accept) => {
      // return only latest persona hash for the same account
      let logItems = this._attributesSet._events.filter(x => x._sender === account);
      if (logItems !== undefined && logItems.length > 0) {
        accept(logItems[logItems.length - 1].personaHash);
      } else {
        accept('0x');
      }
    });
  }

  setAttributes(personaHash, txData) {
    return new Promise((accept) => {
      // create hash of tx
      let size = this._attributesSet._events.length;
      let txHash = "0xaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa" + size;

      // remove old persona from events

      // create log item
      let logItem = {
        personaHash: personaHash,
        _sender: txData.from,
        _timestamp: 1
      };
      this._attributesSet.add(logItem);

      // return tx object
      let tx = {
        tx: txHash
      };
      accept(tx);
    });
  }
}

class UportRegistryMock {
  constructor(isUp) {
    this._instance = new UportRegistryInstanceMock();
    this._isUp = isUp;
  }

  deployed() {
    return new Promise((accept, reject) => {
      if (this._isUp) {
        accept(this._instance);
      } else {
        reject(new Web3Error("Web3 is down"));
      }
    });
  }
}

class Files {
  constructor(isUp) {
    this._isUp = isUp;
    this._storage = [];
  }

  // adds json to storage, but starts from index 1 (because 0 is not base58 character)
  add(json, callback) {
    return new Promise((accept, reject) => {
      if (this._isUp) {
        // add to storage
        this._storage.push(json);
        // create matching hash
        let ipfsHash = "QmYwAPJzv5CZsnA625s3Xf2nemtYgPpHdWEz79ojWnPbd" + this._storage.length;
        // return object with hash
        let results = [
          {
            hash: ipfsHash
          }];
        accept(callback(null, results));
      } else {
        reject(new IpfsError("Ipfs is down"));
      }
    });
  }

  cat(ipfsHash, callback) {
    return new Promise((accept, reject) => {
      if (this._isUp) {
        // get from storage based on hash
        let json = this._storage[parseInt(ipfsHash.replace("QmYwAPJzv5CZsnA625s3Xf2nemtYgPpHdWEz79ojWnPbd", "")) - 1];
        accept(callback(null, json));
      } else {
        reject(new IpfsError("Ipfs is down"));
      }
    });
  }
}

class IPFSMock {
  constructor(isUp) {
    this._files = new Files(isUp);
    this._isUp = isUp;
  }

  isOnline() {
    return this._isUp;
  }

  get files() {
    return this._files;
  }
}

class Eth {
  constructor(isUp) {
    this._isUp = isUp;
    this._accounts = [
      "0x1111111111111111111111111111111111111111",
      "0x2222222222222222222222222222222222222222"
    ];
  }

  get accounts() {
    if (this._isUp) {
      return this._accounts;
    } else {
      throw new Web3Error("Web3Mock is not up");
    }
  }

  getTransaction() {
    return { from: this._accounts[0] };
  }

  getBalance() {
    if (this._isUp) {
      return 100;
    } else {
      throw new Web3Error("Web3Mock is not up");
    }
  }

  getBlock() {
    return { timestamp: "1" };
  }
}

class Web3Mock {
  constructor(isUp) {
    this._eth = new Eth(isUp);
    this.isUp = isUp;
  }

  get eth() {
    return this._eth;
  }

  fromWei(ammount) {
    return ammount;
  }
}

function turnOffIpfs() {
  return new Promise((accept) => {
    let ipfsMock = new IPFSMock(false);
    persistenceApi.__set__("ipfsNode", ipfsMock);
    accept();
  });
}

function setConfiguration(sel) {
  return new Promise((accept) => {
    // prepare based on configuration selection
    web3Mock = new Web3Mock(sel === "local" || sel === "test_no_ipfs");
    let commentContract = new CommentMock(sel === "local" || sel === "test_no_ipfs");
    let uportRegistry = new UportRegistryMock(sel === "local" || sel === "test_no_ipfs");
    let ipfsMock = new IPFSMock(sel === "local" || sel === "test_no_web3");
    config['selection'] = "local";

    // rewire persistence api
    persistenceApi.__set__("web3", web3Mock);
    persistenceApi.__set__("ipfsNode", ipfsMock);
    persistenceApi.__set__("CommentContract", commentContract);
    persistenceApi.__set__("UportRegistry", uportRegistry);
    persistenceApi.__set__("documentDOM", (new JSDOM('')).window);
    persistenceApi.__set__("config", config);
    accept(web3Mock);
  });
}

function initConfigAndProviders() {
  return new Promise((accept) => {
    setConfiguration("local").then((web3Mock) => {
      accounts = web3Mock.eth.accounts;
      accept(web3Mock);
    });
  });
}

describe('Persistence API unit tests', function () {
  this.timeout(120000);

  beforeEach((done) => {
    initConfigAndProviders().then(() => {
      done();
    }).catch((err) => {
      throw err;
    });
  });

  it('Get first account', () => {
    assert.deepEqual(accounts[0], persistenceApi.getFirstAccount(), "First account does not match.");
  });

  it('Get first account when web3 is down', (getFirstAccountWoWeb3Done) => {
    setConfiguration("test_no_web3").then(() => {
      try {
        persistenceApi.getFirstAccount();
        assert.isTrue(false, "should throw exception");
      } catch (err) {
        assert.isTrue(err instanceof Web3Error, "should get instance of Web3Error");
        getFirstAccountWoWeb3Done();
      }
    });
  });

  it('Get accounts', () => {
    assert.deepEqual(accounts, persistenceApi.getAccounts(), "Retrieved accounts do not match.");
  });

  it('Get accounts when web3 is down', (getAccountsNoWeb3Done) => {
    setConfiguration("test_no_web3").then(() => {
      try {
        persistenceApi.getAccounts();
        assert.isTrue(false, "should throw exception");
      } catch (err) {
        assert.isTrue(err instanceof Web3Error, "should get instance of Web3Error");
        getAccountsNoWeb3Done();
      }
    });
  });

  it('Get account balance', () => {
    let expectedBalance = web3Mock.fromWei(web3Mock.eth.getBalance(accounts[0]), 'ether');
    assert.deepEqual(persistenceApi.getAccountBalance(accounts[0]), expectedBalance, "Account balances do not match.");
  });

  it('Get account balance when web3 is down', (getAccountBalanceNoWeb3Done) => {
    setConfiguration("test_no_web3").then(() => {
      try {
        persistenceApi.getAccountBalance(accounts[0]);
        assert.isTrue(false, "should throw exception");
      } catch (err) {
        assert.isTrue(err instanceof Web3Error, "should get instance of Web3Error");
        getAccountBalanceNoWeb3Done();
      }
    });
  });

  it('Add and fetch comments - url with one comment', (addAndFetchComments1Done) => {
    let commentJson1 = commentsTestJson.comment1;
    let expectedComment = new Comment(null, commentJson1.content, commentJson1.url, null, 0, 0, 0, true);
    return persistenceApi.addComment(expectedComment, {
      from: accounts[0]
    }).then((txHash) => {
      assert.match(txHash, /0[xX][0-9a-fA-F]+/, "txHash should be a hexadecimal value.");
      expectedComment.txHash = txHash;
      return persistenceApi.getComments(commentJson1.url).then((actualComments) => {
        expectedComment.timestamp = actualComments[0].timestamp;
        assert.deepEqual(actualComments[0], expectedComment, "retrieved comments do not match added comments.");
        addAndFetchComments1Done();
      });
    });
  });

  it('Add and fetch comments - url with two comments', (addAndFetchComments2Done) => {
    let commentJson1 = commentsTestJson.comment2;
    let commentJson2 = commentsTestJson.comment3;
    let expectedComment1 = new Comment(null, commentJson1.content, commentJson1.url, null, 0, 0, 0, true);
    let expectedComment2 = new Comment(null, commentJson2.content, commentJson2.url, null, 0, 0, 0, true);
    let expectedComments = [expectedComment1, expectedComment2];

    let promises = [
      persistenceApi.addComment(expectedComment1, {
        from: accounts[0]
      }).then((txHash) => {
        assert.match(txHash, /0[xX][0-9a-fA-F]+/, "txHash should be a hexadecimal value.");
        expectedComment1.txHash = txHash;
      }),
      persistenceApi.addComment(expectedComment2, {
        from: accounts[0]
      }).then((txHash) => {
        assert.match(txHash, /0[xX][0-9a-fA-F]+/, "txHash should be a hexadecimal value.");
        expectedComment2.txHash = txHash;
      })
    ];
    return Promise.all(promises).then(() => {
      return persistenceApi.getComments(commentJson1.url).then((actualComments) => {
        // check if all of the added comments are included in retrieved comments
        for (let i = 0; i < expectedComments.length; i++) {
          let actualComment = actualComments.find(x => x.txHash === expectedComments[i].txHash);
          expectedComments[i].timestamp = actualComment.timestamp;
          assert.deepEqual(actualComment, expectedComments[i], "retrieved comments do not match added comments.");
        }
        addAndFetchComments2Done();
      });
    });
  });

  it('Add and fetch all comments - no url', (addAndFetchAllCommentsDone) => {
    let commentJson1 = commentsTestJson.comment1;
    let commentJson2 = commentsTestJson.comment2;
    let expectedComment1 = new Comment(null, commentJson1.content, commentJson1.url, null, 0, 0, 0, true);
    let expectedComment2 = new Comment(null, commentJson2.content, commentJson2.url, null, 0, 0, 0, true);
    let expectedComments = [expectedComment1, expectedComment2];

    let promises = [
      persistenceApi.addComment(expectedComment1, {
        from: accounts[0]
      }).then((txHash) => {
        assert.match(txHash, /0[xX][0-9a-fA-F]+/, "txHash should be a hexadecimal value.");
        expectedComment1.txHash = txHash;
      }),
      persistenceApi.addComment(expectedComment2, {
        from: accounts[0]
      }).then((txHash) => {
        assert.match(txHash, /0[xX][0-9a-fA-F]+/, "txHash should be a hexadecimal value.");
        expectedComment2.txHash = txHash;
      })
    ];
    return Promise.all(promises).then(() => {
      return persistenceApi.getComments().then((actualComments) => {
        // check if all of the added comments are included in retrieved comments
        for (let i = 0; i < expectedComments.length; i++) {
          let actualComment = actualComments.find(x => x.txHash === expectedComments[i].txHash);
          expectedComments[i].timestamp = actualComment.timestamp;
          assert.deepEqual(actualComment, expectedComments[i], "retrieved comments do not match added comments.");
        }
        addAndFetchAllCommentsDone();
      });
    });
  });

  it('Fetch comments - non-existent url', (fetchCommentsNoUrlDone) => {
    let expectedComments = [];
    return persistenceApi.getComments("http://www.some-other-url.com").then((actualComments) => {
      assert.deepEqual(actualComments, expectedComments, "retrieved comments do not match");
      fetchCommentsNoUrlDone();
    });
  });

  it('Add and fetch comments with persona - url with one comment', (addAndFetchCommentsWithPersonaDone) => {
    let commentJson1 = commentsTestJson.comment1;
    let personaJson1 = personasTestJson.persona1;
    let expectedPersona = new Persona(accounts[0], personaJson1.name, personaJson1.avatar, 100, true);
    let expectedComment = new Comment(null, commentJson1.content, commentJson1.url, expectedPersona, 0, 0, 0, true);
    return persistenceApi.addPersona(expectedPersona, {
      from: accounts[0]
    }).then(() => {
      return persistenceApi.addComment(expectedComment, {
        from: accounts[0]
      }).then((txHash2) => {
        assert.match(txHash2, /0[xX][0-9a-fA-F]+/, "txHash should be a hexadecimal value.");
        expectedComment.txHash = txHash2;
        return persistenceApi.getComments(commentJson1.url).then((actualComments) => {
          expectedComment.timestamp = actualComments[0].timestamp;
          assert.deepEqual(actualComments[0], expectedComment, "retrieved comments do not match added comments.");
          addAndFetchCommentsWithPersonaDone();
        });
      });
    });
  });

  it('Add comment with web3 down', (addCommentWeb3DownDone) => {
    let commentJson1 = commentsTestJson.comment1;
    setConfiguration("test_no_web3").then(() => {
      return persistenceApi.addComment(commentJson1, {
        from: accounts[0]
      }).then(() => {
        assert.isTrue(false, "should not return anything");
      }).catch((err) => {
        assert.isTrue(err instanceof Web3Error, "should get instance of Web3Error");
        addCommentWeb3DownDone();
      });
    });
  });

  it('Fetch comments with web3 down', (fetchCommentsWeb3DownDone) => {
    let commentJson1 = commentsTestJson.comment1;
    return persistenceApi.addComment(commentJson1, {
      from: accounts[0]
    }).then((txHash) => {
      assert.match(txHash, /0[xX][0-9a-fA-F]+/, "txHash should be a hexadecimal value.");
      setConfiguration("test_no_web3").then(() => {
        let rejected = true;
        return persistenceApi.getComments(commentJson1.url).then(() => {
          rejected = false;
          throw new Error();
        }).catch((err) => {
          assert.isTrue(err instanceof Web3Error, "should get instance of Web3Error");
          assert.isTrue(rejected, "getComments returned comments, should not happen");
          fetchCommentsWeb3DownDone();
        });
      });
    });
  });

  it('Fetch comments with invalid content length - over maximum size', (fetchCommentsMaxSizeDone) => {
    let commentJson = commentsTestJson.commentWithLongContent;
    return persistenceApi.addComment(commentJson, {
      from: accounts[0]
    }).then((txHash) => {
      assert.match(txHash, /0[xX][0-9a-fA-F]+/, "txHash should be a hexadecimal value.");
      return persistenceApi.getComments(commentJson.url).then((comments) => {
        assert.isEmpty(comments, "getComments returned comments, should return empty array");
        fetchCommentsMaxSizeDone();
      });
    });
  });

  it('Fetch comments with invalid content length - under minimum size', (fetchCommentsContentMinSizeDone) => {
    let commentJson = commentsTestJson.comment1;
    let expectedComment = new Comment(null, "", commentJson.url, null, 0, 0, 0, true);
    return persistenceApi.addComment(expectedComment, {
      from: accounts[0]
    }).then((txHash) => {
      assert.match(txHash, /0[xX][0-9a-fA-F]+/, "txHash should be a hexadecimal value.");
      return persistenceApi.getComments(commentJson.url).then((comments) => {
        assert.isEmpty(comments, "getComments returned comments, should return empty array");
        fetchCommentsContentMinSizeDone();
      });
    });
  });

  it('Fetch comments with comment with malicious content', (fetchCommentsMaliciousContentDone) => {
    let commentJson = commentsTestJson.commentWithMaliciousCommentContent;
    return persistenceApi.addComment(commentJson, {
      from: accounts[0]
    }).then((txHash) => {
      assert.match(txHash, /0[xX][0-9a-fA-F]+/, "txHash should be a hexadecimal value.");
      return persistenceApi.getComments(commentJson.url).then((comments) => {
        assert.isEmpty(comments, "getComments returned comments, should return empty array");
        fetchCommentsMaliciousContentDone();
      });
    });
  });

  it('Fetch comments with invalid url length - over maximum size', (fetchCommentsUrlMaxSizeDone) => {
    let commentJson = commentsTestJson.commentWithLongUrl;
    return persistenceApi.addComment(commentJson, {
      from: accounts[0]
    }).then((txHash) => {
      assert.match(txHash, /0[xX][0-9a-fA-F]+/, "txHash should be a hexadecimal value.");
      return persistenceApi.getComments(commentJson.url).then((comments) => {
        assert.isEmpty(comments, "getComments returned comments, should return empty array");
        fetchCommentsUrlMaxSizeDone();
      });
    });
  });

  it('Fetch comments with invalid url length - under minimum size', (fetchCommentsUrlMinSizeDone) => {
    let commentJson = commentsTestJson.commentWithLongUrl;
    let expectedComment = new Comment(null, commentJson.content, "http:/", null, 0, 0, 0, true);
    return persistenceApi.addComment(expectedComment, {
      from: accounts[0]
    }).then((txHash) => {
      assert.match(txHash, /0[xX][0-9a-fA-F]+/, "txHash should be a hexadecimal value.");
      return persistenceApi.getComments(commentJson.url).then((comments) => {
        assert.isEmpty(comments, "getComments returned comments, should return empty array");
        fetchCommentsUrlMinSizeDone();
      });
    });
  });

  it('Upvote comment - fetch comments with persona and upvote - url with one comment', (upvoteCommentDone) => {
    let commentJson1 = commentsTestJson.comment1;
    let personaJson1 = personasTestJson.persona1;
    let expectedPersona = new Persona(accounts[0], personaJson1.name, personaJson1.avatar, 100, true);
    // creation of comment with one upvote
    let expectedComment = new Comment(null, commentJson1.content, commentJson1.url, expectedPersona, 1, 0, 0, true);

    return persistenceApi.addPersona(expectedPersona, {
      from: accounts[0]
    }).then(() => {
      return persistenceApi.addComment(expectedComment, {
        from: accounts[0]
      }).then((txHash2) => {
        assert.match(txHash2, /0[xX][0-9a-fA-F]+/, "txHash should be a hexadecimal value.");
        expectedComment.txHash = txHash2;
        return persistenceApi.upvoteComment(expectedComment.txHash, {
          from: accounts[0]
        }).then(() => {
          return persistenceApi.getComments(commentJson1.url).then((actualComments) => {
            expectedComment.timestamp = actualComments[0].timestamp;
            assert.deepEqual(actualComments[0], expectedComment, "retrieved comments do not match added comments.");
            upvoteCommentDone();
          });
        });
      });
    });
  });

  it('Upvote comment when web3 is down', (upvoteCommentWeb3Down) => {
    let commentJson1 = commentsTestJson.comment1;
    let personaJson1 = personasTestJson.persona1;
    let expectedPersona = new Persona(accounts[0], personaJson1.name, personaJson1.avatar, 100, true);
    let expectedComment = new Comment(null, commentJson1.content, commentJson1.url, expectedPersona, 1, 0, 0, true);

    return persistenceApi.addPersona(expectedPersona, {
      from: accounts[0]
    }).then(() => {
      return persistenceApi.addComment(expectedComment, {
        from: accounts[0]
      }).then((txHash2) => {
        assert.match(txHash2, /0[xX][0-9a-fA-F]+/, "txHash should be a hexadecimal value.");
        expectedComment.txHash = txHash2;
        setConfiguration("test_no_web3").then(() => {
          return persistenceApi.upvoteComment(expectedComment.txHash, {
            from: accounts[0]
          }).then(() => {
            assert.isTrue(false, "should not return anything");
          }).catch((err) => {
            assert.isTrue(err instanceof Web3Error, "should get instance of Web3Error");
            upvoteCommentWeb3Down();
          });
        });
      });
    });
  });

  it('Downvote comment - fetch comments with persona and downvote - url with one comment', (downvoteCommentDone) => {
    let commentJson1 = commentsTestJson.comment1;
    let personaJson1 = personasTestJson.persona1;
    let expectedPersona = new Persona(accounts[0], personaJson1.name, personaJson1.avatar, 100, true);
    // creation of comment with one downvote
    let expectedComment = new Comment(null, commentJson1.content, commentJson1.url, expectedPersona, 0, 1, 0, true);

    return persistenceApi.addPersona(expectedPersona, {
      from: accounts[0]
    }).then(() => {
      return persistenceApi.addComment(expectedComment, {
        from: accounts[0]
      }).then((txHash2) => {
        assert.match(txHash2, /0[xX][0-9a-fA-F]+/, "txHash should be a hexadecimal value.");
        expectedComment.txHash = txHash2;
        return persistenceApi.downvoteComment(expectedComment.txHash, {
          from: accounts[0]
        }).then(() => {
          return persistenceApi.getComments(commentJson1.url).then((actualComments) => {
            expectedComment.timestamp = actualComments[0].timestamp;
            assert.deepEqual(actualComments[0], expectedComment, "retrieved comments do not match added comments.");
            downvoteCommentDone();
          });
        });
      });
    });
  });

  it('Downvote comment when web3 is down', (downvoteCommentWeb3Down) => {
    let commentJson1 = commentsTestJson.comment1;
    let personaJson1 = personasTestJson.persona1;
    let expectedPersona = new Persona(accounts[0], personaJson1.name, personaJson1.avatar, 100, true);
    let expectedComment = new Comment(null, commentJson1.content, commentJson1.url, expectedPersona, 1, 0, 0, true);
    return persistenceApi.addPersona(expectedPersona, {
      from: accounts[0]
    }).then(() => {
      return persistenceApi.addComment(expectedComment, {
        from: accounts[0]
      }).then((txHash2) => {
        assert.match(txHash2, /0[xX][0-9a-fA-F]+/, "txHash should be a hexadecimal value.");
        expectedComment.txHash = txHash2;
        setConfiguration("test_no_web3").then(() => {
          return persistenceApi.downvoteComment(expectedComment.txHash, {
            from: accounts[0]
          }).then(() => {
            assert.isTrue(false, "should not return anything");
          }).catch((err) => {
            assert.isTrue(err instanceof Web3Error, "should get instance of Web3Error");
            downvoteCommentWeb3Down();
          });
        });
      });
    });
  });

  it('Add and get personas - two personas', (addAndFetchPersona2Done) => {
    let personaJson1 = personasTestJson.persona2;
    let personaJson2 = personasTestJson.persona3;
    let expectedPersona1 = new Persona(accounts[0], personaJson1.name, personaJson1.avatar, 100, true);
    let expectedPersona2 = new Persona(accounts[1], personaJson2.name, personaJson2.avatar, 100, true);
    let expectedPersonas = [expectedPersona1, expectedPersona2];

    let promises = [
      persistenceApi.addPersona(expectedPersona1, {
        from: accounts[0]
      }),
      persistenceApi.addPersona(expectedPersona2, {
        from: accounts[1]
      })
    ];
    return Promise.all(promises).then(() => {
      return persistenceApi.getPersonas(accounts).then((actualPersonas) => {
        // check if all of the added personas are included in retrieved comments
        for (let i = 0; i < expectedPersonas.length; i++) {
          let actualPersona = actualPersonas.find(x => x.account === expectedPersonas[i].account);
          assert.deepEqual(actualPersona, expectedPersonas[i], "retrieved personas do not match added personas.");
        }
        addAndFetchPersona2Done();
      });
    });
  });

  it('Add persona with same account twice', (addSamePersonaTwiceDone) => {
    let persona1Json = personasTestJson.persona1;
    let persona2Json = personasTestJson.persona2;
    let originalPersona = new Persona(accounts[0], persona1Json.name, persona1Json.avatar, 100, true);
    let newPersona = new Persona(accounts[0], persona2Json.name, persona2Json.avatar, 100, true);

    return persistenceApi.addPersona(originalPersona, {
      from: accounts[0]
    }).then((txHash1) => {
      assert.match(txHash1, /0[xX][0-9a-fA-F]+/, "txHash1 should be a hexadecimal value.");
      return persistenceApi.addPersona(newPersona, {
        from: accounts[0]
      }).then((txHash2) => {
        assert.isTrue(false, "should not return anything");
      }).catch((err) => {
        assert.isTrue(err instanceof InputValidationError, "should get instance of InputValidationError");
        addSamePersonaTwiceDone();
      });
    });
  });

  it('Update persona', (updatePersonaDone) => {
    let accountsSubset = [accounts[0]];
    let persona1Json = personasTestJson.persona1;
    let persona2Json = personasTestJson.persona2;
    let originalPersona = new Persona(accounts[0], persona1Json.name, persona1Json.avatar, 100, true);
    let expectedPersona = new Persona(accounts[0], persona2Json.name, persona2Json.avatar, 100, true);

    return persistenceApi.addPersona(originalPersona, {
      from: accounts[0]
    }).then((txHash1) => {
      assert.match(txHash1, /0[xX][0-9a-fA-F]+/, "txHash1 should be a hexadecimal value.");
      return persistenceApi.updatePersona(expectedPersona, {
        from: accounts[0]
      }).then((txHash2) => {
        assert.match(txHash2, /0[xX][0-9a-fA-F]+/, "txHash2 should be a hexadecimal value.");
        return persistenceApi.getPersonas(accountsSubset).then((personas) => {
          assert.equal(personas.length, 1, "retrieved personas length is not 1.");
          assert.deepEqual(personas[0], expectedPersona, "retrieved personas do not match added personas.");
          updatePersonaDone();
        });
      });
    });
  });

  it('Update persona with non-matching accounts', (updatePersonaNonMatchingAccountsDone) => {
    let persona1Json = personasTestJson.persona1;
    let persona2Json = personasTestJson.persona2;
    let originalPersona = new Persona(accounts[0], persona1Json.name, persona1Json.avatar, 100, true);
    let expectedPersona = new Persona(accounts[1], persona2Json.name, persona2Json.avatar, 100, true);

    return persistenceApi.addPersona(originalPersona, {
      from: accounts[0]
    }).then((txHash1) => {
      assert.match(txHash1, /0[xX][0-9a-fA-F]+/, "txHash1 should be a hexadecimal value.");
      return persistenceApi.updatePersona(expectedPersona, {
        from: accounts[0]
      }).then((txHash2) => {
        assert.isTrue(false, "should not return anything");
      }).catch((err) => {
        assert.isTrue(err instanceof InputValidationError, "should get instance of InputValidationError");
        updatePersonaNonMatchingAccountsDone();
      });
    });
  });

  it('Get personas - non-existent persona', (fetchNonExistentPersona2Done) => {
    let accountsSubset = [accounts[0]];
    return persistenceApi.getPersonas(accountsSubset).then((actualPersonas) => {
      assert.isTrue(actualPersonas.length === 0, "retrieved personas should be an empty array.");
      fetchNonExistentPersona2Done();
    });
  });

  it('Get personas with invalid name length - over maximum size', (fetchPersonasMaxSizeDone) => {
    let accountsSubset = [accounts[0]];
    let personaJson = personasTestJson.personaWithLongName;
    let expectedPersona = new Persona(accounts[0], personaJson.name, personaJson.avatar, 100, true);

    return persistenceApi.addPersona(expectedPersona, {
      from: accounts[0]
    }).then((txHash) => {
      assert.match(txHash, /0[xX][0-9a-fA-F]+/, "txHash should be a hexadecimal value.");
      return persistenceApi.getPersonas(accountsSubset).then((personas) => {
        assert.isEmpty(personas, "getPersonas returned an array that is not empty");
        fetchPersonasMaxSizeDone();
      });
    });
  });

  it('Get personas with invalid name length - under minimum size', (fetchPersonasMinSizeDone) => {
    let accountsSubset = [accounts[0]];
    let personaJson = personasTestJson.persona1;
    let expectedPersona = new Persona(accounts[0], "ab", personaJson.avatar, 100, true);

    return persistenceApi.addPersona(expectedPersona, {
      from: accounts[0]
    }).then((txHash) => {
      assert.match(txHash, /0[xX][0-9a-fA-F]+/, "txHash should be a hexadecimal value.");
      return persistenceApi.getPersonas(accountsSubset).then((personas) => {
        assert.isEmpty(personas, "getPersonas returned an array that is not empty");
        fetchPersonasMinSizeDone();
      });
    });
  });

  it('Get personas with name with malicious content', (fetchPersonasMaliciousContentDone) => {
    let accountsSubset = [accounts[0]];
    let personaJson = personasTestJson.personaWithMaliciosContent;
    let expectedPersona = new Persona(accounts[0], personaJson.name, personaJson.avatar, 100, true);

    return persistenceApi.addPersona(expectedPersona, {
      from: accounts[0]
    }).then((txHash) => {
      assert.match(txHash, /0[xX][0-9a-fA-F]+/, "txHash should be a hexadecimal value.");
      return persistenceApi.getPersonas(accountsSubset).then((personas) => {
        assert.isEmpty(personas, "getPersonas returned an array that is not empty");
        fetchPersonasMaliciousContentDone();
      });
    });
  });

  it('Get personas with invalid avatar length - over maximum size', (fetchPersonasAvatarInvalidSizeDone) => {
    let accountsSubset = [accounts[0]];
    let personaJson = personasTestJson.persona1;
    // create avatar image larger than limit by one char
    let largeAvatar = '1';
    for (let i = 0; i < personaSchema.properties.avatar.maxLength; i++) {
      largeAvatar += 'a';
    }
    let expectedPersona = new Persona(accounts[0], personaJson.name, largeAvatar, 100, true);

    return persistenceApi.addPersona(expectedPersona, {
      from: accounts[0]
    }).then((txHash) => {
      assert.match(txHash, /0[xX][0-9a-fA-F]+/, "txHash should be a hexadecimal value.");
      return persistenceApi.getPersonas(accountsSubset).then((personas) => {
        assert.isEmpty(personas, "getPersonas returned an array that is not empty");
        fetchPersonasAvatarInvalidSizeDone();
      });
    });
  });

  it('Get personas with invalid avatar content - not base64 image', (fetchPersonasInvalidAvatarContentDone) => {
    let accountsSubset = [accounts[0]];
    let personaJson = personasTestJson.personaWithInvalidAvatarContent;
    let expectedPersona = new Persona(accounts[0], personaJson.name, personaJson.avatar, 100, true);

    return persistenceApi.addPersona(expectedPersona, {
      from: accounts[0]
    }).then((txHash) => {
      assert.match(txHash, /0[xX][0-9a-fA-F]+/, "txHash should be a hexadecimal value.");
      return persistenceApi.getPersonas(accountsSubset).then((personas) => {
        assert.isEmpty(personas, "getPersonas returned an array that is not empty");
        fetchPersonasInvalidAvatarContentDone();
      });
    });
  });

  it('Add persona when web3 is down', (addPersonaWeb3DownDone) => {
    setConfiguration("test_no_web3").then(() => {
      let personaJson1 = personasTestJson.persona1;
      let expectedPersona = new Persona(accounts[0], personaJson1.name, personaJson1.avatar, 100, true);

      return persistenceApi.addPersona(expectedPersona, {
        from: accounts[0]
      }).then(() => {
        assert.isTrue(false, "should not return anything");
      }).catch((err) => {
        assert.isTrue(err instanceof Web3Error, "should get instance of Web3Error");
        addPersonaWeb3DownDone();
      });
    });
  });

  it('Get personas when web3 is down', (fetchPersona2Web3DownDone) => {
    let personaJson1 = personasTestJson.persona1;
    let expectedPersona = new Persona(accounts[0], personaJson1.name, personaJson1.avatar, 100, true);

    return persistenceApi.addPersona(expectedPersona, {
      from: accounts[0]
    }).then((txHash) => {
      assert.match(txHash, /0[xX][0-9a-fA-F]+/, "txHash should be a hexadecimal value.");
      setConfiguration("test_no_web3").then(() => {
        let accountsSubset = [accounts[0]];
        return persistenceApi.getPersonas(accountsSubset).then(() => {
          assert.isTrue(false, "should not return anything");
        }).catch((err) => {
          assert.isTrue(err instanceof Web3Error, "should get instance of Web3Error");
          fetchPersona2Web3DownDone();
        });
      });
    });
  });

  describe('Persistence API unit tests with ipfs down', function () {
    this.timeout(120000);

    beforeEach((done) => {
      initConfigAndProviders().then(() => {
        done();
      }).catch((err) => {
        throw err;
      });
    });

    it('Add comment with ipfs down', (addCommentIpfsDownDone) => {
      let commentJson1 = commentsTestJson.comment1;
      setConfiguration("test_no_ipfs").then(() => {
        return persistenceApi.addComment(commentJson1, {
          from: accounts[0]
        }).then(() => {
          assert.isTrue(false, "should not return anything");
        }).catch((err) => {
          assert.isTrue(err instanceof IpfsError, "should get instance of IpfsError");
          addCommentIpfsDownDone();
        });
      });
    });

    it('Add persona when ipfs is down', (addPersonaIpfsDownDone) => {
      setConfiguration("test_no_ipfs").then(() => {
        let personaJson1 = personasTestJson.persona1;
        let expectedPersona = new Persona(accounts[0], personaJson1.name, personaJson1.avatar, 100, true);
        return persistenceApi.addPersona(expectedPersona, {
          from: accounts[0]
        }).then(() => {
          assert.isTrue(false, "should not return anything");
        }).catch((err) => {
          assert.isTrue(err instanceof IpfsError, "should get instance of IpfsError");
          addPersonaIpfsDownDone();
        });
      });
    });

    it('Fetch personas when ipfs is down', (fetchPersona2IpfsDownDone) => {
      let personaJson1 = personasTestJson.persona1;
      let expectedPersona = new Persona(accounts[0], personaJson1.name, personaJson1.avatar, 100, true);
      return persistenceApi.addPersona(expectedPersona, {
        from: accounts[0]
      }).then((txHash) => {
        assert.match(txHash, /0[xX][0-9a-fA-F]+/, "txHash should be a hexadecimal value.");
        turnOffIpfs().then(() => {
          let accountsSubset = [accounts[0]];
          return persistenceApi.getPersonas(accountsSubset).then(() => {
            assert.isTrue(false, "should not return anything");
          }).catch((err) => {
            assert.isTrue(err instanceof IpfsError, "should get instance of IpfsError");
            fetchPersona2IpfsDownDone();
          });
        });
      });
    });
  });
});
