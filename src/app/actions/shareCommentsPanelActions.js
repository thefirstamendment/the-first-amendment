import * as types from './actionTypes';

export function showSmallShareCommentPanel(showSmallShareCommentPanel) {
  return { type: types.SHOW_SMALL_SHARE_COMMENT_PANEL, showSmallShareCommentPanel: showSmallShareCommentPanel };
}
