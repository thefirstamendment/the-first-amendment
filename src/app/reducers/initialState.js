export default {
  displaySideBar: false,
  currentPersona: {},
  personas: [],
  accounts: [],
  personaManagementPanelVisible: false,
  comments: [],
  allComments: [],
  unconfirmedCommentsCount: 0,
  errorMessage: "",
  notificationMessage: "",
  commentsLoading: false,
  allCommentsLoading: false,
  showSmallShareCommentPanel: true
};

// TODO: personaManagementPanelVisible, commentsLoading and
// showSmallShareCommentPanel, isAddPersonaModalOpen, isEditPersonaModalOpen, isExploreCommentsModalOpen should not be part of the state?

// TODO: remove comments, we're going to rely on allComments and filter by url

// TODO: are there local and global error messages?
