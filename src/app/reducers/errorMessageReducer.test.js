import {expect} from 'chai';
import errorMessageReducer from './errorMessageReducer';
import * as actions from '../actions/errorActions';
import InputValidationError from '../../common/errors/InputValidationError';

describe("errorMessageReducer test", () => {
  it("should set error message when passed SHOW_ERROR", () => {
    const initState = "";
    const errorMessage = "Error happened";
    const action = actions.showError(new InputValidationError(errorMessage));
    const newState = errorMessageReducer(initState, action);

    expect(newState).to.equal(errorMessage);
  });

  it ("should remove error message when passed HIDE_ERROR", () => {
    const initState = "Error happened";
    const action = actions.hideError();
    const newState = errorMessageReducer(initState, action);

    expect(newState).to.be.empty;
  });
});
