import * as types from '../actions/actionTypes';
import initialState from './initialState';

export default function personaReducer(state = initialState.personas, action) {
  switch (action.type) {
    case types.SET_PERSONAS:
      return action.personas;

    case types.ADD_PERSONA_SUCCESS:
      if (state.find(persona => persona.account == action.persona.account)) {
        return [...state];
      } else {
        return [ // creating a deep copy of an array
          ...state,
          Object.assign({}, action.persona)
        ];
      }

    case types.UPDATE_PERSONA_SUCCESS:
      return [ // creating a deep copy of an array
        ...state.filter(persona => persona.account != action.persona.account),
        Object.assign({}, action.persona)
      ];

    default:
      return state;
  }
}
