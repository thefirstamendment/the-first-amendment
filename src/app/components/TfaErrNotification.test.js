import { expect } from 'chai';
import React from 'react';
import TfaErrNotification from './TfaErrNotification';
import sinon from 'sinon';
import chrome from 'sinon-chrome/extensions';
import { createMount } from '@material-ui/core/test-utils';
import { JSDOM } from 'jsdom';
import Enzyme from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

Enzyme.configure({ adapter: new Adapter() });

describe("<TfaErrNotification /> tests", () => {
  let mount;

  function setup(isOpen, errorMessage) {
    const props = {
      isOpen: isOpen,
      errorMessage: errorMessage,
      hideError: () => {}
    };
    return mount(<TfaErrNotification {...props} />);
  }

  before(() => {
    global.chrome = chrome;
    const jsdom = new JSDOM('<!doctype html><html><body></body></html>');
    const { window } = jsdom;
    global.window = window;
    global.document = window.document;
    mount = createMount();
  });

  after(() => {
    mount.cleanUp();
  });

  it("renders is open with error message", () => {
    const isOpen = true;
    const errorMessage = "Random error message";
    const wrapper = setup(isOpen, errorMessage);
    expect(wrapper.props().isOpen).to.be.true;
    expect(wrapper.props().errorMessage).to.equal(errorMessage);
    expect(wrapper.find("Snackbar").length).to.equal(1);
    expect(wrapper.find("Snackbar").props().open).to.equal(isOpen);
    expect(wrapper.find("Snackbar").props().message).to.exist;
    expect(wrapper.find("h3").length).to.equal(1);
    expect(wrapper.find("h3").text()).to.equal("Ooops, something went wrong");
    expect(wrapper.find("span").length).to.equal(1);
    expect(wrapper.find("span").text()).to.equal(errorMessage);
  });

  it("renders is closed with error message", () => {
    const isOpen = false;
    const errorMessage = "Random error message";
    const wrapper = setup(isOpen, errorMessage);
    expect(wrapper.props().isOpen).to.be.false;
    expect(wrapper.props().errorMessage).to.equal(errorMessage);
    expect(wrapper.find("Snackbar").length).to.equal(1);
    expect(wrapper.find("Snackbar").props().open).to.equal(isOpen);
    expect(wrapper.find("Snackbar").props().message).to.exist;
    expect(wrapper.find("h3").length).to.equal(0);
    expect(wrapper.find("span").length).to.equal(0);
  });

  it("simulates click on close button", () => {
    const hideError = sinon.spy();
    const isOpen = true;
    const errorMessage = "Random error message";
    const wrapper = mount(
      <TfaErrNotification
        isOpen={isOpen}
        errorMessage={errorMessage}
        hideError={hideError}
      />
    );
    wrapper.find("a").simulate("click");
    expect(hideError.calledOnce).to.be.true;
  });
});
