import React from 'react';
import PropTypes from 'prop-types';
import TfaShareCommentPanelLargeLeft from './TfaShareCommentPanelLargeLeft';
import TfaShareCommentPanelLargeRight from './TfaShareCommentPanelLargeRight';
import style from '../css/TfaShareCommentPanelLarge.scss';
import Utils from '../../../common/Utils';

const TfaShareCommentPanelLarge = (props) => {

  const personaImageAvatar = Utils.getAvatarImg(props.currentPersona);
  return (
    <div id="tfaShareCommentPanelLarge" className={style.tfa_share_comment_panel_large}>
      <div className={style.tfa_share_comment_panel_large_body}>
        <TfaShareCommentPanelLargeLeft personaImageAvatar={personaImageAvatar} />
        <TfaShareCommentPanelLargeRight
          addComment={props.addComment}
          handleSmallSharePanelVisible={props.handleSmallSharePanelVisible}
        />
      </div>
    </div>
  );
};

TfaShareCommentPanelLarge.propTypes = {
  currentPersona: PropTypes.object.isRequired,
  addComment: PropTypes.func.isRequired,
  handleSmallSharePanelVisible: PropTypes.func.isRequired
};

export default TfaShareCommentPanelLarge;
