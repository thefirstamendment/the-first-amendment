import {expect} from 'chai';
import Persona from './Persona';

describe('test Persona POJO', ()=> {
  it('should test getters', ()=> {
    let account = '0x01234567890123456789';
    let name = 'Kole Bole';
    let avatar = 'image-avatar-in-bynary';
    let persona = new Persona(account, name, avatar);
    expect(persona.account).to.equal(account);
    expect(persona.name).to.equal(name);
    expect(persona.avatar).to.equal(avatar);
  });
});
