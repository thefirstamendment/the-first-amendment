import { expect } from 'chai';
import React from 'react';
import TfaDisplayMessagePanel from './TfaDisplayMessagePanel';
import chrome from 'sinon-chrome/extensions';
import { createMount } from '@material-ui/core/test-utils';
import { JSDOM } from 'jsdom';
import Enzyme from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

Enzyme.configure({ adapter: new Adapter() });

describe("<TfaDisplayMessagePanel /> tests", () => {
  let mount;

  before(() => {
    global.chrome = chrome;
    const jsdom = new JSDOM('<!doctype html><html><body></body></html>');
    const { window } = jsdom;
    global.window = window;
    global.document = window.document;
    mount = createMount();
  });

  after(() => {
    mount.cleanUp();
  });

  it("renders expected message", () => {
    const expectedMessage = "Random message";
    const wrapper = mount(<TfaDisplayMessagePanel message={expectedMessage} />);
    expect(wrapper.props().message).to.equal(expectedMessage);
    expect(wrapper.find("Typography").length).to.equal(1);
    expect(wrapper.find("Typography").text()).to.equal(expectedMessage);
  });
});
