import * as types from '../actions/actionTypes';
import initialState from './initialState';

export default function unconfirmedCommentsCountReducer(state = initialState.unconfirmedCommentsCount, action) {
  switch (action.type) {
    case types.ADD_COMMENT_SUCCESS:
      if (state == Number.MAX_SAFE_INTEGER) {
        return 0;
      } else {
        return state + 1;
      }

    default:
      return state;
  }
}
