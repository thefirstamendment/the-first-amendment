import React from 'react';
import PropTypes from 'prop-types';
import TfaShareCommentPanelSmall from './TfaShareCommentPanelSmall';
import TfaShareCommentPanelLarge from './TfaShareCommentPanelLarge';
import TfaCommentOrderTabs from './TfaCommentOrderTabs';
import TfaDisplayMessagePanel from '../TfaDisplayMessagePanel';
import TfaCommentList from './TfaCommentList';
import TfaLoadingPanel from '../TfaLoadingPanel';

const TfaCommentsManagementPanel = (props) => {

  let shareCommentPanel = props.showSmallShareCommentPanel ?
    (
      <TfaShareCommentPanelSmall
        currentPersona={props.currentPersona}
        handleSmallSharePanelVisible={props.handleSmallSharePanelVisible}
      />
    ) :
    (
      <TfaShareCommentPanelLarge
        currentPersona={props.currentPersona}
        addComment={props.addComment}
        handleSmallSharePanelVisible={props.handleSmallSharePanelVisible}
      />
    );
  let commentsContent;
  if (props.commentsLoading) {
    commentsContent = (<TfaLoadingPanel />);
  } else if (props.comments.length > 0) {
    commentsContent = (
      <TfaCommentList
        comments={props.comments}
        upvoteComment={props.upvoteComment}
        downvoteComment={props.downvoteComment}
        showUrl={false}
      />
    );
  } else {
    commentsContent = (<TfaDisplayMessagePanel message="Currently no comments for this url..." />);
  }

  return (
    <div>
      {shareCommentPanel}
      <TfaCommentOrderTabs
        commentOrdering={props.commentOrdering}
        selectCommentOrdering={props.selectCommentOrdering}
      />
      {commentsContent}
    </div>
  );
};


TfaCommentsManagementPanel.propTypes = {
  showSmallShareCommentPanel: PropTypes.bool.isRequired,
  currentPersona: PropTypes.object.isRequired,
  comments: PropTypes.array.isRequired,
  addComment: PropTypes.func.isRequired,
  selectCommentOrdering: PropTypes.func.isRequired,
  commentOrdering: PropTypes.number.isRequired,
  upvoteComment: PropTypes.func.isRequired,
  downvoteComment: PropTypes.func.isRequired,
  commentsLoading: PropTypes.bool.isRequired,
  handleSmallSharePanelVisible: PropTypes.func.isRequired
};

export default TfaCommentsManagementPanel;
