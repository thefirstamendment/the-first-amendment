import { expect } from 'chai';
import React from 'react';
import TfaEditPersonaModal from './TfaEditPersonaModal';
import sinon from 'sinon';
import chrome from 'sinon-chrome/extensions';
import Utils from '../../../common/Utils';
import { createMount } from '@material-ui/core/test-utils';
import { JSDOM } from 'jsdom';
import Enzyme from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import personaSchema from '../../../common/schema/PersonaSchema.json';
import personasTestJson from '../../../../test/integration/test_personas.json';
import Persona from '../../../common/Persona';

Enzyme.configure({ adapter: new Adapter() });

function generateAccounts(accountCount) {
  const accounts = [];
  for (let i = 0; i < accountCount; i++) {
    accounts.push("account" + i);
  }
  return accounts;
}

describe("<TfaEditPersonaModal /> tests", () => {
  let mount;
  let getDefaultAvatarImgStub;

  function setup(
    editPersonaModalIsOpen,
    currentPersona,
    closeEditPersonaModal = () => { },
    updatePersona = () => { },
    showError = () => { }
  ) {
    const props = {
      editPersonaModalIsOpen: editPersonaModalIsOpen,
      currentPersona: currentPersona,
      closeEditPersonaModal: closeEditPersonaModal,
      updatePersona: updatePersona,
      showError: showError
    };
    return mount(<TfaEditPersonaModal {...props} />);
  }

  before(() => {
    global.chrome = chrome;
    const jsdom = new JSDOM('<!doctype html><html><body></body></html>');
    const { window } = jsdom;
    global.window = window;
    global.document = window.document;
    getDefaultAvatarImgStub = sinon.stub(Utils, "getDefaultAvatarImg");
    getDefaultAvatarImgStub.returns("defaultAvatarImg");
    mount = createMount();
  });

  after(() => {
    mount.cleanUp();
    getDefaultAvatarImgStub.restore();
  });

  it("renders open edit persona modal with current persona with no avatar", () => {
    const currentPersona = new Persona("account", personasTestJson.persona1.name, "", 0);
    const expectedAvatarImg = Utils.getAvatarImg({});
    const wrapper = setup(true, currentPersona);
    expect(wrapper.props().editPersonaModalIsOpen).to.be.true;
    expect(wrapper.props().currentPersona).to.equal(currentPersona);
    expect(wrapper.props().closeEditPersonaModal).to.exist;
    expect(wrapper.props().updatePersona).to.exist;
    expect(wrapper.find("Dialog").length).to.equal(1);
    expect(wrapper.find("Dialog").props().disableRestoreFocus).to.exist;
    expect(wrapper.find("Dialog").props().open).to.be.true;
    expect(wrapper.find("Dialog").props().onClose).to.exist;
    expect(wrapper.find("DialogTitle").length).to.equal(1);
    expect(wrapper.find("DialogTitle").text()).to.equal("Edit Persona");
    expect(wrapper.find("DialogContent").length).to.equal(1);
    expect(wrapper.find("DialogActions").length).to.equal(1);
    expect(wrapper.find("Button").length).to.equal(2);
    expect(wrapper.find("Button").at(0).props().disabled).to.exist;
    expect(wrapper.find("Button").at(1).props().color).to.equal("default");
    expect(wrapper.find("Button").at(1).props().onClick).to.exist;
    expect(wrapper.find("FormControl").length).to.equal(4);
    expect(wrapper.find("TfaAvatarDragAndDrop").length).to.equal(1);
    expect(wrapper.find("TfaAvatarDragAndDrop").props().avatar).to.not.be.undefined;
    expect(wrapper.find("TfaAvatarDragAndDrop").props().avatar).to.not.equal("");
    expect(wrapper.find("TfaAvatarDragAndDrop").props().avatar).to.equal(expectedAvatarImg);
    expect(wrapper.find("TfaAvatarDragAndDrop").props().setAvatar).to.exist;
    expect(wrapper.find("TfaAvatarDragAndDrop").props().showError).to.exist;
    expect(wrapper.find("TextField").length).to.equal(1);
    expect(wrapper.find("TextField").props().fullWidth).to.exist;
    expect(wrapper.find("TextField").props().label).to.equal("Username");
    expect(wrapper.find("TextField").props().margin).to.equal("normal");
    expect(wrapper.find("TextField").props().value).to.equal(personasTestJson.persona1.name);
    expect(wrapper.find("TextField").props().onChange).to.exist;
    expect(wrapper.find("TextField").props().inputProps.maxLength).to.equal(personaSchema.properties.name.maxLength);
    expect(wrapper.find("InputLabel").length).to.equal(2);
    expect(wrapper.find("InputLabel").at(1).text()).to.equal("Account");
    expect(wrapper.find("Select").length).to.equal(1);
    expect(wrapper.find("Select").props().native).to.exist;
    expect(wrapper.find("Select").props().disabled).to.exist;
    expect(wrapper.find("option").length).to.equal(1);
    expect(wrapper.find("InputAdornment").length).to.equal(1);
    expect(wrapper.find("InputAdornment").props().position).to.equal("end");
    expect(wrapper.find("Tooltip").length).to.equal(2);
  });

  it("renders open edit persona modal with current persona with avatar", () => {
    const currentPersona = new Persona("account", personasTestJson.persona1.name, personasTestJson.persona1.avatar, 0);
    const wrapper = setup(true, currentPersona);
    expect(wrapper.props().editPersonaModalIsOpen).to.be.true;
    expect(wrapper.props().currentPersona).to.equal(currentPersona);
    expect(wrapper.props().closeEditPersonaModal).to.exist;
    expect(wrapper.props().updatePersona).to.exist;
    expect(wrapper.find("Dialog").length).to.equal(1);
    expect(wrapper.find("Dialog").props().disableRestoreFocus).to.exist;
    expect(wrapper.find("Dialog").props().open).to.be.true;
    expect(wrapper.find("Dialog").props().onClose).to.exist;
    expect(wrapper.find("DialogTitle").length).to.equal(1);
    expect(wrapper.find("DialogTitle").text()).to.equal("Edit Persona");
    expect(wrapper.find("DialogContent").length).to.equal(1);
    expect(wrapper.find("DialogActions").length).to.equal(1);
    expect(wrapper.find("Button").length).to.equal(2);
    expect(wrapper.find("Button").at(0).props().disabled).to.exist;
    expect(wrapper.find("Button").at(1).props().color).to.equal("default");
    expect(wrapper.find("Button").at(1).props().onClick).to.exist;
    expect(wrapper.find("FormControl").length).to.equal(4);
    expect(wrapper.find("TfaAvatarDragAndDrop").length).to.equal(1);
    expect(wrapper.find("TfaAvatarDragAndDrop").props().avatar).to.equal(personasTestJson.persona1.avatar);
    expect(wrapper.find("TfaAvatarDragAndDrop").props().setAvatar).to.exist;
    expect(wrapper.find("TfaAvatarDragAndDrop").props().showError).to.exist;
    expect(wrapper.find("TextField").length).to.equal(1);
    expect(wrapper.find("TextField").props().fullWidth).to.exist;
    expect(wrapper.find("TextField").props().label).to.equal("Username");
    expect(wrapper.find("TextField").props().margin).to.equal("normal");
    expect(wrapper.find("TextField").props().value).to.equal(personasTestJson.persona1.name);
    expect(wrapper.find("TextField").props().onChange).to.exist;
    expect(wrapper.find("TextField").props().inputProps.maxLength).to.equal(personaSchema.properties.name.maxLength);
    expect(wrapper.find("InputLabel").length).to.equal(2);
    expect(wrapper.find("InputLabel").at(1).text()).to.equal("Account");
    expect(wrapper.find("Select").length).to.equal(1);
    expect(wrapper.find("Select").props().native).to.exist;
    expect(wrapper.find("Select").props().disabled).to.exist;
    expect(wrapper.find("option").length).to.equal(1);
    expect(wrapper.find("InputAdornment").length).to.equal(1);
    expect(wrapper.find("InputAdornment").props().position).to.equal("end");
    expect(wrapper.find("Tooltip").length).to.equal(2);
  });

  it("simulates click on close button", () => {
    const accounts = generateAccounts(3);
    const closeEditPersonaModal = sinon.spy();
    const wrapper = setup(true, accounts, closeEditPersonaModal);
    wrapper.find("Button").at(1).simulate("click");
    expect(closeEditPersonaModal.calledOnce).to.be.true;
  });

  it("simulates click on add persona button disabled", () => {
    const accounts = generateAccounts(3);
    const closeEditPersonaModal = sinon.spy();
    const updatePersona = sinon.spy();
    const wrapper = setup(true, accounts, closeEditPersonaModal, updatePersona);
    wrapper.find("Button").at(0).simulate("click");
    expect(updatePersona.calledOnce).to.be.false;
  });

  it("simulates click on add persona button enabled", () => {
    const accounts = generateAccounts(3);
    const closeEditPersonaModal = sinon.spy();
    const updatePersona = sinon.spy();
    const wrapper = setup(true, accounts, closeEditPersonaModal, updatePersona);
    wrapper.find("TextField").props().onChange({target: {value: "username"}});
    wrapper.find("Button").at(0).simulate("click");
    expect(updatePersona.calledOnce).to.be.true;
  });
});
