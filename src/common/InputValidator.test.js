import {
  expect
} from 'chai';
import InputValidator from './InputValidator';
import {
  JSDOM
} from 'jsdom';
import personaSchema from './schema/PersonaSchema.json';
import commentSchema from './schema/CommentSchema.json';

let inputValidator;
let scripts = [
  '<div>MyNick</div>', // plain html
  '<h2>Some header</h2>', // header
  '<!-- Blah blah -->Whee', // html comments
  '<script>alert("ruhroh!");</script>', // just js
  '<a href="java&#0000001script:alert(\'foo\')">Hax</a>', //character codes 1-32
  '<a href="java&#0000000script:alert(\'foo\')">Hax</a>', //character codes 1-32
  '<p a', // bad markup
  '<<img src="javascript:evil"/>img src="javascript:evil"/>', // double <
  '!<__proto__>!', // universal Object prototype
  '<a name="&lt;silly&gt;">&lt;Kapow!&gt;</a>', // silly elements
  '<a href="&#106;&#97;&#118;&#97;&#115;&#99;&#114;&#105;&#112;&#116;&#58;&#97;&#108;&#101;&#114;&#116;&#40;&#39;&#88;&#83;&#83;&#39;&#41;">Hax</a>', //  sneaky encoded javascript url
  '<a href="JAVASCRIPT:alert(\'foo\')">Hax</a>', // uppercase javascript url
  '<a href="java<!-- -->script:alert(\'foo\')">Hax</a>', // comment in the middle
  '<a href="java\0&#14;\t\r\n script:alert(\'foo\')">Hax</a>', // character codes 1-32
  '<img src="fallback.jpg" srcset="foo.jpg 100w 2x, bar.jpg 200w 1x" />', // srcset
  '<img src="fallback.jpg" srcset="foo.jpg 100w 2x, bar.jpg 200w 1x, javascript:alert(1) 100w 2x" />', // bogus srcset
  '<span data-<script>alert(1)//>', // metachars
  '<p dir="ltr"><strong>beste</strong><em>testestes</em><s>testestset</s><u>testestest</u></p><ul dir="ltr"> <li><u>test</u></li></ul><blockquote dir="ltr"> <ol> <li><u>​test</u></li><li><u>test</u></li><li style="text-align: right;"><u>test</u></li><li style="text-align: justify;"><u>test</u></li></ol> <p><u><span style="color:#00FF00;">test</span></u></p><p><span style="color:#00FF00"><span style="font-size:36px;">TESTETESTESTES</span></span></p></blockquote>', // styles
];

describe('test InputValidator', () => {
  before(() => {
    inputValidator = new InputValidator((new JSDOM('')).window);
  });

  /************************* is valid persona object  ******************/

  it('is valid persona object - should allow valid json', () => {
    let persona1 = { name: "NickJoe", avatar: "data:image/jpeg;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HAwCAAAAC0lEQVR42mNkYAAAAAYAAjCB0C8AAAAASUVORK5CYII=" };
    expect(inputValidator.isValidPersonaObject(persona1)).to.equal(true);
    let persona2 = { name: "NickJoe", avatar: "" };
    expect(inputValidator.isValidPersonaObject(persona2)).to.equal(true);
  });

  it('is valid persona object - should allow json with extra properties', () => {
    let persona1 = { name: "name", avatar: "data:image/jpeg;base64,1234", extra: "extra property" };
    expect(inputValidator.isValidPersonaObject(persona1)).to.equal(true);
  });

  it('is valid persona object - should not allow null, undefined or non-object argument', () => {
    expect(inputValidator.isValidPersonaObject(null)).to.equal(false);
    expect(inputValidator.isValidPersonaObject(undefined)).to.equal(false);
    expect(inputValidator.isValidPersonaObject("stringy")).to.equal(false);
    expect(inputValidator.isValidPersonaObject('')).to.equal(false);
    expect(inputValidator.isValidPersonaObject(' ')).to.equal(false);
    expect(inputValidator.isValidPersonaObject(true)).to.equal(false);
    expect(inputValidator.isValidPersonaObject(false)).to.equal(false);
    expect(inputValidator.isValidPersonaObject(55)).to.equal(false);
  });

  it('is valid persona object - should not allow json without required properties', () => {
    let persona1 = { name: "Nick" };
    let persona2 = { avatar: "" };
    let persona3 = {};
    expect(inputValidator.isValidPersonaObject(persona1)).to.equal(false);
    expect(inputValidator.isValidPersonaObject(persona2)).to.equal(false);
    expect(inputValidator.isValidPersonaObject(persona3)).to.equal(false);
  });

  it('is valid persona object - should not allow json with wrong properties', () => {
    let persona1 = { name2: "name", avatar: "" };
    let persona2 = { name: "name", avatar2: "" };
    expect(inputValidator.isValidPersonaObject(persona1)).to.equal(false);
    expect(inputValidator.isValidPersonaObject(persona2)).to.equal(false);
  });

  it('is valid persona object - should not allow null, undefined, empty or non-string name', () => {
    let persona1 = { name: null, avatar: "" };
    let persona2 = { name: undefined, avatar: "" };
    let persona3 = { name: "", avatar: "" };
    let persona4 = { name: true, avatar: "" };
    let persona5 = { name: 55, avatar: "" };
    expect(inputValidator.isValidPersonaObject(persona1)).to.equal(false);
    expect(inputValidator.isValidPersonaObject(persona2)).to.equal(false);
    expect(inputValidator.isValidPersonaObject(persona3)).to.equal(false);
    expect(inputValidator.isValidPersonaObject(persona4)).to.equal(false);
    expect(inputValidator.isValidPersonaObject(persona5)).to.equal(false);
  });

  it('is valid persona object - should not allow null, undefined, empty or non-string avatar', () => {
    let persona1 = { name: "Nick", avatar: null };
    let persona2 = { name: "Nick", avatar: undefined };
    let persona3 = { name: "Nick", avatar: true };
    let persona4 = { name: "Nick", avatar: 55 };
    expect(inputValidator.isValidPersonaObject(persona1)).to.equal(false);
    expect(inputValidator.isValidPersonaObject(persona2)).to.equal(false);
    expect(inputValidator.isValidPersonaObject(persona3)).to.equal(false);
    expect(inputValidator.isValidPersonaObject(persona4)).to.equal(false);
  });

  it('is valid persona object - should not allow name shorter/longer than min/max length', () => {
    let persona1 = { name: "as", avatar: "" };
    let persona2 = { name: "123456789012345678901234567890123", avatar: "" };
    expect(inputValidator.isValidPersonaObject(persona1)).to.equal(false);
    expect(inputValidator.isValidPersonaObject(persona2)).to.equal(false);
  });

  it('is valid persona object - should not allow avatar longer than max length', () => {
    let largeAvatar = 'data:image/jpeg;base64,';
    for (let i = 0; i < personaSchema.properties.avatar.maxLength - 22; i++) {
      largeAvatar += 'a';
    }
    let persona1 = { name: "as", avatar: largeAvatar };
    expect(inputValidator.isValidPersonaObject(persona1)).to.equal(false);
  });

  /************************* is valid comment object  ******************/

  it('is valid comment object - should allow valid json', () => {
    let comment = { content: "Comment number one!", url: "https://www.example.com" };
    expect(inputValidator.isValidCommentObject(comment)).to.equal(true);
  });

  it('is valid comment object - should allow json with extra properties', () => {
    let comment1 = { content: "Comment number one!", url: "https://www.example.com", extra: "extra property" };
    expect(inputValidator.isValidCommentObject(comment1)).to.equal(true);
  });

  it('is valid comment object - should not allow null, undefined or non-object argument', () => {
    expect(inputValidator.isValidCommentObject(null)).to.equal(false);
    expect(inputValidator.isValidCommentObject(undefined)).to.equal(false);
    expect(inputValidator.isValidCommentObject("stringy")).to.equal(false);
    expect(inputValidator.isValidCommentObject('')).to.equal(false);
    expect(inputValidator.isValidCommentObject(' ')).to.equal(false);
    expect(inputValidator.isValidCommentObject(true)).to.equal(false);
    expect(inputValidator.isValidCommentObject(false)).to.equal(false);
    expect(inputValidator.isValidCommentObject(55)).to.equal(false);
  });

  it('is valid comment object - should not allow json without required properties', () => {
    let comment1 = { content: "comment content" };
    let comment2 = { url: "https://www.example.com" };
    let comment3 = {};
    expect(inputValidator.isValidCommentObject(comment1)).to.equal(false);
    expect(inputValidator.isValidCommentObject(comment2)).to.equal(false);
    expect(inputValidator.isValidCommentObject(comment3)).to.equal(false);
  });

  it('is valid comment object - should not allow json with wrong properties', () => {
    let comment1 = { content2: "Comment number one!", url: "https://www.example.com" };
    let comment2 = { content: "Comment number one!", url2: "https://www.example.com" };
    expect(inputValidator.isValidCommentObject(comment1)).to.equal(false);
    expect(inputValidator.isValidCommentObject(comment2)).to.equal(false);
  });

  it('is valid comment object - should not allow null, undefined, empty or non-string content', () => {
    let comment1 = { content: null, url: "https://www.example.com" };
    let comment2 = { content: undefined, url: "https://www.example.com" };
    let comment3 = { content: "", url: "https://www.example.com" };
    let comment4 = { content: true, url: "https://www.example.com" };
    let comment5 = { content: 55, url: "https://www.example.com" };
    expect(inputValidator.isValidCommentObject(comment1)).to.equal(false);
    expect(inputValidator.isValidCommentObject(comment2)).to.equal(false);
    expect(inputValidator.isValidCommentObject(comment3)).to.equal(false);
    expect(inputValidator.isValidCommentObject(comment4)).to.equal(false);
    expect(inputValidator.isValidCommentObject(comment5)).to.equal(false);
  });

  it('is valid comment object - should not allow null, undefined, empty or non-string url', () => {
    let comment1 = { content: "content", url: null };
    let comment2 = { content: "content", url: undefined };
    let comment3 = { content: "content", url: "" };
    let comment4 = { content: "content", url: true };
    let comment5 = { content: "content", url: 55 };
    expect(inputValidator.isValidCommentObject(comment1)).to.equal(false);
    expect(inputValidator.isValidCommentObject(comment2)).to.equal(false);
    expect(inputValidator.isValidCommentObject(comment3)).to.equal(false);
    expect(inputValidator.isValidCommentObject(comment4)).to.equal(false);
    expect(inputValidator.isValidCommentObject(comment5)).to.equal(false);
  });

  it('is valid comment object - should not allow content shorter/longer than min/max length', () => {
    let comment1 = { content: "", url: "https://www.example.com" };
    let largeComment = '1';
    for (let i = 0; i < commentSchema.properties.content.maxLength; i++) {
      largeComment += 'a';
    }
    let comment2 = { content: largeComment, url: "https://www.example.com" };
    expect(inputValidator.isValidCommentObject(comment1)).to.equal(false);
    expect(inputValidator.isValidCommentObject(comment2)).to.equal(false);
  });

  it('is valid comment object - should not allow url shorter/longer than min/max length', () => {
    let comment1 = { content: "Comment", url: "http:/" };
    let largeUrl = 'https://www.example.com/';
    for (let i = 0; i < commentSchema.properties.url.maxLength - 23; i++) {
      largeUrl += 'a';
    }
    let comment2 = { content: "Comment", url: largeUrl };
    expect(inputValidator.isValidCommentObject(comment1)).to.equal(false);
    expect(inputValidator.isValidCommentObject(comment2)).to.equal(false);
  });

  /************************* is valid comment content  ******************/

  it('is valid comment content - should allow text', () => {
    expect(inputValidator.isCommentContentValid('This is my comment.')).to.equal(true);
    expect(inputValidator.isCommentContentValid('a')).to.equal(true);
    expect(inputValidator.isCommentContentValid('.')).to.equal(true);
    expect(inputValidator.isCommentContentValid('<')).to.equal(true);
    expect(inputValidator.isCommentContentValid('http')).to.equal(true);
    expect(inputValidator.isCommentContentValid('2 + 2 < 5')).to.equal(true);
  });

  it('is valid comment content - should allow emails and plain text urls', () => {
    expect(inputValidator.isCommentContentValid('nick@mail.com')).to.equal(true);
    expect(inputValidator.isCommentContentValid('http://www.example.com')).to.equal(true);
  });

  it('is valid comment content - should not allow null/undefined, empty, or non-string', () => {
    expect(inputValidator.isCommentContentValid(null)).to.equal(false);
    expect(inputValidator.isCommentContentValid(undefined)).to.equal(false);
    expect(inputValidator.isCommentContentValid('')).to.equal(false);
    expect(inputValidator.isCommentContentValid(' ')).to.equal(false);
    expect(inputValidator.isCommentContentValid(true)).to.equal(false);
    expect(inputValidator.isCommentContentValid(55)).to.equal(false);
  });

  it('is valid comment content - should not allow content shorter than min length', () => {
    expect(inputValidator.isCommentContentValid("")).to.equal(false);
    expect(inputValidator.isCommentContentValid('1')).to.equal(true);
  });

  it('is valid comment content - should not allow content longer than max length', () => {
    let longContent = '';
    for (let i = 0; i < commentSchema.properties.content.maxLength; i++) {
      longContent += 'a';
    }
    expect(inputValidator.isCommentContentValid(longContent)).to.equal(true);
    expect(inputValidator.isCommentContentValid(longContent + '1')).to.equal(false);
  });

  it('is valid comment content - should not allow whitespace at the beggining and the end', () => {
    expect(inputValidator.isCommentContentValid(' some text ')).to.equal(false);
  });

  it('is valid comment content - should not allow the content of script elements', () => {
    for (let i = 0; i < scripts.length; i++) {
      expect(inputValidator.isCommentContentValid(scripts[i])).to.equal(false);
    }
  });

  /************************* is valid comment url  ******************/

  it('is valid comment url - should allow normal urls including ip address', () => {
    expect(inputValidator.isCommentUrlValid("https://accounts.google.com/ServiceLogin/signinchooser?service=mail&passive=true&rm=false&continue=https%3A%2F%2Fmail.google.com%2Fmail%2F&ss=1&scc=1&ltmpl=default&ltmplcache=2&emr=1&osid=1&flowName=GlifWebSignIn&flowEntry=ServiceLogin")).to.equal(true);
    expect(inputValidator.isCommentUrlValid("https://www.youtube.com/embed/c2IlcS7AHxM")).to.equal(true);
    expect(inputValidator.isCommentUrlValid("https://trello.com/c/BXfav8pL/28-5-a-validation-of-input-parameters")).to.equal(true);
    expect(inputValidator.isCommentUrlValid("http://www.bbc.com/news/av/entertainment-arts-44049177/rock-star-serj-tankian-on-armenia-s-unique-revolution")).to.equal(true);
    expect(inputValidator.isCommentUrlValid("http://192.168.0.1/")).to.equal(true);
    expect(inputValidator.isCommentUrlValid("http://localhost/")).to.equal(true);
  });

  it('is valid comment url - should not allow null/undefined, empty, or non-string', () => {
    expect(inputValidator.isCommentUrlValid(null)).to.equal(false);
    expect(inputValidator.isCommentUrlValid(undefined)).to.equal(false);
    expect(inputValidator.isCommentUrlValid('')).to.equal(false);
    expect(inputValidator.isCommentUrlValid(' ')).to.equal(false);
    expect(inputValidator.isCommentUrlValid(true)).to.equal(false);
    expect(inputValidator.isCommentUrlValid(55)).to.equal(false);
  });

  it('is valid comment url - should not allow urls with protocols other than http and https', () => {
    expect(inputValidator.isCommentUrlValid("mailto:deb@empirerecords.biz")).to.equal(false);
    expect(inputValidator.isCommentUrlValid("ftp://billgates:moremoney@files.microsoft.com/special/secretplans")).to.equal(false);
    expect(inputValidator.isCommentUrlValid("imap://localhost/inbox/sysadmin")).to.equal(false);
    expect(inputValidator.isCommentUrlValid("ssh://username@hostname://path")).to.equal(false);
    expect(inputValidator.isCommentUrlValid("httpss://www.youtube.com/embed/c2IlcS7AHxM")).to.equal(false);
    expect(inputValidator.isCommentUrlValid("htps://www.youtube.com/embed/c2IlcS7AHxM")).to.equal(false);
  });

  it('is valid comment url - should not allow urls with invalid formatting', () => {
    expect(inputValidator.isCommentUrlValid("http:/192.168.0.1/")).to.equal(false);
    expect(inputValidator.isCommentUrlValid("192.168.0.1")).to.equal(false);

    expect(inputValidator.isCommentUrlValid("http")).to.equal(false);
    expect(inputValidator.isCommentUrlValid("http:")).to.equal(false);
    expect(inputValidator.isCommentUrlValid("http://")).to.equal(false);
    expect(inputValidator.isCommentUrlValid("https:www.richardsonnen.com")).to.equal(false);
    expect(inputValidator.isCommentUrlValid("http:www.richardsonnen.com")).to.equal(false);

    // TODO: this should be invalid too, should be 0-255
    //expect(inputValidator.isCommentUrlValid("http://256.168.0.1/")).to.equal(false);
  });

  it('is valid comment url - should not allow url shorter than min length', () => {
    expect(inputValidator.isCommentUrlValid("http://")).to.equal(false);
    expect(inputValidator.isCommentUrlValid('http://a')).to.equal(true);
  });

  it('is valid comment url - should not allow url longer than max length', () => {
    let longUrl = 'http://t.co/';
    for (let i = 0; i < commentSchema.properties.url.maxLength - 12; i++) {
      longUrl += 'a';
    }
    expect(inputValidator.isCommentUrlValid(longUrl)).to.equal(true);
    expect(inputValidator.isCommentUrlValid(longUrl + '1')).to.equal(false);
  });

  it('is valid comment url - should not allow the content of script elements', () => {
    for (let i = 0; i < scripts.length; i++) {
      expect(inputValidator.isCommentUrlValid(scripts[i])).to.equal(false);
    }
  });

  /************************* is valid persona name  ******************/

  it('is valid persona name - should allow alpha numeric text', () => {
    expect(inputValidator.isPersonaNameValid('MyNick')).to.equal(true);
    expect(inputValidator.isPersonaNameValid('Joe')).to.equal(true);
    expect(inputValidator.isPersonaNameValid('123')).to.equal(true);
    expect(inputValidator.isPersonaNameValid('NICK124')).to.equal(true);
  });

  it('is valid persona name - should allow hyphen in name', () => {
    expect(inputValidator.isPersonaNameValid('My-Ni-ck')).to.equal(true);
    expect(inputValidator.isPersonaNameValid('My-Nick')).to.equal(true);
  });

  it('is valid persona name - should not allow null/undefined, empty, or non-string', () => {
    expect(inputValidator.isPersonaNameValid(null)).to.equal(false);
    expect(inputValidator.isPersonaNameValid(undefined)).to.equal(false);
    expect(inputValidator.isPersonaNameValid('')).to.equal(false);
    expect(inputValidator.isPersonaNameValid(' ')).to.equal(false);
    expect(inputValidator.isPersonaNameValid(true)).to.equal(false);
    expect(inputValidator.isPersonaNameValid(55)).to.equal(false);
  });

  it('is valid persona name - should not allow name shorter than min length', () => {
    let shortName = '';
    for (let i = 0; i < personaSchema.properties.name.minLength - 1; i++) {
      shortName += 'a';
    }
    expect(inputValidator.isPersonaNameValid(shortName)).to.equal(false);
    expect(inputValidator.isPersonaNameValid(shortName + '1')).to.equal(true);
  });

  it('is valid persona name - should not allow name longer than max length', () => {
    let longName = '';
    for (let i = 0; i < personaSchema.properties.name.maxLength; i++) {
      longName += 'a';
    }
    expect(inputValidator.isPersonaNameValid(longName)).to.equal(true);
    expect(inputValidator.isPersonaNameValid(longName + '1')).to.equal(false);
  });

  it('is valid persona name - should not allow emails', () => {
    expect(inputValidator.isPersonaNameValid('nick@mail.com')).to.equal(false);
  });

  it('is valid persona name - should not allow any html tags', () => {
    expect(inputValidator.isPersonaNameValid('<div>MyNick</div>')).to.equal(false);
    expect(inputValidator.isPersonaNameValid('<h2>MyNick</h2>')).to.equal(false);
  });

  it('is valid persona name - should not allow plain text url', () => {
    expect(inputValidator.isPersonaNameValid('http://www.example.com  ')).to.equal(false);
  });

  it('is valid persona name - should not allow punctuation in name', () => {
    expect(inputValidator.isPersonaNameValid('MyNick,')).to.equal(false);
    expect(inputValidator.isPersonaNameValid('My,Nick')).to.equal(false);
    expect(inputValidator.isPersonaNameValid('MyNick;')).to.equal(false);
    expect(inputValidator.isPersonaNameValid('My;Nick')).to.equal(false);
    expect(inputValidator.isPersonaNameValid('MyNick!')).to.equal(false);
    expect(inputValidator.isPersonaNameValid('MyNick?')).to.equal(false);
    expect(inputValidator.isPersonaNameValid('MyNick.')).to.equal(false);
    expect(inputValidator.isPersonaNameValid('.MyNick.')).to.equal(false);
    expect(inputValidator.isPersonaNameValid('My.Nick')).to.equal(false);
    expect(inputValidator.isPersonaNameValid('My Nick')).to.equal(false);
    expect(inputValidator.isPersonaNameValid('MyNick ')).to.equal(false);
    expect(inputValidator.isPersonaNameValid(' MyNick ')).to.equal(false);
  });

  it('is valid persona name - should not allow hyphen on the beggining or the end', () => {
    expect(inputValidator.isPersonaNameValid('MyNick-')).to.equal(false);
    expect(inputValidator.isPersonaNameValid('-MyNick')).to.equal(false);
  });

  it('is valid persona name - should not multiple hyphens', () => {
    expect(inputValidator.isPersonaNameValid('My--Nick')).to.equal(false);
    expect(inputValidator.isPersonaNameValid('My---Nick')).to.equal(false);
    expect(inputValidator.isPersonaNameValid('My--------Nick')).to.equal(false);
  });

  it('is valid persona name - should not allow only hyphens in name', () => {
    expect(inputValidator.isPersonaNameValid('--')).to.equal(false);
    expect(inputValidator.isPersonaNameValid('---')).to.equal(false);
    expect(inputValidator.isPersonaNameValid('----')).to.equal(false);
  });

  it('is valid persona name - should not allow the content of json', () => {
    expect(inputValidator.isPersonaNameValid('{ nonTextTags: fibble}')).to.equal(false);
  });

  it('is valid persona name - should not allow html comments', () => {
    expect(inputValidator.isPersonaNameValid('<!-- Blah blah -->Whee')).to.equal(false);
  });

  it('is valid persona name - should not allow the content of script elements', () => {
    for (let i = 0; i < scripts.length; i++) {
      expect(inputValidator.isPersonaNameValid(scripts[i])).to.equal(false);
    }
  });

  /************************* is valid persona avatar  ******************/

  it('is valid persona avatar - should allow jpeg and png text, or empty string (default avatar)', () => {
    // transparent 1x1 png
    expect(inputValidator.isPersonaAvatarValid('data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HAwCAAAAC0lEQVR42mNkYAAAAAYAAjCB0C8AAAAASUVORK5CYII=')).to.equal(true);

    // charlize.jpeg
    expect(inputValidator.isPersonaAvatarValid('data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBxMTEhUSEhIWFRUWFxcVFRcXFRcXFxcXFRUXFxcXGBUYHSggGBolHRUVITEhJSkrLi4uFx8zODMtNygtLisBCgoKDg0OGxAQGi0lHyUtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLf/AABEIARMAtwMBIgACEQEDEQH/xAAbAAABBQEBAAAAAAAAAAAAAAAFAAIDBAYBB//EAD8QAAECBAQDBQcBBgYCAwAAAAEAAgMEESEFEjFBIlFhBhNxgZEyQqGxwdHw4RQjM2KCshVScpLC8aLSQ2Nz/8QAGQEAAgMBAAAAAAAAAAAAAAAAAQQAAgMF/8QAJBEAAgICAgEEAwEAAAAAAAAAAAECEQMhEjFBBCIyURMzcWH/2gAMAwEAAhEDEQA/APQMYl2N7sBgu4BETh7KZsosENjz8KM1pzCxr6J8PtFBLjCzCtFtTKA+FibC4tLKcVB1RSewoOyEAa3VMSMJpLy7eoU+I4q0BlH0uFCEceSAjNblFwli0sIbS6gKZHxWGYrHZhbqp5jEYTq1cKU5o7ASSmHhzGnKNFFJSTe9eABZStxyE1o4h6pkLF4IcXZhfqhsgydhd29opUF1EQjy4Ow0QyNjEEmpcDeuqjmO0sJpHENOf0UYTEdpYIbGcWi1VdwOMCxLGo7Iri5gJryB+qoyeaGDwup4V+SH5I/ZOD+jPdrX1iUUWEQbVXMaDi8kgq7hreBFbARxmpnc7qd2qUR9lYBXDUgxPonNCqwoQYm0UgK4QgEiTSVIWrharAIiUkiElABhsnHrSjgnPkIkJwzWqea9AjzkAlpBCA9sYrTQtOlEbIPbJRC1puQeqoYzh8UkANK0mDYvD7hmdwrSh8VemsWgVBJGiFhPPhg0etMpquRZaJDNH1XoBxmB3mo+CzvamZZEeCzqpZKABBUeU1oN1aDUUkZINFTr+WHJZzycS0Y2UJfDtz8dArzJRraflfAforr4ZJAGvwb5blWJeVDb1qd3HX9Ak5zb7GYxSBrpR50BA5udlHoLn4JrsPdqXV/3fMuKMuY46ev/AGm9wB7Z+v6fBZmlmVxOUdS7S4dKVHqaoRLOF2g0O7XAgrcRpRrrB1NrBpPyWN7W4dFg0jNIcK2cL+RBFPIrWE5LoylFPsrxGUN0y3NVJQumIZc2neNBJA96mopseoQuJFdzKax5eX9MJ4+IddQbqJ8QDdBf2g0uSmd8tCoXdMU3TBNnmh2cJhegQJmb6qN08OaFPjFROiqEC/7ekhGdcUsNHohhFRxIbjqSfFGnStAonQlYqBDCITHAow6DVQOlidlCAtrzVWoDiTQKwcPV3DpUN6lUnLigxVsnk4NOI6D8rTmi0Flq77dBzPVU5duY1Pst06nmrsNxI5k7cz9gknKxlKixDZ6fEqaHCvfU6DkE5jco5n5812ZBDaD2n2ryG5+iq9F4q3QHxjFg05GDMRtcE+CBuxiNU5WutfKamo3FfzZayWwRguW1PM6q7CwtgGgWVNjHGK0YKYnot6AtrSlrA725KI4s6IHMe05XUBBvY6g9Qd16FHwxh90IXNYCyhoAK8gp7kTjFnmWBfupmmZwBNAdRroRt4opOSbO/OYDi15Gujh8ir+Kdn/e0c3T9fT4KKclQ9gObipbn4dd/Hyqrwm07MsmPVGb7QSbWHhQZoRjEYbveVDul0YyTVoRqitdNurrYPIJzpNwvREANLSo3MRN0ueSaJYqBBzGlJEf2dJQhox2qc2mcWRCU7UQX7hZCK4GxCqEBulkQHpMLEIbtCrQmWrzrD8Qy6my08lHa4AhByolB1zs1miqmgNGgPidh5/VC2xSbaDdTS8TN0aPj4pHJl5MZhCkF2kEUFmC9efVW5L/ADU6Dw5oW1xeQ0VDfziP0CLMO3kqIuy4zWvJTQ+J1trITNzfujw/PzmjmFso2+uqryt0awjSstMhqQMUraJ604kciAw1Vjw0TAVSMEGgxZn8Rlaiu/wWOxGWyCniD0vUH6L0CbasrjDaGh0OvTavyWbRZvRjnxBEFDY6HxG/mhDm0NPJFJyFkiVBqCTXpvp6qrizKEPpY/RMYJ74iWWPks4VDBN0fZDZS4WcwqOKgkojiE6ALFPC4SMlDI2Vd2Fw9BRZl2MuGhT2Ys43qpYaDcXBW80kBiY08nVJAgny6rRZVFXPCjIUADYUrU0J/PotHIwqUYwX5BVpOXq7QV6/nijcs4N4W9STu7p0HRL5Xo1gtksaFkbTV513v+imlYJpewVSWnGl5rUkb0JRCHMMO9fI1STa7GkmXpd9LNt11Knjx2wmVrxHRDJnEWQmkm1Pz80WcnMZL3WqfC9ByHM9PuquZpHHbNfgMExHZ3aDRa+XCwmHTs02GMsJgHIuObztRPh9uDCOWLDpzpdGCo1kvo9EYnVWcwbtPCjisNwPTf0RsxrVC2UkZOLLYVaOVk+0PaiJDcWQYZc4anQDzQCFi0eK799ELBUggG9tQhyvoKg12byaNll+0jeDNyT5RrHCsKO4kW9rMLbOaf8AtT4lAzQntOpafWiqSWjBzlK3BFbVtYjQ/RQzsGsEgj2XDfQEUr8vJSYdAMRvdRcwzHLDIbYOGhd0qAFcdIvbDc19+EH89figpVkRnKD/AB2wLhUrUURCbw6raKxgkAXVmemhD1C6q6OezLxcDdsq3+HuGoW0k5tjk+LLtdpRCg2Ycy9NUlp4+DApKUSzOuip8CLXVVCVdkYZJIHTxt/2hZA3hEMuqevw3V9jOKg3+it4ZL93Dv7b7NHIAVUstKcYIpcUHmPqlsuzWGmLDZCjYjtCTlHzr+ck+DIgcT3udU2FTdHJWWvFaNGlp9WH7J2HSNTmeNPZCSad0ORerMtjOB5oDnkULqGm4aDUDpbZNksJDGCguL+a1vaNwbDBO5AA5k7elVDhLGkDdDj7qNscvY2efY5MTz4zIUIBjNS8nhsd/t1V/BsCmHkd69lAL54NW6E0oKGubKA4ONl6JGwxj9QppXC2s0TCfijKS82zHy8mYLw9sMs/zCliNKh249D0W3lXVhKhi0KtvVWZL+HToqVRblYAxjDnxK5bZveBFQOld+uyzkx2KY55NCGk7lxfQuqSHf5qVGtF6LLwwVY/ZxyRi6JJJ6Z512U7HPlosR3ekwyeBpJqByJOtFp5llBRGI8PKg069Bu2Hikilh8i0Q2EimXiP9LST8kQxSQaYRO/dtHqwj6Lsf8AhuaLVFPhf50VjHQQLacQH9LQAqRW7K5n7UjzHA496HVWccbUKjC4Izh/MQPVXsUrlqF1ltHMfZng4g2U8PEHNOq5qo3QCTQBQIWl8c2IXU3DsH3ckiAzzId1oMMhthjMb7j7V5/JCIDUdlxwBxOlug3P0WcgoKSLnONXHjJr0aLWA8lqJWWAGc7aeKCYLCaBU1PPTVaATQNARbYJecvBpFFrDGECKSLuyn5/p6pMJa4ClK+Z5qeCCG3HEb08NqqWKytHDqEuzddGW7cRc3dwwRWuY+IIoPMB/ou4DH4B5KtjTc8zxUo3Ll9HA/3DdNwuM0Oo01BAOo8tPNZy+XIax/GjZyrleaEElZkIjDmKjVa2VaBuMRL5Ryv4Kxh8RpYOIaXVXE8N70e0QdDQkV8xcKnKYA+F7DzlrfM5zj6uqSOirsskvsMQIwBqDUIi2IKITKYVDhk5a8Rqbkgk6m6sRHUCltEaTOTsSqCReJ4b1ViYmUGxaf7iE+OQTkFaDU1IFB1uhHbJPSDUUtIILrZajnQVOnlfxUWJ4g0MJdoGOd/uWck8YE1ABDS1zXNzi5GrqUPgG+VAq3aiZIGUdK+A/AtkouSUROblVyAkE5ouY6m/qiuIQjksgck7jBWjbEqn10KsCSmGk+0isOVawKzEsKhAp6beailEQBls8zSqSyUCIQ6pSUsND4QRyQHC4eBHTn9kHa1EcNfcjalPiK/VUkiI0+EQ8zWkaEH+41Wow2Ra3jd5H6D7rN4C7K0A7XptTqjDozopAzWrfwSDasaS0X4sUucXN0Fh9VYhmjG1G+nQmh+Ckk4DSP5R8VXm5nNFaxugN/LX5/BBosjKdopA99FhZizNQseNiWUoehBcPPwWcwcGDMMgvNSYQH9TL09HH0Wx7Su/ek0rUAHytbqK/Nef9rHOb3czDNXQ8hrzAcRfoRYrFvfEZh0mbyXBLtaKzO97CIc3ibSpG/kg+C4g2PDbEYbEV6jmD1CPuj5mgFFF5GejdvGNcWCE8uBoc5yCvxKlh9vDoYTfKJbw9lcxLBmxb+y7nSx8QoYeDRBwlrDyNR/6qsnNM6fp4+ilj9y35tse/t1EBvAY4dHuBp5go5KzUWK0Oczuw7Ymrr86WVGRwAAhzzWlw0eyPG10dFkY8vIv6uXp7Swr+vZXjQR6LKdvHVlu7Gr3Np/Qc9fVoWsmYlASsBOTn7TGBA4Q4hnUNNC7zd8KKXSsSkr0HsHkmwZRjALlwcT5W+SCdqDR/wAPotHInMQ3YU+Dh9ih3a3DjlLwLAkH4G/5ut/TP3WKZ/oycqOMeK08BooszJNq4LRvbRq6SFGMmJtjTSqY6GwitkCmm1NSVD3r9AShZKDMSSYdgkqEF0UjdJEhG9tFfweUdEe1rbaEn85qmHB2l0akHGHCc5utaeZNFSbpEQch0YMtbXr12BViScd7CvyAVOQhFwaa718bVHlcIlEAoGt2B8zukJriNRdhSBPV6DQAdd/qrMsxrBUXdz/NkEha+Xz/AEors3OiFDy6vdt03/PsqRlfZdqjN45FL3OINavYxnI5ahxHSjj6rMzUUOztOnE3TYnfzPxVrF8Zo4kvzOdZlLsht6UsfHfawK5Oy47kO1dlNx7wJqPMWWE/kMw6BHZaZdLlzfdzGo+o+Xkt5Lz4cLFYXAoWYkbg2PSnxvX0RV8GJCNRpy28igpNMYcE4m7kng6orClmm9AvO5XHg2zzl8fujkHtTBpeKwf1D7rVTRl+KSNXMNa0IVHmAEPdjTYg4CHdRceqihwHvNTp+aIuVg4UdmpgvsNFl8OYGl3KG31pU+pNFrnwALdFlBDvGA1DgaeD2/ZZz8A+zSdn4BJDuRofVW+1zwJWO0ivCHDxa4foosAjZWsbu4io5VN/mqHb7EQGxGC/CGkf6rn4JnAhLMzHYUbhaR4OXyWQwiOQ4LYsiVAK6KFWB24eSTVWYcoxmqJMQvEJN7jY2RAPjTsNoskgsxLOG1V1CwgGXjuYag2WuweaBBa64qGkA34gCCsnLtB94DxNPmj2GPhsa8l+Z2UWb09nipzppXZZyLI28qGtAA00+IHrony4PeA7GwHMrO4TjDYjsruE1ApsCPZp0WmmGFozDVtwOtdPVJZou0MY2qZfl4VYltBr0oLfMBA+3Uu7JmBIbdrxzoajyP5qtDh7qmtKE+0OW4HxPqq3apofDd/MG/7tPoAqyjReLs8v/wAOMVwPvU8Kj6U9AihiUZl91tg7Sr6mpIOnJX4cABuXQ8/+NfD6qnNS9AGnQ0Lh/b6GiXmvAzBlfB4YEV1ByPrVauDCDm0Iqg+Dyn7x1f8AK31GvzWpw5lFWGxl6QEjdnw80NvFTynY6G24APV11o40D3lPCNlpwRT8jBsrhDGX1Pw9FZa2xVmIospRord9lGMLrIzxMOZc6lq8Q5sIFadQarZvZdCp2C3PU79OXyKElooyhJR+6OcnM2xadzXT6eiy+IzrokV7w4BzvaaaUd4Vt8kdn3MAMNxGV2lD7JP0Kxk8zI/KDUbEfJNel6EvUL3BKWcWm7A0+H0K0cs8kLIy79FqJF/CE/EWZIY5DqKGbxLLYpkeIcxKDTL85uiAOS8yx4ukhspLE6JKEoxrIlFekJihJ/KVuEPDFITRZlglAmMjqg22r+cvmvScHx1keGHA8QpnHiLkdNfReT5qhXMGnXQ31aaeqrPGpIMZcT16HMd09hBBY+wI1Dhs70PoqPbWf7trIVbnid5aeOoQbDsUf/8AHDiRnm9mktbXQUFac6k8kexDBnTLIJmG5IgaM4BFSHXINNqj4LJwUds0TctIzMlMviuAYCR7xAqKbg1pQ8j80ddh4Li4g12rpy0/Ko9KYeyE3KxoAHIJ/wCz30SuV8nodww4LYJkJTK7T8KJwIVCpIMG6tNh1VIwo1lO2Sw21CeYQXJbkrRYta0ZN0U3Q02Kyit5FDMNQaCpA5zbobictnby6oy9iqxoaz6LdnneKy8SHVz2kt5tuPFw91ZeM7O4kDVexOgIBifZaG8lzOB3QcJ8R9lvhyxj2L5sLfRhpdhqAtVIQyAFTPZyYaQe7LhzbQi2vUeaJS4c3hcC003FLc7p9NdiTT6K85LEmybDw1oudURLlVnoZI4TRFNPoDTQ0xWsSQl8m86mqSOwFLD+yUR93vDegufVaTDexMuPaBef5ifkKBGoEAohBhkLlPNN+TrxwY49IExexMo4fw8v+kkfVKT7HwYfsw2nq4Zj8Ufa0qzDFEVKVdlHCN9IiEkWQWBrC6uckMB1zjKS1oOmUcWyuzzKPA5NbvXbnurH7TBDGQ4rcxeDQZMw4y+1aWrlPhbSorXxX+K6m1B6NCYyfBC2L9jIS5cMRca2qmbACWobTQ1kRvmpRFC53ATmwFNgdD4Dr1V9oVVkOimZFV0Ulvoe4KrMlWHxFVIqpIkSq96rxHK4+Cq8SCsmmbKio881EYgVl8BVosoqh0OlI7M2V3sOoHDzs7xBuiH+AnK5p47l7XF4JDqAZQAxtGuoTvdCBLUROLOxg2G9sSjBwxAQDUsuBWleIEXr7m1UzgdpwYt6hU1NAzuhyVOZk2nZaGdgDOSNHUcNNHCu1lSfCWEouLGIyUkZeNKFul/FJHnytUldeoyrVlH6fE3dFyDAU4hKdoXHWVaLchobRKq44pqIGWouHPeYbwBlyQgcxs4Zi5wLedHGljWqfNtrEef5j8LKrFlw6agOAhksDGmr6PbwB3C2oBNC3YmhItVW4hq5x/mPzKay/FCeH5NjQ1TNCYAnBLjJIAnCiiqk0o2AmJUZCVUqqMiOEJALtV2qARryoHKZzlE9BhRCVG5qkKbRVLkLmp8qxhDxEYXtA7wNAJdVoI4Q25dlc6w10XSF2VtEbXQnKfB3D9VbG6kmUyK4tHXxWuhQnsD2toW0ie22lwHCpINDobhQi6bKw2iDEa0irXMeWinC4tDXCtc27bFraWpqohEV8yqZTA7gTvhpJzHhJZUa2OD1G56ic5Mc7bdWKklVKxtvJMhtVtjLI0RvQpWIDOFgcbODiGvIHDCDCHtAGa7W6k02sSVNDvdPgyr/ANodGc1w9scRhluVubIYZHGCQamtLUHu37DCYzeBXB5HUTSE9IpcYI6JBIrlUAiK5mXS5NqoE7mTSUiUqoWE4SmlOLk3MgQ4uJVSqoEcAoojTqNdlICk56sgNkUOpmI7CQQYbntsS4d44u1rRjeBopqSKnZUYraLRS+GmjHOJDgAXN4SC7uu7JNRUWpoaWCERWWW2dXTF/TurRUhxElBFBBSSwyycOVOWmMziepA8rKQvs49EFwuPYfmt0U9grRqpc2qicpCzODeevgNft5oNKRbBaTBofCXke1p/pG/mfotsatmOWVIIZAdQh8zJFt23HxH3REFOCZlFS7FYzcegE1y6XItHk2uvoeY+vNDI8o9moqOY+vJLyxtDMckWQvoonqQhMcFk0apjKroXCU5qoXEUxxUxCaWKAIMy6CuuTKqBHkpLjWq5K4e59/ZbzOp8AtFBvopKaj2VYbC40aCTyRiRw0NOZ13bch9z1VuXl2sFGjxO58SpCUxDGois8rlpCKDY1L0/eDQ2d47FFymPYHAtdobFXlHkqM4S4uzFzCS7isEw3Fp20PMbFJJuNDykmirL3JHRZSVi5HFp90kelvstXDNHLIY+8NmCRo4/wDkB+eizvZqlaNZgQMV7WDe7jyaNT9PNb1ppQDQWWd7ISHdQQ5w44lHO6D3W/U9T0RyG66fxQ4xObmnykWWlSBQsKmatDMcE5NCcEAlSPINdccJ+HohUzBLTRw+x8FoUyLCDhRwqFnLGmaQyNdmWcF1pVvEJMsPMHQ/m6ohKSjTHYyTWi9DFqqGPZWJc2VSciUKlaAuyuSk1Nc+iM4LIVpFeOrB/wAj9FeELYJz4rZLh2H2zRBfZvLx69EST01NKKXQlKTk7ZxcITklYqRuTHKVwUTlCA/GcOEdlAcrh7J+YPRdVzMkg4J7YVka0jAxXXJWYnJcRI0JrtDFh18A8VHmKjzWjjmgJ6IBF/jQT/8AbD/vC50fkjrPUX/D02BGsrMJyES0SyJQnrqnFL0NysQyqkIqZnNVLIsroTWuTkAnV1cSqoQbFhhwIIqCs3iEsYbqHTUHn+q0pUE5LiI0tPkeR2KzyQ5I0x5OL/wDSjrKlOuorVCwEHUWQeJGMR4Y25JyjxSr+h1d2X8Fk++fVw4G69TsFrVXkJUQmBjdtTzO5U6bhHihLJPkxJJJK5mdXKJVSJUIMeoXKV5UDyoBkcVySrzUSgSVjNsw+IeyUGb/ABIX/wCjP7gupLlR+SO4/gzawERglJJdZnFLsEq3BSSVSyJQnJJIBOpJJKEEuJJKEAfaawBGuV3wp90I7IMBmDUaMJHQ1Ar6EriSVf7RyP6WbVJJJNCZxJJJQgkkklCEUVQREklAMG4keE/m6SSSkgRP/9k=')).to.equal(true);

    // empty
    expect(inputValidator.isPersonaAvatarValid('')).to.equal(true);
  });

  it('is valid persona avatar - should not allow null/undefined, empty, or non-string', () => {
    expect(inputValidator.isPersonaAvatarValid(null)).to.equal(false);
    expect(inputValidator.isPersonaAvatarValid(undefined)).to.equal(false);
    expect(inputValidator.isPersonaAvatarValid(' ')).to.equal(false);
    expect(inputValidator.isPersonaAvatarValid(true)).to.equal(false);
    expect(inputValidator.isPersonaAvatarValid(55)).to.equal(false);
  });

  it('is valid persona avatar - should not allow avatar longer than max length', () => {
    let largeAvatar = 'data:image/jpeg;base64,';
    for (let i = 0; i < personaSchema.properties.avatar.maxLength - 22; i++) {
      largeAvatar += 'a';
    }
    expect(inputValidator.isPersonaAvatarValid(largeAvatar)).to.equal(false);
  });

  it('is valid persona avatar - should not allow avatar other than jpeg and png', () => {
    expect(inputValidator.isPersonaAvatarValid("dat:image/jpeg;base64,")).to.equal(false);
    expect(inputValidator.isPersonaAvatarValid("dataimage/jpeg;base64,")).to.equal(false);
    expect(inputValidator.isPersonaAvatarValid("data:image/jpg;base64,")).to.equal(false);
    expect(inputValidator.isPersonaAvatarValid("data:image/jpeg;base32,")).to.equal(false);
    expect(inputValidator.isPersonaAvatarValid("data:image/jpeg;case64,")).to.equal(false);
    expect(inputValidator.isPersonaAvatarValid("data:image/jpegbase64,")).to.equal(false);
    expect(inputValidator.isPersonaAvatarValid("data:text/jpeg;base64,")).to.equal(false);
    expect(inputValidator.isPersonaAvatarValid("data:image/gif;base64,")).to.equal(false);
    expect(inputValidator.isPersonaAvatarValid("data:image/bmp;base64,")).to.equal(false);
    expect(inputValidator.isPersonaAvatarValid("data:text/javascript;base64,")).to.equal(false);
    expect(inputValidator.isPersonaAvatarValid("000000000000000000000000000,")).to.equal(false);
    expect(inputValidator.isPersonaAvatarValid(" data:image/jpeg;base64,")).to.equal(false);
    expect(inputValidator.isPersonaAvatarValid("data:image/jpeg; base64,")).to.equal(false);
    expect(inputValidator.isPersonaAvatarValid('data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAAB CAQAAAC1HAwCAAAAC0lEQVR42mNkYAAAAAYAAjCB0C8AAAAASUVORK5CYII=')).to.equal(false);
    expect(inputValidator.isPersonaAvatarValid('data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAAB+CAQAAAC1HAwCAAAAC0lEQVR42mNkYAAAAAYAAjCB0C8AAAAASUVORK5CYII=')).to.equal(false);
  });

  it('is valid persona avatar - should not allow text url', () => {
    expect(inputValidator.isPersonaAvatarValid('https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRW6h0vCb_ZoWndQe2L4DppkD8eRwR17-PsQFpZj0-uauHLGei-')).to.equal(false);
  });

  it('is valid persona avatar - should not allow the content of script elements', () => {
    for (let i = 0; i < scripts.length; i++) {
      expect(inputValidator.isPersonaAvatarValid(scripts[i])).to.equal(false);
    }
  });

  /************************* sanitize comment content ******************/

  it('sanitize comment content - should leave plain text', () => {
    expect(inputValidator.sanitizeComment('This is my comment.')).to.equal('This is my comment.');
  });

  it('sanitize comment content - should strip simple tag', () => {
    expect(inputValidator.sanitizeComment('  <div><p>Hello <b>there</b></p></div>  ')).to.equal('Hello there');
  });

  it('sanitize comment content - should leave plain text url', () => {
    expect(inputValidator.sanitizeComment('  http://www.example.com  ')).to.equal('http://www.example.com');
  });

  it('sanitize comment content - should extract url anchor text', () => {
    expect(inputValidator.sanitizeComment('<a href="http://www.example.com" whizbang="whangle">www.example.com</a>')).to.equal('www.example.com');
    expect(inputValidator.sanitizeComment('<a href="hello.html">Hi</a>')).to.equal('Hi');
  });

  it('sanitize comment content - should concatenate text with html text', () => {
    expect(inputValidator.sanitizeComment('text then link <a href="http://www.example.com">www.example.com</a>')).to.equal('text then link www.example.com');
  });

  it('sanitize comment content - should remove custom tag', () => {
    expect(inputValidator.sanitizeComment('<div><wiggly>Hello</wiggly></div>')).to.equal('Hello');
  });

  it('sanitize comment content - should remove unclosed img tags', () => {
    expect(inputValidator.sanitizeComment('<img src="foo.jpg"><p>Whee<p>Again<p>Wow<b>cool</b>')).to.equal('WheeAgainWowcool');
  });

  it('sanitize comment content - should drop the content of script elements', () => {
    expect(inputValidator.sanitizeComment('<script>alert("ruhroh!");</script><p>Paragraph</p>')).to.equal('Paragraph');
  });

  it('sanitize comment content - should drop the content of style elements', () => {
    expect(inputValidator.sanitizeComment('<style>.foo { color: blue; }</style><p>Paragraph</p>')).to.equal('Paragraph');
  });

  it('sanitize comment content - should leave the content of json', () => {
    expect(inputValidator.sanitizeComment('{ nonTextTags: fibble}')).to.equal('{ nonTextTags: fibble}');
  });

  it('sanitize comment content - should drop the content of silly elements', () => {
    expect(inputValidator.sanitizeComment('<a name="&lt;silly&gt;">&lt;Kapow!&gt;</a>')).to.equal(''); // deletes the <Kapow!>
  });

  it('sanitize comment content - should dump html comments', () => {
    expect(inputValidator.sanitizeComment('<p><!-- Blah blah -->Whee</p>')).to.equal('Whee');
  });

  it('sanitize comment content - should dump a sneaky encoded javascript url', () => {
    expect(inputValidator.sanitizeComment('<a href="&#106;&#97;&#118;&#97;&#115;&#99;&#114;&#105;&#112;&#116;&#58;&#97;&#108;&#101;&#114;&#116;&#40;&#39;&#88;&#83;&#83;&#39;&#41;">Hax</a>')).to.equal('Hax');
  });

  it('sanitize comment content - should dump an uppercase javascript url', () => {
    expect(inputValidator.sanitizeComment('<a href="JAVASCRIPT:alert(\'foo\')">Hax</a>')).to.equal('Hax');
  });

  it('sanitize comment content - should dump a javascript URL with a comment in the middle (probably only respected by browsers in XML data islands, but just in case someone enables those)', () => {
    expect(inputValidator.sanitizeComment('<a href="java<!-- -->script:alert(\'foo\')">Hax</a>')).to.equal('Hax');
  });

  it('sanitize comment content - should dump character codes 1-32 before testing scheme', () => {
    expect(inputValidator.sanitizeComment('<a href="java\0&#14;\t\r\n script:alert(\'foo\')">Hax</a>')).to.equal('Hax');
  });

  it('sanitize comment content - should dump character codes 1-32 even when escaped with padding rather than trailing', () => {
    expect(inputValidator.sanitizeComment('<a href="java&#0000001script:alert(\'foo\')">Hax</a>')).to.equal('Hax');
    expect(inputValidator.sanitizeComment('<a href="java&#0000000script:alert(\'foo\')">Hax</a>')).to.equal('Hax');
  });

  it('sanitize comment content - should not crash on bad markup', () => {
    expect(inputValidator.sanitizeComment('<p a')).to.equal('');
  });

  it('sanitize comment content - should not be faked out by double <', () => {
    expect(inputValidator.sanitizeComment('<<img src="javascript:evil"/>img src="javascript:evil"/>')).to.equal('');
  });

  it('sanitize comment content - should not be faked out by double < 2', () => {
    expect(inputValidator.sanitizeComment('<<img src="javascript:evil"/>h1>2new h1 with validation<<img src="javascript:evil"/>/h1>')).to.equal('2new h1 with validation');
  });

  it('sanitize comment content - should not crash due to tag names that are properties of the universal Object prototype', () => {
    expect(inputValidator.sanitizeComment('!<__proto__>!')).to.equal('!!');
  });

  it('sanitize comment content - should discard srcset by default', () => {
    expect(inputValidator.sanitizeComment('<img src="fallback.jpg" srcset="foo.jpg 100w 2x, bar.jpg 200w 1x" />')).to.equal('');
  });

  it('sanitize comment content - should drop bogus srcset', () => {
    expect(inputValidator.sanitizeComment('<img src="fallback.jpg" srcset="foo.jpg 100w 2x, bar.jpg 200w 1x, javascript:alert(1) 100w 2x" />')).to.equal('');
  });

  it('sanitize comment content - drop attribute names with meta-characters', () => {
    expect(inputValidator.sanitizeComment('<span data-<script>alert(1)//>')).to.equal('alert(1)//>');
  });

  it('sanitize comment content - should sanitize styles correctly', () => {
    expect(inputValidator.sanitizeComment('<p dir="ltr"><strong>beste</strong><em>testestes</em><s>testestset</s><u>testestest</u></p><ul dir="ltr"> <li><u>test</u></li></ul><blockquote dir="ltr"> <ol> <li><u>​test</u></li><li><u>test</u></li><li style="text-align: right;"><u>test</u></li><li style="text-align: justify;"><u>test</u></li></ol> <p><u><span style="color:#00FF00;">test</span></u></p><p><span style="color:#00FF00"><span style="font-size:36px;">TESTETESTESTES</span></span></p></blockquote>')).to.equal('bestetestestestestestsettestestest test  ​testtesttesttest testTESTETESTESTES');
  });

  it('sanitize comment content - should allow only hostnames in an iframe that are whitelisted', () => {
    expect(inputValidator.sanitizeComment('<iframe src="https://www.youtube.com/embed/c2IlcS7AHxM"></iframe>')).to.equal('');
  });
});
