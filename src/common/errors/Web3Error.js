/*jshint esversion: 6 */
"use strict";

export default class Web3Error extends Error {
  constructor(message) {
    super(message);
    Web3Error.prototype = Error.prototype;
  }
}
