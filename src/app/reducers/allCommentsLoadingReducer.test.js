import {expect} from 'chai';
import allCommentsLoadingReducer from './allCommentsLoadingReducer';
import * as allCommentsLoadingActions from '../actions/allCommentsLoadingActions';
import * as allCommentsActions from '../actions/allCommentsActions';

describe("allCommentsLoadingReducer test", () => {
  it("should set all comments loading when passed SET_ALL_COMMENTS_LOADING", () => {
    const initState = false;
    const action = allCommentsLoadingActions.setAllCommentsLoading(true);
    const newState = allCommentsLoadingReducer(initState, action);

    expect(newState).to.be.true;
  });

  it("should set all comments loading to false when passed GET_ALL_COMMENTS_SUCCESS", () => {
    const initState = true;
    const action = allCommentsActions.getAllCommentsSuccess([]);
    const newState = allCommentsLoadingReducer(initState, action);

    expect(newState).to.be.false;
  });
});
