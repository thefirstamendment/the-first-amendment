import * as types from '../actions/actionTypes';
import initialState from './initialState';

export default function displaySidebarReducer(state = initialState.displaySideBar, action) {
  switch (action.type) {
    case types.DISPLAY_SIDEBAR:
      return action.isSidebarVisible;

    default:
      return state;
  }
}
