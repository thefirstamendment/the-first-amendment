/*jshint esversion: 6 */
"use strict";

class Persona {
  constructor(account, name, avatar, balance, isEnabled = false) {
    this.account = account;
    this.name = name;
    this.avatar = avatar;
    this.balance = balance;
    this.isEnabled = isEnabled;
  }
}

module.exports = Persona;
