import React from 'react';
import PropTypes from 'prop-types';
import { TextField, FormGroup, FormControl, Tooltip } from '@material-ui/core/';
import style from '../css/TfaShareCommentPanelSmall.scss';
import Utils from '../../../common/Utils';

const TfaShareCommentPanelSmall = ({ currentPersona, handleSmallSharePanelVisible }) => {
  const noCurrentPersona = Utils.isEmptyObject(currentPersona);
  const formControlDisabled = noCurrentPersona || !currentPersona.isEnabled;
  let tooltipTitle = "";
  let placeholder = "";
  if (noCurrentPersona) {
    tooltipTitle = "Please register at least one persona to be able to leave comments";
    placeholder = " Please register persona!";
  } else if (!currentPersona.isEnabled) {
    tooltipTitle = "This option is disabled until selected persona is mined and stored on blockchain";
    placeholder = " Please select confirmed persona!";
  }

  const smallCommentTextField = formControlDisabled ?
    (<Tooltip
      enterDelay={100}
      id="tfa_header_panel_tooltip"
      leaveDelay={100}
      placement="bottom"
      title={tooltipTitle}
      classes={{
        popper: style.tfa_tooltip_popover,
        tooltip: style.tfa_tooltip
      }}
    >
      <TextField
        id="tfaShareCommentPanelInputSmall"
        className={style.tfa_share_comment_small_input}
        placeholder={placeholder}
        fullWidth
        disabled
        margin="normal"
        InputProps={{
          classes: {
            input: style.tfa_share_comment_small_input,
          },
        }}
      />
    </Tooltip>) :
    (<TextField
      id="tfaShareCommentPanelInputSmall"
      className={style.tfa_share_comment_small_input}
      placeholder=" Speak up!"
      fullWidth
      autoComplete="off"
      margin="normal"
      onFocus={() => handleSmallSharePanelVisible(false)}
      InputProps={{
        classes: {
          input: style.tfa_share_comment_small_input
        }
      }}
    />);

  return (
    <div id="tfaShareCommentPanelSmall" className={style.tfa_share_comment_panel_small}>
      <div className={style.tfa_share_comment_panel_small_body}>
        <FormGroup>
          <FormControl className={style.form_control_tfa_small_input}>
            {smallCommentTextField}
          </FormControl>
        </FormGroup>
      </div>
    </div>
  );
};

TfaShareCommentPanelSmall.propTypes = {
  currentPersona: PropTypes.object.isRequired,
  handleSmallSharePanelVisible: PropTypes.func.isRequired
};

export default TfaShareCommentPanelSmall;
