import { expect } from 'chai';
import thunk from 'redux-thunk';
import configureMockStore from 'redux-mock-store';
import persistenceApi from '../api/persistenceApi';
import Persona from '../common/Persona';
import Comment from '../common/Comment';
import sinon from 'sinon';
import * as types from '../app/actions/actionTypes';
import * as allCommentsActions from '../app/actions/allCommentsActions';
import * as commentsActions from '../app/actions/commentsActions';
import * as personaActions from '../app/actions/personaActions';
import aliases from './aliases';
import { alias } from 'react-chrome-redux';
import Web3Error from '../common/errors/Web3Error';

const middleware = [alias(aliases), thunk];
const mockStore = configureMockStore(middleware);

function generateComments(commentsCount) {
  const tokenPersona = new Persona("account", "name", "", 0);
  const comments = [];
  for (let i = 0; i < commentsCount; i++) {
    comments.push(new Comment("txHash" + i, "content" + i, "url" + i, tokenPersona, 0, 0, 0, true));
  }
  return comments;
}

describe("aliases tests", () => {

  it("tests getAllCommentsAlias success", (done) => {
    const comments = generateComments(3);
    const getCommentsStub = sinon.stub(persistenceApi, "getComments").resolves(comments);
    const expectedActions = [
      { type: types.SET_ALL_COMMENTS_LOADING, allCommentsLoading: true },
      { type: types.GET_ALL_COMMENTS_SUCCESS, allComments: comments }
    ];
    const store = mockStore({ comments: [] }, expectedActions);
    store.dispatch(allCommentsActions.getAllComments()).then(() => {
      const actions = store.getActions();
      expect(actions[0]).to.deep.equal(expectedActions[0]);
      expect(actions[1]).to.deep.equal(expectedActions[1]);
      getCommentsStub.restore();
      done();
    });
  });

  it("tests getAllCommentsAlias with exception", (done) => {
    const comments = generateComments(3);
    const getCommentsStub = sinon.stub(persistenceApi, "getComments").rejects(new Web3Error("error message"));
    const expectedActions = [
      { type: types.SET_ALL_COMMENTS_LOADING, allCommentsLoading: true },
      { type: types.SET_ALL_COMMENTS_LOADING, allCommentsLoading: false },
      { type: types.SHOW_ERROR, errorMessage: "error message" }
    ];
    const store = mockStore({ comments: [] }, expectedActions);
    store.dispatch(allCommentsActions.getAllComments()).then(() => {
      const actions = store.getActions();
      for (let i = 0; i < actions.length; i++) {
        expect(actions[i]).to.deep.equal(expectedActions[i]);
      }
      getCommentsStub.restore();
      done();
    });
  });

  it("tests getCommentsAlias success", (done) => {
    const comments = generateComments(3);
    const getCommentsStub = sinon.stub(persistenceApi, "getComments").resolves(comments);
    const expectedActions = [
      { type: types.SET_COMMENTS_LOADING, commentsLoading: true },
      { type: types.GET_COMMENTS_SUCCESS, comments: comments }
    ];
    const store = mockStore({ comments: [] }, expectedActions);
    store.dispatch(commentsActions.getComments("url")).then(() => {
      const actions = store.getActions();
      expect(actions[0]).to.deep.equal(expectedActions[0]);
      expect(actions[1]).to.deep.equal(expectedActions[1]);
      getCommentsStub.restore();
      done();
    });
  });

  it("tests getCommentsAlias with exception", (done) => {
    const comments = generateComments(3);
    const getCommentsStub = sinon.stub(persistenceApi, "getComments").rejects(new Web3Error("error message"));
    const expectedActions = [
      { type: types.SET_COMMENTS_LOADING, commentsLoading: true },
      { type: types.SET_COMMENTS_LOADING, commentsLoading: false },
      { type: types.SHOW_ERROR, errorMessage: "error message" }
    ];
    const store = mockStore({ comments: [] }, expectedActions);
    store.dispatch(commentsActions.getComments("url")).then(() => {
      const actions = store.getActions();
      for (let i = 0; i < actions.length; i++) {
        expect(actions[i]).to.deep.equal(expectedActions[i]);
      }
      getCommentsStub.restore();
      done();
    });
  });

  it("tests addCommentAlias success", (done) => {
    const comments = generateComments(1);
    const addCommentStub = sinon.stub(persistenceApi, "addComment").resolves("newTxHash");
    const getCommentTimestampStub = sinon.stub(persistenceApi, "getCommentTimestamp").returns(1);
    const newComment = Object.assign({}, comments[0]);
    newComment.txHash = "newTxHash";
    newComment.timestamp = 1;
    const expectedActions = [
      { type: types.SHOW_SMALL_SHARE_COMMENT_PANEL, showSmallShareCommentPanel: true },
      { type: types.ADD_COMMENT_SUCCESS, comment: comments[0] },
      { type: types.UPDATE_COMMENT_SUCCESS, txHash: comments[0].txHash, newComment: newComment }
    ];
    const store = mockStore({ comments: [] }, expectedActions);
    store.dispatch(commentsActions.addComment(comments[0])).then(() => {
      const actions = store.getActions();
      for (let i = 0; i < actions.length; i++) {
        expect(actions[i]).to.deep.equal(expectedActions[i]);
      }
      addCommentStub.restore();
      getCommentTimestampStub.restore();
      done();
    });
  });

  it("tests addCommentAlias with exception", (done) => {
    const comments = generateComments(1);
    const addCommentStub = sinon.stub(persistenceApi, "addComment").rejects(new Web3Error("error message"));
    const getCommentTimestampStub = sinon.stub(persistenceApi, "getCommentTimestamp").returns(1);
    const newComment = Object.assign({}, comments[0]);
    newComment.txHash = "newTxHash";
    newComment.timestamp = 1;
    const expectedActions = [
      { type: types.SHOW_SMALL_SHARE_COMMENT_PANEL, showSmallShareCommentPanel: true },
      { type: types.ADD_COMMENT_SUCCESS, comment: comments[0] },
      { type: types.SHOW_ERROR, errorMessage: "error message" }
    ];
    const store = mockStore({ comments: [] }, expectedActions);
    store.dispatch(commentsActions.addComment(comments[0])).then(() => {
      const actions = store.getActions();
      for (let i = 0; i < actions.length; i++) {
        expect(actions[i]).to.deep.equal(expectedActions[i]);
      }
      addCommentStub.restore();
      getCommentTimestampStub.restore();
      done();
    });
  });

  it("tests upvoteCommentAlias success", (done) => {
    const comments = generateComments(1);
    const upvoteCommentStub = sinon.stub(persistenceApi, "upvoteComment").resolves(3);
    const newComment1 = Object.assign({}, comments[0]);
    newComment1.upvoteCount = comments[0].upvoteCount + 1;
    newComment1.isEnabled = false;
    const newComment2 = Object.assign({}, comments[0]);
    newComment2.upvoteCount = 3;
    const expectedActions = [
      {type: types.UPDATE_COMMENT_SUCCESS, txHash: comments[0].txHash, newComment: newComment1},
      { type: types.UPDATE_COMMENT_SUCCESS, txHash: comments[0].txHash, newComment: newComment2 }
    ];
    const store = mockStore({ comments: [] }, expectedActions);
    store.dispatch(commentsActions.upvoteComment(comments[0], "addr")).then(() => {
      const actions = store.getActions();
      for (let i = 0; i < actions.length; i++) {
        expect(actions[i]).to.deep.equal(expectedActions[i]);
      }
      upvoteCommentStub.restore();
      done();
    });
  });

  it("tests upvoteCommentAlias with exception", (done) => {
    const comments = generateComments(1);
    const upvoteCommentStub = sinon.stub(persistenceApi, "upvoteComment").rejects(new Web3Error("error message"));
    const newComment1 = Object.assign({}, comments[0]);
    newComment1.upvoteCount = comments[0].upvoteCount + 1;
    newComment1.isEnabled = false;
    const expectedActions = [
      {type: types.UPDATE_COMMENT_SUCCESS, txHash: comments[0].txHash, newComment: newComment1},
      { type: types.SHOW_ERROR, errorMessage: "error message" }
    ];
    const store = mockStore({ comments: [] }, expectedActions);
    store.dispatch(commentsActions.upvoteComment(comments[0], "addr")).then(() => {
      const actions = store.getActions();
      for (let i = 0; i < actions.length; i++) {
        expect(actions[i]).to.deep.equal(expectedActions[i]);
      }
      upvoteCommentStub.restore();
      done();
    });
  });

  it("tests downvoteCommentAlias success", (done) => {
    const comments = generateComments(1);
    const downvoteCommentStub = sinon.stub(persistenceApi, "downvoteComment").resolves(3);
    const newComment1 = Object.assign({}, comments[0]);
    newComment1.downvoteCount = comments[0].downvoteCount + 1;
    newComment1.isEnabled = false;
    const newComment2 = Object.assign({}, comments[0]);
    newComment2.downvoteCount = 3;
    const expectedActions = [
      {type: types.UPDATE_COMMENT_SUCCESS, txHash: comments[0].txHash, newComment: newComment1},
      { type: types.UPDATE_COMMENT_SUCCESS, txHash: comments[0].txHash, newComment: newComment2 }
    ];
    const store = mockStore({ comments: [] }, expectedActions);
    store.dispatch(commentsActions.downvoteComment(comments[0], "addr")).then(() => {
      const actions = store.getActions();
      for (let i = 0; i < actions.length; i++) {
        expect(actions[i]).to.deep.equal(expectedActions[i]);
      }
      downvoteCommentStub.restore();
      done();
    });
  });

  it("tests upvoteCommentAlias with exception", (done) => {
    const comments = generateComments(1);
    const downvoteCommentStub = sinon.stub(persistenceApi, "downvoteComment").rejects(new Web3Error("error message"));
    const newComment1 = Object.assign({}, comments[0]);
    newComment1.downvoteCount = comments[0].downvoteCount + 1;
    newComment1.isEnabled = false;
    const expectedActions = [
      {type: types.UPDATE_COMMENT_SUCCESS, txHash: comments[0].txHash, newComment: newComment1},
      { type: types.SHOW_ERROR, errorMessage: "error message" }
    ];
    const store = mockStore({ comments: [] }, expectedActions);
    store.dispatch(commentsActions.downvoteComment(comments[0], "addr")).then(() => {
      const actions = store.getActions();
      for (let i = 0; i < actions.length; i++) {
        expect(actions[i]).to.deep.equal(expectedActions[i]);
      }
      downvoteCommentStub.restore();
      done();
    });
  });

  it("tests addPersonaAlias success", (done) => {
    const persona = new Persona("account", "name", "", 0);
    const newPersona = Object.assign({}, persona);
    newPersona.isEnabled = true;
    const addPersonaStub = sinon.stub(persistenceApi, "addPersona").resolves(newPersona);
    const expectedActions = [
      { type: types.ADD_PERSONA_SUCCESS, persona: persona },
      { type: types.UPDATE_PERSONA_SUCCESS, persona: newPersona },
      { type: types.SHOW_NOTIFICATION, notificationMessage: "Persona [ " + persona.name + " ] is now ready for use" }
    ];
    const store = mockStore({ personas: [] }, expectedActions);
    store.dispatch(personaActions.addPersona(persona, persona.account)).then(() => {
      const actions = store.getActions();
      for (let i = 0; i < actions.length; i++) {
        expect(actions[i]).to.deep.equal(expectedActions[i]);
      }
      addPersonaStub.restore();
      done();
    });
  });

  it("tests addPersonaAlias with exception", (done) => {
    const persona = new Persona("account", "name", "", 0);
    const newPersona = Object.assign({}, persona);
    newPersona.isEnabled = true;
    const addPersonaStub = sinon.stub(persistenceApi, "addPersona").rejects(new Web3Error("error message"));
    const expectedActions = [
      { type: types.ADD_PERSONA_SUCCESS, persona: persona },
      { type: types.SHOW_ERROR, errorMessage: "error message" }
    ];
    const store = mockStore({ personas: [] }, expectedActions);
    store.dispatch(personaActions.addPersona(persona, persona.account)).then(() => {
      const actions = store.getActions();
      for (let i = 0; i < actions.length; i++) {
        expect(actions[i]).to.deep.equal(expectedActions[i]);
      }
      addPersonaStub.restore();
      done();
    });
  });

  it("tests updatePersonaAlias success", (done) => {
    const persona = new Persona("account", "name", "", 0);
    const newPersona = Object.assign({}, persona);
    newPersona.isEnabled = true;
    const updatePersonaStub = sinon.stub(persistenceApi, "updatePersona").resolves("txHash");
    const expectedActions = [
      { type: types.UPDATE_PERSONA_SUCCESS, persona: persona },
      { type: types.UPDATE_PERSONA_SUCCESS, persona: newPersona }
    ];
    const store = mockStore({ personas: [] }, expectedActions);
    store.dispatch(personaActions.updatePersona(persona, persona.account)).then(() => {
      const actions = store.getActions();
      for (let i = 0; i < actions.length; i++) {
        expect(actions[i]).to.deep.equal(expectedActions[i]);
      }
      updatePersonaStub.restore();
      done();
    });
  });

  it("tests updatePersonaAlias with exception", (done) => {
    const persona = new Persona("account", "name", "", 0);
    const newPersona = Object.assign({}, persona);
    newPersona.isEnabled = true;
    const updatePersonaStub = sinon.stub(persistenceApi, "updatePersona").rejects(new Web3Error("error message"));
    const expectedActions = [
      { type: types.UPDATE_PERSONA_SUCCESS, persona: persona },
      { type: types.SHOW_ERROR, errorMessage: "error message" }
    ];
    const store = mockStore({ personas: [] }, expectedActions);
    store.dispatch(personaActions.updatePersona(persona, persona.account)).then(() => {
      const actions = store.getActions();
      for (let i = 0; i < actions.length; i++) {
        expect(actions[i]).to.deep.equal(expectedActions[i]);
      }
      updatePersonaStub.restore();
      done();
    });
  });
});
