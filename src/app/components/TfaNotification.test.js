import { expect } from 'chai';
import React from 'react';
import TfaNotification from './TfaNotification';
import sinon from 'sinon';
import chrome from 'sinon-chrome/extensions';
import { createMount } from '@material-ui/core/test-utils';
import { JSDOM } from 'jsdom';
import Enzyme from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

Enzyme.configure({ adapter: new Adapter() });

describe("<TfaNotification /> tests", () => {
  let mount;

  function setup(isOpen, notificationMessage) {
    const props = {
      isOpen: isOpen,
      notificationMessage: notificationMessage,
      hideNotification: () => {}
    };
    return mount(<TfaNotification {...props} />);
  }

  before(() => {
    global.chrome = chrome;
    const jsdom = new JSDOM('<!doctype html><html><body></body></html>');
    const { window } = jsdom;
    global.window = window;
    global.document = window.document;
    mount = createMount();
  });

  after(() => {
    mount.cleanUp();
  });

  it("renders is open with notification message", () => {
    const isOpen = true;
    const notificationMessage = "Random notification message";
    const wrapper = setup(isOpen, notificationMessage);
    expect(wrapper.props().isOpen).to.be.true;
    expect(wrapper.props().notificationMessage).to.equal(notificationMessage);
    expect(wrapper.find("Snackbar").length).to.equal(1);
    expect(wrapper.find("Snackbar").props().open).to.equal(isOpen);
    expect(wrapper.find("Snackbar").props().message).to.exist;
    expect(wrapper.find("h3").length).to.equal(1);
    expect(wrapper.find("h3").text()).to.equal("Notification");
    expect(wrapper.find("span").length).to.equal(1);
    expect(wrapper.find("span").text()).to.equal(notificationMessage);
  });

  it("renders is closed with error message", () => {
    const isOpen = false;
    const notificationMessage = "Random notification message";
    const wrapper = setup(isOpen, notificationMessage);
    expect(wrapper.props().isOpen).to.be.false;
    expect(wrapper.props().notificationMessage).to.equal(notificationMessage);
    expect(wrapper.find("Snackbar").length).to.equal(1);
    expect(wrapper.find("Snackbar").props().open).to.equal(isOpen);
    expect(wrapper.find("Snackbar").props().message).to.exist;
    expect(wrapper.find("h3").length).to.equal(0);
    expect(wrapper.find("span").length).to.equal(0);
  });

  it("simulates click on close button", () => {
    const hideNotification = sinon.spy();
    const isOpen = true;
    const notificationMessage = "Random notification message";
    const wrapper = mount(
      <TfaNotification
        isOpen={isOpen}
        notificationMessage={notificationMessage}
        hideNotification={hideNotification}
      />
    );
    wrapper.find("a").simulate("click");
    expect(hideNotification.calledOnce).to.be.true;
  });
});
