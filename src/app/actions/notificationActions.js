import * as types from './actionTypes';

export function showNotification(notificationMessage) {
  return { type: types.SHOW_NOTIFICATION, notificationMessage: notificationMessage };
}

export function hideNotification() {
  return { type: types.HIDE_NOTIFICATION };
}
