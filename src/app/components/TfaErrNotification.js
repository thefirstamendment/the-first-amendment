import React from 'react';
import PropTypes from 'prop-types';
import { Snackbar } from '@material-ui/core/';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faExclamationTriangle, faTimes } from '@fortawesome/free-solid-svg-icons';
import style from './css/TfaErrNotification.scss';

const TfaErrNotification = (props) => {
  const errMessage = (
    <div className={style.tfa_err_message}>
      <div className={style.tfa_err_header}>
        <div className={style.tfa_fa_err_icon}>
          <FontAwesomeIcon icon={faExclamationTriangle} size="lg" />
        </div>
        <div>
          <h3>Ooops, something went wrong</h3>
        </div>
      </div>
      <div className={style.tfa_err_content}>
        <span>{props.errorMessage}</span>
      </div>
      <div className={style.tfa_err_action}>
        <a href="#" rel="noopener" role="button" onClick={() => props.hideError()}>
          <FontAwesomeIcon icon={faTimes} />
        </a>&nbsp;
      </div>
    </div>
  );

  return (
    <Snackbar
      classes={{
        root: style.tfa_err_notification
      }}
      anchorOrigin={{
        vertical: 'top',
        horizontal: 'right',
      }}
      open={props.isOpen}
      SnackbarContentProps={{
        'aria-describedby': 'message-id',
      }}
      message={errMessage}
    />
  );
};

TfaErrNotification.propTypes = {
  isOpen: PropTypes.bool.isRequired,
  hideError: PropTypes.func.isRequired,
  errorMessage: PropTypes.string.isRequired
};

export default TfaErrNotification;

