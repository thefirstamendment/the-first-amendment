import React from 'react';
import PropTypes from 'prop-types';
import { Typography } from '@material-ui/core/';
import style from './css/TfaDisplayMessagePanel.scss';

const TfaDisplayMessagePanel = ({ message }) => {
  return (
    <div>
      <Typography className={style.tfa_message} variant="headline" component="h3">
        {message}
      </Typography>
    </div>
  );
};

TfaDisplayMessagePanel.propTypes = {
  message: PropTypes.string.isRequired,
};

export default TfaDisplayMessagePanel;
