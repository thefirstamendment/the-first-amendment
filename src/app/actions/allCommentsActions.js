import * as types from './actionTypes';

export function getAllComments() {
  return { type: types.GET_ALL_COMMENTS };
}

export function getAllCommentsSuccess(allComments) {
  return { type: types.GET_ALL_COMMENTS_SUCCESS, allComments: allComments };
}
