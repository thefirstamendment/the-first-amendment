import React from 'react';
import PropTypes from 'prop-types';
import { Avatar, Tooltip } from '@material-ui/core/';
import Dropzone from 'react-dropzone';
import style from '../css/TfaAvatarDragAndDrop.scss';
import InputValidationError from '../../../common/errors/InputValidationError';

export default class TfaAvatarDragAndDrop extends React.Component {
  constructor(props) {
    super(props);
    this.onDrop = this.onDrop.bind(this);
  }

  onDrop(acceptedFiles, rejectedFiles) {
    const reader = new FileReader();
    reader.addEventListener("load", () => {
      this.props.setAvatar(reader.result);
    }, false);
    if (acceptedFiles.length > 0) {
      reader.readAsDataURL(acceptedFiles[0]);
    } else {
      this.props.showError(new InputValidationError("Avatar image must be jpeg/png format, not larger than 40kB"));
    }
  }

  render() {
    return (
      <div>
        <Tooltip
          enterDelay={100}
          id="tfa_avatar_drag_and_drop_tooltip"
          leaveDelay={100}
          placement="bottom"
          title="Click here or drag and drop to set avatar image"
          classes={{
            popper: style.tfa_tooltip_popover,
            tooltip: style.tfa_tooltip
          }}
        >
          <Dropzone
            id="tfaDropzoneComponent"
            className={style.tfa_dropzone_img_size96}
            onDrop={this.onDrop}
            accept="image/jpeg, image/png"
            multiple={false}
            maxSize={50000}>
            <Avatar
              className={style.tfa_avatar_centered_img_size96}
              src={this.props.avatar}
            />
          </Dropzone>
        </Tooltip>
      </div>
    );
  }
}

TfaAvatarDragAndDrop.propTypes = {
  avatar: PropTypes.string.isRequired,
  setAvatar: PropTypes.func.isRequired,
  showError: PropTypes.func.isRequired
};
