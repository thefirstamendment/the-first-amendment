import {expect} from 'chai';
import showSmallShareCommentPanelReducer from './showSmallShareCommentPanelReducer';
import * as actions from '../actions/shareCommentsPanelActions';

describe("showSmallShareCommentPanelReducer test", () => {
  it("should set show small share comment panel when passed SHOW_SMALL_SHARE_COMMENT_PANEL", () => {
    const initState = false;
    const action1 = actions.showSmallShareCommentPanel(true);
    let newState = showSmallShareCommentPanelReducer(initState, action1);

    expect(newState).to.be.true;

    const action2 = actions.showSmallShareCommentPanel(false);
    newState = showSmallShareCommentPanelReducer(newState, action2);

    expect(newState).to.be.false;
  });
});
