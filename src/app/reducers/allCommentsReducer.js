import * as types from '../actions/actionTypes';
import initialState from './initialState';

export default function allCommentsReducer(state = initialState.allComments, action) {
  switch (action.type) {
    case types.GET_ALL_COMMENTS_SUCCESS:
      return action.allComments;

    case types.ADD_COMMENT_SUCCESS:
      if (state.find(comment => comment.txHash == action.comment.txHash)) {
        return [...state];
      } else {
        return [ // creating a deep copy of an array
          ...state,
          Object.assign({}, action.comment)
        ];
      }

    case types.UPDATE_COMMENT_SUCCESS:
      return [...state.filter(comment => comment.txHash != action.txHash), Object.assign({}, action.newComment)];

    default:
      return state;
  }
}
