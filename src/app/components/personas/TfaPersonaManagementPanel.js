import React from 'react';
import PropTypes from 'prop-types';
import TfaDisplayMessagePanel from '../TfaDisplayMessagePanel';
import TfaCurrentPersona from './TfaCurrentPersona';
import TfaPersonaManagementButtonPanel from './TfaPersonaManagementButtonPanel';
import TfaUnselectedPersonas from './TfaUnselectedPersonas';
import TfaAddPersonaModal from './TfaAddPersonaModal';
import TfaEditPersonaModal from './TfaEditPersonaModal';
import Utils from '../../../common/Utils';

const assembleAvailableAccounts = (accounts, personas, noCurrentPersona) => {
  if (noCurrentPersona) {
    return accounts; // all accounts are available
  }
  // filter out accounts taken by personas
  return accounts.filter((account) => !personas.map((persona) => persona.account).includes(account));
};

const TfaPersonaManagementPanel = (props) => {
  const noCurrentPersona = Utils.isEmptyObject(props.currentPersona);
  const unselectedPersonas = props.personas.filter(persona => persona.account != props.currentPersona.account);
  const availableAccounts = assembleAvailableAccounts(props.accounts, props.personas, noCurrentPersona);
  const personaManagementPanelBody = noCurrentPersona ?
    (<TfaDisplayMessagePanel message="No registered personas..." />) :
    (
      <div>
        <TfaCurrentPersona
          currentPersona={props.currentPersona}
          openEditPersonaModal={props.openEditPersonaModal}
        />
        <br />
        {
          (unselectedPersonas.length > 0) &&
          <TfaUnselectedPersonas
            personas={unselectedPersonas}
            selectPersona={props.selectPersona}
          />
        }
      </div>
    );
  const editPersonaModal = noCurrentPersona ?
    (<br />) :
    (
      <TfaEditPersonaModal
        editPersonaModalIsOpen={props.isEditPersonaModalOpen}
        closeEditPersonaModal={props.closeEditPersonaModal}
        currentPersona={props.currentPersona}
        updatePersona={props.updatePersona}
        showError={props.showError}
      />
    );

  return (
    <div>
      {personaManagementPanelBody}
      <TfaPersonaManagementButtonPanel
        togglePersonaManagementPanel={props.togglePersonaManagementPanel}
        openAddPersonaModal={props.openAddPersonaModal}
      />
      <TfaAddPersonaModal
        addPersonaModalIsOpen={props.isAddPersonaModalOpen}
        closeAddPersonaModal={props.closeAddPersonaModal}
        accounts={availableAccounts}
        addPersona={props.addPersona}
        showError={props.showError}
      />
      {editPersonaModal}
    </div>
  );
};


TfaPersonaManagementPanel.propTypes = {
  openAddPersonaModal: PropTypes.func.isRequired,
  openEditPersonaModal: PropTypes.func.isRequired,
  closeAddPersonaModal: PropTypes.func.isRequired,
  closeEditPersonaModal: PropTypes.func.isRequired,
  togglePersonaManagementPanel: PropTypes.func.isRequired,
  accounts: PropTypes.array.isRequired,
  currentPersona: PropTypes.object.isRequired,
  personas: PropTypes.array.isRequired,
  addPersona: PropTypes.func.isRequired,
  updatePersona: PropTypes.func.isRequired,
  selectPersona: PropTypes.func.isRequired,
  isAddPersonaModalOpen: PropTypes.bool.isRequired,
  isEditPersonaModalOpen: PropTypes.bool.isRequired,
  showError: PropTypes.func.isRequired
};

export default TfaPersonaManagementPanel;
