import * as types from './actionTypes';

export function addCommentSuccess(comment) {
  return { type: types.ADD_COMMENT_SUCCESS, comment: comment };
}

export function getCommentsSuccess(comments) {
  return { type: types.GET_COMMENTS_SUCCESS, comments: comments };
}

export function updateCommentSuccess(txHash, newComment) {
  return {type: types.UPDATE_COMMENT_SUCCESS, txHash: txHash, newComment: newComment};
}

export function addComment(comment) {
  return { type: types.ADD_COMMENT, comment: comment };
}

export function getComments(url) {
  return { type: types.GET_COMMENTS, url: url };
}

export function upvoteComment(comment, address) {
  return { type: types.UPVOTE_COMMENT, comment: comment, address: address };
}

export function downvoteComment(comment, address) {
  return { type: types.DOWNVOTE_COMMENT, comment: comment, address: address };
}
