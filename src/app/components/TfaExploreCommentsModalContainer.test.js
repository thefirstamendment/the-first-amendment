import { expect } from 'chai';
import React from 'react';
import { TfaExploreCommentsModalContainer } from './TfaExploreCommentsModalContainer';
import chrome from 'sinon-chrome/extensions';
import { createMount } from '@material-ui/core/test-utils';
import { JSDOM } from 'jsdom';
import Enzyme from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import Comment from '../../common/Comment';
import Persona from '../../common/Persona';

Enzyme.configure({ adapter: new Adapter() });

describe("<TfaExploreCommentsModalContainer /> tests", () => {
  let mount;

  function setup(
    exploreCommentsModalIsOpen,
    closeExploreCommentsModal,
    commentsActions = {},
    errorActions = {},
    allComments,
    allCommentsLoading,
    currentPersona
    ) {
    const props = {
      exploreCommentsModalIsOpen: exploreCommentsModalIsOpen,
      closeExploreCommentsModal: closeExploreCommentsModal,
      commentsActions: commentsActions,
      errorActions: errorActions,
      allComments: allComments,
      allCommentsLoading: allCommentsLoading,
      currentPersona: currentPersona
    };
    return mount(<TfaExploreCommentsModalContainer {...props} />);
  }

  before(() => {
    global.chrome = chrome;
    const jsdom = new JSDOM('<!doctype html><html><body></body></html>');
    const { window } = jsdom;
    global.window = window;
    global.document = window.document;
    mount = createMount();
  });

  after(() => {
    mount.cleanUp();
  });

  it("renders explore comments modal container without comments and current persona", () => {
    const wrapper = setup(false, null, {}, {}, [], false, null);
    expect(wrapper.props().exploreCommentsModalIsOpen).to.be.false;
    expect(wrapper.props().closeExploreCommentsModal).to.be.null;
    expect(wrapper.props().commentsActions).to.exist;
    expect(wrapper.props().errorActions).to.exist;
    expect(wrapper.props().allComments).to.be.empty;
    expect(wrapper.props().allCommentsLoading).to.be.false;
    expect(wrapper.props().currentPersona).to.be.null;
    expect(wrapper.find("TfaExploreCommentsModal").length).to.equal(1);
  });

  it("renders explore comments modal container with comments and current persona", () => {
    const persona = new Persona("account", "name", "", 0);
    const comment = new Comment("txHash", "content", "url", persona, 0, 0, 0);
    const wrapper = setup(false, null, {}, {}, [comment], false, persona);
    expect(wrapper.props().exploreCommentsModalIsOpen).to.be.false;
    expect(wrapper.props().closeExploreCommentsModal).to.be.null;
    expect(wrapper.props().commentsActions).to.exist;
    expect(wrapper.props().errorActions).to.exist;
    expect(wrapper.props().allComments[0]).to.equal(comment);
    expect(wrapper.props().allCommentsLoading).to.be.false;
    expect(wrapper.props().currentPersona).to.equal(persona);
    expect(wrapper.find("TfaExploreCommentsModal").length).to.equal(1);
  });
});
