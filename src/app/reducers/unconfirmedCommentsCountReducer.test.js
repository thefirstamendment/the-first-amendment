import {expect} from 'chai';
import unconfirmedCommentsCountReducer from './unconfirmedCommentsCountReducer';
import * as actions from '../actions/commentsActions';

describe("unconfirmedCommentsCountReducer test", () => {
  it("should increment unconfirmed comments count when passed ADD_UNCONFIRMED_COMMENT", () => {
    const initState = 0;
    const action = actions.addCommentSuccess({});
    let newState = unconfirmedCommentsCountReducer(initState, action);

    expect(newState).to.equal(1);
  });

  it ("should reset unconfirmed comments count to 0 when MAX_SAFE_INTEGER is reached", () => {
    const initState = Number.MAX_SAFE_INTEGER;
    const action = actions.addCommentSuccess({});
    let newState = unconfirmedCommentsCountReducer(initState, action);

    expect(newState).to.equal(0);
  });
});
