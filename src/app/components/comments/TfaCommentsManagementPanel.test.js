import { expect } from 'chai';
import React from 'react';
import TfaCommentsManagementPanel from './TfaCommentsManagementPanel';
import chrome from 'sinon-chrome/extensions';
import sinon from 'sinon';
import Utils from '../../../common/Utils';
import { createMount } from '@material-ui/core/test-utils';
import Comment from '../../../common/Comment';
import Persona from '../../../common/Persona';
import personasTestJson from '../../../../test/integration/test_personas.json';
import { JSDOM } from 'jsdom';
import Enzyme from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

Enzyme.configure({ adapter: new Adapter() });

function generateComments(commentsCount) {
  const tokenPersona = new Persona("account", "name", "", 0);
  const comments = [];
  for (let i = 0; i < commentsCount; i++) {
    comments.push(new Comment("txHash" + i, "content" + i, "url" + i, tokenPersona, 0, 0, 0, true));
  }
  return comments;
}

describe("<TfaCommentsManagementPanel /> tests", () => {
  let mount;

  function setup(
    showSmallShareCommentPanel,
    currentPersona,
    comments,
    commentOrdering,
    commentsLoading,
    addComment = () => { },
    selectCommentOrdering = () => { },
    upvoteComment = () => { },
    downvoteComment = () => { },
    handleSmallSharePanelFocus = () => { }
  ) {
    const props = {
      showSmallShareCommentPanel: showSmallShareCommentPanel,
      currentPersona: currentPersona,
      comments: comments,
      commentOrdering: commentOrdering,
      commentsLoading: commentsLoading,
      addComment: addComment,
      selectCommentOrdering: selectCommentOrdering,
      upvoteComment: upvoteComment,
      downvoteComment: downvoteComment,
      handleSmallSharePanelFocus: handleSmallSharePanelFocus
    };
    return mount(<TfaCommentsManagementPanel {...props} />);
  }

  before(() => {
    global.chrome = chrome;
    const jsdom = new JSDOM('<!doctype html><html><body></body></html>');
    const { window } = jsdom;
    global.window = window;
    global.document = window.document;
    mount = createMount();
  });

  after(() => {
    mount.cleanUp();
  });

  it("renders comments management panel small share comment panel, empty current persona, comments loading", () => {
    const wrapper = setup(true, {}, [], 0, true);
    expect(wrapper.props().showSmallShareCommentPanel).to.be.true;
    expect(wrapper.props().currentPersona).to.be.empty;
    expect(wrapper.props().comments).to.be.empty;
    expect(wrapper.props().commentOrdering).to.equal(0);
    expect(wrapper.props().commentsLoading).to.be.true;
    expect(wrapper.props().addComment).to.exist;
    expect(wrapper.props().selectCommentOrdering).to.exist;
    expect(wrapper.props().upvoteComment).to.exist;
    expect(wrapper.props().downvoteComment).to.exist;
    expect(wrapper.props().handleSmallSharePanelFocus).to.exist;
    expect(wrapper.find("TfaShareCommentPanelLarge").length).to.equal(0);
    expect(wrapper.find("TfaCommentList").length).to.equal(0);
    expect(wrapper.find("TfaDisplayMessagePanel").length).to.equal(0);
    expect(wrapper.find("TfaShareCommentPanelSmall").length).to.equal(1);
    expect(wrapper.find("TfaShareCommentPanelSmall").props().currentPersona).to.be.empty;
    expect(wrapper.find("TfaCommentOrderTabs").length).to.equal(1);
    expect(wrapper.find("TfaCommentOrderTabs").props().commentOrdering).to.equal(0);
    expect(wrapper.find("TfaCommentOrderTabs").props().selectCommentOrdering).to.exist;
    expect(wrapper.find("TfaLoadingPanel").length).to.equal(1);
  });

  it("renders comments management panel large share comment panel, empty current persona, comments loading", () => {
    const wrapper = setup(false, {}, [], 1, true);
    expect(wrapper.props().showSmallShareCommentPanel).to.be.false;
    expect(wrapper.props().currentPersona).to.be.empty;
    expect(wrapper.props().comments).to.be.empty;
    expect(wrapper.props().commentOrdering).to.equal(1);
    expect(wrapper.props().commentsLoading).to.be.true;
    expect(wrapper.props().addComment).to.exist;
    expect(wrapper.props().selectCommentOrdering).to.exist;
    expect(wrapper.props().upvoteComment).to.exist;
    expect(wrapper.props().downvoteComment).to.exist;
    expect(wrapper.props().handleSmallSharePanelFocus).to.exist;
    expect(wrapper.find("TfaShareCommentPanelLarge").length).to.equal(1);
    expect(wrapper.find("TfaShareCommentPanelLarge").props().currentPersona).to.be.empty;
    expect(wrapper.find("TfaShareCommentPanelLarge").props().addComment).to.exist;
    expect(wrapper.find("TfaCommentList").length).to.equal(0);
    expect(wrapper.find("TfaDisplayMessagePanel").length).to.equal(0);
    expect(wrapper.find("TfaShareCommentPanelSmall").length).to.equal(0);
    expect(wrapper.find("TfaCommentOrderTabs").length).to.equal(1);
    expect(wrapper.find("TfaCommentOrderTabs").props().commentOrdering).to.equal(1);
    expect(wrapper.find("TfaCommentOrderTabs").props().selectCommentOrdering).to.exist;
    expect(wrapper.find("TfaLoadingPanel").length).to.equal(1);
  });

  it("renders comments management panel small share comment panel, empty current persona, empty comments", () => {
    const wrapper = setup(false, {}, [], 1, false);
    expect(wrapper.props().showSmallShareCommentPanel).to.be.false;
    expect(wrapper.props().currentPersona).to.be.empty;
    expect(wrapper.props().comments).to.be.empty;
    expect(wrapper.props().commentOrdering).to.equal(1);
    expect(wrapper.props().commentsLoading).to.be.false;
    expect(wrapper.props().addComment).to.exist;
    expect(wrapper.props().selectCommentOrdering).to.exist;
    expect(wrapper.props().upvoteComment).to.exist;
    expect(wrapper.props().downvoteComment).to.exist;
    expect(wrapper.props().handleSmallSharePanelFocus).to.exist;
    expect(wrapper.find("TfaShareCommentPanelLarge").length).to.equal(1);
    expect(wrapper.find("TfaShareCommentPanelLarge").props().currentPersona).to.be.empty;
    expect(wrapper.find("TfaShareCommentPanelLarge").props().addComment).to.exist;
    expect(wrapper.find("TfaCommentList").length).to.equal(0);
    expect(wrapper.find("TfaDisplayMessagePanel").length).to.equal(1);
    expect(wrapper.find("TfaDisplayMessagePanel").props().message).to.equal("Currently no comments for this url...");
    expect(wrapper.find("TfaShareCommentPanelSmall").length).to.equal(0);
    expect(wrapper.find("TfaCommentOrderTabs").length).to.equal(1);
    expect(wrapper.find("TfaCommentOrderTabs").props().commentOrdering).to.equal(1);
    expect(wrapper.find("TfaCommentOrderTabs").props().selectCommentOrdering).to.exist;
    expect(wrapper.find("TfaLoadingPanel").length).to.equal(0);
  });

  it("renders comments management panel with comments, no unconfirmed comments", () => {
    const comments = generateComments(2);
    const wrapper = setup(true, {}, comments, 1, false);
    expect(wrapper.props().showSmallShareCommentPanel).to.be.true;
    expect(wrapper.props().currentPersona).to.be.empty;
    expect(wrapper.props().comments.length).to.equal(2);
    expect(wrapper.props().comments).to.equal(comments);
    expect(wrapper.props().commentOrdering).to.equal(1);
    expect(wrapper.props().commentsLoading).to.be.false;
    expect(wrapper.props().addComment).to.exist;
    expect(wrapper.props().selectCommentOrdering).to.exist;
    expect(wrapper.props().upvoteComment).to.exist;
    expect(wrapper.props().downvoteComment).to.exist;
    expect(wrapper.props().handleSmallSharePanelFocus).to.exist;
    expect(wrapper.find("TfaShareCommentPanelLarge").length).to.equal(0);
    expect(wrapper.find("TfaDisplayMessagePanel").length).to.equal(0);
    expect(wrapper.find("TfaShareCommentPanelSmall").length).to.equal(1);
    expect(wrapper.find("TfaShareCommentPanelSmall").props().currentPersona).to.be.empty;
    expect(wrapper.find("TfaCommentOrderTabs").length).to.equal(1);
    expect(wrapper.find("TfaCommentOrderTabs").props().commentOrdering).to.equal(1);
    expect(wrapper.find("TfaCommentOrderTabs").props().selectCommentOrdering).to.exist;
    expect(wrapper.find("TfaLoadingPanel").length).to.equal(0);
    expect(wrapper.find("TfaCommentList").length).to.equal(1);
    expect(wrapper.find("TfaCommentList").props().comments).to.equal(comments);
    expect(wrapper.find("TfaCommentList").props().upvoteComment).to.exist;
    expect(wrapper.find("TfaCommentList").props().downvoteComment).to.exist;
  });

  it("renders comments management panel with unconfirmed comments", () => {
    const comments = generateComments(2);
    comments.forEach(comment => {
      comment.isEnabled = false;
    });
    const wrapper = setup(true, {}, comments, 1, false);
    expect(wrapper.props().showSmallShareCommentPanel).to.be.true;
    expect(wrapper.props().currentPersona).to.be.empty;
    expect(wrapper.props().comments.length).to.equal(2);
    expect(wrapper.props().comments).to.equal(comments);
    expect(wrapper.props().commentOrdering).to.equal(1);
    expect(wrapper.props().commentsLoading).to.be.false;
    expect(wrapper.props().addComment).to.exist;
    expect(wrapper.props().selectCommentOrdering).to.exist;
    expect(wrapper.props().upvoteComment).to.exist;
    expect(wrapper.props().downvoteComment).to.exist;
    expect(wrapper.props().handleSmallSharePanelFocus).to.exist;
    expect(wrapper.find("TfaShareCommentPanelLarge").length).to.equal(0);
    expect(wrapper.find("TfaDisplayMessagePanel").length).to.equal(0);
    expect(wrapper.find("TfaShareCommentPanelSmall").length).to.equal(1);
    expect(wrapper.find("TfaShareCommentPanelSmall").props().currentPersona).to.be.empty;
    expect(wrapper.find("TfaCommentOrderTabs").length).to.equal(1);
    expect(wrapper.find("TfaCommentOrderTabs").props().commentOrdering).to.equal(1);
    expect(wrapper.find("TfaCommentOrderTabs").props().selectCommentOrdering).to.exist;
    expect(wrapper.find("TfaLoadingPanel").length).to.equal(0);
    expect(wrapper.find("TfaCommentList").length).to.equal(1);
    expect(wrapper.find("TfaCommentList").props().comments).to.equal(comments);
    expect(wrapper.find("TfaCommentList").props().upvoteComment).to.exist;
    expect(wrapper.find("TfaCommentList").props().downvoteComment).to.exist;
  });

  it("renders comments management panel with unconfirmed comments, small share comment panel and existing disabled persona", () => {
    const comments = generateComments(2);
    comments.forEach(comment => {
      comment.isEnabled = false;
    });
    const currentPersona = new Persona("account", personasTestJson.persona1.name, personasTestJson.persona1.avatar, 0);
    const wrapper = setup(true, currentPersona, comments, 1, false);
    expect(wrapper.props().showSmallShareCommentPanel).to.be.true;
    expect(wrapper.props().currentPersona).to.equal(currentPersona);
    expect(wrapper.props().comments.length).to.equal(2);
    expect(wrapper.props().comments).to.equal(comments);
    expect(wrapper.props().commentOrdering).to.equal(1);
    expect(wrapper.props().commentsLoading).to.be.false;
    expect(wrapper.props().addComment).to.exist;
    expect(wrapper.props().selectCommentOrdering).to.exist;
    expect(wrapper.props().upvoteComment).to.exist;
    expect(wrapper.props().downvoteComment).to.exist;
    expect(wrapper.props().handleSmallSharePanelFocus).to.exist;
    expect(wrapper.find("TfaShareCommentPanelLarge").length).to.equal(0);
    expect(wrapper.find("TfaDisplayMessagePanel").length).to.equal(0);
    expect(wrapper.find("TfaShareCommentPanelSmall").length).to.equal(1);
    expect(wrapper.find("TfaShareCommentPanelSmall").props().currentPersona).to.equal(currentPersona);
    expect(wrapper.find("TfaCommentOrderTabs").length).to.equal(1);
    expect(wrapper.find("TfaCommentOrderTabs").props().commentOrdering).to.equal(1);
    expect(wrapper.find("TfaCommentOrderTabs").props().selectCommentOrdering).to.exist;
    expect(wrapper.find("TfaLoadingPanel").length).to.equal(0);
    expect(wrapper.find("TfaCommentList").length).to.equal(1);
    expect(wrapper.find("TfaCommentList").props().comments).to.equal(comments);
    expect(wrapper.find("TfaCommentList").props().upvoteComment).to.exist;
    expect(wrapper.find("TfaCommentList").props().downvoteComment).to.exist;
  });

  it("renders comments management panel with unconfirmed comments, large share comment panel and existing unconfirmed persona", () => {
    const comments = generateComments(2);
    comments.forEach(comment => {
      comment.isEnabled = false;
    });
    const currentPersona = new Persona("account", personasTestJson.persona1.name, personasTestJson.persona1.avatar, 0);
    const wrapper = setup(false, currentPersona, comments, 1, false);
    expect(wrapper.props().showSmallShareCommentPanel).to.be.false;
    expect(wrapper.props().currentPersona).to.equal(currentPersona);
    expect(wrapper.props().comments.length).to.equal(2);
    expect(wrapper.props().comments).to.equal(comments);
    expect(wrapper.props().commentOrdering).to.equal(1);
    expect(wrapper.props().commentsLoading).to.be.false;
    expect(wrapper.props().addComment).to.exist;
    expect(wrapper.props().selectCommentOrdering).to.exist;
    expect(wrapper.props().upvoteComment).to.exist;
    expect(wrapper.props().downvoteComment).to.exist;
    expect(wrapper.props().handleSmallSharePanelFocus).to.exist;
    expect(wrapper.find("TfaShareCommentPanelLarge").length).to.equal(1);
    expect(wrapper.find("TfaShareCommentPanelLarge").props().currentPersona).to.equal(currentPersona);
    expect(wrapper.find("TfaShareCommentPanelLarge").props().addComment).to.exist;
    expect(wrapper.find("TfaDisplayMessagePanel").length).to.equal(0);
    expect(wrapper.find("TfaShareCommentPanelSmall").length).to.equal(0);
    expect(wrapper.find("TfaCommentOrderTabs").length).to.equal(1);
    expect(wrapper.find("TfaCommentOrderTabs").props().commentOrdering).to.equal(1);
    expect(wrapper.find("TfaCommentOrderTabs").props().selectCommentOrdering).to.exist;
    expect(wrapper.find("TfaLoadingPanel").length).to.equal(0);
    expect(wrapper.find("TfaCommentList").length).to.equal(1);
    expect(wrapper.find("TfaCommentList").props().comments).to.equal(comments);
    expect(wrapper.find("TfaCommentList").props().upvoteComment).to.exist;
    expect(wrapper.find("TfaCommentList").props().downvoteComment).to.exist;
  });

  it("simulates add comment click disabled", () => {
    const comments = generateComments(2);
    const currentPersona = new Persona("account", personasTestJson.persona1.name, personasTestJson.persona1.avatar, 0, true);
    const addComment = sinon.spy();
    const wrapper = setup(false, currentPersona, comments, 1, false, addComment);
    wrapper.find("Button").simulate("click");
    expect(addComment.called).to.be.false;
  });

  it("simulates add comment click enabled", () => {
    const comments = generateComments(2);
    const currentPersona = new Persona("account", personasTestJson.persona1.name, personasTestJson.persona1.avatar, 0, true);
    const addComment = sinon.spy();
    const wrapper = setup(false, currentPersona, comments, 1, false, addComment);
    wrapper.find("TextField").props().onChange({ target: { value: "test" } });
    wrapper.find("Button").simulate("click");
    expect(addComment.called).to.be.true;
  });

  it("simulates comment ordering selection", () => {
    const comments = generateComments(2);
    const currentPersona = new Persona("account", personasTestJson.persona1.name, personasTestJson.persona1.avatar, 0, true);
    const addComment = sinon.spy();
    const selectCommentOrdering = sinon.spy();
    const wrapper = setup(false, currentPersona, comments, 1, false, addComment, selectCommentOrdering);
    wrapper.find("Tab").at(0).simulate("click");
    expect(selectCommentOrdering.calledWith(0)).to.be.true;
  });

  it("simulates upvote/downvote comment disabled", () => {
    const comments = generateComments(2);
    comments.forEach(comment => {
      comment.isEnabled = false;
    });
    const currentPersona = new Persona("account", personasTestJson.persona1.name, personasTestJson.persona1.avatar, 0, true);
    const addComment = sinon.spy();
    const selectCommentOrdering = sinon.spy();
    const upvoteComment = sinon.spy();
    const downvoteComment = sinon.spy();
    // upvoting/downvoting is disabled because comments are not confirmed
    const wrapper = setup(false, currentPersona, comments, 1, false, addComment, selectCommentOrdering, upvoteComment, downvoteComment);
    wrapper.find("a").at(0).simulate("click");
    expect(upvoteComment.called).to.be.false;
    wrapper.find("a").at(1).simulate("click");
    expect(downvoteComment.called).to.be.false;
  });

  it("simulates upvote/downvote comment enabled", () => {
    const comments = generateComments(2);
    const currentPersona = new Persona("account", personasTestJson.persona1.name, personasTestJson.persona1.avatar, 0, true);
    const addComment = sinon.spy();
    const selectCommentOrdering = sinon.spy();
    const upvoteComment = sinon.spy();
    const downvoteComment = sinon.spy();
    // upvoting/downvoting is enabled because comments are confirmed
    const wrapper = setup(false, currentPersona, comments, 1, false, addComment, selectCommentOrdering, upvoteComment, downvoteComment);
    wrapper.find("a").at(0).simulate("click");
    expect(upvoteComment.calledOnce).to.be.true;
    wrapper.find("a").at(1).simulate("click");
    expect(downvoteComment.calledOnce).to.be.true;
  });
});
