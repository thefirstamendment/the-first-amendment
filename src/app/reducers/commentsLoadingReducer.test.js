import {expect} from 'chai';
import commentsLoadingReducer from './commentsLoadingReducer';
import * as commentsLoadingActions from '../actions/commentsLoadingActions';
import * as commentsActions from '../actions/commentsActions';

describe("commentsLoadingReducer test", () => {
  it("should set comments loading when passed SET_COMMENTS_LOADING", () => {
    const initState = false;
    const action = commentsLoadingActions.setCommentsLoading(true);
    const newState = commentsLoadingReducer(initState, action);

    expect(newState).to.be.true;
  });

  it("should set comments loading to false when passed GET_COMMENTS_SUCCESS", () => {
    const initState = true;
    const action = commentsActions.getCommentsSuccess([]);
    const newState = commentsLoadingReducer(initState, action);

    expect(newState).to.be.false;
  });
});
