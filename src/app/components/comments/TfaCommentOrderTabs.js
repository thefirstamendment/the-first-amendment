import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Tabs, Tab } from '@material-ui/core/';
import style from '../css/TfaCommentOrderTabs.scss';

export default class TfaCommentOrderTabs extends Component {
  constructor(props) {
    super(props);
    this.handleSelect = this.handleSelect.bind(this);
  }

  handleSelect(event, commentOrdering) {
    event.preventDefault();
    this.props.selectCommentOrdering(commentOrdering);
  }

  render() {
    return (
      <Tabs
        className={style.tfa_tabs}
        value={this.props.commentOrdering}
        onChange={this.handleSelect}
        indicatorColor="primary"
        textColor="primary"
        fullWidth
      >
        <Tab
          className={style.tfa_tab}
          label="New" />
        <Tab
          className={style.tfa_tab}
          label="Hot" />
      </Tabs>
    );
  }
}

TfaCommentOrderTabs.propTypes = {
  selectCommentOrdering: PropTypes.func.isRequired,
  commentOrdering: PropTypes.number.isRequired
};
