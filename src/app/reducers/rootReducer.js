import { combineReducers } from 'redux';
import personas from './personasReducer';
import showPersonaManagementPanel from './showPersonaManagementPanelReducer';
import displaySidebar from './displaySidebarReducer';
import currentPersona from './currentPersonaReducer';
import accounts from './accountsReducer';
import showSmallShareCommentPanel from './showSmallShareCommentPanelReducer';
import errorMessage from './errorMessageReducer';
import commentsLoading from './commentsLoadingReducer';
import comments from './commentsReducer';
import allCommentsLoading from './allCommentsLoadingReducer';
import allComments from './allCommentsReducer';
import unconfirmedCommentsCount from './unconfirmedCommentsCountReducer';
import notificationMessage from './notificationMessageReducer';

const rootReducer = combineReducers({
  personas: personas,
  showPersonaManagementPanel: showPersonaManagementPanel,
  displaySidebar: displaySidebar,
  currentPersona: currentPersona,
  accounts: accounts,
  showSmallShareCommentPanel: showSmallShareCommentPanel,
  errorMessage: errorMessage,
  notificationMessage: notificationMessage,
  commentsLoading: commentsLoading,
  comments: comments,
  allCommentsLoading: allCommentsLoading,
  allComments: allComments,
  unconfirmedCommentsCount: unconfirmedCommentsCount
});

export default rootReducer;
