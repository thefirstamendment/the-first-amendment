import React from 'react';
import PropTypes from 'prop-types';
import { Card, CardContent, Avatar, Tooltip } from '@material-ui/core/';
import style from '../css/TfaCurrentPersona.scss';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faEdit } from '@fortawesome/free-solid-svg-icons';
import Utils from '../../../common/Utils';

const TfaCurrentPersona = ({ currentPersona, openEditPersonaModal }) => {
  const currentPersonaAvatar = Utils.getAvatarImg(currentPersona);
  let currentPersonaPanelStyle;
  let currentPersonaPanelRightStyle;
  if (currentPersona.isEnabled) {
    currentPersonaPanelStyle = style.current_persona_panel;
    currentPersonaPanelRightStyle = style.current_persona_panel_right;
  } else {
    currentPersonaPanelStyle = style.current_persona_panel_disabled;
    currentPersonaPanelRightStyle = style.current_persona_panel_right_disabled;
  }
  const editCurrentPersona = currentPersona.isEnabled ?
    (
      <Tooltip
        enterDelay={100}
        id="tfa_edit_persona_tooltip"
        leaveDelay={100}
        placement="bottom"
        title="Edit persona"
        classes={{
          popper: style.tfa_tooltip_popover,
          tooltip: style.tfa_tooltip
        }}
      >
        <a className={style.tfa_edit_current_persona_btn} href="#" rel="noopener" role="button" onClick={openEditPersonaModal}>
          <FontAwesomeIcon icon={faEdit} />
        </a>
      </Tooltip>
    ) : (
      <span className={style.tfa_pending_message}>Pending...</span>
    );
  return (
    <Card className={currentPersonaPanelStyle}>
      <CardContent className={style.current_persona_panel_content}>
        <div className={style.current_persona_panel_left}>
          <Avatar
            src={currentPersonaAvatar}
            className={style.tfa_avatar_img_size96}
          />
        </div>
        <div className={currentPersonaPanelRightStyle}>
          <h4>
            <strong>{currentPersona.name}</strong>
            {editCurrentPersona}
          </h4>
          <br />
          <p>
            {currentPersona.account}
          </p>
          <p>
            Balance: {Math.round(currentPersona.balance * 1000000) / 1000000} ETH
          </p>
        </div>
      </CardContent>
    </Card>
  );
};

TfaCurrentPersona.propTypes = {
  currentPersona: PropTypes.object.isRequired,
  openEditPersonaModal: PropTypes.func.isRequired
};

export default TfaCurrentPersona;

