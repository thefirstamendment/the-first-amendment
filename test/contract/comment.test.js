const Comment = artifacts.require("../Comment.sol"); //eslint-disable-line no-undef

contract('Comment', (accounts) => { //eslint-disable-line no-undef
  it("should return CommentAdded event in tx logs", () => {
    let comment;
    let account_one = accounts[0];
    let urlHash = "0x01234567890123456789";
    let commentHash = "0x01234567890123456789012345678901";
    return Comment.deployed().then((instance) => {
      comment = instance;
      return comment.addComment(urlHash, commentHash, { from: account_one });
    }).then((result) => {
      assert.equal(result.logs.length, 1, "No events were triggered."); //eslint-disable-line no-undef
    });
  });

  it("should fire a CommentAdded event", (test2done) => {
    let comment;
    let account_one = accounts[0];
    let urlHash = "0x01234567890123456789";
    let commentHash = "0x01234567890123456789012345678901";
    Comment.deployed().then((instance) => {
      comment = instance;
      let commentAddedEvent = comment.CommentAdded();
      commentAddedEvent.watch((error, result) => {
        if (error) {
          console.log(error); //eslint-disable-line no-console
        } else {
          assert.equal(result.args.urlHashRef.substring(0, 22), urlHash, "url hash from event doesn't match"); //eslint-disable-line no-undef
          assert.equal(result.args.commentHashRef.substring(0, 34), commentHash, "comment hash from event doesn't match"); //eslint-disable-line no-undef
          commentAddedEvent.stopWatching();
          test2done();
        }
      });
      comment.addComment(urlHash, commentHash, { from: account_one });
    });
  });

  it("should return CommentUpvoted event in tx log", () => {
    let comment;
    let account_one = accounts[0];
    let txHash = "0x01234567890123456789";
    return Comment.deployed().then((instance) => {
      comment = instance;
      return comment.upvoteComment(txHash, { from: account_one });
    }).then((result) => {
      assert.equal(result.logs.length, 1, "No events were triggered."); //eslint-disable-line no-undef
    });
  });

  it("should fire a CommentUpvoted event", (test4done) => {
    let comment;
    let account_one = accounts[0];
    let txHash = "0x01234567890123456789";
    Comment.deployed().then((instance) => {
      comment = instance;
      let commentUpvotedEvent = comment.CommentUpvoted();
      commentUpvotedEvent.watch((error, result) => {
        if (error) {
          console.log(error); //eslint-disable-line no-console
        } else {
          assert.equal(result.args.txHashRef.substring(0, 22), txHash, "url hash from event doesn't match"); //eslint-disable-line no-undef
          commentUpvotedEvent.stopWatching();
          test4done();
        }
      });
      comment.upvoteComment(txHash, { from: account_one });
    });
  });

  it("should return CommentDownvoted event in tx log", () => {
    let comment;
    let account_one = accounts[0];
    let txHash = "0x01234567890123456789";
    return Comment.deployed().then((instance) => {
      comment = instance;
      return comment.downvoteComment(txHash, { from: account_one });
    }).then((result) => {
      assert.equal(result.logs.length, 1, "No events were triggered."); //eslint-disable-line no-undef
    });
  });

  it("should fire a CommentDownvoted event", (test6done) => {
    let comment;
    let account_one = accounts[0];
    let txHash = "0x01234567890123456789";
    Comment.deployed().then((instance) => {
      comment = instance;
      let commentDownvotedEvent = comment.CommentDownvoted();
      commentDownvotedEvent.watch((error, result) => {
        if (error) {
          console.log(error); //eslint-disable-line no-console
        } else {
          assert.equal(result.args.txHashRef.substring(0, 22), txHash, "url hash from event doesn't match"); //eslint-disable-line no-undef
          commentDownvotedEvent.stopWatching();
          test6done();
        }
      });
      comment.downvoteComment(txHash, { from: account_one });
    });
  });

  it("should parse CommentAdded events", (test7done) => {
    let comment;
    let account_one = accounts[0];
    let urlHash1 = "0x23456789012345678901";
    let urlHash2 = "0x98765432109876543210";
    let commentHash = "0x01234567890123456789012345678901";
    Comment.deployed().then((instance) => {
      comment = instance;
      let commentAddedEvent = comment.CommentAdded({
        urlHashRef: urlHash1
      }, {
          fromBlock: 0,
          toBlock: 'latest'
        });
      return comment.addComment(urlHash1, commentHash, { from: account_one }).then(() => {
        return comment.addComment(urlHash2, commentHash, { from: account_one });
      }).then(() => {
        commentAddedEvent.get((err, commentRecords) => {
          assert.equal(commentRecords.length, 1, "Wrong number of comments returned"); //eslint-disable-line no-undef
          assert.equal(commentRecords[0].args.urlHashRef.substring(0, 22), urlHash1, "url hash from event doesn't match"); //eslint-disable-line no-undef
          commentAddedEvent.stopWatching();
          test7done();
        });
      });
    });
  });

  it("should count upvotes", (test8done) => {
    let comment;
    let account_one = accounts[0];
    let txHash = "0x99999999999999999999";
    Comment.deployed().then((instance) => {
      comment = instance;
      let commentUpvotedEvent = comment.CommentUpvoted({
        txHashRef: txHash
      }, {
          fromBlock: 0,
          toBlock: 'latest'
        });
      return comment.upvoteComment(txHash, { from: account_one }).then(() => {
        return comment.upvoteComment(txHash, { from: account_one }).then(() => {
          commentUpvotedEvent.get((err, upvotes) => {
            assert.equal(upvotes.length, 2, "Unexpected number of upvotes"); //eslint-disable-line no-undef
            commentUpvotedEvent.stopWatching();
            test8done();
          });
        });
      });
    });
  });

  it("should count downvotes", (test9done) => {
    let comment;
    let account_one = accounts[0];
    let txHash = "0x99999999999999999999";
    Comment.deployed().then((instance) => {
      comment = instance;
      let commentDownvotedEvent = comment.CommentDownvoted({
        txHashRef: txHash
      }, {
          fromBlock: 0,
          toBlock: 'latest'
        });
      return comment.downvoteComment(txHash, { from: account_one }).then(() => {
        return comment.downvoteComment(txHash, { from: account_one }).then(() => {
          commentDownvotedEvent.get((err, upvotes) => {
            assert.equal(upvotes.length, 2, "Unexpected number of downvotes"); //eslint-disable-line no-undef
            commentDownvotedEvent.stopWatching();
            test9done();
          });
        });
      });
    });
  });
});
