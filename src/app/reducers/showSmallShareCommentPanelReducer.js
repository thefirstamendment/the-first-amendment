import * as types from '../actions/actionTypes';
import initialState from './initialState';

export default function showSmallShareCommentPanelReducer(state = initialState.showSmallShareCommentPanel, action) {
  switch (action.type) {
    case types.SHOW_SMALL_SHARE_COMMENT_PANEL:
      return action.showSmallShareCommentPanel;

    default:
      return state;
  }
}
