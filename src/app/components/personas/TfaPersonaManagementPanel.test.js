import { expect } from 'chai';
import React from 'react';
import TfaPersonaManagementPanel from './TfaPersonaManagementPanel';
import chrome from 'sinon-chrome/extensions';
import sinon from 'sinon';
import { createMount } from '@material-ui/core/test-utils';
import Persona from '../../../common/Persona';
import { JSDOM } from 'jsdom';
import Enzyme from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

Enzyme.configure({ adapter: new Adapter() });

function generatePersonas(personasCount) {
  const personas = [];
  for (let i = 0; i < personasCount; i++) {
    personas.push(new Persona("account" + i, "name" + i, "", 0, true));
  }
  return personas;
}

function generateAccounts(accountsCount) {
  const accounts = [];
  for (let i = 0; i < accountsCount; i++) {
    accounts.push("account" + i);
  }
  return accounts;
}

describe("<TfaPersonaManagementPanel /> tests", () => {
  let mount;

  function setup(
    accounts,
    currentPersona,
    personas,
    isAddPersonaModalOpen,
    isEditPersonaModalOpen,
    togglePersonaManagementPanel = () => { },
    openAddPersonaModal = () => { },
    addPersona = () => { },
    updatePersona = () => { },
    selectPersona = () => { },
    openEditPersonaModal = () => { },
    closeAddPersonaModal = () => { },
    closeEditPersonaModal = () => { },
    showError = () => { }
  ) {
    const props = {
      accounts: accounts,
      currentPersona: currentPersona,
      personas: personas,
      isAddPersonaModalOpen: isAddPersonaModalOpen,
      isEditPersonaModalOpen: isEditPersonaModalOpen,
      togglePersonaManagementPanel: togglePersonaManagementPanel,
      addPersona: addPersona,
      updatePersona: updatePersona,
      selectPersona: selectPersona,
      openAddPersonaModal: openAddPersonaModal,
      openEditPersonaModal: openEditPersonaModal,
      closeAddPersonaModal: closeAddPersonaModal,
      closeEditPersonaModal: closeEditPersonaModal,
      showError: showError
    };
    return mount(<TfaPersonaManagementPanel {...props} />);
  }

  before(() => {
    global.chrome = chrome;
    const jsdom = new JSDOM('<!doctype html><html><body></body></html>');
    const { window } = jsdom;
    global.window = window;
    global.document = window.document;
    mount = createMount();
  });

  after(() => {
    mount.cleanUp();
  });

  it("renders persona management panel no current persona", () => {
    const wrapper = setup([], {}, [], false, false);
    expect(wrapper.props().accounts).to.be.empty;
    expect(wrapper.props().currentPersona).to.be.empty;
    expect(wrapper.props().personas).to.be.empty;
    expect(wrapper.props().isAddPersonaModalOpen).to.be.false;
    expect(wrapper.props().isEditPersonaModalOpen).to.be.false;
    expect(wrapper.props().togglePersonaManagementPanel).to.exist;
    expect(wrapper.props().addPersona).to.exist;
    expect(wrapper.props().updatePersona).to.exist;
    expect(wrapper.props().selectPersona).to.exist;
    expect(wrapper.props().openAddPersonaModal).to.exist;
    expect(wrapper.props().openEditPersonaModal).to.exist;
    expect(wrapper.props().closeAddPersonaModal).to.exist;
    expect(wrapper.props().closeEditPersonaModal).to.exist;
    expect(wrapper.props().showError).to.exist;
    expect(wrapper.find("TfaDisplayMessagePanel").length).to.equal(1);
    expect(wrapper.find("TfaDisplayMessagePanel").props().message).to.equal("No registered personas...");
    expect(wrapper.find("TfaCurrentPersona").length).to.equal(0);
    expect(wrapper.find("TfaUnselectedPersonas").length).to.equal(0);
    expect(wrapper.find("TfaPersonaManagementButtonPanel").length).to.equal(1);
    expect(wrapper.find("TfaPersonaManagementButtonPanel").props().togglePersonaManagementPanel).to.exist;
    expect(wrapper.find("TfaPersonaManagementButtonPanel").props().openAddPersonaModal).to.exist;
    expect(wrapper.find("TfaAddPersonaModal").length).to.equal(1);
    expect(wrapper.find("TfaAddPersonaModal").props().addPersonaModalIsOpen).to.be.false;
    expect(wrapper.find("TfaAddPersonaModal").props().closeAddPersonaModal).to.exist;
    expect(wrapper.find("TfaAddPersonaModal").props().accounts).to.be.empty;
    expect(wrapper.find("TfaAddPersonaModal").props().addPersona).to.exist;
    expect(wrapper.find("TfaAddPersonaModal").props().showError).to.exist;
    expect(wrapper.find("TfaEditPersonaModal").length).to.equal(0);
  });

  it("renders persona management panel with current persona", () => {
    const personas = generatePersonas(1);
    const currentPersona = personas[0];
    const wrapper = setup([], currentPersona, personas, false, false);
    expect(wrapper.props().accounts).to.be.empty;
    expect(wrapper.props().currentPersona).to.equal(currentPersona);
    expect(wrapper.props().personas).to.equal(personas);
    expect(wrapper.props().isAddPersonaModalOpen).to.be.false;
    expect(wrapper.props().isEditPersonaModalOpen).to.be.false;
    expect(wrapper.props().togglePersonaManagementPanel).to.exist;
    expect(wrapper.props().addPersona).to.exist;
    expect(wrapper.props().updatePersona).to.exist;
    expect(wrapper.props().selectPersona).to.exist;
    expect(wrapper.props().openAddPersonaModal).to.exist;
    expect(wrapper.props().openEditPersonaModal).to.exist;
    expect(wrapper.props().closeAddPersonaModal).to.exist;
    expect(wrapper.props().closeEditPersonaModal).to.exist;
    expect(wrapper.props().showError).to.exist;
    expect(wrapper.find("TfaDisplayMessagePanel").length).to.equal(0);
    expect(wrapper.find("TfaCurrentPersona").length).to.equal(1);
    expect(wrapper.find("TfaCurrentPersona").props().currentPersona).to.equal(currentPersona);
    expect(wrapper.find("TfaCurrentPersona").props().openEditPersonaModal).to.exist;
    expect(wrapper.find("TfaUnselectedPersonas").length).to.equal(0);
    expect(wrapper.find("TfaPersonaManagementButtonPanel").length).to.equal(1);
    expect(wrapper.find("TfaPersonaManagementButtonPanel").props().togglePersonaManagementPanel).to.exist;
    expect(wrapper.find("TfaPersonaManagementButtonPanel").props().openAddPersonaModal).to.exist;
    expect(wrapper.find("TfaAddPersonaModal").length).to.equal(1);
    expect(wrapper.find("TfaAddPersonaModal").props().addPersonaModalIsOpen).to.be.false;
    expect(wrapper.find("TfaAddPersonaModal").props().closeAddPersonaModal).to.exist;
    expect(wrapper.find("TfaAddPersonaModal").props().accounts).to.be.empty;
    expect(wrapper.find("TfaAddPersonaModal").props().addPersona).to.exist;
    expect(wrapper.find("TfaAddPersonaModal").props().showError).to.exist;
    expect(wrapper.find("TfaEditPersonaModal").length).to.equal(1);
    expect(wrapper.find("TfaEditPersonaModal").props().editPersonaModalIsOpen).to.be.false;
    expect(wrapper.find("TfaEditPersonaModal").props().closeEditPersonaModal).to.exist;
    expect(wrapper.find("TfaEditPersonaModal").props().currentPersona).to.equal(currentPersona);
    expect(wrapper.find("TfaEditPersonaModal").props().updatePersona).to.exist;
    expect(wrapper.find("TfaEditPersonaModal").props().showError).to.exist;
  });

  it("renders persona management panel with current persona and unselected personas", () => {
    const accounts = generateAccounts(4);
    const personas = generatePersonas(3);
    const currentPersona = personas[0];
    const wrapper = setup(accounts, currentPersona, personas, false, false);
    expect(wrapper.props().accounts).to.equal(accounts);
    expect(wrapper.props().currentPersona).to.equal(currentPersona);
    expect(wrapper.props().personas).to.equal(personas);
    expect(wrapper.props().isAddPersonaModalOpen).to.be.false;
    expect(wrapper.props().isEditPersonaModalOpen).to.be.false;
    expect(wrapper.props().togglePersonaManagementPanel).to.exist;
    expect(wrapper.props().addPersona).to.exist;
    expect(wrapper.props().updatePersona).to.exist;
    expect(wrapper.props().selectPersona).to.exist;
    expect(wrapper.props().openAddPersonaModal).to.exist;
    expect(wrapper.props().openEditPersonaModal).to.exist;
    expect(wrapper.props().closeAddPersonaModal).to.exist;
    expect(wrapper.props().closeEditPersonaModal).to.exist;
    expect(wrapper.props().showError).to.exist;
    expect(wrapper.find("TfaDisplayMessagePanel").length).to.equal(0);
    expect(wrapper.find("TfaCurrentPersona").length).to.equal(1);
    expect(wrapper.find("TfaCurrentPersona").props().currentPersona).to.equal(currentPersona);
    expect(wrapper.find("TfaCurrentPersona").props().openEditPersonaModal).to.exist;
    expect(wrapper.find("TfaUnselectedPersonas").length).to.equal(1);
    expect(wrapper.find("TfaUnselectedPersonas").props().personas).to.deep.equal([personas[1], personas[2]]);
    expect(wrapper.find("TfaUnselectedPersonas").props().selectPersona).to.exist;
    expect(wrapper.find("TfaPersonaManagementButtonPanel").length).to.equal(1);
    expect(wrapper.find("TfaPersonaManagementButtonPanel").props().togglePersonaManagementPanel).to.exist;
    expect(wrapper.find("TfaPersonaManagementButtonPanel").props().openAddPersonaModal).to.exist;
    expect(wrapper.find("TfaAddPersonaModal").length).to.equal(1);
    expect(wrapper.find("TfaAddPersonaModal").props().addPersonaModalIsOpen).to.be.false;
    expect(wrapper.find("TfaAddPersonaModal").props().closeAddPersonaModal).to.exist;
    expect(wrapper.find("TfaAddPersonaModal").props().accounts).to.deep.equal([accounts[3]]);
    expect(wrapper.find("TfaAddPersonaModal").props().addPersona).to.exist;
    expect(wrapper.find("TfaAddPersonaModal").props().showError).to.exist;
    expect(wrapper.find("TfaEditPersonaModal").length).to.equal(1);
    expect(wrapper.find("TfaEditPersonaModal").props().editPersonaModalIsOpen).to.be.false;
    expect(wrapper.find("TfaEditPersonaModal").props().closeEditPersonaModal).to.exist;
    expect(wrapper.find("TfaEditPersonaModal").props().currentPersona).to.equal(currentPersona);
    expect(wrapper.find("TfaEditPersonaModal").props().updatePersona).to.exist;
    expect(wrapper.find("TfaEditPersonaModal").props().showError).to.exist;
  });

  it("renders persona management panel with current persona and unselected personas, add persona modal open", () => {
    const accounts = generateAccounts(4);
    const personas = generatePersonas(3);
    const currentPersona = personas[0];
    const wrapper = setup(accounts, currentPersona, personas, true, false);
    expect(wrapper.props().accounts).to.equal(accounts);
    expect(wrapper.props().currentPersona).to.equal(currentPersona);
    expect(wrapper.props().personas).to.equal(personas);
    expect(wrapper.props().isAddPersonaModalOpen).to.be.true;
    expect(wrapper.props().isEditPersonaModalOpen).to.be.false;
    expect(wrapper.props().togglePersonaManagementPanel).to.exist;
    expect(wrapper.props().addPersona).to.exist;
    expect(wrapper.props().updatePersona).to.exist;
    expect(wrapper.props().selectPersona).to.exist;
    expect(wrapper.props().openAddPersonaModal).to.exist;
    expect(wrapper.props().openEditPersonaModal).to.exist;
    expect(wrapper.props().closeAddPersonaModal).to.exist;
    expect(wrapper.props().closeEditPersonaModal).to.exist;
    expect(wrapper.props().showError).to.exist;
    expect(wrapper.find("TfaDisplayMessagePanel").length).to.equal(0);
    expect(wrapper.find("TfaCurrentPersona").length).to.equal(1);
    expect(wrapper.find("TfaCurrentPersona").props().currentPersona).to.equal(currentPersona);
    expect(wrapper.find("TfaCurrentPersona").props().openEditPersonaModal).to.exist;
    expect(wrapper.find("TfaUnselectedPersonas").length).to.equal(1);
    expect(wrapper.find("TfaUnselectedPersonas").props().personas).to.deep.equal([personas[1], personas[2]]);
    expect(wrapper.find("TfaUnselectedPersonas").props().selectPersona).to.exist;
    expect(wrapper.find("TfaPersonaManagementButtonPanel").length).to.equal(1);
    expect(wrapper.find("TfaPersonaManagementButtonPanel").props().togglePersonaManagementPanel).to.exist;
    expect(wrapper.find("TfaPersonaManagementButtonPanel").props().openAddPersonaModal).to.exist;
    expect(wrapper.find("TfaAddPersonaModal").length).to.equal(1);
    expect(wrapper.find("TfaAddPersonaModal").props().addPersonaModalIsOpen).to.be.true;
    expect(wrapper.find("TfaAddPersonaModal").props().closeAddPersonaModal).to.exist;
    expect(wrapper.find("TfaAddPersonaModal").props().accounts).to.deep.equal([accounts[3]]);
    expect(wrapper.find("TfaAddPersonaModal").props().addPersona).to.exist;
    expect(wrapper.find("TfaAddPersonaModal").props().showError).to.exist;
    expect(wrapper.find("TfaEditPersonaModal").length).to.equal(1);
    expect(wrapper.find("TfaEditPersonaModal").props().editPersonaModalIsOpen).to.be.false;
    expect(wrapper.find("TfaEditPersonaModal").props().closeEditPersonaModal).to.exist;
    expect(wrapper.find("TfaEditPersonaModal").props().currentPersona).to.equal(currentPersona);
    expect(wrapper.find("TfaEditPersonaModal").props().updatePersona).to.exist;
    expect(wrapper.find("TfaEditPersonaModal").props().showError).to.exist;
  });

  it("renders persona management panel with current persona and unselected personas, edit persona modal open", () => {
    const accounts = generateAccounts(4);
    const personas = generatePersonas(3);
    const currentPersona = personas[0];
    const wrapper = setup(accounts, currentPersona, personas, false, true);
    expect(wrapper.props().accounts).to.equal(accounts);
    expect(wrapper.props().currentPersona).to.equal(currentPersona);
    expect(wrapper.props().personas).to.equal(personas);
    expect(wrapper.props().isAddPersonaModalOpen).to.be.false;
    expect(wrapper.props().isEditPersonaModalOpen).to.be.true;
    expect(wrapper.props().togglePersonaManagementPanel).to.exist;
    expect(wrapper.props().addPersona).to.exist;
    expect(wrapper.props().updatePersona).to.exist;
    expect(wrapper.props().selectPersona).to.exist;
    expect(wrapper.props().openAddPersonaModal).to.exist;
    expect(wrapper.props().openEditPersonaModal).to.exist;
    expect(wrapper.props().closeAddPersonaModal).to.exist;
    expect(wrapper.props().closeEditPersonaModal).to.exist;
    expect(wrapper.props().showError).to.exist;
    expect(wrapper.find("TfaDisplayMessagePanel").length).to.equal(0);
    expect(wrapper.find("TfaCurrentPersona").length).to.equal(1);
    expect(wrapper.find("TfaCurrentPersona").props().currentPersona).to.equal(currentPersona);
    expect(wrapper.find("TfaCurrentPersona").props().openEditPersonaModal).to.exist;
    expect(wrapper.find("TfaUnselectedPersonas").length).to.equal(1);
    expect(wrapper.find("TfaUnselectedPersonas").props().personas).to.deep.equal([personas[1], personas[2]]);
    expect(wrapper.find("TfaUnselectedPersonas").props().selectPersona).to.exist;
    expect(wrapper.find("TfaPersonaManagementButtonPanel").length).to.equal(1);
    expect(wrapper.find("TfaPersonaManagementButtonPanel").props().togglePersonaManagementPanel).to.exist;
    expect(wrapper.find("TfaPersonaManagementButtonPanel").props().openAddPersonaModal).to.exist;
    expect(wrapper.find("TfaAddPersonaModal").length).to.equal(1);
    expect(wrapper.find("TfaAddPersonaModal").props().addPersonaModalIsOpen).to.be.false;
    expect(wrapper.find("TfaAddPersonaModal").props().closeAddPersonaModal).to.exist;
    expect(wrapper.find("TfaAddPersonaModal").props().accounts).to.deep.equal([accounts[3]]);
    expect(wrapper.find("TfaAddPersonaModal").props().addPersona).to.exist;
    expect(wrapper.find("TfaAddPersonaModal").props().showError).to.exist;
    expect(wrapper.find("TfaEditPersonaModal").length).to.equal(1);
    expect(wrapper.find("TfaEditPersonaModal").props().editPersonaModalIsOpen).to.be.true;
    expect(wrapper.find("TfaEditPersonaModal").props().closeEditPersonaModal).to.exist;
    expect(wrapper.find("TfaEditPersonaModal").props().currentPersona).to.equal(currentPersona);
    expect(wrapper.find("TfaEditPersonaModal").props().updatePersona).to.exist;
    expect(wrapper.find("TfaEditPersonaModal").props().showError).to.exist;
  });

  it("simulates close button click", () => {
    const accounts = generateAccounts(4);
    const personas = generatePersonas(3);
    const currentPersona = personas[0];
    const togglePersonaManagementPanel = sinon.spy();
    const wrapper = setup(accounts, currentPersona, personas, false, false, togglePersonaManagementPanel);
    wrapper.find("Button").at(1).simulate("click");
    expect(togglePersonaManagementPanel.calledOnce).to.be.true;
  });

  it("simulates add persona button click", () => {
    const accounts = generateAccounts(4);
    const personas = generatePersonas(3);
    const currentPersona = personas[0];
    const togglePersonaManagementPanel = sinon.spy();
    const openAddPersonaModal = sinon.spy();
    const wrapper = setup(accounts, currentPersona, personas, false, false, togglePersonaManagementPanel, openAddPersonaModal);
    wrapper.find("Button").at(0).simulate("click");
    expect(openAddPersonaModal.calledOnce).to.be.true;
  });
});
