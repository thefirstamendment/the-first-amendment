import * as types from './actionTypes';

export function addPersonaSuccess(persona) {
  return { type: types.ADD_PERSONA_SUCCESS, persona: persona };
}

export function updatePersonaSuccess(persona) {
  return { type: types.UPDATE_PERSONA_SUCCESS, persona: persona };
}

export function setPersonas(personas) {
  return { type: types.SET_PERSONAS, personas: personas };
}

export function addPersona(persona) {
  return { type: types.ADD_PERSONA, persona: persona };
}

export function updatePersona(persona) {
  return { type: types.UPDATE_PERSONA, persona: persona };
}
