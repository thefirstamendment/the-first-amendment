import * as types from '../actions/actionTypes';
import initialState from './initialState';
import Utils from '../../common/Utils';

export default function currentPersonaReducer(state = initialState.currentPersona, action) {
  switch (action.type) {
    case types.SET_CURRENT_PERSONA:
      return Object.assign({}, state, action.persona);

    case types.ADD_PERSONA_SUCCESS:
      if (Utils.isEmptyObject(state) || action.persona.account == state.account) {
        return Object.assign({}, state, action.persona);
      } else {
        return state;
      }

    case types.UPDATE_PERSONA_SUCCESS:
      if (action.persona.account == state.account) {
        return Object.assign({}, state, action.persona);
      } else {
        return state;
      }

    default:
      return state;
  }
}
