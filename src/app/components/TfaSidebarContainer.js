import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import TfaErrNotification from './TfaErrNotification';
import { bindActionCreators } from 'redux';
import TfaSidebar from './TfaSidebar';
import * as allCommentsActions from '../actions/allCommentsActions';
import * as errorActions from '../actions/errorActions';
import * as notificationActions from '../actions/notificationActions';

class TfaSidebarContainer extends React.Component {

  constructor(props) {
    super(props);
    this.handleErrorNotificationClose = this.handleErrorNotificationClose.bind(this);
    this.handleNotificationClose = this.handleNotificationClose.bind(this);
    this.openExploreCommentsModal = this.openExploreCommentsModal.bind(this);
    this.closeExploreCommentsModal = this.closeExploreCommentsModal.bind(this);
    this.state = {
      isExploreCommentsModalOpen: false
    };
  }

  handleErrorNotificationClose() {
    this.props.errorActions.hideError();
    // TODO: handleErrorNotificationClose should provide a fallback mechanism based on the error type
  }

  handleNotificationClose() {
    this.props.notificationActions.hideNotification();
  }

  openExploreCommentsModal() {
    this.props.allCommentsActions.getAllComments();
    this.setState({ isExploreCommentsModalOpen: true });
  }

  closeExploreCommentsModal() {
    this.setState({ isExploreCommentsModalOpen: false });
  }

  componentDidCatch(error, info) {
    this.props.errorActions.showError(error);
  }

  render() {
    return (
      <TfaSidebar
        showPersonaManagementPanel={this.props.showPersonaManagementPanel}
        displaySidebar={this.props.displaySidebar}
        errorMessage={this.props.errorMessage}
        handleErrorNotificationClose={this.handleErrorNotificationClose}
        notificationMessage={this.props.notificationMessage}
        handleNotificationClose={this.handleNotificationClose}
        isExploreCommentsModalOpen={this.state.isExploreCommentsModalOpen}
        openExploreCommentsModal={this.openExploreCommentsModal}
        closeExploreCommentsModal={this.closeExploreCommentsModal}
      />
    );
  }
}

TfaSidebarContainer.propTypes = {
  errorActions: PropTypes.object.isRequired,
  notificationActions: PropTypes.object.isRequired,
  allCommentsActions: PropTypes.object.isRequired,
  showPersonaManagementPanel: PropTypes.bool.isRequired,
  displaySidebar: PropTypes.bool.isRequired,
  errorMessage: PropTypes.string.isRequired,
  notificationMessage: PropTypes.string.isRequired
};

function mapStateToProps(state, ownProps) {
  return {
    showPersonaManagementPanel: state.showPersonaManagementPanel,
    displaySidebar: state.displaySidebar,
    errorMessage: state.errorMessage,
    notificationMessage: state.notificationMessage
  };
}

function mapDispatchToProps(dispatch) {
  return {
    errorActions: bindActionCreators(errorActions, dispatch),
    notificationActions: bindActionCreators(notificationActions, dispatch),
    allCommentsActions: bindActionCreators(allCommentsActions, dispatch)
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(TfaSidebarContainer);
