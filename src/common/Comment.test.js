import {expect} from 'chai';
import Persona from './Persona';
import Comment from './Comment';

describe('test Comment POJO', ()=> {
  it('should test getters', ()=> {
    let txHash = '0x01234567890123456789';
    let content = 'tfa sucks';
    let url = 'http://www.tfa.com';
    let persona = new Persona('0x01234567890123456789', 'John Doe');
    let upvoteCount = 3;
    let downvoteCount = 2;
    let timestamp = '1522238147613';
    let comment = new Comment(txHash, content, url, persona, upvoteCount, downvoteCount, timestamp);
    expect(comment.txHash).to.equal(txHash);
    expect(comment.content).to.equal(content);
    expect(comment.url).to.equal(url);
    expect(comment.persona).to.equal(persona);
    expect(comment.upvoteCount).to.equal(upvoteCount);
    expect(comment.downvoteCount).to.equal(downvoteCount);
    expect(comment.timestamp).to.equal(timestamp);
  });
});
