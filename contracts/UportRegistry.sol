pragma solidity ^0.4.19;

contract UportRegistry {
  event AttributesSet(address indexed _sender, uint _timestamp);
  mapping(address => bytes) ipfsAttributeLookup;

  function setAttributes(bytes ipfsHash) public {
    ipfsAttributeLookup[msg.sender] = ipfsHash;
    AttributesSet(msg.sender, now);
  }

  function getAttributes(address personaAddress) public constant returns(bytes) {
    return ipfsAttributeLookup[personaAddress];
  }
}
