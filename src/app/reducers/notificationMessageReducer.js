import * as types from '../actions/actionTypes';
import initialState from './initialState';

export default function notificationMessageReducer(state = initialState.notificationMessage, action) {
  switch (action.type) {
    case types.SHOW_NOTIFICATION:
      return action.notificationMessage;

    case types.HIDE_NOTIFICATION:
      return "";

    default:
      return state;
  }
}
