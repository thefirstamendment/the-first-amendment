/*jshint esversion: 6 */
"use strict";

export default class IpfsError extends Error {
  constructor(message) {
    super(message);
    IpfsError.prototype = Error.prototype;
  }
}
