import { expect } from 'chai';
import React from 'react';
import TfaLoadingPanel from './TfaLoadingPanel';
import chrome from 'sinon-chrome/extensions';
import { createMount } from '@material-ui/core/test-utils';
import { JSDOM } from 'jsdom';
import Enzyme from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

Enzyme.configure({ adapter: new Adapter() });

describe("<TfaLoadingPanel /> tests", () => {
  let mount;

  before(() => {
    global.chrome = chrome;
    const jsdom = new JSDOM('<!doctype html><html><body></body></html>');
    const { window } = jsdom;
    global.window = window;
    global.document = window.document;
    mount = createMount();
  });

  after(() => {
    mount.cleanUp();
  });

  it("has CircularProgress element", () => {
    const wrapper = mount(<TfaLoadingPanel />);
    expect(wrapper.find("CircularProgress").length).to.equal(1);
    expect(wrapper.find("CircularProgress").props().size).to.equal(50);
  });
});
