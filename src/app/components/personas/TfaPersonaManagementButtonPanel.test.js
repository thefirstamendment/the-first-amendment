import { expect } from 'chai';
import React from 'react';
import TfaPersonaManagementButtonPanel from './TfaPersonaManagementButtonPanel';
import sinon from 'sinon';
import chrome from 'sinon-chrome/extensions';
import { createMount } from '@material-ui/core/test-utils';
import { JSDOM } from 'jsdom';
import Enzyme from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

Enzyme.configure({ adapter: new Adapter() });

describe("<TfaPersonaManagementButtonPanel /> tests", () => {
  let mount;

  function setup() {
    const props = {
      togglePersonaManagementPanel: () => { },
      openAddPersonaModal: () => { }
    };
    return mount(<TfaPersonaManagementButtonPanel {...props} />);
  }

  before(() => {
    global.chrome = chrome;
    const jsdom = new JSDOM('<!doctype html><html><body></body></html>');
    const { window } = jsdom;
    global.window = window;
    global.document = window.document;
    mount = createMount();
  });

  after(() => {
    mount.cleanUp();
  });

  it("renders persona management button panel", () => {
    const wrapper = setup();
    expect(wrapper.props().togglePersonaManagementPanel).to.exist;
    expect(wrapper.props().openAddPersonaModal).to.exist;
    expect(wrapper.find("div").length).to.equal(1);
    expect(wrapper.find("Button").length).to.equal(2);
    expect(wrapper.find("Button").at(0).children().text().trim()).to.equal("Add Persona");
    expect(wrapper.find("Button").at(0).props().color).to.equal("primary");
    expect(wrapper.find("Button").at(1).children().text().trim()).to.equal("Close");
    expect(wrapper.find("Button").at(1).props().color).to.equal("default");
  });

  it("simulates click on add persona button", () => {
    const openAddPersonaModal = sinon.spy();
    const togglePersonaManagementPanel = () => { };
    const wrapper = mount(
      <TfaPersonaManagementButtonPanel
        togglePersonaManagementPanel={togglePersonaManagementPanel}
        openAddPersonaModal={openAddPersonaModal}
      />
    );
    wrapper.find("Button").at(0).simulate("click");
    expect(openAddPersonaModal.calledOnce).to.be.true;
  });

  it("simulates click on close button", () => {
    const openAddPersonaModal = () => { };
    const togglePersonaManagementPanel = sinon.spy();
    const wrapper = mount(
      <TfaPersonaManagementButtonPanel
        togglePersonaManagementPanel={togglePersonaManagementPanel}
        openAddPersonaModal={openAddPersonaModal}
      />
    );
    wrapper.find("Button").at(1).simulate("click");
    expect(togglePersonaManagementPanel.calledOnce).to.be.true;
  });
});
