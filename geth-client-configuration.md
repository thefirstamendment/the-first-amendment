# How to configure and run Geth client for TFA demo 

TFA demo is deployed on Ropsten testnet. 

## Windows

1 Install the Ethereum Wallet from https://www.ethereum.org/

2 Open Command Prompt as administrator
```
cd C:\Users\YOUR_USERNAME\AppData\Roaming\Ethereum Wallet\binaries\Geth\unpacked
```
3 Start syncing (port 8545)
```
geth --testnet --fast --cache=12000 --rpc
```
4 Attach to Geth from admin console #2:
```
geth attach ipc:\\.\pipe\geth.ipc
```

5 Create an account (in another console)
```
geth account new
```
You should get an account, something like: 
```
0x281709ab752228829b23b767415d764579cf1cfb
```
Enter a password for generated account:
```
pass: YOUR_PASSWORD
```

6 Top up with some Ropsten ether on http://faucet.ropsten.be:3001/ and then check  balance in console #2 with:
```
web3.eth.getBalance('0x281709ab752228829b23b767415d764579cf1cfb');
```

7 Keys are generated for mainnet, so we have to copy it into ropsten folder. Copy keys (UTC file) from 
```
C:\Users\YOUR_USERNAME\AppData\Roaming\Ethereum\keystore
```
to 
```
C:\Users\YOUR_USERNAME\AppData\Roaming\Ethereum\testnet\keystore
```

8 Unlock your generated account in console #2:
```
personal.unlockAccount('YOUR_ACCOUNT', 'YOUR_PASSWORD', 300000);
```

Geth is now syncing. To be able to use TFA app, Geth needs to be syncronized to the last block. To check progress, you can detach in console #2 with 
```
exit
```
and attach again with 
```
geth attach ipc:\\.\pipe\geth.ipc
```








