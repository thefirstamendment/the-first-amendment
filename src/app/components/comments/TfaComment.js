import React from 'react';
import PropTypes from 'prop-types';
import { Avatar, ListItem, Tooltip } from '@material-ui/core/';
import style from '../css/TfaComment.scss';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faChevronUp, faChevronDown } from '@fortawesome/free-solid-svg-icons';
import TimeAgo from 'react-timeago';
import Utils from '../../../common/Utils';

export default class TfaComment extends React.Component {

  constructor(props) {
    super(props);
    this.getReadableTimestamp = this.getReadableTimestamp.bind(this);
    this.upvoteComment = this.upvoteComment.bind(this);
    this.downvoteComment = this.downvoteComment.bind(this);
    this.getUrlAnchorText = this.getUrlAnchorText.bind(this);
  }

  getReadableTimestamp(timestamp) {
    return new Date(timestamp * 1000).toUTCString();
  }

  upvoteComment() {
    this.props.upvoteComment(this.props.comment.txHash);
  }

  downvoteComment() {
    this.props.downvoteComment(this.props.comment.txHash);
  }

  getUrlAnchorText(url) {
    if(url.length > 100) {
      return url.substring(0, 100) + "...";
    }

    return url;
  }

  render() {
    const personaAvatarImg = Utils.getAvatarImg(this.props.comment.persona);
    const timeAgo = this.props.comment.isEnabled ?
      (<TimeAgo className={style.tfa_time_ago} date={new Date(this.props.comment.timestamp * 1000)} live={false} />) :
      (<span className={style.tfa_time_ago}>Pending...</span>);
    const tfaCommentPanelFooter = this.props.comment.isEnabled ?
      (
        <div className={style.tfa_comment_panel_right_footer}>
          <a className={style.tfa_upvote_btn} href="#" rel="noopener" role="button" onClick={this.upvoteComment} >
            <FontAwesomeIcon icon={faChevronUp} />
          </a>&nbsp;
            <span>{this.props.comment.upvoteCount}</span>&nbsp;&nbsp;&nbsp;&nbsp;
          <a className={style.tfa_downvote_btn} href="#" rel="noopener" role="button" onClick={this.downvoteComment} >
            <FontAwesomeIcon icon={faChevronDown} />
          </a>&nbsp;
            <span>{this.props.comment.downvoteCount}</span>
        </div>
      ) : (
        <Tooltip
          enterDelay={100}
          id="tfa_comment_panel_right_footer_tooltip"
          leaveDelay={100}
          placement="bottom"
          title="This option is disabled until comment is mined and stored on blockchain"
          classes={{
            popper: style.tfa_tooltip_popover,
            tooltip: style.tfa_tooltip
          }}
        >
          <div className={style.tfa_comment_panel_right_footer}>
            <a className={style.tfa_upvote_downvote_btn_disabled} href="#" rel="noopener" role="button" >
              <FontAwesomeIcon icon={faChevronUp} />
            </a>&nbsp;
              <span>{this.props.comment.upvoteCount}</span>&nbsp;&nbsp;&nbsp;&nbsp;
            <a className={style.tfa_upvote_downvote_btn_disabled} href="#" rel="noopener" role="button" >
              <FontAwesomeIcon icon={faChevronDown} />
            </a>&nbsp;
              <span>{this.props.comment.downvoteCount}</span>
          </div>
        </Tooltip>
      );

      const tfaCommentPanelUrl = this.props.showUrl ?
        (<div className={style.tfa_comment_panel_right_footer}>
          <a className={style.tfa_link} href={this.props.comment.url} >
            {this.getUrlAnchorText(this.props.comment.url)}
          </a>
        </div>
        ) : ("");

    return (
      <ListItem className={style.tfa_comment_li} divider>
        <div className={style.tfa_comment_panel}>
          <div className={style.tfa_comment_panel_left}>
            <Avatar
              src={personaAvatarImg}
              className={style.tfa_avatar_img_size48}
            />
          </div>
          <div className={style.tfa_comment_panel_right}>
            <div className={style.tfa_comment_panel_right_header}>
              <strong> {this.props.comment.persona.name} </strong>
              {timeAgo}
            </div>
            <div className={style.tfa_comment_panel_right_content}>
              <p>{this.props.comment.content}</p>
            </div>
            {tfaCommentPanelFooter}
            {tfaCommentPanelUrl}
          </div>
        </div>
      </ListItem>
    );
  }
}

TfaComment.propTypes = {
  comment: PropTypes.object.isRequired,
  upvoteComment: PropTypes.func.isRequired,
  downvoteComment: PropTypes.func.isRequired,
  showUrl: PropTypes.bool.isRequired
};

