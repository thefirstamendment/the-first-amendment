import React from 'react';
import PropTypes from 'prop-types';
import { Avatar, Tooltip, Button } from '@material-ui/core/';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faBars } from '@fortawesome/free-solid-svg-icons';
import style from '../css/TfaHeaderPanel.scss';
import Utils from '../../../common/Utils';

const TfaHeaderPanel = (props) => {
  const currentPersonaName = Utils.getPersonaName(props.currentPersona);
  const currentPersonaAvatar = Utils.getAvatarImg(props.currentPersona);
  const personaManagementTooltipTitle = props.showPersonaManagementPanel ? "Return to comments management panel" : "Persona profile and settings";
  const exploreCommentsTooltipTitle = "Explore latest comments on the network";
  return (
    <div className={style.tfa_header_panel}>
      <div className={style.tfa_header_panel_left}>
        <Tooltip
          enterDelay={100}
          id="tfa_header_panel_explore_comments_tooltip"
          leaveDelay={100}
          placement="bottom"
          title={exploreCommentsTooltipTitle}
          classes={{
            popper: style.tfa_tooltip_popover,
            tooltip: style.tfa_tooltip
          }}
        >
          <Button className={style.tfa_settings_button} variant="flat" onClick={() => props.openExploreCommentsModal()}>
            <FontAwesomeIcon icon={faBars} />
          </Button>
        </Tooltip>
      </div>
      <div className={style.tfa_header_panel_center}>
        <p id="tfaCurrentPersonaGreeting">Hello, {currentPersonaName}</p>
      </div>
      <div className={style.tfa_header_panel_right}>
        <Tooltip
          enterDelay={100}
          id="tfa_header_panel_persona_management_tooltip"
          leaveDelay={100}
          placement="bottom"
          title={personaManagementTooltipTitle}
          classes={{
            popper: style.tfa_tooltip_popover,
            tooltip: style.tfa_tooltip
          }}
        >
          <a id="tfaPersonaManagementButton" href="#" rel="noopener" role="button" onClick={() => props.togglePersonaManagementPanel()} >
            <Avatar
              src={currentPersonaAvatar}
              className={style.tfa_avatar_img_size32}
            />
          </a>
        </Tooltip>
      </div>
    </div>
  );
};

TfaHeaderPanel.propTypes = {
  togglePersonaManagementPanel: PropTypes.func.isRequired,
  showPersonaManagementPanel: PropTypes.bool.isRequired,
  currentPersona: PropTypes.object.isRequired,
  openExploreCommentsModal: PropTypes.func.isRequired
};

export default TfaHeaderPanel;
