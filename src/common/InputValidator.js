/*jshint esversion: 6 */
"use strict";
import createDOMPurify from 'dompurify';
import unescape from 'validator/lib/unescape';
import config from '../../config.json';
import Ajv from 'ajv';
import personaSchema from './schema/PersonaSchema.json';
import commentSchema from './schema/CommentSchema.json';
import validUrl from 'valid-url';
import validDataUrl from 'valid-data-url';

// sanitize configuration
const sanitizeConfig = {
  ALLOWED_TAGS: [],
  ALLOWED_ATTR: []
};

const universalObjectPrototype = "<__proto__>";

export default class InputValidator {
  constructor(window) {
    this.domPurify = createDOMPurify(window);
    this.ajv = new Ajv();
  }

  isValidPersonaObject(persona) {
    // check if null/undefined, and if object
    if (persona == null || typeof (persona) !== "object") {
      return false;
    }

    if (this.ajv.validate(personaSchema, persona)) {
      // validate persona json size
      if (JSON.stringify(persona).length > config.ipfsPersonaMaxSize) {
        return false;
      }

      // validate persona name
      if (!this.isPersonaNameValid(persona.name)) {
        return false;
      }
      // validate persona avatar
      if (!this.isPersonaAvatarValid(persona.avatar)) {
        return false;
      }

      return true;
    }

    return false;
  }

  isValidCommentObject(comment) {
    // check if null/undefined, and if object
    if (comment == null || typeof (comment) !== "object") {
      return false;
    }

    if (this.ajv.validate(commentSchema, comment)) {
      // validate comment json size
      if (JSON.stringify(comment).length > config.ipfsCommentMaxSize) {
        return false;
      }

      // validate comment
      if (!this.isCommentContentValid(comment.content)) {
        return false;
      }

      // validate url
      if (!this.isCommentUrlValid(comment.url)) {
        return false;
      }

      return true;
    }

    return false;
  }

  /**
   * sanitize user comment. Removes html, javascript, and css code from comment.
   *
   * @param {string} content user comment
   * @return {string} Sanitized user comment
   */
  sanitizeComment(content) {
    let trimmedComment, textContent = content;
    do {
      trimmedComment = this.trimCommentContent(textContent);
      textContent = unescape(this.domPurify.sanitize(trimmedComment, sanitizeConfig));
      // workaround to node/window problem
      if (textContent.includes(universalObjectPrototype)) {
        textContent = textContent.replace(universalObjectPrototype, "");
      }
    } while (trimmedComment !== textContent);
    return textContent;
  }

  /**
   * checks if persona name is valid
   *
   * @param {string} name persona name
   * @return {boolean} true if name is valid, otherwise false
   */
  isPersonaNameValid(name) {
    // check if null/undefined, and if string
    if (name == null || typeof (name) !== "string") {
      return false;
    }

    // check if content smaller than min length
    if (name.length < personaSchema.properties.name.minLength) {
      return false;
    }

    // check if content size exceeds max length
    if (name.length > personaSchema.properties.name.maxLength) {
      return false;
    }

    const nameRegex = /(?!.*(-){2,}.*)[a-z0-9](?:[a-z0-9-]{1,32}[a-z0-9])?\b/ig;

    let res = nameRegex.exec(name);
    return res !== null ? res[0] === name : false;
  }

  /**
   * checks if persona avatar (image) is valid
   *
   * @param {string} avatar persona image
   * @return {boolean} true if avatar is valid, otherwise false
   */
  isPersonaAvatarValid(avatar) {
    // check if null/undefined, and if string
    if (avatar == null || typeof (avatar) !== "string") {
      return false;
    }

    // check if avatar is empty, this is allowed if persona didn't set image
    if (avatar.length === 0) {
      return true;
    }

    // check avatar size exceeds max length
    if (avatar.length > personaSchema.properties.avatar.maxLength) {
      return false;
    }

    // check if png or jpeg
    let isPng = avatar.startsWith("data:image/png;base64,");
    let isJpg = avatar.startsWith("data:image/jpeg;base64,");
    if (isPng || isJpg) {
      let base64 = avatar.substring(isPng ? 22 : 23, avatar.length);
       if (validDataUrl(avatar)) {
        const base64Regex = /^([0-9a-zA-Z+/]{4})*(([0-9a-zA-Z+/]{2}==)|([0-9a-zA-Z+/]{3}=))?$/;
        return base64Regex.test(base64);
      }
    }

    return false;
  }

  /**
   * checks if comment content is valid
   *
   * @param {string} content contents of the comment
   * @return {boolean} true if content is valid, otherwise false
   */
  isCommentContentValid(content) {
    // check if null/undefined, and if string
    if (content == null || typeof (content) !== "string") {
      return false;
    }

    // check if content smaller than min length
    if (content.length < commentSchema.properties.content.minLength) {
      return false;
    }

    // check if content size exceeds max length
    if (content.length > commentSchema.properties.content.maxLength) {
      return false;
    }

    // if needs sanitation, not valid
    if (content !== this.sanitizeComment(content)) {
      return false;
    }

    return true;
  }

  /**
   * checks if comment url is valid
   *
   * @param {string} url url where comment is posted
   * @return {boolean} true if url is valid, otherwise false
   */
  isCommentUrlValid(url) {
    // check if null/undefined, and if string
    if (url == null || typeof (url) !== "string") {
      return false;
    }

    // check if url size smaller than min length
    if (url.length < commentSchema.properties.url.minLength) {
      return false;
    }

    // check if url size exceeds max length
    if (url.length > commentSchema.properties.url.maxLength) {
      return false;
    }
    return (validUrl.isWebUri(url) !== undefined);
  }

  /**
   * Trim user comment if comment is longer than max length
   * specified in configuration (in bytes).
   *
   * @param {string} content user comment
   * @return {string} Trimmed user comment
   */
  trimCommentContent(content) {
    if (content.length > commentSchema.properties.content.maxLength) {
      return content.substring(0, commentSchema.properties.content.maxLength).trim();
    } else {
      return content.trim();
    }
  }
}
