import { expect } from 'chai';
import React from 'react';
import TfaShareCommentPanelLargeRight from './TfaShareCommentPanelLargeRight';
import chrome from 'sinon-chrome/extensions';
import sinon from 'sinon';
import { createMount } from '@material-ui/core/test-utils';
import { JSDOM } from 'jsdom';
import Enzyme from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

Enzyme.configure({ adapter: new Adapter() });

describe("<TfaShareCommentPanelLargeRight /> tests", () => {
  let mount;

  function setup(addComment = () => { }) {
    const props = {
      addComment: addComment,
    };
    return mount(<TfaShareCommentPanelLargeRight {...props} />);
  }

  before(() => {
    global.chrome = chrome;
    const jsdom = new JSDOM('<!doctype html><html><body></body></html>');
    const { window } = jsdom;
    global.window = window;
    global.document = window.document;
    mount = createMount();
  });

  after(() => {
    mount.cleanUp();
  });

  it("renders share comment panel large right with add comment disabled", () => {
    const wrapper = setup();
    expect(wrapper.props().addComment).to.exist;
    expect(wrapper.find("FormGroup").length).to.equal(1);
    expect(wrapper.find("FormControl").length).to.equal(2);
    expect(wrapper.find("TextField").length).to.equal(1);
    expect(wrapper.find("TextField").props().multiline).to.exist;
    expect(wrapper.find("TextField").props().fullWidth).to.exist;
    expect(wrapper.find("TextField").props().placeholder).to.equal("Speak up!");
    expect(wrapper.find("TextField").props().rows).to.equal("1");
    expect(wrapper.find("TextField").props().rowsMax).to.equal("4");
    expect(wrapper.find("TextField").props().margin).to.equal("none");
    expect(wrapper.find("TextField").props().onChange).to.exist;
    expect(wrapper.find("Button").length).to.equal(1);
    expect(wrapper.find("Button").props().color).to.equal("primary");
    expect(wrapper.find("Button").props().onClick).to.not.exist;
    expect(wrapper.find("Button").props().disabled).to.exist;
  });

  it("simulates add comment click disabled", () => {
    const addComment = sinon.spy();
    const wrapper = setup(addComment);
    wrapper.find("Button").simulate("click");
    expect(addComment.called).to.be.false;
  });

  it("simulates add comment click enabled", () => {
    const addComment = sinon.spy();
    const wrapper = setup(addComment);
    wrapper.find("TextField").props().onChange({target: {value: "test"}});
    wrapper.find("Button").simulate("click");
    expect(addComment.calledOnce).to.be.true;
  });
});
