pragma solidity ^0.4.19;

contract Comment {
  event CommentAdded(bytes20 indexed urlHashRef, bytes32 commentHashRef);
  event CommentUpvoted(bytes20 indexed txHashRef);
  event CommentDownvoted(bytes20 indexed txHashRef);

  function addComment(bytes20 urlHashRef, bytes32 commentHashRef) public {
    CommentAdded(urlHashRef, commentHashRef);
  }

  function upvoteComment(bytes20 txHashRef) public {
    CommentUpvoted(txHashRef);
  }

  function downvoteComment(bytes20 txHashRef) public {
    CommentDownvoted(txHashRef);
  }
}
