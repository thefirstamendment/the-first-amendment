import { expect } from 'chai';
import React from 'react';
import TfaAvatarDragAndDrop from './TfaAvatarDragAndDrop';
import chrome from 'sinon-chrome/extensions';
import { createMount } from '@material-ui/core/test-utils';
import { JSDOM } from 'jsdom';
import Enzyme from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

Enzyme.configure({ adapter: new Adapter() });

describe("<TfaAvatarDragAndDrop /> tests", () => {
  let mount;

  function setup(
    avatar,
    setAvatar = () => { },
    showError = () => { }
  ) {
    const props = {
      avatar: avatar,
      showError: showError,
      setAvatar: setAvatar,
    };
    return mount(<TfaAvatarDragAndDrop {...props} />);
  }

  before(() => {
    global.chrome = chrome;
    const jsdom = new JSDOM('<!doctype html><html><body></body></html>');
    const { window } = jsdom;
    global.window = window;
    global.document = window.document;
    mount = createMount();
  });

  after(() => {
    mount.cleanUp();
  });

  it("renders avatar drag and drop with existing avatar", () => {
    const avatar = "avatar";
    const wrapper = setup(avatar);
    expect(wrapper.props().avatar).to.equal(avatar);
    expect(wrapper.props().showError).to.exist;
    expect(wrapper.props().setAvatar).to.exist;
    expect(wrapper.find("Tooltip").length).to.equal(1);
    expect(wrapper.find("Tooltip").props().enterDelay).to.equal(100);
    expect(wrapper.find("Tooltip").props().leaveDelay).to.equal(100);
    expect(wrapper.find("Tooltip").props().placement).to.equal("bottom");
    expect(wrapper.find("Tooltip").props().title).to.equal("Click here or drag and drop to set avatar image");
    expect(wrapper.find("#tfaDropzoneComponent").at(0).props().accept).to.equal("image/jpeg, image/png");
    expect(wrapper.find("#tfaDropzoneComponent").at(0).props().onDrop).to.exist;
    expect(wrapper.find("#tfaDropzoneComponent").at(0).props().multiple).to.be.false;
    expect(wrapper.find("#tfaDropzoneComponent").at(0).props().maxSize).to.equal(50000);
    expect(wrapper.find("Avatar").length).to.equal(1);
    expect(wrapper.find("Avatar").props().src).to.equal(avatar);
  });
});
