import 'jquery';
import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { Store } from 'react-chrome-redux';
import TfaSidebarContainer from '../app/components/TfaSidebarContainer';
import InputValidator from '../common/InputValidator';
import * as reduxPortName from './reduxPortName';
import { displaySidebar } from '../app/actions/displaySidebarActions';
import { showSmallShareCommentPanel } from '../app/actions/shareCommentsPanelActions';
import { getComments } from '../app/actions/commentsActions';
import Utils from '../common/Utils';

// init store
const store = new Store({
  portName: reduxPortName.THE_FIRST_AMENDMENT_PORT
});

store.ready().then(() => {
  // handle messages from background.js
  manageShareCommentPanelDisplayStyle();
  chrome.runtime.onMessage.addListener((request) => { //eslint-disable-line no-undef
    if (request.callFunction == "toggleSidebar") {
      toggleSidebar(request.messageParams);
    }
  });
});

function renderSidebar() {
  const unsubscribe = store.subscribe(() => {
    unsubscribe(); // make sure to only fire once
    ReactDOM.render(
      <Provider store={store}>
        <TfaSidebarContainer />
      </Provider>,
      document.getElementById('tfaSidebar')
    );
  });
}

function toggleSidebar(response) {
  if (!isSidebarMounted()) {
    // init sidebar
    createAndAppendSidebar();
  }
  store.dispatch(displaySidebar(response.displaySidebar));
  if (response.displaySidebar) {
    store.dispatch(getComments(Utils.getCurrentUrl()));
  }
}

function createAndAppendSidebar() {
  let tfaSidebar = document.createElement('div');
  tfaSidebar.id = "tfaSidebar";
  document.body.appendChild(tfaSidebar);
  renderSidebar();
}

function isSidebarMounted() {
  return $('#tfaSidebar').length;
}

function manageShareCommentPanelDisplayStyle() {
  $('body').click((evt) => {
    if (($("#tfaShareCommentPanelLarge")[0] != undefined)
      && (($("#tfaShareCommentPanelLarge")[0].contains(evt.target)) || ($("#tfaShareCommentPanelInputLarge").val() != ""))
      || ($("#tfaPersonaManagementButton")[0].contains(evt.target))) {
      return;
    } else if (($("#tfaShareCommentPanelInputSmall")[0] != undefined)
      && $("#tfaShareCommentPanelInputSmall")[0].contains(evt.target)
      && !$("#tfaShareCommentPanelInputSmall").is(':disabled')) {
      store.dispatch(showSmallShareCommentPanel(false));
    } else {
      store.dispatch(showSmallShareCommentPanel(true));
    }
  });
}
