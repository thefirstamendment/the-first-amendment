const ipfsAPI = require('ipfs-api');
const config = require('../../config.json');
const Web3 = require('web3');
const ipfs = ipfsAPI();

let web3Provider, ipfsProvider;

module.exports = (cb) => {
  if (!web3Provider && !ipfsProvider) {
    // setup web3 host
    let host = config[config['selection']].web3Host;
    let web3port = config[config['selection']].web3Port;
    let web3 = new Web3();
    web3Provider = new web3.providers.HttpProvider('http://' + host + ':' + web3port);

    cb(null, { web3Provider: web3Provider, ipfsProvider: ipfs });
  } else {
    cb(null, { web3Provider: web3Provider, ipfsProvider: ipfsProvider });
  }
};
