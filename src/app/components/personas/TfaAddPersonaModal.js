import React from 'react';
import PropTypes from 'prop-types';
import {
  Dialog,
  Button,
  DialogTitle,
  DialogContent,
  DialogActions,
  FormControl,
  FormGroup,
  InputLabel,
  TextField,
  Select,
  InputAdornment,
  Tooltip
} from '@material-ui/core/';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faSave, faInfo } from '@fortawesome/free-solid-svg-icons';
import Persona from '../../../common/Persona';
import personaSchema from '../../../common/schema/PersonaSchema.json';
import TfaAvatarDragAndDrop from './TfaAvatarDragAndDrop';
import TfaDisplayMessagePanel from '../TfaDisplayMessagePanel';
import InputValidator from '../../../common/InputValidator';
import style from '../css/TfaPersonaModal.scss';
import Utils from '../../../common/Utils';

export default class TfaAddPersonaModal extends React.Component {

  constructor(props) {
    super(props);
    this.addPersona = this.addPersona.bind(this);
    this.handleAvatarChange = this.handleAvatarChange.bind(this);
    this.handleNameChange = this.handleNameChange.bind(this);
    this.handleAccountChange = this.handleAccountChange.bind(this);
    this.handleModalClose = this.handleModalClose.bind(this);
    this.inputValidator = new InputValidator(window);
    this.state = {
      name: '',
      isNameValid: true,
      avatar: '',
      isAvatarValid: true,
      isAvatarEdited: false,
      account: '',
      isAccountValid: false
    };
  }

  handleModalClose() {
    this.setState({
      name: '',
      isNameValid: true,
      avatar: '',
      isAvatarValid: true,
      isAvatarEdited: false,
      account: '',
      isAccountValid: false
    });
    this.props.closeAddPersonaModal();
  }

  addPersona() {
    let avatar = !this.state.isAvatarEdited ? '' : this.state.avatar;
    this.props.addPersona(new Persona(this.state.account, this.state.name.trim(), avatar));
    this.handleModalClose();
  }

  handleAvatarChange(dataUrl) {
    this.setState({
      avatar: dataUrl,
      isAvatarValid: this.inputValidator.isPersonaAvatarValid(dataUrl),
      isAvatarEdited: true
    });
  }

  handleNameChange(e) {
    // disable hijacking of text input field from native page by restoring focus on blur event
    const textField = document.activeElement;
    document.activeElement.onblur = (e) => {
      e.preventDefault();
      textField.focus();
    };
    this.setState({
      name: e.target.value,
      isNameValid: this.inputValidator.isPersonaNameValid(e.target.value)
    });
  }

  handleAccountChange(e) {
    if (!e.target.value.length > 0) {
      this.setState({
        account: e.target.value,
        isAccountValid: false
      });
    } else {
      this.setState({
        account: e.target.value,
        isAccountValid: true
      });
    }
  }

  render() {
    let dialogContent;
    let saveButton;
    const tooltipTitle = (
      <ol>
        <li>Username may only contain alphanumeric characters or hyphens.</li>
        <li>Username cannot have multiple consecutive hyphens.</li>
        <li>Username cannot begin or end with a hyphen.</li>
        <li>Minimum is 3 characters.</li>
      </ol>
    );
    const usernameEndAdornment = (
      <InputAdornment position="end">
        <Tooltip
          enterDelay={100}
          id="tfa_username_rules_tooltip"
          leaveDelay={100}
          placement="bottom"
          title={tooltipTitle}
          classes={{
            popper: style.tfa_tooltip_popover,
            tooltip: style.tfa_tooltip
          }}
        >
          <FontAwesomeIcon className={this.state.isNameValid ? style.tfa_username_rules : style.tfa_username_rules_error} icon={faInfo} />
        </Tooltip>
      </InputAdornment>
    );
    if (this.props.accounts.length > 0) {
      let accountOptions = this.props.accounts.map((account) => <option key={account} value={account}>{account}</option>);
      saveButton = (this.state.isNameValid && this.state.isAccountValid && this.state.isAvatarValid) ?
        (
          <Button className={style.save_btn} variant="raised" color="primary" onClick={() => this.addPersona()} >Save&nbsp;&nbsp;
            <FontAwesomeIcon icon={faSave} />
          </Button>
        ) :
        (
          <Button className={style.save_btn_disabled} variant="raised" color="primary" disabled >Save&nbsp;&nbsp;
            <FontAwesomeIcon icon={faSave} />
          </Button>
        );
      const defaultAvatarImage = Utils.getDefaultAvatarImg();
      let avatar = !this.state.isAvatarEdited ? defaultAvatarImage : this.state.avatar;
      dialogContent = (
        <DialogContent className={style.tfa_dialog_content}>
          <FormGroup>
            <FormControl className={style.tfa_avatar_selector_form_control}>
              <TfaAvatarDragAndDrop
                avatar={avatar}
                setAvatar={this.handleAvatarChange}
                showError={this.props.showError}
              />
            </FormControl>
            <FormControl className={style.tfa_form_control_name}>
              <TextField
                className={style.tfa_name_input}
                fullWidth
                id="tfaPersonaName"
                label="Username"
                margin="normal"
                error={!this.state.isNameValid}
                value={this.state.name}
                onChange={this.handleNameChange}
                inputProps={{
                  maxLength: personaSchema.properties.name.maxLength
                }}
                InputProps={{
                  classes: {
                    input: style.tfa_name_input,
                  },
                  endAdornment: usernameEndAdornment
                }}
              />
            </FormControl>
            <FormControl
              className={style.tfa_form_control_account}
              classes={{ root: style.tfa_form_control_account_root }}
            >
              <InputLabel htmlFor="age-native-simple">Account</InputLabel>
              <Select
                className={style.tfa_select}
                native
                value={this.state.account}
                onChange={this.handleAccountChange}
              >
                <option value="" />
                {accountOptions}
              </Select>
            </FormControl>
          </FormGroup>
        </DialogContent>
      );
    } else {
      dialogContent = (
        <DialogContent className={style.tfa_dialog_content}>
          <TfaDisplayMessagePanel
            message="No available accounts for persona creation..."
          />
        </DialogContent>
      );
      saveButton = (
        <Button className={style.save_btn_disabled} variant="raised" color="primary" disabled >Save&nbsp;&nbsp;
          <FontAwesomeIcon icon={faSave} />
        </Button>
      );
    }

    return (
      <Dialog
        disableRestoreFocus
        open={this.props.addPersonaModalIsOpen}
        onClose={() => this.props.closeAddPersonaModal()}
        aria-labelledby="form-dialog-title"
        className={style.tfa_dialog}
      >
        <DialogTitle className={style.tfa_dialog_title}>Add New Persona</DialogTitle>
        {dialogContent}
        <DialogActions>
          {saveButton}
          <Button className={style.close_btn} variant="raised" color="default" onClick={this.handleModalClose}>Close</Button>
        </DialogActions>
      </Dialog>
    );
  }
}

TfaAddPersonaModal.propTypes = {
  addPersonaModalIsOpen: PropTypes.bool.isRequired,
  closeAddPersonaModal: PropTypes.func.isRequired,
  accounts: PropTypes.array.isRequired,
  addPersona: PropTypes.func.isRequired,
  showError: PropTypes.func.isRequired
};
