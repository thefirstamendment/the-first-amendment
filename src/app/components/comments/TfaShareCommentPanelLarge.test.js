import { expect } from 'chai';
import React from 'react';
import sinon from 'sinon';
import chrome from 'sinon-chrome/extensions';
import { JSDOM } from 'jsdom';
import TfaShareCommentPanelLarge from './TfaShareCommentPanelLarge';
import { createMount } from '@material-ui/core/test-utils';
import personasTestJson from '../../../../test/integration/test_personas.json';
import Persona from '../../../common/Persona';
import Enzyme from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import Utils from '../../../common/Utils';

Enzyme.configure({ adapter: new Adapter() });

describe("<TfaShareCommentPanelLarge /> tests", () => {
  let mount;
  let getDefaultAvatarImgStub;

  function setup(currentPersona) {
    const props = {
      currentPersona: currentPersona,
      addComment: () => { }
    };
    return mount(<TfaShareCommentPanelLarge {...props} />);
  }


  before(() => {
    global.chrome = chrome;
    const jsdom = new JSDOM('<!doctype html><html><body></body></html>');
    const { window } = jsdom;
    global.window = window;
    global.document = window.document;
    getDefaultAvatarImgStub = sinon.stub(Utils, "getDefaultAvatarImg");
    getDefaultAvatarImgStub.returns("defaultAvatarImg");
    mount = createMount();
  });

  after(() => {
    mount.cleanUp();
    getDefaultAvatarImgStub.restore();
  });

  it("renders share comment panel large with empty current persona", () => {
    const currentPersona = {};
    const expectedPersonaImageAvatar = Utils.getAvatarImg(currentPersona);
    const wrapper = setup(currentPersona);
    expect(wrapper.props().currentPersona).to.be.empty;
    expect(wrapper.props().addComment).to.be.exist;
    expect(wrapper.find("TfaShareCommentPanelLargeLeft").length).to.equal(1);
    expect(wrapper.find("TfaShareCommentPanelLargeLeft").props().personaImageAvatar).to.not.be.undefined;
    expect(wrapper.find("TfaShareCommentPanelLargeLeft").props().personaImageAvatar).to.not.equal("");
    expect(wrapper.find("TfaShareCommentPanelLargeLeft").props().personaImageAvatar).to.equal(expectedPersonaImageAvatar);
    expect(wrapper.find("TfaShareCommentPanelLargeRight").length).to.equal(1);
    expect(wrapper.find("TfaShareCommentPanelLargeRight").props().addComment).to.exist;
  });

  it("renders share comment panel large with undefined current persona", () => {
    const currentPersona = undefined;
    const expectedPersonaImageAvatar = Utils.getAvatarImg(currentPersona);
    const wrapper = setup(currentPersona);
    expect(wrapper.props().currentPersona).to.be.undefined;
    expect(wrapper.props().addComment).to.be.exist;
    expect(wrapper.find("TfaShareCommentPanelLargeLeft").length).to.equal(1);
    expect(wrapper.find("TfaShareCommentPanelLargeLeft").props().personaImageAvatar).to.not.be.undefined;
    expect(wrapper.find("TfaShareCommentPanelLargeLeft").props().personaImageAvatar).to.not.equal("");
    expect(wrapper.find("TfaShareCommentPanelLargeLeft").props().personaImageAvatar).to.equal(expectedPersonaImageAvatar);
    expect(wrapper.find("TfaShareCommentPanelLargeRight").length).to.equal(1);
    expect(wrapper.find("TfaShareCommentPanelLargeRight").props().addComment).to.exist;
  });

  it("renders share comment panel large with existing current persona", () => {
    const currentPersona = new Persona("account", personasTestJson.persona1.name, personasTestJson.persona1.avatar, 0);
    const wrapper = setup(currentPersona);
    expect(wrapper.props().currentPersona).to.equal(currentPersona);
    expect(wrapper.props().addComment).to.be.exist;
    expect(wrapper.find("TfaShareCommentPanelLargeLeft").length).to.equal(1);
    expect(wrapper.find("TfaShareCommentPanelLargeLeft").props().personaImageAvatar).to.not.equal("");
    expect(wrapper.find("TfaShareCommentPanelLargeLeft").props().personaImageAvatar).to.equal(currentPersona.avatar);
    expect(wrapper.find("TfaShareCommentPanelLargeRight").length).to.equal(1);
    expect(wrapper.find("TfaShareCommentPanelLargeRight").props().addComment).to.exist;
  });
});
