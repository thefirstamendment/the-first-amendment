import React from 'react';
import PropTypes from 'prop-types';
import TfaPersonaManagementPanel from './TfaPersonaManagementPanel';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as personaManagementPanelActions from '../../actions/personaManagementPanelActions';
import * as personaActions from '../../actions/personaActions';
import * as currentPersonaActions from '../../actions/currentPersonaActions';
import * as errorActions from '../../actions/errorActions';
import InputValidationError from '../../../common/errors/InputValidationError';

export class TfaPersonaManagementPanelContainer extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      isAddPersonaModalOpen: false,
      isEditPersonaModalOpen: false
    };
    this.openAddPersonaModal = this.openAddPersonaModal.bind(this);
    this.closeAddPersonaModal = this.closeAddPersonaModal.bind(this);
    this.openEditPersonaModal = this.openEditPersonaModal.bind(this);
    this.closeEditPersonaModal = this.closeEditPersonaModal.bind(this);
    this.togglePersonaManagementPanel = this.togglePersonaManagementPanel.bind(this);
    this.addPersona = this.addPersona.bind(this);
    this.updatePersona = this.updatePersona.bind(this);
    this.selectPersona = this.selectPersona.bind(this);
    this.showError = this.showError.bind(this);
  }

  addPersona(persona) {
    this.props.personaActions.addPersona(persona);
  }

  updatePersona(persona) {
    this.props.personaActions.updatePersona(persona);
  }

  selectPersona(account) {
    const newCurrentPersona = this.props.personas.find(persona => persona.account === account);
    if (newCurrentPersona) {
      this.props.currentPersonaActions.setCurrentPersona(newCurrentPersona);
    } else {
      this.props.errorActions.showError(new InputValidationError("No existing personas match selected persona account!"));
    }
  }

  togglePersonaManagementPanel() {
    this.props.personaManagementPanelActions.togglePersonaManagementPanel();
  }

  openAddPersonaModal() {
    this.setState({ isAddPersonaModalOpen: true });
  }

  closeAddPersonaModal() {
    this.setState({ isAddPersonaModalOpen: false });
  }

  openEditPersonaModal() {
    this.setState({ isEditPersonaModalOpen: true });
  }

  closeEditPersonaModal() {
    this.setState({ isEditPersonaModalOpen: false });
  }

  showError(error) {
    this.props.errorActions.showError(error);
  }

  render() {
    return (
      <TfaPersonaManagementPanel
        accounts={this.props.accounts}
        personas={this.props.personas}
        currentPersona={this.props.currentPersona}
        togglePersonaManagementPanel={this.togglePersonaManagementPanel}
        addPersona={this.addPersona}
        updatePersona={this.updatePersona}
        selectPersona={this.selectPersona}
        openAddPersonaModal={this.openAddPersonaModal}
        openEditPersonaModal={this.openEditPersonaModal}
        closeAddPersonaModal={this.closeAddPersonaModal}
        closeEditPersonaModal={this.closeEditPersonaModal}
        isAddPersonaModalOpen={this.state.isAddPersonaModalOpen}
        isEditPersonaModalOpen={this.state.isEditPersonaModalOpen}
        showError={this.showError}
      />
    );
  }
}

TfaPersonaManagementPanelContainer.propTypes = {
  errorActions: PropTypes.object.isRequired,
  personaManagementPanelActions: PropTypes.object.isRequired,
  personaActions: PropTypes.object.isRequired,
  currentPersonaActions: PropTypes.object.isRequired,
  togglePersonaManagementPanel: PropTypes.func.isRequired,
  accounts: PropTypes.array.isRequired,
  currentPersona: PropTypes.object.isRequired,
  personas: PropTypes.array.isRequired,
  addPersona: PropTypes.func.isRequired,
  updatePersona: PropTypes.func.isRequired,
  selectPersona: PropTypes.func.isRequired
};

function mapStateToProps(state, ownProps) {
  return {
    accounts: state.accounts,
    personas: state.personas,
    currentPersona: state.currentPersona,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    personaManagementPanelActions: bindActionCreators(personaManagementPanelActions, dispatch),
    personaActions: bindActionCreators(personaActions, dispatch),
    currentPersonaActions: bindActionCreators(currentPersonaActions, dispatch),
    errorActions: bindActionCreators(errorActions, dispatch)
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(TfaPersonaManagementPanelContainer);
