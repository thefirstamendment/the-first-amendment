/*jshint esversion: 6 */
"use strict";

export default class InputValidationError extends Error {
  constructor(message) {
    super(message);
    InputValidationError.prototype = Error.prototype;
  }
}
