import { expect } from 'chai';
import React from 'react';
import TfaUnselectedPersonas from './TfaUnselectedPersonas';
import chrome from 'sinon-chrome/extensions';
import { createMount } from '@material-ui/core/test-utils';
import Persona from '../../../common/Persona';
import { JSDOM } from 'jsdom';
import Enzyme from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import raf from 'raf';

Enzyme.configure({ adapter: new Adapter() });

function generatePersonas(personasCount) {
  const personas = [];
  for (let i = 0; i < personasCount; i++) {
    personas.push(new Persona("account" + i, "name" + i, "", 0, true));
  }
  return personas;
}

describe("<TfaUnselectedPersonas /> tests", () => {
  let mount;

  function setup(personas) {
    const props = {
      selectPersona: () => { },
      personas: personas
    };
    return mount(<TfaUnselectedPersonas {...props} />);
  }


  before(() => {
    global.chrome = chrome;
    const jsdom = new JSDOM('<!doctype html><html><body></body></html>');
    const { window } = jsdom;
    global.window = window;
    global.document = window.document;
    // components that use maps need to have requestAnimationFrame polyfill
    raf.polyfill(global);
    mount = createMount();
  });

  after(() => {
    mount.cleanUp();
  });

  it("renders empty unselected personas list", () => {
    const wrapper = setup([]);
    expect(wrapper.props().selectPersona).to.exist;
    expect(wrapper.props().personas).to.be.empty;
    expect(wrapper.find("List").length).to.equal(1);
    expect(wrapper.find("List").props().disablePadding).to.exist;
    expect(wrapper.find("TfaUnselectedPersona").length).to.equal(0);
  });

  it("renders unselected personas list without unconfirmed personas", () => {
    const personas = generatePersonas(2);
    const wrapper = setup(personas);
    expect(wrapper.props().selectPersona).to.exist;
    expect(wrapper.props().personas.length).to.equal(2);
    expect(wrapper.find("List").length).to.equal(1);
    expect(wrapper.find("List").props().disablePadding).to.exist;
    expect(wrapper.find("TfaUnselectedPersona").length).to.equal(2);
    for (let i = 0; i < wrapper.find("TfaUnselectedPersona").length; i++) {
      expect(wrapper.find("TfaUnselectedPersona").at(i).key()).to.equal(personas[i].account);
      expect(wrapper.find("TfaUnselectedPersona").at(i).props().persona).to.equal(personas[i]);
      expect(wrapper.find("TfaUnselectedPersona").at(i).props().selectPersona).to.exist;
    }
  });

  it("renders unselected personas list with all unconfirmed personas", () => {
    const personas = generatePersonas(2);
    personas.forEach(persona => {
      persona.isEnabled = false;
    });
    const wrapper = setup(personas);
    expect(wrapper.props().selectPersona).to.exist;
    expect(wrapper.props().personas.length).to.equal(2);
    expect(wrapper.find("List").length).to.equal(1);
    expect(wrapper.find("List").props().disablePadding).to.exist;
    expect(wrapper.find("TfaUnselectedPersona").length).to.equal(2);
    for (let i = 0; i < wrapper.find("TfaUnselectedPersona").length; i++) {
      expect(wrapper.find("TfaUnselectedPersona").at(i).key()).to.equal(personas[i].account);
      expect(wrapper.find("TfaUnselectedPersona").at(i).props().persona).to.equal(personas[i]);
      expect(wrapper.find("TfaUnselectedPersona").at(i).props().selectPersona).to.exist;
    }
  });

  it("renders unselected personas list with one unconfirmed persona", () => {
    const personas = generatePersonas(2);
    personas[1].isEnabled = false;
    const wrapper = setup(personas);
    expect(wrapper.props().selectPersona).to.exist;
    expect(wrapper.props().personas.length).to.equal(2);
    expect(wrapper.find("List").length).to.equal(1);
    expect(wrapper.find("List").props().disablePadding).to.exist;
    expect(wrapper.find("TfaUnselectedPersona").length).to.equal(2);
    expect(wrapper.find("TfaUnselectedPersona").at(0).key()).to.equal(personas[0].account);
    expect(wrapper.find("TfaUnselectedPersona").at(0).props().persona).to.equal(personas[0]);
    expect(wrapper.find("TfaUnselectedPersona").at(0).props().selectPersona).to.exist;
    expect(wrapper.find("TfaUnselectedPersona").at(1).key()).to.equal(personas[1].account);
    expect(wrapper.find("TfaUnselectedPersona").at(1).props().persona).to.equal(personas[1]);
    expect(wrapper.find("TfaUnselectedPersona").at(1).props().selectPersona).to.exist;
  });
});
