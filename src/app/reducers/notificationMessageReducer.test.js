import {expect} from 'chai';
import notificationMessageReducer from './notificationMessageReducer';
import * as actions from '../actions/notificationActions';

describe("notificationMessageReducer test", () => {
  it("should set notification message when passed SHOW_NOTIFICATION", () => {
    const initState = "";
    const notificationMessage = "This is notification";
    const action = actions.showNotification(notificationMessage);
    const newState = notificationMessageReducer(initState, action);

    expect(newState).to.equal(notificationMessage);
  });

  it ("should remove notification message when passed HIDE_NOTIFICATION", () => {
    const initState = "This is notification";
    const action = actions.hideNotification();
    const newState = notificationMessageReducer(initState, action);

    expect(newState).to.be.empty;
  });
});
