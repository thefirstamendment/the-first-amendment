/*eslint-disable no-console*/
import fs from 'fs';
import config from '../../config.json';

config['selection'] = process.argv[2];
let output2 = JSON.stringify(config, null, 2) + '\n';
fs.writeFile('./config.json', output2, (err) => {
  if (err) {
    console.error("There was an error writing to config file: " + err);
  }
  console.log("Config selection updated to: " + process.argv[2]);
});
