/*jshint esversion: 6 */
"use strict";

class Comment {
  constructor(txHash, content, url, persona, upvoteCount, downvoteCount, timestamp, isEnabled = false) {
    this.txHash = txHash;
    this.content = content;
    this.url = url;
    this.persona = persona;
    this.upvoteCount = upvoteCount;
    this.downvoteCount = downvoteCount;
    this.timestamp = timestamp;
    this.isEnabled = isEnabled;
  }
}

module.exports = Comment;

