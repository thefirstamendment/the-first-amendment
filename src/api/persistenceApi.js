/*jshint esversion: 6 */
// Required Modules
const InputValidator = require('../common/InputValidator');
const bs58 = require('bs58');
const Web3 = require('web3');
const decache = require('decache');
const SHA1 = require("crypto-js/sha1");
const config = require('../../config.json');
const Persona = require('../common/Persona.js');
const Comment = require('../common/Comment.js');
const IpfsError = require('../common/errors/IpfsError.js');
const Web3Error = require('../common/errors/Web3Error.js');
const InputValidationError = require('../common/errors/InputValidationError.js');
const Promise = require("bluebird");
const IPFS = require('ipfs'); // for local ipfs
const IpfsAPI = require('ipfs-api'); // for infura ipfs
const TruffleContract = require("truffle-contract");
let web3, ipfsNode, ipfsInfura;
let CommentContract, UportRegistry;
let documentDOM;

function startIpfs() {
  return new Promise((accept, reject) => {
    setFallbackTimer(reject, new IpfsError('Ipfs failed to start before timeout.'));
    ipfsNode = new IPFS();
    ipfsNode.once('start', () => {
      accept();
      return;
    });
  });
}

function stopIpfs() {
  return new Promise((accept, reject) => {
    ipfsNode.stop((err) => {
      if (err) {
        reject(new IpfsError('Could not stop IPFS.'));
        return;
      }
    });
    ipfsNode.once('stop', () => {
      accept();
      return;
    });
  });
}

function getIpfsApi() {
  if (config.selection !== "local") {
    return ipfsInfura;
  }

  return ipfsNode.files;
}

/**
 * setup of web3 provider, Comment provider and ipfs provider
 */
function setup(window) {
  documentDOM = window;

  let web3Provider;
  // Is there an injected web3 instance?
  if (typeof window !== undefined && typeof window.web3 !== 'undefined') {
    web3Provider = window.web3.currentProvider;
    //console.log("Using injected web3");
  } else {
    // If no injected web3 instance is detected, fall back to Ganache
    let web3Host = config[config.selection].web3Host;
    let web3Port = config[config.selection].web3Port;
    web3Provider = new Web3.providers.HttpProvider("http://" + web3Host + ":" + web3Port);
    //console.log("Using web3 from http://" + web3Host + ":" + web3Port);
  }
  web3 = new Web3(web3Provider);

  setupContracts();

  if (config.selection !== "local") {
    // setup ipfs
    let ipfsHost = config[config.selection].ipfsHost;
    let ipfsPort = config[config.selection].ipfsPort;
    let ipfsProtocol = config[config.selection].ipfsProtocol;

    ipfsInfura = IpfsAPI({
      host: ipfsHost,
      port: ipfsPort,
      protocol: ipfsProtocol
    });
  }
}


function setupContracts() {
  // Get the necessary contract artifact files at the run time
  // NOTE: because how migrate is generating json files, we
  // need to get the latest version at the time we need contracts
  decache('../../build/contracts/Comment.json');
  decache('../../build/contracts/UportRegistry.json');
  let CommentABI = require('../../build/contracts/Comment.json');
  let UportRegistryABI = require('../../build/contracts/UportRegistry.json');

  // instantiate it with truffle-contract
  CommentContract = TruffleContract(CommentABI);
  UportRegistry = TruffleContract(UportRegistryABI);

  // Set the provider for our contracts
  CommentContract.setProvider(web3.currentProvider);
  UportRegistry.setProvider(web3.currentProvider);
}

/**
 * retrieve the first operational account provided by ethereum wallet
 *
 * @return {string} the first operational account provided by ethereum wallet
 */
function getFirstAccount() {
  try {
    return web3.eth.accounts[0];
  } catch (err) {
    throw new Web3Error("Could not get first account.");
  }
}

/**
 * retrieve all accounts from ethereum wallet
 *
 * @return {string} all accounts from ethereum wallet
 */
function getAccounts() {
  try {
    return web3.eth.accounts;
  } catch (err) {
    throw new Web3Error("Could not get accounts.");
  }
}

/**
 * retrieve account balance in ether
 *
 * @param {string} account address of the account
 * @return {string} account balance in ether
 */
function getAccountBalance(account) {
  try {
    return web3.fromWei(web3.eth.getBalance(account), 'ether');
  } catch (err) {
    throw new Web3Error("Could not get account balance.");
  }
}

/**
 * upload the comment to blockchain and ipfs
 *
 * @param {Object} comment         comment that is being uploaded
 * @param {Object} txData          aux data for blockchain transaction (sender's address)
 * @return {Object}                promise containing the transaction hash
 */
function addComment(comment, txData) {
  return new Promise((accept, reject) => {
    // check if ipfs node is online
    if (config.selection === "local" && !ipfsNode.isOnline()) {
      reject(new IpfsError('Ipfs not online.'));
      return;
    }
    // add commentJson file to ipfs
    let commentJson = assembleCommentJson(comment.content, comment.url);
    getIpfsApi().add(commentJson, (err, result) => {
      if (err) {
        reject(new IpfsError('Could not add comment json to ipfs node.'));
        return;
      }
      // finally, call ethereum contract and add url and comment hashes
      return CommentContract.deployed().then(function (instance) {
        let commentContract = instance;
        // convert from ipfs hash to 32 byte hex value
        let commentHashHexTrimmed = base58ToHex(result[0].hash).substr(4);
        let urlSha1 = SHA1(comment.url).toString();
        return commentContract.addComment('0x' + urlSha1, '0x' + commentHashHexTrimmed, txData).then((tx) => {
          accept(tx.tx);
          return;
        }).catch(() => {
          reject(new Web3Error('Could not add comment.'));
          return;
        });
      }).catch(() => {
        reject(new Web3Error('Could not get deployed Comment contract.'));
        return;
      });
    });
  });
}

/**
 * fetches all the comments associated to the specified url
 *
 * @param  {string}     url   url of the comments that are to be retrieved. If set to null/undefined, all comments are returned.
 * @return {Comment[]}        array of comments associated to the specified url if url specified, or array of all comments
 */
function getComments(url) {
  return new Promise((accept, reject) => {
    return CommentContract.deployed().then(function (instance) {
      let commentContract = instance;
      let comments = [];
      return getCommentRecordsFromTxLog(commentContract, url).then((commentRecords) => {
        if (commentRecords.length > 0) {
          let count = 0;
          for (let commentLog of commentRecords) {
            buildComment(commentLog, commentContract).then((comment) => {
              if (comment) {
                comments.push(comment);
              }
              if (++count >= commentRecords.length) {
                accept(comments);
                return;
              }
            }).catch((err) => {
              // could not build comment
              reject(err);
              return;
            });
          }
        } else {
          accept(comments);
          return;
        }
      }).catch((err) => {
        // could not get comment records from tx log
        reject(err);
        return;
      });
    }).catch(() => {
      reject(new Web3Error('Could not get deployed Comment contract.'));
      return;
    });
  });
}

/**
 * retrieves all the comment records (from CommentAdded event) associated to the specified url
 *
 * @param  {TruffleContract} commentContract  address of the deployed Comment contract
 * @param  {string}          url              url of the comments that are to be retrieved
 * @return {Object[]}                         array of comment records associated to the specified url
 */
function getCommentRecordsFromTxLog(commentContract, url) {
  return new Promise((accept, reject) => {
    let urlFilter = (url != null) ? {
      urlHashRef: '0x' + SHA1(url).toString()
    } : {};
    let commentAddedEvent = commentContract.CommentAdded(urlFilter, {
      fromBlock: 0,
      toBlock: 'latest'
    });
    // fallback timer is set because filter.get does not report error
    setFallbackTimer(reject, new Web3Error('Could not get comment added events before timeout.'));
    commentAddedEvent.get((err, commentRecords) => {
      if (err) {
        reject(new Web3Error('Could not get comment events.'));
        return;
      }

      accept(commentRecords);
      return;
    });
    commentAddedEvent.stopWatching();
  });
}

/**
 * builds and returns one Comment object from comment record
 *
 * @param  {object}    commentLog       comment log record from CommentAdded event
 * @param  {string}    commentContract  address of the deployed Comment contract
 * @return {Comment}                    Comment object containing comment text, url, upvote/downvote count, and optionally persona.
 */
function buildComment(commentLog, commentContract) {
  let txHash = commentLog.transactionHash;
  let txHash20Bytes = txHash.substring(0, 22);
  let account = web3.eth.getTransaction(txHash).from;
  return new Promise((accept, reject) => {
    return getUpvoteCountFromTxLog(commentContract, txHash20Bytes).then((upvoteCount) => {
      return getDownvoteCountFromTxLog(commentContract, txHash20Bytes).then((downvoteCount) => {
        let hexCommentHash = "1220" + commentLog.args.commentHashRef.slice(2);
        let baseCommentHash = hexToBase58(hexCommentHash);
        let timestamp = getCommentTimestamp(txHash);
        return getIpfsObject(baseCommentHash).then((commentJson) => {
          if (commentJson) {
            return getPersona(account).then((persona) => {
              let comment = new Comment(txHash, commentJson.content, commentJson.url, persona, upvoteCount, downvoteCount, timestamp, true);
              accept(comment);
              return;
            }).catch((err) => {
              // could not get persona
              reject(err);
              return;
            });
          } else {
            accept(null);
            return;
          }
        }).catch((err) => {
          // could not get ipfs object
          reject(err);
          return;
        });
      }).catch((err) => {
        // could not get downvote count
        reject(err);
        return;
      });
    }).catch((err) => {
      // could not get upvote count
      reject(err);
      return;
    });
  });
}

function getUpvoteCountFromTxLog(commentContract, txHash) {
  return getCommentVoteCountFromTxLog(commentContract, txHash, true);
}

function getDownvoteCountFromTxLog(commentContract, txHash) {
  return getCommentVoteCountFromTxLog(commentContract, txHash, false);
}

/**
 * retrieves upvote or downvote count of a Comment
 *
 * @param  {string}  commentContract  address of the deployed Comment contract
 * @param  {string}  txHash           transaction hash of a comment
 * @param  {boolean} isUpvote         true if upvote is being retrieved, otherwise false
 * @return {int}                      upvote/downvote count
 */
function getCommentVoteCountFromTxLog(commentContract, txHash, isUpvote) {
  let arg1 = {
    txHashRef: txHash
  };
  let arg2 = {
    fromBlock: 0,
    toBlock: 'latest'
  };
  let commentVoteEvent = isUpvote ?
    commentContract.CommentUpvoted(arg1, arg2) :
    commentContract.CommentDownvoted(arg1, arg2);
  // TODO: should remove promise on all filters because they are sync functions
  return new Promise((accept, reject) => {
    // fallback timer is set because filter.get does not report error
    setFallbackTimer(reject, new Web3Error('Could not get upvote/downvote events before timeout.'));
    commentVoteEvent.get((err, upvoteRecords) => {
      if (err) {
        reject(new Web3Error('Could not get upvote/downvote events.'));
        return;
      }

      accept(upvoteRecords.length);
      return;
    });
    commentVoteEvent.stopWatching();
  });
}

/**
 * retrieves timestamp when comment was included in mined block
 *
 * @param  {string} txHash          transaction hash of a comment
 * @return {Number}                 the unix timestamp for when the comment was included in mined block
 */
function getCommentTimestamp(txHash) {
  return web3.eth.getBlock(web3.eth.getTransaction(txHash).blockNumber).timestamp;
}

/**
 * upload the persona to blockchain and ipfs
 *
 * @param {Persona} persona        persona to add
 * @param {Object} txData          aux data for blockchain transaction (sender's address)
 * @return {Object}                promise containing the transaction id
 */
function addPersona(persona, txData) {
  return new Promise((accept, reject) => {
    return getPersona(persona.account).then((existingPersona) => {
      if (existingPersona !== null) {
        reject(new InputValidationError("Persona with that account already exists"));
        return;
      }

      return addPersonaInternal(persona, txData).then((tx) => {
        accept(tx);
        return;
      }).catch((err) => {
        reject(err);
        return;
      });
    }).catch((err) => {
      reject(err);
      return;
    });
  });
}

function addPersonaInternal(persona, txData) {
  return new Promise((accept, reject) => {
    if (config.selection === "local" && !ipfsNode.isOnline()) {
      reject(new IpfsError('Ipfs not online.'));
      return;
    }
    // add personaJson file to ipfs
    let personaJson = assemblePersonaJson(persona);
    getIpfsApi().add(personaJson, (err, ipfsResult) => {
      if (err) {
        reject(new IpfsError('Could not add persona json to ipfs node.'));
        return;
      }
      // finally, call ethereum contract and add persona hash
      return UportRegistry.deployed().then(function (instance) {
        let uportRegistryContract = instance;
        let personaHashHexTrimmed = base58ToHex(ipfsResult[0].hash);
        return uportRegistryContract.setAttributes('0x' + personaHashHexTrimmed, txData).then((tx) => {
          persona.balance = getAccountBalance(persona.account);
          accept(tx.tx);
          return;
        }).catch(() => {
          reject(new Web3Error('Could not add persona.'));
          return;
        });
      }).catch(() => {
        reject(new Web3Error('Could not get deployed UportRegistry contract.'));
        return;
      });
    });
  });
}

/**
 * retrieves Persona object associated to the specified account
 *
 * @param  {string} account         ethereum account connected to a persona
 * @return {Persona}                Persona object associated to the specified account
 */
function getPersona(account) {
  return new Promise((accept, reject) => {
    return UportRegistry.deployed().then(function (instance) {
      let uportRegistryContract = instance;
      return uportRegistryContract.getAttributes(account).then((ipfsHashHex) => {
        let ipfsHash = hexToBase58(ipfsHashHex.slice(2));
        if (ipfsHash.length < 46) {
          accept(null);
          return;
        }

        return getIpfsObject(ipfsHash).then((personaJSON) => {
          let balance = getAccountBalance(account);
          accept(personaJSON !== null ? new Persona(account, personaJSON.name, personaJSON.avatar, balance, true) : null);
          return;
        }).catch((err) => {
          // could not get ipfs object
          reject(err);
          return;
        });
      }).catch(() => {
        reject(new Web3Error('Could not get persona.'));
        return;
      });
    }).catch(() => {
      reject(new Web3Error('Could not get deployed UportRegistry contract.'));
      return;
    });
  });
}

/**
 * retrieves an array of Persona objects associated to the specified accounts
 *
 * @param  {string[]} accounts      array of accounts possibly connected to persona
 * @return {Persona[]}              array of Persona objects associated to the specified accounts
 */
function getPersonas(accounts) {
  return new Promise((accept, reject) => {
    let personas = [];
    let counter = 0;
    for (let i = 0; i < accounts.length; i++) {
      getPersona(accounts[i]).then((persona) => {
        if (persona) {
          personas.push(persona);
        }
        if (++counter >= accounts.length) {
          accept(personas);
          return;
        }
      }).catch((err) => {
        // could not get persona
        reject(err);
        return;
      });
    }
  });
}

/**
 * updates persona on the blockchain and ipfs. Since data can't be updated on
 * ipfs, it actually creates new ipfs object, and updates blockchain.
 *
 * @param {string} account         account of persona to update
 * @param {Persona} persona        persona to update
 * @param {Object} txData          aux data for blockchain transaction (sender's address)
 * @return {Object}                promise containing the transaction id
 */
function updatePersona(persona, txData) {
  return new Promise((accept, reject) => {
    return getPersona(persona.account).then((existingPersona) => {
      if (existingPersona === null) {
        reject(new InputValidationError("Persona with provided account doesn't exist"));
        return;
      }

      return addPersonaInternal(persona, txData).then((tx) => {
        accept(tx);
        return;
      }).catch((err) => {
        reject(err);
        return;
      });
    }).catch((err) => {
      reject(err);
      return;
    });
  });
}

/**
 * upvotes an existing comment (already present on blockchain)
 *
 * @param  {string}  txHash           transaction hash of a comment
 * @param  {Object}  txData           aux data for blockchain transaction (sender's address)
 * @return {int}                      current upvote count after upvoting
 */
function upvoteComment(txHash, txData) {
  return new Promise((accept, reject) => {
    return CommentContract.deployed().then(function (instance) {
      let commentContract = instance;
      let txHashSubstr = txHash.substring(0, 22);
      return commentContract.upvoteComment(txHashSubstr, txData).then(() => {
        return getUpvoteCountFromTxLog(commentContract, txHashSubstr).then((upvoteCount) => {
          accept(upvoteCount);
          return;
        }).catch((err) => {
          // could not get upvote count
          reject(err);
          return;
        });
      }).catch(() => {
        reject(new Web3Error('Could not upvote comment.'));
        return;
      });
    }).catch(() => {
      reject(new Web3Error('Could not get deployed Comment contract.'));
      return;
    });
  });
}

/**
 * downvotes an existing comment (already present on blockchain)
 *
 * @param  {string}  txHash           transaction hash of a comment
 * @param  {Object}  txData           aux data for blockchain transaction (sender's address)
 * @return {int}                      current downvote count after upvoting
 */
function downvoteComment(txHash, txData) {
  return new Promise((accept, reject) => {
    return CommentContract.deployed().then(function (instance) {
      let commentContract = instance;
      let txHash20Bytes = txHash.substring(0, 22);
      return commentContract.downvoteComment(txHash20Bytes, txData).then(() => {
        return getDownvoteCountFromTxLog(commentContract, txHash20Bytes).then((downvoteCount) => {
          accept(downvoteCount);
          return;
        }).catch((err) => {
          // could not get downvote count
          reject(err);
          return;
        });
      }).catch(() => {
        reject(new Web3Error('Could not downvote comment.'));
        return;
      });
    }).catch(() => {
      reject(new Web3Error('Could not get deployed Comment contract.'));
      return;
    });
  });
}

/**
 * retrieves a specific object from ipfs
 *
 * @param  {string} ipfsHash hash of the object to be retrieved
 * @return {Object}          a JSON representation of the object with the specified hash
 */
function getIpfsObject(ipfsHash) {
  return new Promise((accept, reject) => {
    if (config.selection === "local" && !ipfsNode.isOnline()) {
      reject(new IpfsError('Ipfs not online.'));
      return;
    }
    // TODO: BUG - if cat doesn't find object on time, it will never return
    // timer is set to make sure that function returns when ipfs object too big
    setFallbackTimer(reject, new IpfsError('Ipfs cat operation failed to fetch before timeout.'));
    let params = [ipfsHash];
    if (config.selection !== "local") {
      params[1] = {
        buffer: true
      };
    }
    getIpfsApi().cat(...params, (err, text) => {
      if (err) {
        reject(new IpfsError('Ipfs cat operation failed to fetch object.'));
        return;
      }
      if (text) {
        let ipfsObject = JSON.parse(text);
        accept(isIpfsObjectValid(ipfsObject) ? ipfsObject : null);
        return;
      } else {
        accept(null);
        return;
      }
    });
  });
}

/**
 * checks the ipfs object's size and contents to determine if it is valid.
 *
 * @param  {object} ipfsObject ipfs object
 * @return {boolean}           true ifobject is valid, otherwise false
 */
function isIpfsObjectValid(ipfsObject) {
  let inputValidator = new InputValidator(documentDOM);
  if (inputValidator.isValidCommentObject(ipfsObject)) {
    return true;
  }
  if (inputValidator.isValidPersonaObject(ipfsObject)) {
    return true;
  }

  return false;
}

/**
 * creates the appropriate comment JSON for the specified comment text and url
 *
 * @param  {string} comment text of the comment
 * @param  {string} url     url that the comment is associated to
 * @return {Object}         JSON representation of the comment
 */
function assembleCommentJson(comment, url) {
  return new Buffer('{"content":"' + JSON.stringify(comment).slice(1, -1) + '", "url":"' + url + '"}');
}

/**
 * creates the appropriate persona JSON for the specified name
 *
 * @param  {Persona} persona persona objects
 * @return {Object}          JSON representation of the persona
 */
function assemblePersonaJson(persona) {
  return new Buffer('{"name":"' + persona.name + '", "avatar":"' + persona.avatar + '"}');
}

/**
 * transforms base hash value to hexadecimal representation
 *
 * @param  {string} b58 base hash value
 * @return {string}     hexadecimal hash value
 */
function base58ToHex(b58) {
  let hexBuf = new Buffer(bs58.decode(b58));
  return hexBuf.toString('hex');
}

/**
 * transforms hexadecimal hash value to it's base representation
 *
 * @param  {string} hexStr hexadecimal hash value
 * @return {string}        base hash value
 */
function hexToBase58(hexStr) {
  let buf = new Buffer(hexStr, 'hex');
  return bs58.encode(buf);
}

/**
 * fires a reject with error message after timeout
 *
 * @param  {function} reject reject function of a promise
 * @param  {Error} error     error that gets thrown in reject function
 */
function setFallbackTimer(reject, error) {
  setTimeout(function () {
    reject(error);
    return;
  }, 10000);
}

// Module exports
module.exports.setup = setup;
module.exports.getFirstAccount = getFirstAccount;
module.exports.getAccounts = getAccounts;
module.exports.getAccountBalance = getAccountBalance;
module.exports.startIpfs = startIpfs;
module.exports.stopIpfs = stopIpfs;
module.exports.addComment = addComment;
module.exports.getComments = getComments;
module.exports.upvoteComment = upvoteComment;
module.exports.downvoteComment = downvoteComment;
module.exports.addPersona = addPersona;
module.exports.getPersonas = getPersonas;
module.exports.updatePersona = updatePersona;
module.exports.getCommentTimestamp = getCommentTimestamp;
