import { expect } from 'chai';
import React from 'react';
import TfaSidebar from './TfaSidebar';
import chrome from 'sinon-chrome/extensions';
import { createShallow } from '@material-ui/core/test-utils';
import { JSDOM } from 'jsdom';
import Enzyme from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

Enzyme.configure({ adapter: new Adapter() });

describe("<TfaSidebar /> tests", () => {
  // we're using shallow rendering here because TfaExploreCommentsModalContainer is connected component
  let shallow;

  function setup(
    showPersonaManagementPanel,
    displaySidebar,
    errorMessage,
    notificationMessage,
    isExploreCommentsModalOpen,
    handleErrorNotificationClose = () => { },
    handleNotificationClose = () => { },
    openExploreCommentsModal = () => { },
    closeExploreCommentsModal = () => { }
  ) {
    const props = {
      showPersonaManagementPanel: showPersonaManagementPanel,
      displaySidebar: displaySidebar,
      errorMessage: errorMessage,
      notificationMessage: notificationMessage,
      isExploreCommentsModalOpen: isExploreCommentsModalOpen,
      handleErrorNotificationClose: handleErrorNotificationClose,
      handleNotificationClose: handleNotificationClose,
      openExploreCommentsModal: openExploreCommentsModal,
      closeExploreCommentsModal: closeExploreCommentsModal
    };
    return shallow(<TfaSidebar {...props} />);
  }

  before(() => {
    global.chrome = chrome;
    const jsdom = new JSDOM('<!doctype html><html><body></body></html>');
    const { window } = jsdom;
    global.window = window;
    global.document = window.document;
    shallow = createShallow();
  });

  it("renders sidebar with comments management panel, no error message, no notification message", () => {
    const wrapper = setup(false, true, "", "", false);
    // Drawer
    expect(wrapper.getElement().props.variant).to.equal("persistent");
    expect(wrapper.getElement().props.anchor).to.equal("right");
    expect(wrapper.getElement().props.open).to.be.true;
    // TfaHeaderPanelContainer
    expect(wrapper.children().at(0).props().openExploreCommentsModal).to.exist;
    //TfaCommentsManagementPanelContainer
    expect(wrapper.children().at(1).props()).to.be.empty;
    // TfaNotification
    expect(wrapper.children().at(2).props().isOpen).to.be.empty;
    expect(wrapper.children().at(2).props().notificationMessage).to.equal("");
    expect(wrapper.children().at(2).props().hideNotification).to.exist;
    // TfaErrorNotification
    expect(wrapper.children().at(3).props().isOpen).to.be.empty;
    expect(wrapper.children().at(3).props().errorMessage).to.equal("");
    expect(wrapper.children().at(3).props().hideError).to.exist;
    // TfaExploreCommentsModal
    expect(wrapper.children().at(4).props().exploreCommentsModalIsOpen).to.be.false;
    expect(wrapper.children().at(4).props().closeExploreCommentsModal).to.exist;
  });

  it("renders sidebar with comments management panel, with error message and notification message", () => {
    const wrapper = setup(false, true, "error message", "notification message", false);
    // Drawer
    expect(wrapper.getElement().props.variant).to.equal("persistent");
    expect(wrapper.getElement().props.anchor).to.equal("right");
    expect(wrapper.getElement().props.open).to.be.true;
    // TfaHeaderPanelContainer
    expect(wrapper.children().at(0).props().openExploreCommentsModal).to.exist;
    //TfaCommentsManagementPanelContainer
    expect(wrapper.children().at(1).props()).to.be.empty;
    // TfaNotification
    expect(wrapper.children().at(2).props().isOpen).to.be.true;
    expect(wrapper.children().at(2).props().notificationMessage).to.equal("notification message");
    expect(wrapper.children().at(2).props().hideNotification).to.exist;
    // TfaErrorNotification
    expect(wrapper.children().at(3).props().isOpen).to.be.true;
    expect(wrapper.children().at(3).props().errorMessage).to.equal("error message");
    expect(wrapper.children().at(3).props().hideError).to.exist;
    // TfaExploreCommentsModal
    expect(wrapper.children().at(4).props().exploreCommentsModalIsOpen).to.be.false;
    expect(wrapper.children().at(4).props().closeExploreCommentsModal).to.exist;
  });

  it("renders sidebar with persona management panel, no error message, no notification message, explore comments modal open", () => {
    const wrapper = setup(true, true, "", "", true);
    // Drawer
    expect(wrapper.getElement().props.variant).to.equal("persistent");
    expect(wrapper.getElement().props.anchor).to.equal("right");
    expect(wrapper.getElement().props.open).to.be.true;
    // TfaHeaderPanelContainer
    expect(wrapper.children().at(0).props().openExploreCommentsModal).to.exist;
    // TfaPersonaManagementPanelContainer
    expect(wrapper.children().at(1).props()).to.be.empty;
    // TfaNotification
    expect(wrapper.children().at(2).props().isOpen).to.be.empty;
    expect(wrapper.children().at(2).props().notificationMessage).to.equal("");
    expect(wrapper.children().at(2).props().hideNotification).to.exist;
    // TfaErrorNotification
    expect(wrapper.children().at(3).props().isOpen).to.be.empty;
    expect(wrapper.children().at(3).props().errorMessage).to.equal("");
    expect(wrapper.children().at(3).props().hideError).to.exist;
    // TfaExploreCommentsModal
    expect(wrapper.children().at(4).props().exploreCommentsModalIsOpen).to.be.true;
    expect(wrapper.children().at(4).props().closeExploreCommentsModal).to.exist;
  });
});
