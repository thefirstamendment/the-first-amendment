import React from 'react';
import PropTypes from 'prop-types';
import TfaUnselectedPersona from './TfaUnselectedPersona';
import { List } from '@material-ui/core/';
import style from '../css/TfaUnselectedPersonas.scss';
import Utils from '../../../common/Utils';

const TfaUnselectedPersonas = (props) => {
  const unselectedPersonas = props.personas.map((persona) =>
    <TfaUnselectedPersona
      key={persona.account}
      persona={persona}
      selectPersona={props.selectPersona}
    />
  );

  return (
    <List component="ol" className={style.tfa_unselected_personas} disablePadding>
      {unselectedPersonas}
    </List>
  );
};

TfaUnselectedPersonas.propTypes = {
  personas: PropTypes.array.isRequired,
  selectPersona: PropTypes.func.isRequired
};

export default TfaUnselectedPersonas;
