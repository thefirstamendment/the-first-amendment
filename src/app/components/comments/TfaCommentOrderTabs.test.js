import { expect } from 'chai';
import React from 'react';
import TfaCommentOrderTabs from './TfaCommentOrderTabs';
import sinon from 'sinon';
import chrome from 'sinon-chrome/extensions';
import { createMount } from '@material-ui/core/test-utils';
import { JSDOM } from 'jsdom';
import Enzyme from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

Enzyme.configure({ adapter: new Adapter() });

describe("<TfaCommentOrderTabs /> tests", () => {
  let mount;

  function setup(commentOrdering) {
    const props = {
      selectCommentOrdering: () => { },
      commentOrdering: commentOrdering,
    };
    return mount(<TfaCommentOrderTabs {...props} />);
  }

  before(() => {
    global.chrome = chrome;
    const jsdom = new JSDOM('<!doctype html><html><body></body></html>');
    const { window } = jsdom;
    global.window = window;
    global.document = window.document;
    mount = createMount();
  });

  after(() => {
    mount.cleanUp();
  });

  it("renders comments ordering tabs with NEW selected", () => {
    const wrapper = setup(0);
    expect(wrapper.props().commentOrdering).to.equal(0);
    expect(wrapper.props().selectCommentOrdering).to.exist;
    expect(wrapper.find("Tabs").length).to.equal(1);
    expect(wrapper.find("Tabs").props().onChange).to.exist;
    expect(wrapper.find("Tabs").props().value).to.equal(0);
    expect(wrapper.find("Tabs").props().indicatorColor).to.equal("primary");
    expect(wrapper.find("Tabs").props().textColor).to.equal("primary");
    expect(wrapper.find("Tabs").props().fullWidth).to.exist;
    expect(wrapper.find("Tab").length).to.equal(2);
    expect(wrapper.find("Tab").at(0).props().label).to.equal("New");
  });

  it("renders comments ordering tabs with HOT selected", () => {
    const wrapper = setup(1);
    expect(wrapper.props().commentOrdering).to.equal(1);
    expect(wrapper.props().selectCommentOrdering).to.exist;
    expect(wrapper.find("Tabs").length).to.equal(1);
    expect(wrapper.find("Tabs").props().onChange).to.exist;
    expect(wrapper.find("Tabs").props().value).to.equal(1);
    expect(wrapper.find("Tabs").props().indicatorColor).to.equal("primary");
    expect(wrapper.find("Tabs").props().textColor).to.equal("primary");
    expect(wrapper.find("Tabs").props().fullWidth).to.exist;
    expect(wrapper.find("Tab").length).to.equal(2);
    expect(wrapper.find("Tab").at(1).props().label).to.equal("Hot");
  });

  it("simulates click on New tab", () => {
    const selectCommentOrdering = sinon.spy();
    const wrapper = mount(
      <TfaCommentOrderTabs
        selectCommentOrdering={selectCommentOrdering}
        commentOrdering={1}
      />
    );
    wrapper.find("Tab").at(0).simulate("click");
    expect(selectCommentOrdering.calledWith(0)).to.be.true;
    expect(selectCommentOrdering.calledWith(1)).to.be.false;
  });

  it("simulates click on Hot tab", () => {
    const selectCommentOrdering = sinon.spy();
    const wrapper = mount(
      <TfaCommentOrderTabs
        selectCommentOrdering={selectCommentOrdering}
        commentOrdering={0}
      />
    );
    wrapper.find("Tab").at(1).simulate("click");
    expect(selectCommentOrdering.calledWith(0)).to.be.false;
    expect(selectCommentOrdering.calledWith(1)).to.be.true;
  });
});
