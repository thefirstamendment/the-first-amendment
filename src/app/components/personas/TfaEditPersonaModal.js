import React from 'react';
import PropTypes from 'prop-types';
import {
  Dialog,
  Button,
  DialogTitle,
  DialogContent,
  DialogActions,
  FormControl,
  FormGroup,
  InputLabel,
  TextField,
  Select,
  InputAdornment,
  Tooltip
} from '@material-ui/core/';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faSave, faInfo } from '@fortawesome/free-solid-svg-icons';
import Persona from '../../../common/Persona';
import personaSchema from '../../../common/schema/PersonaSchema.json';
import TfaAvatarDragAndDrop from './TfaAvatarDragAndDrop';
import InputValidator from '../../../common/InputValidator';
import style from '../css/TfaPersonaModal.scss';
import Utils from '../../../common/Utils';

export default class TfaEditPersonaModal extends React.Component {

  constructor(props) {
    super(props);
    this.updatePersona = this.updatePersona.bind(this);
    this.handleAvatarChange = this.handleAvatarChange.bind(this);
    this.handleNameChange = this.handleNameChange.bind(this);
    this.handleModalClose = this.handleModalClose.bind(this);
    this.inputValidator = new InputValidator(window);
    this.state = {
      name: "" + props.currentPersona.name,
      isNameValid: true,
      isNameUpdated: false,
      avatar: "" + props.currentPersona.avatar,
      isAvatarValid: true,
      isAvatarUpdated: false
    };
  }

  componentWillReceiveProps(props) {
    this.setState({
      name: "" + props.currentPersona.name,
      isNameValid: true,
      isNameUpdated: false,
      avatar: "" + props.currentPersona.avatar,
      isAvatarValid: true,
      isAvatarUpdated: false
    });
  }

  handleModalClose() {
    this.setState({
      name: "",
      isNameValid: true,
      isNameUpdated: false,
      avatar: "",
      isAvatarValid: true,
      isAvatarUpdated: false
    });
    this.props.closeEditPersonaModal();
  }

  updatePersona() {
    this.props.updatePersona(new Persona(this.props.currentPersona.account, this.state.name.trim(), this.state.avatar));
    this.handleModalClose();
  }

  handleAvatarChange(dataUrl) {
    this.setState({
      avatar: dataUrl,
      isAvatarValid: this.inputValidator.isPersonaAvatarValid(dataUrl),
      isAvatarUpdated: dataUrl !== this.props.currentPersona.avatar
    });
  }

  handleNameChange(e) {
    // disable hijacking of text input field from native page by restoring focus on blur event
    const textField = document.activeElement;
    document.activeElement.onblur = (e) => {
      e.preventDefault();
      textField.focus();
    };
    this.setState({
      name: e.target.value,
      isNameValid: this.inputValidator.isPersonaNameValid(e.target.value),
      isNameUpdated: e.target.value !== this.props.currentPersona.name
    });
  }

  render() {
    const tooltipTitle = (
      <ol>
        <li>Username may only contain alphanumeric characters or hyphens.</li>
        <li>Username cannot have multiple consecutive hyphens.</li>
        <li>Username cannot begin or end with a hyphen.</li>
        <li>Minimum is 3 characters.</li>
      </ol>
    );
    const usernameEndAdornment = (
      <InputAdornment position="end">
        <Tooltip
          enterDelay={100}
          id="tfa_username_rules_tooltip"
          leaveDelay={100}
          placement="bottom"
          title={tooltipTitle}
          classes={{
            popper: style.tfa_tooltip_popover,
            tooltip: style.tfa_tooltip
          }}
        >
          <FontAwesomeIcon className={this.state.isNameValid ? style.tfa_username_rules : style.tfa_username_rules_error} icon={faInfo} />
        </Tooltip>
      </InputAdornment>
    );
    const saveButton = (this.state.isNameValid && this.state.isAvatarValid && (this.state.isNameUpdated || this.state.isAvatarUpdated)) ?
      (
        <Button className={style.save_btn} variant="raised" color="primary" onClick={() => this.updatePersona()} >Save&nbsp;&nbsp;
        <FontAwesomeIcon icon={faSave} />
        </Button>
      ) :
      (
        <Button className={style.save_btn_disabled} variant="raised" color="primary" disabled >Save&nbsp;&nbsp;
        <FontAwesomeIcon icon={faSave} />
        </Button>
      );
    const defaultAvatarImage = Utils.getDefaultAvatarImg();
    const avatarImage = (this.state.avatar.length > 0) ? this.state.avatar : defaultAvatarImage;

    return (
      <Dialog
        disableRestoreFocus
        open={this.props.editPersonaModalIsOpen}
        onClose={() => this.props.closeEditPersonaModal()}
        aria-labelledby="form-dialog-title"
        className={style.tfa_dialog}
      >
        <DialogTitle className={style.tfa_dialog_title}>Edit Persona</DialogTitle>
        <DialogContent className={style.tfa_dialog_content}>
          <FormGroup>
            <FormControl className={style.tfa_avatar_selector_form_control}>
              <TfaAvatarDragAndDrop
                avatar={avatarImage}
                setAvatar={this.handleAvatarChange}
                showError={this.props.showError}
              />
            </FormControl>
            <FormControl className={style.tfa_form_control_name}>
              <TextField
                className={style.tfa_name_input}
                fullWidth
                id="tfaPersonaName"
                label="Username"
                margin="normal"
                error={!this.state.isNameValid}
                value={this.state.name}
                onChange={this.handleNameChange}
                inputProps={{ maxLength: personaSchema.properties.name.maxLength }}
                InputProps={{
                  classes: {
                    input: style.tfa_name_input,
                  },
                  endAdornment: usernameEndAdornment
                }}
              />
            </FormControl>
            <FormControl
              className={style.tfa_form_control_account}
              classes={{ root: style.tfa_form_control_account_root }}
            >
              <InputLabel htmlFor="age-native-simple">Account</InputLabel>
              <Select
                className={style.tfa_select}
                disabled
                native
                value={this.props.currentPersona.account}
              >
                <option value="">{this.props.currentPersona.account}</option>
              </Select>
            </FormControl>
          </FormGroup>
        </DialogContent>
        <DialogActions>
          {saveButton}
          <Button className={style.close_btn} variant="raised" color="default" onClick={this.handleModalClose}>Close</Button>
        </DialogActions>
      </Dialog>
    );
  }
}

TfaEditPersonaModal.propTypes = {
  editPersonaModalIsOpen: PropTypes.bool.isRequired,
  closeEditPersonaModal: PropTypes.func.isRequired,
  updatePersona: PropTypes.func.isRequired,
  currentPersona: PropTypes.object.isRequired,
  showError: PropTypes.func.isRequired
};
