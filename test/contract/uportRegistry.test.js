const UportRegistry = artifacts.require("../UportRegistry.sol"); //eslint-disable-line no-undef

contract('UportRegistry', (accounts) => { //eslint-disable-line no-undef
  it("should return AttributesSet event in tx log", () => {
    let uportRegistry;
    let account_one = accounts[0];
    let ipfsHash = "01234567890123456789";
    return UportRegistry.deployed().then((instance) => {
      uportRegistry = instance;
      return uportRegistry.setAttributes(ipfsHash, { from: account_one });
    }).then((result) => {
      assert.equal(result.logs.length, 1, "No events were triggered."); //eslint-disable-line no-undef
    });
  });

  it("should fire an AttributesSet event", (fireAttributesSetDone) => {
    let uportRegistry;
    let account_one = accounts[0];
    let ipfsHash = "01234567890123456789";
    UportRegistry.deployed().then((instance) => {
      uportRegistry = instance;
      let attributesSetEvent = uportRegistry.AttributesSet();
      attributesSetEvent.watch((error, result) => {
        if (error) {
          console.log(error); //eslint-disable-line no-console
        } else {
          assert.equal(result.args._sender, account_one, "sender from event doesn't match"); //eslint-disable-line no-undef
          attributesSetEvent.stopWatching();
          fireAttributesSetDone();
        }
      });
      uportRegistry.setAttributes(ipfsHash, { from: account_one });
    });
  });

  it("should retrieve attributes that have been set", (retrieveAttributesDone) => {
    let uportRegistry;
    let account_one = accounts[0];
    let ipfsHash = "0x01234567890123456789";
    UportRegistry.deployed().then((instance) => {
      uportRegistry = instance;
      return uportRegistry.setAttributes(ipfsHash, { from: account_one });
    }).then(() => {
      return uportRegistry.getAttributes(account_one);
    }).then((result) => {
      assert.equal(result, ipfsHash, "restored attributes do not match set attributes"); //eslint-disable-line no-undef
      retrieveAttributesDone();
    });
  });
});

