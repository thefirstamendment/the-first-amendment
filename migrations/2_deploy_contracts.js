var Comment = artifacts.require("./Comment.sol");
var UportRegistry = artifacts.require("./UportRegistry.sol");

module.exports = function(deployer) {
  deployer.deploy(Comment);
  deployer.deploy(UportRegistry);
};
