import * as types from './actionTypes';

export function setCurrentPersona(persona) {
  return { type: types.SET_CURRENT_PERSONA, persona: persona };
}
