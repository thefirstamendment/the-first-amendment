import {expect} from 'chai';
import currentPersonaReducer from './currentPersonaReducer';
import * as currentPersonaActions from '../actions/currentPersonaActions';
import * as personaActions from '../actions/personaActions';
import Persona from '../../common/Persona';
import personasTestJson from '../../../test/integration/test_personas.json';

describe("currentPersonaReducer test", () => {
  it("should set current persona when passed SET_CURRENT_PERSONA", () => {
    const initState = {};
    const newCurrentPersona = new Persona("account", personasTestJson.persona1.name, personasTestJson.persona1.avatar, 0);
    const action = currentPersonaActions.setCurrentPersona(newCurrentPersona);
    const newState = currentPersonaReducer(initState, action);

    expect(newState.account).to.equal(newCurrentPersona.account);
    expect(newState.name).to.equal(newCurrentPersona.name);
    expect(newState.avatar).to.equal(newCurrentPersona.avatar);
    expect(newState.balance).to.equal(newCurrentPersona.balance);
  });

  it("should set current persona when passed ADD_PERSONA_SUCCESS with empty current persona", () => {
    const currentPersona = {};
    const newCurrentPersona = new Persona("account", personasTestJson.persona1.name, personasTestJson.persona1.avatar, 1);
    const action = personaActions.addPersonaSuccess(newCurrentPersona);
    const newState = currentPersonaReducer(currentPersona, action);

    expect(newState.account).to.equal(newCurrentPersona.account);
    expect(newState.name).to.equal(newCurrentPersona.name);
    expect(newState.avatar).to.equal(newCurrentPersona.avatar);
    expect(newState.balance).to.equal(newCurrentPersona.balance);
  });

  it("should set current persona when passed ADD_PERSONA_SUCCESS with the same account", () => {
    const currentPersona = new Persona("account", personasTestJson.persona1.name, personasTestJson.persona1.avatar, 0);
    const newCurrentPersona = new Persona("account", personasTestJson.persona1.name, personasTestJson.persona1.avatar, 1);
    const action = personaActions.addPersonaSuccess(newCurrentPersona);
    const newState = currentPersonaReducer(currentPersona, action);

    expect(newState.account).to.equal(newCurrentPersona.account);
    expect(newState.name).to.equal(newCurrentPersona.name);
    expect(newState.avatar).to.equal(newCurrentPersona.avatar);
    expect(newState.balance).to.equal(newCurrentPersona.balance);
  });

  it("should not set current persona when passed ADD_PERSONA_SUCCESS with different account", () => {
    const currentPersona = new Persona("account", personasTestJson.persona1.name, personasTestJson.persona1.avatar, 0);
    const newCurrentPersona = new Persona("account1", personasTestJson.persona1.name, personasTestJson.persona1.avatar, 0);
    const action = personaActions.addPersonaSuccess(newCurrentPersona);
    const newState = currentPersonaReducer(currentPersona, action);

    expect(newState.account).to.equal(currentPersona.account);
    expect(newState.name).to.equal(currentPersona.name);
    expect(newState.avatar).to.equal(currentPersona.avatar);
    expect(newState.balance).to.equal(currentPersona.balance);
  });

  it("should set current persona when passed UPDATE_PERSONA_SUCCESS with the same account", () => {
    const currentPersona = new Persona("account", personasTestJson.persona1.name, personasTestJson.persona1.avatar, 0);
    const newCurrentPersona = new Persona("account", personasTestJson.persona1.name, personasTestJson.persona1.avatar, 1);
    const action = personaActions.updatePersonaSuccess(newCurrentPersona);
    const newState = currentPersonaReducer(currentPersona, action);

    expect(newState.account).to.equal(newCurrentPersona.account);
    expect(newState.name).to.equal(newCurrentPersona.name);
    expect(newState.avatar).to.equal(newCurrentPersona.avatar);
    expect(newState.balance).to.equal(newCurrentPersona.balance);
  });

  it("should not set current persona when passed UPDATE_PERSONA_SUCCESS with different account", () => {
    const currentPersona = new Persona("account", personasTestJson.persona1.name, personasTestJson.persona1.avatar, 0);
    const newCurrentPersona = new Persona("account1", personasTestJson.persona1.name, personasTestJson.persona1.avatar, 0);
    const action = personaActions.updatePersonaSuccess(newCurrentPersona);
    const newState = currentPersonaReducer(currentPersona, action);

    expect(newState.account).to.equal(currentPersona.account);
    expect(newState.name).to.equal(currentPersona.name);
    expect(newState.avatar).to.equal(currentPersona.avatar);
    expect(newState.balance).to.equal(currentPersona.balance);
  });
});
